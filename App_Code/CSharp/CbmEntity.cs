﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

/// <summary>
/// CbmEntity 的摘要描述
/// </summary>
public class CbmEntity
{
    public CbmEntity()
    {
        //
        // TODO: 在這裡新增建構函式邏輯
        //
    }

    public string CheckNumber { get; set; }
    public int Height { get; set; }
    public int Width { get; set; }
    public int Length { get; set; }
    public string PicPath { get; set; }
    public bool IsNormal { get; set; }
    public decimal Cont { get; set; }

}