﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// CommonClass 的摘要描述
/// </summary>
namespace CommonClass
{

    public class ttDeliveryScanLog
    {
        public decimal log_id { get; set; }
        public string driver_code { get; set; }
        public string check_number { get; set; }
        public string scan_item { get; set; }
        public DateTime scan_date { get; set; }
        public string area_arrive_code { get; set; }
        public string platform { get; set; }
        public string car_number { get; set; }
        public string sowage_rate { get; set; }
        public string area { get; set; }
        public string ship_mode { get; set; }
        public string goods_type { get; set; }
        public int pieces { get; set; }
        public double weight { get; set; }
        public int runs { get; set; }
        public int plates { get; set; }
        public string exception_option { get; set; }
        public string arrive_option { get; set; }
        public string sign_form_image { get; set; }
        public string sign_field_image { get; set; }
        public DateTime deliveryupload { get; set; }
        public DateTime photoupload { get; set; }
        public DateTime cdate { get; set; }
        public string cuser { get; set; }
        public string tracking_number { get; set; }
        public int Del_status { get; set; }
        public int VoucherMoney { get; set; }
        public int CashMoney { get; set; }
        public bool write_off_type { get; set; }
        public string receive_option { get; set; }
        public string send_option { get; set; }
        public string delivery_option { get; set; }
        public string option_category { get; set; }
    }
    public class Post_Condition
    {
        public long id { get; set; }
        public string PostId { get; set; }
        public long request_id { get; set; }
        public string check_number { get; set; }
        public string Weight { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime UpdateTime { get; set; }
        public bool IsDel { get; set; }
    }
    public class PostCollectOnDelivery_Condition
    {
        public long id { get; set; }
        public string PostId { get; set; }
        public long request_id { get; set; }
        public string check_number { get; set; }
        public string Weight { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime UpdateTime { get; set; }

    }
    public class tcDeliveryRequests_Condition
    {
        public decimal request_id { get; set; }
        public string pricing_type { get; set; }
        public string customer_code { get; set; }
        public string check_number { get; set; }
        public string check_type { get; set; }
        public string order_number { get; set; }
        public string receive_customer_code { get; set; }
        public string subpoena_category { get; set; }
        public string receive_tel1 { get; set; }
        public string receive_tel1_ext { get; set; }
        public string receive_tel2 { get; set; }
        public string receive_contact { get; set; }
        public string receive_zip { get; set; }
        public string receive_city { get; set; }
        public string receive_area { get; set; }
        public string receive_address { get; set; }
        public string area_arrive_code { get; set; }
        public bool receive_by_arrive_site_flag { get; set; }
        public string arrive_address { get; set; }
        public int? pieces { get; set; }
        public int? plates { get; set; }
        public int? cbm { get; set; }
        public int? CbmSize { get; set; }
        public int? collection_money { get; set; }
        public int? arrive_to_pay_freight { get; set; }
        public int? arrive_to_pay_append { get; set; }
        public string send_contact { get; set; }
        public string send_tel { get; set; }
        public string send_zip { get; set; }
        public string send_city { get; set; }
        public string send_area { get; set; }
        public string send_address { get; set; }
        public bool donate_invoice_flag { get; set; }
        public bool electronic_invoice_flag { get; set; }
        public string uniform_numbers { get; set; }
        public string arrive_mobile { get; set; }
        public string arrive_email { get; set; }
        public string invoice_memo { get; set; }
        public string invoice_desc { get; set; }
        public string product_category { get; set; }
        public string special_send { get; set; }
        public DateTime arrive_assign_date { get; set; }
        public string time_period { get; set; }
        public bool receipt_flag { get; set; }
        public bool pallet_recycling_flag { get; set; }
        public string Pallet_type { get; set; }
        public string supplier_code { get; set; }
        public string supplier_name { get; set; }
        public DateTime supplier_date { get; set; }
        public int receipt_numbe { get; set; }
        public int supplier_fee { get; set; }
        public int csection_fee { get; set; }
        public int remote_fee { get; set; }
        public int total_fee { get; set; }
        public DateTime print_date { get; set; }
        public bool print_flag { get; set; }
        public DateTime checkout_close_date { get; set; }
        public bool add_transfer { get; set; }
        public string sub_check_number { get; set; }
        public string import_randomCode { get; set; }
        public string close_randomCode { get; set; }
        public bool turn_board { get; set; }
        public bool upstairs { get; set; }
        public bool difficult_delivery { get; set; }
        public int turn_board_fee { get; set; }
        public int upstairs_fee { get; set; }
        public int difficult_fee { get; set; }
        public DateTime? cancel_date { get; set; }
        public string cuser { get; set; }
        public DateTime cdate { get; set; }
        public string uuser { get; set; }
        public DateTime udate { get; set; }
        public int HCTstatus { get; set; }
        public bool is_pallet { get; set; }
        public long pallet_request_id { get; set; }
        public bool Less_than_truckload { get; set; }
        public string Distributor { get; set; }
        public string temperate { get; set; }
        public string DeliveryType { get; set; }
        public double cbmLength { get; set; }
        public double cbmWidth { get; set; }
        public double cbmHeight { get; set; }
        public double cbmWeight { get; set; }
        public double cbmCont { get; set; }
        public string bagno { get; set; }
        public string ArticleNumber { get; set; }
        public string SendPlatform { get; set; }
        public string ArticleName { get; set; }
        public bool round_trip { get; set; }
        public DateTime? ship_date { get; set; }
        public int VoucherMoney { get; set; }
        public int CashMoney { get; set; }
        public string pick_up_good_type { get; set; }
        public DateTime latest_scan_date { get; set; }
        public DateTime delivery_complete_date { get; set; }
        public string pic_path { get; set; }
        public string send_station_scode { get; set; }
        public bool Is_write_off { get; set; }
        public string return_check_number { get; set; }
        public int freight { get; set; }
        public DateTime latest_dest_date { get; set; }
        public DateTime latest_delivery_date { get; set; }
        public DateTime latest_arrive_option_date { get; set; }
        public string latest_dest_driver { get; set; }
        public string latest_delivery_driver { get; set; }
        public string latest_arrive_option_driver { get; set; }
        public string latest_arrive_option { get; set; }
        public DateTime delivery_date { get; set; }
        public string payment_method { get; set; }
        public string catch_cbm_pic_path_from_S3 { get; set; }
        public string catch_taichung_cbm_pic_path_from_S3 { get; set; }
        public string latest_scan_item { get; set; }
        public string latest_scan_arrive_option { get; set; }
        public string latest_scan_driver_code { get; set; }
        public string latest_dest_exception_option { get; set; }
        public bool holiday_delivery { get; set; }
        public string roundtrip_checknumber { get; set; }
        public DateTime closing_date { get; set; }
        public int custom_label { get; set; }
        public int latest_pick_up_scan_log_id { get; set; }
        public string orange_r1_1_uuser { get; set; }
        public bool sign_paper_print_flag { get; set; }
    }
    public class JubFuAPPWebServiceLog_Condition
    {
        public int? id { get; set; }
        public string sessionid { get; set; }
        public string type { get; set; }
        public string action { get; set; }
        public string message { get; set; }
        public DateTime? cdate { get; set; }

    }
    /// <summary>
    /// 派遣任務總表
    /// </summary>
    public class DispatchStatistics
    {
        //派遣類型編號
        public string task_type { get; set; }

        //派遣類型名稱
        public string type_name { get; set; }

        //件數
        public int cnt { get; set; }

    }

    /// <summary>
    /// 派遣任務
    /// </summary>
    public class DispatchTask
    {
        public int task_id { get; set; }

        //派遣類型編號
        public int task_type { get; set; }

        //派遣類型名稱
        public string type_name { get; set; }

        //任務日期
        public string task_date { get; set; }

        //供應商編號
        public string supplier_no { get; set; }

        //供應商名稱
        public string supplier_shortname { get; set; }

        //司機
        public string driver { get; set; }

        //車牌
        public string car_license { get; set; }

        //備註
        public string memo { get; set; }

    }

    /// <summary>
    /// 派遣託運單
    /// </summary>
    public class DispatchTaskDetail
    {
        //任務編號
        public int task_id { get; set; }

        //計價模式
        public string pricing_name { get; set; }

        //發送日期
        public string print_date { get; set; }

        //貨號
        public string check_number { get; set; }

        //收件人
        public string receive_contact { get; set; }

        //出貨客戶
        public string send_contact { get; set; }

        //出貨地點
        public string send_address { get; set; }

        //出貨板數
        public int plates { get; set; }

        //出貨件數
        public int pieces { get; set; }

        //區配商代碼
        public string supplier_code { get; set; }

        //區配商名稱
        public string supplier_name { get; set; }

        //到著碼
        public string area_arrive_code { get; set; }

        //到著碼名稱
        public string area_arrive_name { get; set; }

        //是否集貨
        public bool status_5 { get; set; }

        //是否卸集
        public bool status_6 { get; set; }

    }


    public class ReturnRelivery
    {
        public int request_id { get; set; }

        public string check_number { get; set; }

        public string order_number { get; set; }

        public string receive_customer_code { get; set; }

        public string receive_tel { get; set; }

        public string receive_contact { get; set; }

        public string receive_address { get; set; }

        public string send_contact { get; set; }

        public string send_tel { get; set; }

        public string send_address { get; set; }

        public string memo { get; set; }

    }

    public class Inventory
    {
        public int id { get; set; }

        public string station_code { get; set; }

        public string station_name { get; set; }

        public string driver_code { get; set; }

        public string driver_name { get; set; }

        public string scan_date { get; set; }

        public string check_number { get; set; }

    }

    public class Transfer
    {
        public int id { get; set; }

        public string station_code { get; set; }

        public string station_name { get; set; }

        public string driver_code { get; set; }

        public string driver_name { get; set; }

        public string scan_date { get; set; }

        public string check_number { get; set; }

    }

    /// <summary>
    /// 司機資料
    /// </summary>
    public class tbDrivers
    {
        public decimal driver_id { get; set; }
        public string driver_code { get; set; }
        public string driver_name { get; set; }
        public string driver_mobile { get; set; }
        public string supplier_code { get; set; }
        public string emp_code { get; set; }
        public string site_code { get; set; }
        public string area { get; set; }
        public string login_password { get; set; }
        public DateTime? login_date { get; set; }
        public bool? active_flag { get; set; }
        public bool? external_driver { get; set; }
        public string push_id { get; set; }
        public string wgs_x { get; set; }
        public string wgs_y { get; set; }
        public string station { get; set; }
        public DateTime? cdate { get; set; }
        public string cuser { get; set; }
        public DateTime? udate { get; set; }
        public string uuser { get; set; }
        public string office { get; set; }
        public string CollectionNo { get; set; }
        public string driver_type { get; set; }
        public string driver_type_code { get; set; }
        public string checkout_vendor { get; set; }
    }

    /// <summary>
    /// 月結袋綁定
    /// </summary>
    public class BagNoBinding
    {
        public long id { get; set; }
        public long RequestId { get; set; }
        public string CheckNumber { get; set; }
        public string BagNo { get; set; }
        public string DriverCode { get; set; }
        public bool IsBind { get; set; }
        public DateTime cdate { get; set; }
        public DateTime? udate { get; set; }
    }
}