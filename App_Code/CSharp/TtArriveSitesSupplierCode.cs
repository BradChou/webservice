﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// GetttArriveSitesSupplierCode 的摘要描述
/// </summary>
public class TtArriveSitesSupplierCode
{

    public TtArriveSitesSupplierCode()
    {
        //
        // TODO: 在這裡新增建構函式邏輯
        //
    }
    public class TtArriveSites
    {
        public string PostCity { get; set; }
        public string PostArea { get; set; }
        public string Zip { get; set; }
        public string SupplierCode { get; set; }
    }
    public static string GetttArriveSitesSupplierCode(string post_city, string post_area)
    {
        List<TtArriveSites> TtArriveSitesList = new List<TtArriveSites>();
        DataTable dt = new DataTable();
        string wherestr = "";
        JObject jObjMaster = new JObject();
        Boolean Status=true;
        string ErrorMsg = string.Empty;
        string sJson = "";
        string SupplierCode = "";
        //"select A.*,B.supplier_name from ttArriveSites as A left join tbSuppliers as B on B.supplier_code=A.supplier_code and B.active_flag=1 where Len(A.supplier_code)>0 order by B.supplier_code"
        try
        {
            if (string.IsNullOrEmpty(post_city.Trim())|| string.IsNullOrEmpty(post_area.Trim()))
            {
                Status = false;
                ErrorMsg = "請輸入縣市區域";
            }
            else
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Parameters.AddWithValue("@post_city", post_city);
                    wherestr += " and A.post_city=@post_city";
                    cmd.Parameters.AddWithValue("@post_area", post_area);
                    wherestr += " and A.post_area=@post_area";
                    cmd.CommandText = string.Format(string.Format(@"select A.supplier_code from ttArriveSites as A 
                                                        left join tbSuppliers as B on B.supplier_code=A.supplier_code and B.active_flag=1 
                                                        where Len(A.supplier_code)>0 {0} order by B.supplier_code", wherestr));
                    try
                    {
                        dt = dbAdapter.getDataTable(cmd);
                        if (dt == null || dt.Rows.Count == 0)
                        {
                            Status = false;
                            ErrorMsg = "查無區配資料";
                        }
                        else
                        {
                            SupplierCode = dt.Rows[0]["supplier_code"].ToString();
                        }
                    }
                    catch (Exception e)
                    {
                        Status = false;
                        ErrorMsg = e.Message;
                    }
                }
            }
        }
        catch(Exception e)
        {
            Status = false;
            ErrorMsg = "資料庫錯誤:"+ e.Message;
        }
        jObjMaster = new JObject(
                       new JProperty("resultcode", Status),
                       new JProperty("resultdesc", ErrorMsg),
                       new JProperty("supplier_code", SupplierCode)
               );
        sJson = jObjMaster.ToString();
        if (Status==false)
        {
            WriteLog("err_err", "GetttArriveSitesSupplierCode", post_city + post_area, ErrorMsg);
        }
        return sJson;
    }


    #region 執行歷程寫log到資料庫ttAppLog
    /// <summary>
    /// 
    /// </summary> 
    public static void WriteLog(string type, string fun_name, string memo, string log)
    {
        string project_name = "JUNFU_APP_WebService";
        string db_junfu_sql_conn_str = ConfigurationManager.ConnectionStrings["dbSqlJunfu_ConnStr"].ConnectionString;
        try
        {
            using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
            {

                string sql = "Insert into ttAppLog (app_project, app_fun_name, app_type, app_memo, app_log) values (@app_project, @app_fun_name, @app_type, @app_memo, @app_log)";

                using (SqlCommand command = new SqlCommand(sql, conn))
                {
                    conn.Open();

                    command.Parameters.Add("@app_project", SqlDbType.VarChar).Value = project_name;
                    command.Parameters.Add("@app_fun_name", SqlDbType.VarChar).Value = fun_name;
                    command.Parameters.Add("@app_type", SqlDbType.VarChar).Value = type;
                    command.Parameters.Add("@app_memo", SqlDbType.VarChar).Value = memo;
                    command.Parameters.Add("@app_log", SqlDbType.VarChar).Value = log;
                    command.ExecuteNonQuery();

                }
                conn.Close();

            }

        }
        catch (Exception e)
        {
            //這邊例外, 一樣會寫DB, 但不會再第二次catch了.
            if (!fun_name.Equals("writeLog"))
                WriteLog("err_err", "writeLog", memo, e.Message);
        }
        finally
        {
        }

    }
    #endregion
}