﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using CommonClass;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using Dapper;
using System.Net;
using System.Drawing.Imaging;
using System.Runtime.Serialization;
using RestSharp;
using System.Collections;
using System.Web.Script.Serialization;

/// <summary>
/// WebService 的摘要描述
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下列一行。
//[System.Web.Script.Services.ScriptService]
public class WebService : System.Web.Services.WebService
{

    private string project_name = "JUNFU_APP_WebService";

    private int db_Timeout = 120; //120秒
    private string db_junfu_sql_conn_str = ConfigurationManager.ConnectionStrings["dbSqlJunfu_ConnStr"].ConnectionString;
    private string db_junfu_trans_conn_str = ConfigurationManager.ConnectionStrings["dbSqlJunfu_Trans_ConnStr"].ConnectionString;
    private string dbSqloraArea_ConnStr = ConfigurationManager.ConnectionStrings["dbSqloraArea_ConnStr"].ConnectionString;
    private string localPathDaYuan = ConfigurationManager.AppSettings["LocalDaYuan"];
    private string localPathTaiChung = ConfigurationManager.AppSettings["LocalTaiChung"];
    private string S3PathDaYuan = ConfigurationManager.AppSettings["S3PathDaYuan"];
    private string S3PathTaiChung = ConfigurationManager.AppSettings["S3PathTaiChung"];
    private string ReportUrl = ConfigurationManager.AppSettings["ReportUrl"];
    private string ReportFolder = ConfigurationManager.AppSettings["ReportFolder"];
    private string ReportAccount = ConfigurationManager.AppSettings["ReportAccount"];
    private string ReportPassword = ConfigurationManager.AppSettings["ReportPassword"];
    private string ImageUrl = ConfigurationManager.AppSettings["ImageUrl"];

    #region 管理者APP
    public class chimeiAdmin_Info
    {
        //安卓伺服器金鑰
        public static string ANDROID_SERVER_API_KEY
        {
            get
            {
                try
                {
                    return ConfigurationManager.AppSettings["AndroidKey"];//System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AndroidKey"]);

                }
                catch
                {
                    return "";
                }
            }
        }
        //安卓寄件者ID
        public static string ANDROID_SENDER_ID
        {
            get
            {
                try
                {
                    return ConfigurationManager.AppSettings["AndroidID"];//System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, ConfigurationManager.AppSettings["AndroidID"]);

                }
                catch
                {
                    return "";
                }
            }
        }
    }
    #endregion

    //private string LogFolder = "C:\\IIS_WEB\\sunlit_app\\oracle_log\\";

    public WebService()
    {

        //如果使用設計的元件，請取消註解下列一行
        //InitializeComponent(); 
    }

    [WebMethod]
    public string PostCollectionMoneyNoBinding(string Type, string CheckNumber, string PostId, string DriverCode)
    {

        Guid g = Guid.NewGuid();
        var sessionid = g.ToString();
        JubFuAPPWebServiceLog(sessionid, "PostCollectionMoneyNoBinding", "Start", "OK");

        string parameter = JsonConvert.SerializeObject(new { Type, CheckNumber, PostId, DriverCode });
        JubFuAPPWebServiceLog(sessionid, "PostCollectionMoneyNoBinding", "parameter", parameter);
        DateTime dateTime = DateTime.Now;

        bool success = true;

        string strAppRequest = JsonConvert.SerializeObject(new { resultcode = true, resultdesc = "成功" }, Formatting.Indented);

        if (string.IsNullOrEmpty(Type))
        {
            return JsonConvert.SerializeObject(new { resultcode = true, resultdesc = "無類別" }, Formatting.Indented);
        }
        if (string.IsNullOrEmpty(CheckNumber))
        {
            return JsonConvert.SerializeObject(new { resultcode = true, resultdesc = "無託運號" }, Formatting.Indented);
        }
        if (string.IsNullOrEmpty(PostId))
        {
            return JsonConvert.SerializeObject(new { resultcode = true, resultdesc = "無郵局號" }, Formatting.Indented);
        }
        if (string.IsNullOrEmpty(DriverCode))
        {
            return JsonConvert.SerializeObject(new { resultcode = true, resultdesc = "無司機" }, Formatting.Indented);
        }
        //檢查司機

        var driverFSEHeader = new List<string>() { "pp520", "pp920" };//磅稱機專用帳號

        var d = DriverCode.ToLower();

        if (!driverFSEHeader.Contains(d))
        {
            return JsonConvert.SerializeObject(new { resultcode = true, resultdesc = "無權限" }, Formatting.Indented);
        }


        try
        {
            long tempRequest_id = 0;
            string tempCheckNumber = string.Empty;
            string tempPostId = string.Empty;
            //檢查託運單
            var Info = GettcDeliveryRequestsBycheck_number(CheckNumber);
            //檢查郵局號碼
            var Info2 = GettcDeliveryRequestsBycheck_number(PostId);

            if ((Info == null && Info2 == null) || (Info != null && Info2 != null))
            {
                return JsonConvert.SerializeObject(new { resultcode = true, resultdesc = "託運單號有誤" }, Formatting.Indented);
            }

            Post_Condition post = new Post_Condition();

            if (Info == null)
            {
                tempRequest_id = (long)Info2.request_id;

                tempCheckNumber = PostId;
                tempPostId = CheckNumber;

            }
            else
            {
                tempRequest_id = (long)Info.request_id;
                tempCheckNumber = CheckNumber;
                tempPostId = PostId;
            }

            if (tempPostId.Length != 14)
            {
                return JsonConvert.SerializeObject(new { resultcode = true, resultdesc = "郵局號碼有誤，請重新綁定" }, Formatting.Indented);
            }


            post.check_number = tempCheckNumber;
            post.PostId = tempPostId;
            post.request_id = tempRequest_id;

            switch (Type)
            {
                case "01"://綁定

                    //確定有過磅稱
                    var _CheckPostCollectionMoneyPending = CheckPostCollectionMoneyPending(tempCheckNumber);

                    if (_CheckPostCollectionMoneyPending == null)
                    {
                        return JsonConvert.SerializeObject(new { resultcode = true, resultdesc = "請先過磅" }, Formatting.Indented);
                    }

                    post.Weight = _CheckPostCollectionMoneyPending.Weight;

                    success = DoPostCollectionMoneyNoBinding(post);

                    ttDeliveryScanLog _ttDeliveryScanLog = new ttDeliveryScanLog();
                    _ttDeliveryScanLog.driver_code = DriverCode;
                    _ttDeliveryScanLog.check_number = tempCheckNumber;
                    _ttDeliveryScanLog.scan_item = "7";
                    _ttDeliveryScanLog.scan_date = DateTime.Now;
                    _ttDeliveryScanLog.tracking_number = tempPostId;
                    _ttDeliveryScanLog.cdate = DateTime.Now;







                    //更新貨態
                    using (var cn = new SqlConnection(db_junfu_sql_conn_str))
                    {
                        string sql =
                            @"  INSERT INTO [dbo].[ttDeliveryScanLog]
           (
            [driver_code]
           ,[check_number]
           ,[scan_item]
           ,[scan_date]
           ,[cdate]
           ,[tracking_number]
            )
     VALUES
           (
            @driver_code
           ,@check_number
           ,@scan_item
           ,@scan_date
           ,@cdate
           ,@tracking_number
      
)
                    ";
                        cn.Execute(sql, _ttDeliveryScanLog);
                    }

                    //增加至新表 ttDeliveryScanLog2
                    //更新貨態
                    using (var cn = new SqlConnection(db_junfu_sql_conn_str))
                    {
                        string sql =
                            @"  INSERT INTO [dbo].[ttDeliveryScanLog2]
           (
            [driver_code]
           ,[check_number]
           ,[scan_item]
           ,[scan_date]
           ,[cdate]
           ,[tracking_number]
            )
     VALUES
           (
            @driver_code
           ,@check_number
           ,@scan_item
           ,@scan_date
           ,@cdate
           ,@tracking_number
      
)
                    ";
                        cn.Execute(sql, _ttDeliveryScanLog);
                    }


                    break;
                case "02"://解除綁定
                          //
                    success = DelPostCollectionMoneyNoBinding(post);

                    break;

                default:
                    return JsonConvert.SerializeObject(new { resultcode = true, resultdesc = "Type異常" }, Formatting.Indented);

            }
            if (!success)
            {
                return JsonConvert.SerializeObject(new { resultcode = true, resultdesc = "資料庫異常" }, Formatting.Indented);
            }

        }
        catch (Exception e)
        {
            return JsonConvert.SerializeObject(new { resultcode = true, resultdesc = "系統異常 :" + e.Message }, Formatting.Indented);

        }
        JubFuAPPWebServiceLog(sessionid, "PostCollectionMoneyNoBinding", "End", "OK");
        return strAppRequest;

    }



    [WebMethod]
    public string HelloWorld(String request_id, string sign_field_image_name)
    {

        // var ssss = HttpContext.Current.Request.UserAgent;

        string UserAgent = this.Context.Request.UserAgent;
        string IP = this.Context.Request.UserHostAddress;


        Guid g = Guid.NewGuid();
        var sessionid = g.ToString();
        JubFuAPPWebServiceLog(sessionid, "HelloWorld", "Start", request_id);

        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dtAppRequest = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;



        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";
        return JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, serverdate = serverdate });

    }

    #region 1、帳號登入驗證
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    [WebMethod]
    public string loginCheck(string driver_code, string login_password)
    {
        writeLog("call", "loginCheck", driver_code + "_" + login_password, "");
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dtAppRequest = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;
        string driver_name = string.Empty;
        string supplier_code = string.Empty;
        string type = "0";                  // 0: 峻富一般司機  1:外車司機
        string station = string.Empty;      // 站所代碼
        string station_name = string.Empty; // 站所名稱
        string corp_type = "A";             // A:峻富司機 / B:零擔司機


        if (driver_code.Equals(""))
        {
            resultcode = false;
            error_msg = "司機工號未傳入";
        }
        else
        {

            try
            {
                using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                {
                    StringBuilder sql = new StringBuilder();
                    sql.Append(@" select A.*, B.station_scode, B.station_name  
                        from tbDrivers A with(nolock) 
                        left join tbStation B with(nolock) on A.station= B.station_scode
                        where driver_code = @driver_code and  A.active_flag <> 0");

                    using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                    {
                        conn.Open();
                        command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;

                        SqlDataAdapter da = new SqlDataAdapter(command);
                        da.SelectCommand.CommandTimeout = db_Timeout;
                        da.Fill(dtAppRequest);
                        conn.Close();
                    }
                }
                writeLog("ref", "loginCheck", driver_code + "_" + login_password, "");
            }
            catch (Exception e)
            {
                resultcode = false;
                error_msg = e.Message;
                writeLog("err", "loginCheck", driver_code + "_" + login_password, e.Message);
            }
            finally
            {
            }


            //將欄位名稱轉成小寫
            if (dtAppRequest != null && dtAppRequest.Rows.Count > 0)
            {
                foreach (DataColumn column in dtAppRequest.Columns)
                {
                    column.ColumnName = column.ColumnName.ToLower();
                }

                //處理資料
                DataRow row = dtAppRequest.Rows[0];
                if (row["active_flag"].ToString().Equals("0"))
                {
                    dtAppRequest.Clear();
                    resultcode = false;
                    error_msg = "帳號已停用";
                }
                else if (!row["login_password"].ToString().Equals(login_password))
                {
                    dtAppRequest.Clear();
                    resultcode = false;
                    error_msg = "密碼輸入錯誤";
                }
                else
                {
                    //寫入登入時間
                    writeLoginDate(driver_code);
                    driver_name = row["driver_name"].ToString();
                    supplier_code = row["supplier_code"].ToString();
                    //if (row["supplier_code"].ToString() == "Z88")
                    if (Convert.ToBoolean(row["external_driver"]) == true)
                    {
                        type = "1";
                    }

                    station = row["station_scode"].ToString();
                    station_name = row["station_name"].ToString();

                    if (row["driver_code"].ToString() != "" && row["driver_code"].ToString().Substring(0, 1) == "Z")
                    {
                        corp_type = "B";
                    }
                    else
                    {
                        corp_type = "A";
                    }
                }
            }
            else
            {
                resultcode = false;
                error_msg = "帳號不存在";
            }

        }

        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, driver_name = driver_name, supplier_code = supplier_code, type = type, corp_type = corp_type, station = station, station_name = station_name }, Formatting.Indented);
        return strAppRequest;
    }
    /// <summary>
    /// 帳號登入驗證v2
    /// </summary>
    /// <param name="driver_code"></param>
    /// <param name="login_password"></param>
    /// <param name="device"></param>
    /// <param name="product"></param>
    /// <param name="brand"></param>
    /// <param name="versionCode"></param>
    /// <returns></returns>
    [WebMethod]
    public string loginCheckV2(string driver_code, string login_password, string device, string product, string brand, string versionCode)
    {
        writeLog("call", "loginCheck", driver_code + "_" + login_password, "");
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dtAppRequest = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;
        string driver_name = string.Empty;
        string supplier_code = string.Empty;
        string type = "0";                  // 0: 峻富一般司機  1:外車司機
        string station = string.Empty;      // 站所代碼
        string station_name = string.Empty; // 站所名稱
        string corp_type = "A";             // A:峻富司機 / B:零擔司機
        string work_Station_Scode = string.Empty; //作業站所代碼
        string work_Send_Code = string.Empty; //作業集區
        string work_Delivery_Code = string.Empty; //作業配區
        string work_Station_Name = string.Empty; //作業配區




        if (driver_code.Equals(""))
        {
            resultcode = false;
            error_msg = "司機工號未傳入";
        }
        else
        {

            try
            {
                using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                {
                    StringBuilder sql = new StringBuilder();
                    sql.Append(@"select A.*, B.station_scode, B.station_name,C.station_name as work_Station_Name
                        from tbDrivers A with(nolock) 
                        left join tbStation B with(nolock) on A.station= B.station_scode
                        left join tbStation C with(nolock) on A.Work_Station_Scode= C.station_scode
                        where driver_code = @driver_code and  A.active_flag <> 0
                           --and c.station_scode=a.Work_Station_Scode
");

                    using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                    {
                        conn.Open();
                        command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;

                        SqlDataAdapter da = new SqlDataAdapter(command);
                        da.SelectCommand.CommandTimeout = db_Timeout;
                        da.Fill(dtAppRequest);
                        conn.Close();
                    }
                }
                writeLog("ref", "loginCheck", driver_code + "_" + login_password, "");
            }
            catch (Exception e)
            {
                resultcode = false;
                error_msg = e.Message;
                writeLog("err", "loginCheck", driver_code + "_" + login_password, e.Message);
            }
            finally
            {
            }


            //將欄位名稱轉成小寫
            if (dtAppRequest != null && dtAppRequest.Rows.Count > 0)
            {
                foreach (DataColumn column in dtAppRequest.Columns)
                {
                    column.ColumnName = column.ColumnName.ToLower();
                }

                //處理資料
                DataRow row = dtAppRequest.Rows[0];
                if (row["active_flag"].ToString().Equals("0"))
                {
                    dtAppRequest.Clear();
                    resultcode = false;
                    error_msg = "帳號已停用";
                }
                else if (!row["login_password"].ToString().Equals(login_password))
                {
                    dtAppRequest.Clear();
                    resultcode = false;
                    error_msg = "密碼輸入錯誤";
                }
                else
                {
                    //寫入登入時間
                    writeLoginDate(driver_code, device, product, brand, versionCode);
                    driver_name = row["driver_name"].ToString();
                    supplier_code = row["supplier_code"].ToString();
                    //if (row["supplier_code"].ToString() == "Z88")
                    if (Convert.ToBoolean(row["external_driver"]) == true)
                    {
                        type = "1";
                    }

                    station = row["station_scode"].ToString();
                    station_name = row["station_name"].ToString();

                    if (row["driver_code"].ToString() != "" && row["driver_code"].ToString().Substring(0, 1) == "Z")
                    {
                        corp_type = "B";
                    }
                    else
                    {
                        corp_type = "A";
                    }
                    work_Station_Scode = row["work_Station_Scode"].ToString();
                    work_Send_Code = row["work_Send_Code"].ToString();
                    work_Delivery_Code = row["work_Delivery_Code"].ToString();
                    work_Station_Name = row["work_Station_Name"].ToString();
                }
            }
            else
            {
                resultcode = false;
                error_msg = "帳號不存在";
            }

        }

        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, driver_name = driver_name, supplier_code = supplier_code, type = type, corp_type = corp_type, station = station, station_name = station_name, work_Station_Name = work_Station_Name, Work_Station_Scode = work_Station_Scode, Work_Send_Code = work_Send_Code, Work_Delivery_Code = work_Delivery_Code }, Formatting.Indented);
        return strAppRequest;
    }
    /// <summary>
    /// 帳號登入驗證v3
    /// </summary>
    /// <param name="driver_code"></param>
    /// <param name="login_password"></param>
    /// <param name="device"></param>
    /// <param name="product"></param>
    /// <param name="brand"></param>
    /// <param name="versionCode"></param>
    /// <param name="mobiletoken"></param>
    /// <param name="mobiletype"></param>
    /// <returns></returns>
    [WebMethod]
    public string loginCheckV3(string driver_code, string login_password, string device, string product, string brand, string versionCode, string mobiletoken, string mobiletype)
    {
        writeLog("call", "loginCheck", driver_code + "_" + login_password, "");
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dtAppRequest = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;
        string driver_name = string.Empty;
        string supplier_code = string.Empty;
        string type = "0";                  // 0: 峻富一般司機  1:外車司機
        string station = string.Empty;      // 站所代碼
        string station_name = string.Empty; // 站所名稱
        string corp_type = "A";             // A:峻富司機 / B:零擔司機
        string work_Station_Scode = string.Empty; //作業站所代碼
        string work_Send_Code = string.Empty; //作業集區
        string work_Delivery_Code = string.Empty; //作業配區
        string work_Station_Name = string.Empty; //作業配區


        if (driver_code.Equals(""))
        {
            resultcode = false;
            error_msg = "司機工號未傳入";
        }
        else
        {

            try
            {
                using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                {
                    StringBuilder sql = new StringBuilder();
                    sql.Append(@"select A.*, B.station_scode, B.station_name,C.station_name as work_Station_Name
                        from tbDrivers A with(nolock) 
                        left join tbStation B with(nolock) on A.station= B.station_scode
                        left join tbStation C with(nolock) on A.Work_Station_Scode= C.station_scode
                        where driver_code = @driver_code and  A.active_flag <> 0
                           --and c.station_scode=a.Work_Station_Scode");

                    using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                    {
                        conn.Open();
                        command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;

                        SqlDataAdapter da = new SqlDataAdapter(command);
                        da.SelectCommand.CommandTimeout = db_Timeout;
                        da.Fill(dtAppRequest);
                        conn.Close();
                    }
                }
                writeLog("ref", "loginCheck", driver_code + "_" + login_password, "");
            }
            catch (Exception e)
            {
                resultcode = false;
                error_msg = e.Message;
                writeLog("err", "loginCheck", driver_code + "_" + login_password, e.Message);
            }
            finally
            {
            }


            //將欄位名稱轉成小寫
            if (dtAppRequest != null && dtAppRequest.Rows.Count > 0)
            {
                foreach (DataColumn column in dtAppRequest.Columns)
                {
                    column.ColumnName = column.ColumnName.ToLower();
                }

                //處理資料
                DataRow row = dtAppRequest.Rows[0];
                if (row["active_flag"].ToString().Equals("0"))
                {
                    dtAppRequest.Clear();
                    resultcode = false;
                    error_msg = "帳號已停用";
                }
                else if (!row["login_password"].ToString().Equals(login_password))
                {
                    dtAppRequest.Clear();
                    resultcode = false;
                    error_msg = "密碼輸入錯誤";
                }
                else
                {
                    //寫入登入時間
                    writeLoginDate(driver_code, device, product, brand, versionCode, mobiletoken, mobiletype);
                    driver_name = row["driver_name"].ToString();
                    supplier_code = row["supplier_code"].ToString();
                    //if (row["supplier_code"].ToString() == "Z88")
                    if (Convert.ToBoolean(row["external_driver"]) == true)
                    {
                        type = "1";
                    }

                    station = row["station_scode"].ToString();
                    station_name = row["station_name"].ToString();

                    if (row["driver_code"].ToString() != "" && row["driver_code"].ToString().Substring(0, 1) == "Z")
                    {
                        corp_type = "B";
                    }
                    else
                    {
                        corp_type = "A";
                    }
                    work_Station_Scode = row["work_Station_Scode"].ToString();
                    work_Send_Code = row["work_Send_Code"].ToString();
                    work_Delivery_Code = row["work_Delivery_Code"].ToString();
                    work_Station_Name = row["work_Station_Name"].ToString();
                }
            }
            else
            {
                resultcode = false;
                error_msg = "帳號不存在";
            }

        }

        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, driver_name = driver_name, supplier_code = supplier_code, type = type, corp_type = corp_type, station = station, station_name = station_name, work_Station_Name = work_Station_Name, Work_Station_Scode = work_Station_Scode, Work_Send_Code = work_Send_Code, Work_Delivery_Code = work_Delivery_Code }, Formatting.Indented);
        return strAppRequest;
    }

    /// <summary>
    /// 更新司機資訊
    /// </summary>
    /// <param name="driver_code"></param>
    /// <param name="Work_Station_Scode"></param>
    /// <param name="Work_Send_Code"></param>
    /// <param name="Work_Delivery_Code"></param>
    /// <returns></returns>
    [WebMethod]
    public string UpdateDriver(string driver_code, string Work_Station_Scode, string Work_Send_Code, string Work_Delivery_Code)
    {
        string serverdate = System.DateTime.Now.ToString("yyyyMMddHHmmss");
        writeLog("call", "UpdateDriver", driver_code + "_" + serverdate, "");
        DataTable dtAppRequest = new DataTable();

        Boolean resultcode = true;
        string error_msg = string.Empty;
        string work_Station_Scode = string.Empty; //作業站所代碼
        string work_Send_Code = string.Empty; //作業集區
        string work_Delivery_Code = string.Empty; //作業配區

        try
        {
            using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
            {
                StringBuilder sql = new StringBuilder();
                sql.Append(@"
UPDATE tbDrivers
SET Work_Station_Scode=@Work_Station_Scode,Work_Send_Code=@Work_Send_Code,Work_Delivery_Code=@Work_Delivery_Code
WHERE driver_code = @driver_code");

                using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                {
                    conn.Open();
                    command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                    command.Parameters.Add("@Work_Station_Scode", SqlDbType.VarChar).Value = Work_Station_Scode;
                    command.Parameters.Add("@Work_Send_Code", SqlDbType.VarChar).Value = Work_Send_Code;
                    command.Parameters.Add("@Work_Delivery_Code", SqlDbType.VarChar).Value = Work_Delivery_Code;

                    SqlDataAdapter da = new SqlDataAdapter(command);
                    da.SelectCommand.CommandTimeout = db_Timeout;
                    da.Fill(dtAppRequest);
                    conn.Close();
                    error_msg = "更新成功";
                }
            }

            writeLog("ref", "UpdateDriver", driver_code + "_" + serverdate, "");

        }
        catch (Exception e)
        {
            resultcode = false;
            error_msg = e.Message;
            writeLog("err", "UpdateDriver", driver_code + "_" + serverdate, e.Message);
        }
        finally
        {
        }
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg }, Formatting.Indented);
        return strAppRequest;
    }

    /// <summary>
    /// 取得駐區碼
    /// </summary>
    /// <param name="Work_Station_Scode">站所代碼</param>
    /// <returns></returns>
    [WebMethod]
    public string GetDataByStationScode(string Work_Station_Scode)
    {
        string serverdate = System.DateTime.Now.ToString("yyyyMMddHHmmss");
        //writeLog("call", "GetDataByStationScode", Work_Station_Scode + "_" + serverdate, "");
        DataTable dtAppRequest = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;
        string Work_Station_Name = string.Empty;
        string Send_Code_Data = string.Empty;
        string Delivery_Code_Data = string.Empty;
        try
        {
            using (SqlConnection conn = new SqlConnection(dbSqloraArea_ConnStr))
            {
                StringBuilder sql = new StringBuilder();
                sql.Append(@"select distinct station_name,md_no as Send_Code_Data from orgArea where station_scode=@Work_Station_Scode
and md_no !=''
UNION
select distinct station_name,sd_no as Send_Code_Data from orgArea where station_scode=@Work_Station_Scode
and sd_no!='' ");

                using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                {
                    conn.Open();
                    command.Parameters.Add("@Work_Station_Scode", SqlDbType.VarChar).Value = Work_Station_Scode;

                    SqlDataAdapter da = new SqlDataAdapter(command);
                    da.SelectCommand.CommandTimeout = db_Timeout;
                    da.Fill(dtAppRequest);
                    conn.Close();
                }
            }

            if (dtAppRequest.Rows.Count == 0)
            {
                resultcode = false;
                error_msg = "查無此站所代碼";
                string Request = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, Work_Station_Name = Work_Station_Name, Send_Code_Data = Send_Code_Data, Delivery_Code_Data = Delivery_Code_Data }, Formatting.Indented);

                return Request;
            }


            //處理資料
            DataRow r = dtAppRequest.Rows[0];
            Work_Station_Name = r["station_name"].ToString();

            DataRow row /*= dtAppRequest.Rows[0]*/;

            Send_Code_Data += "[";
            for (int i = 0; i < dtAppRequest.Rows.Count; i++)
            {

                row = dtAppRequest.Rows[i];
                Send_Code_Data += row["Send_Code_Data"].ToString();
                Send_Code_Data += ",";
            }
            Send_Code_Data = Send_Code_Data.Remove(Send_Code_Data.ToString().LastIndexOf(','), 1);
            Send_Code_Data += "]";

            //第二段
            dtAppRequest = new DataTable();
            using (SqlConnection conn = new SqlConnection(dbSqloraArea_ConnStr))
            {
                StringBuilder sql = new StringBuilder();
                sql.Append(@"select distinct station_name,SendSD as Delivery_Code_Data from orgArea where station_scode=@Work_Station_Scode
and SendSD !=''
UNION
select distinct station_name,SendMD as Delivery_Code_Data from orgArea where station_scode=@Work_Station_Scode
and SendMD!='' ");

                using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                {
                    conn.Open();
                    command.Parameters.Add("@Work_Station_Scode", SqlDbType.VarChar).Value = Work_Station_Scode;

                    SqlDataAdapter da = new SqlDataAdapter(command);
                    da.SelectCommand.CommandTimeout = db_Timeout;
                    da.Fill(dtAppRequest);
                    conn.Close();
                }
            }

            //處理資料
            //DataRow row = dtAppRequest.Rows[0];
            DataRow row2 /*= dtAppRequest.Rows[0]*/;
            Delivery_Code_Data += "[";
            for (int i = 0; i < dtAppRequest.Rows.Count; i++)
            {
                row2 = dtAppRequest.Rows[i];
                Delivery_Code_Data += row2["Delivery_Code_Data"].ToString();
                Delivery_Code_Data += ",";
            }
            Delivery_Code_Data = Delivery_Code_Data.Remove(Delivery_Code_Data.ToString().LastIndexOf(','), 1);
            Delivery_Code_Data += "]";


            //writeLog("ref", "UpdateDriver", driver_code + "_" + serverdate, "");

        }
        catch (Exception e)
        {
            resultcode = false;
            error_msg = e.Message;
            // writeLog("err", "UpdateDriver", driver_code + "_" + serverdate, e.Message);
        }
        finally
        {
        }
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, Work_Station_Name = Work_Station_Name, Send_Code_Data = Send_Code_Data, Delivery_Code_Data = Delivery_Code_Data }, Formatting.Indented);

        return strAppRequest;
    }



    #endregion

    #region 2、貨號掃讀資訊回傳
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    [WebMethod]
    public string checkScancode(string check_number)
    {
        writeLog("call", "checkScancode", check_number, "");
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dtAppRequest = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;
        string subpoena_category = string.Empty;
        string money = "0";

        if (check_number.Equals(""))
        {
            resultcode = false;
            error_msg = "託運單號未傳入";
        }
        else
        {

            try
            {
                using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                {
                    StringBuilder sql = new StringBuilder();
                    sql.Append(" select top 1 * from tcDeliveryRequests where check_number = @check_number order by request_id desc");

                    using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                    {
                        conn.Open();
                        command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;

                        SqlDataAdapter da = new SqlDataAdapter(command);
                        da.SelectCommand.CommandTimeout = db_Timeout;
                        da.Fill(dtAppRequest);
                        conn.Close();

                    }
                }
                writeLog("ref", "checkScancode", check_number, "");
            }
            catch (Exception e)
            {
                resultcode = false;
                error_msg = e.Message;
                writeLog("err", "checkScancode", check_number, e.Message);
            }
            finally
            {
            }


            //將欄位名稱轉成小寫
            if (dtAppRequest != null && dtAppRequest.Rows.Count > 0)
            {
                foreach (DataColumn column in dtAppRequest.Columns)
                {
                    column.ColumnName = column.ColumnName.ToLower();
                }

                //處理資料
                DataRow row = dtAppRequest.Rows[0];
                subpoena_category = row["subpoena_category"].ToString();
                switch (subpoena_category)
                {
                    case "21":  //到付
                        //money = row["arrive_to_pay_freight"] != DBNull.Value ? Convert.ToInt32(row["arrive_to_pay_freight"]).ToString("N0") : "0";
                        money = row["arrive_to_pay_freight"] != DBNull.Value ? row["arrive_to_pay_freight"].ToString() : "0";
                        break;
                    case "25":  //到付追加
                        //money = row["arrive_to_pay_append"] != DBNull.Value ?   Convert.ToInt32(row["arrive_to_pay_append"]).ToString("N0") : "0";
                        money = row["arrive_to_pay_append"] != DBNull.Value ? row["arrive_to_pay_append"].ToString() : "0";
                        break;
                    case "41":  //代收貨款
                                //money = row["collection_money"] !=  DBNull.Value ?  Convert.ToInt32(row["collection_money"]).ToString("N0") : "0" ;
                        money = row["collection_money"] != DBNull.Value ? row["collection_money"].ToString() : "0";
                        break;
                }
            }
        }

        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, subpoena_category = subpoena_category, money = money }, Formatting.Indented);
        return strAppRequest;
    }
    #endregion
    #region Getchecknumber
    /// <summary>
    /// 取得託運單資訊
    /// </summary>
    /// <returns></returns>
    [WebMethod]
    public string Getchecknumber(string check_number, string driver_code)
    {
        string UserAgent = this.Context.Request.UserAgent;
        string IP = this.Context.Request.UserHostAddress;
        Guid g = Guid.NewGuid();
        var sessionid = g.ToString();
        JubFuAPPWebServiceLog(sessionid, "Getchecknumber", "Start", "OK");
        JubFuAPPWebServiceLog(sessionid, "Getchecknumber", "UserAgent", UserAgent);
        JubFuAPPWebServiceLog(sessionid, "Getchecknumber", "IP", IP);
        string parameter = JsonConvert.SerializeObject(new { check_number, driver_code });
        JubFuAPPWebServiceLog(sessionid, "Getchecknumber", "parameter", parameter);

        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

        bool resultcode = true;
        string error_msg = string.Empty;
        string subpoena_category = string.Empty;
        string money = "0";
        string subpoena_category_Text = string.Empty;

        string pieces = string.Empty;
        string request_id = string.Empty;
        string receipt_flag = string.Empty;
        if (check_number.Equals(""))
        {
            resultcode = false;
            error_msg = "託運單號未傳入";
        }
        else
        {

            var info = GettcDeliveryRequestsBycheck_number(check_number);

            if (info == null)
            {
                error_msg = "查無此託運單號";
                return JsonConvert.SerializeObject(new { resultcode, error_msg }, Formatting.Indented);
            }
            request_id = info.request_id.ToString();

            pieces = info.pieces.HasValue ? info.pieces.ToString() : "--";

            subpoena_category = info.subpoena_category;

            switch (subpoena_category)
            {
                case "21":  //到付
                    subpoena_category_Text = "到付";
                    money = info.arrive_to_pay_freight.HasValue ? info.arrive_to_pay_freight.ToString() : "0";
                    break;
                case "25":  //到付追加
                    subpoena_category_Text = "到付追加";
                    money = info.arrive_to_pay_append.HasValue ? info.arrive_to_pay_append.ToString() : "0";
                    break;
                case "41":  //代收貨款
                    subpoena_category_Text = "代收貨款";
                    money = info.collection_money.HasValue ? info.collection_money.ToString() : "0";
                    break;
                default:
                    money = "0";
                    break;
            }

            receipt_flag = info.receipt_flag ? "1" : "0";
        }

        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode, request_id, check_number, subpoena_category, subpoena_category_Text, money, pieces, receipt_flag, resultdesc = error_msg }, Formatting.Indented);
        JubFuAPPWebServiceLog(sessionid, "Getchecknumber", "End", "OK");
        return strAppRequest;
    }
    #endregion
    #region CheckDeliveryStatus
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    [WebMethod]
    public string CheckDeliveryStatus(string check_number)
    {
        string UserAgent = this.Context.Request.UserAgent;
        string IP = this.Context.Request.UserHostAddress;
        Guid g = Guid.NewGuid();
        var sessionid = g.ToString();
        JubFuAPPWebServiceLog(sessionid, "CheckDeliveryStatus", "Start", "OK");
        JubFuAPPWebServiceLog(sessionid, "CheckDeliveryStatus", "UserAgent", UserAgent);
        JubFuAPPWebServiceLog(sessionid, "CheckDeliveryStatus", "IP", IP);
        string parameter = JsonConvert.SerializeObject(new { check_number });
        JubFuAPPWebServiceLog(sessionid, "CheckDeliveryStatus", "parameter", parameter);

        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

        bool resultcode = true;
        string error_msg = string.Empty;


        string pieces = string.Empty;
        string request_id = string.Empty;
        if (check_number.Equals(""))
        {
            resultcode = false;
            error_msg = "託運單號未傳入";
        }
        else
        {
            var info = GettcDeliveryRequestsBycheck_number(check_number);

            if (info == null)
            {
                error_msg = "查無此託運單號";
            }
            var list = GetttDeliveryScanLogListBycheck_number(check_number).Where(x => DateTime.Compare(x.scan_date, DateTime.Now.AddMinutes(-15)) > 0);

            if (list != null && list.Count() > 0)
            {
                var list3 = list.Where(x => x.scan_item.Equals("3") && (x.arrive_option.Equals("3") || x.arrive_option.Equals("7") || x.arrive_option.Equals("8") || x.arrive_option.Equals("9"))).ToList();

                if (list3 != null && list3.Count() > 0)
                {
                    var list4 = list.Where(x => x.scan_item.Equals("4")).ToList();

                    if (list4 != null && list4.Count() > 0)
                    {
                        error_msg = "OK";
                    }
                    else
                    {
                        error_msg = "缺圖，請重新補圖";
                    }
                }
                else
                {
                    error_msg = "未有配達紀錄";
                }
            }
            else
            {
                error_msg = "未有配達紀錄";
            }

        }

        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode, resultdesc = error_msg }, Formatting.Indented);
        JubFuAPPWebServiceLog(sessionid, "CheckDeliveryStatus", "End", "OK");
        return strAppRequest;
    }
    #endregion
    #region 3、託運單掃讀資料上傳
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    [WebMethod]
    public string sendUploadData(string scan_item, string driver_code, string check_number, string scan_date, string area_arrive_code, string platform, string car_number, string sowage_rate, string area, string exception_option, string arrive_option, string sign_form_image, string sign_field_image)
    {
        writeLog("call", "sendUploadData", scan_item + "_" + driver_code + "_" + check_number + "_" + scan_date + "_" + area_arrive_code + "_" + platform + "_" + car_number + "_" + sowage_rate + "_" + area + "_" + exception_option + "_" + arrive_option, "");
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dtAppRequest = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;

        if (scan_item.Equals(""))
        {
            resultcode = false;
            error_msg = "作業類別未傳入";
        }
        else if (driver_code.Equals(""))
        {
            resultcode = false;
            error_msg = "司機代碼未傳入";
        }
        else if (check_number.Equals(""))
        {
            resultcode = false;
            error_msg = "託運單號未傳入";

        }
        else if (!(scan_item.Equals("1") || scan_item.Equals("2") || scan_item.Equals("3") || scan_item.Equals("4")))
        {
            resultcode = false;
            error_msg = "作業類別錯誤";
        }
        else if (scan_item.Equals("4") && (sign_form_image.Equals("") || sign_field_image.Equals("")))
        {
            resultcode = false;
            error_msg = "簽單圖檔未傳入";
        }

        else
        {


            try
            {

                //到著掃讀
                if (scan_item.Equals("1"))
                {

                    using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                    {
                        StringBuilder sql = new StringBuilder();
                        sql.Append(" insert into ttDeliveryScanLog (driver_code, check_number, scan_item, scan_date, area_arrive_code, platform, car_number, sowage_rate, area, exception_option) values (@driver_code, @check_number, @scan_item, @scan_date, @area_arrive_code, @platform, @car_number, @sowage_rate, @area, @exception_option)");

                        using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                        {
                            conn.Open();
                            command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                            command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                            command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                            command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                            command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;
                            command.Parameters.Add("@platform", SqlDbType.VarChar).Value = platform;
                            command.Parameters.Add("@car_number", SqlDbType.VarChar).Value = car_number;
                            command.Parameters.Add("@sowage_rate", SqlDbType.VarChar).Value = sowage_rate;
                            command.Parameters.Add("@area", SqlDbType.VarChar).Value = area;
                            command.Parameters.Add("@exception_option", SqlDbType.VarChar).Value = exception_option;

                            SqlDataAdapter da = new SqlDataAdapter(command);
                            da.SelectCommand.CommandTimeout = db_Timeout;
                            da.Fill(dtAppRequest);
                            conn.Close();

                        }
                    }
                }

                //配送掃讀
                else if (scan_item.Equals("2"))
                {

                    using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                    {
                        StringBuilder sql = new StringBuilder();
                        sql.Append(" insert into ttDeliveryScanLog (driver_code, check_number, scan_item, scan_date, exception_option, area_arrive_code) values (@driver_code, @check_number, @scan_item, @scan_date, @exception_option, @area_arrive_code)");

                        using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                        {
                            conn.Open();
                            command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                            command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                            command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                            command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                            command.Parameters.Add("@exception_option", SqlDbType.VarChar).Value = exception_option;
                            command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;

                            SqlDataAdapter da = new SqlDataAdapter(command);
                            da.SelectCommand.CommandTimeout = db_Timeout;
                            da.Fill(dtAppRequest);
                            conn.Close();

                        }
                    }


                }

                //配達掃讀
                else if (scan_item.Equals("3"))
                {

                    //DateTime now = System.DateTime.Now; //string drawString = time.ToString("yyyy/MM/dd HH:mm");
                    //string upload_time = now.ToString("yyyy/MM/dd HH:mm");

                    //託運單圖檔名
                    //string sign_form_image_name = driver_code + "_" + check_number + "_" + now.ToString("yyyyMMddHHmmss") + ".jpg";
                    //託運單簽名欄位圖檔名
                    //string sign_field_image_name = "sign" + "_" + check_number  + "_" + now.ToString("yyyyMMddHHmmss") + ".jpg";

                    using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                    {
                        StringBuilder sql = new StringBuilder();
                        sql.Append(" insert into ttDeliveryScanLog (driver_code, check_number, scan_item, scan_date, arrive_option, area_arrive_code) values (@driver_code, @check_number, @scan_item, @scan_date, @arrive_option, @area_arrive_code)");

                        using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                        {
                            conn.Open();
                            command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                            command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                            command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                            command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                            command.Parameters.Add("@arrive_option", SqlDbType.VarChar).Value = arrive_option;
                            //command.Parameters.Add("@sign_form_image", SqlDbType.VarChar).Value = sign_form_image_name;
                            //command.Parameters.Add("@sign_field_image", SqlDbType.VarChar).Value = sign_field_image_name;
                            command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;

                            SqlDataAdapter da = new SqlDataAdapter(command);
                            da.SelectCommand.CommandTimeout = db_Timeout;
                            da.Fill(dtAppRequest);
                            conn.Close();
                        }
                    }

                    //寫入圖檔
                    //SaveImgFromBase64(sign_form_image, sign_form_image_name, upload_time);
                    //SaveImgFromBase64(sign_field_image, sign_field_image_name, upload_time);

                }

                //簽單掃讀
                else if (scan_item.Equals("4"))
                {

                    DateTime now = System.DateTime.Now; //string drawString = time.ToString("yyyy/MM/dd HH:mm");
                    string upload_time = now.ToString("yyyy/MM/dd HH:mm");

                    //託運單圖檔名
                    string sign_form_image_name = driver_code + "_" + check_number + "_" + now.ToString("yyyyMMddHHmmss") + ".jpg";
                    //託運單簽名欄位圖檔名
                    string sign_field_image_name = "sign" + "_" + check_number + "_" + now.ToString("yyyyMMddHHmmss") + ".jpg";

                    using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                    {
                        StringBuilder sql = new StringBuilder();
                        sql.Append(" insert into ttDeliveryScanLog (driver_code, check_number, scan_item, scan_date, arrive_option, sign_form_image, sign_field_image, area_arrive_code) values (@driver_code, @check_number, @scan_item, @scan_date, @arrive_option, @sign_form_image, @sign_field_image, @area_arrive_code)");

                        using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                        {
                            conn.Open();
                            command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                            command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                            command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                            command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                            command.Parameters.Add("@arrive_option", SqlDbType.VarChar).Value = arrive_option;
                            command.Parameters.Add("@sign_form_image", SqlDbType.VarChar).Value = sign_form_image_name;
                            command.Parameters.Add("@sign_field_image", SqlDbType.VarChar).Value = sign_field_image_name;
                            command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;

                            SqlDataAdapter da = new SqlDataAdapter(command);
                            da.SelectCommand.CommandTimeout = db_Timeout;
                            da.Fill(dtAppRequest);
                            conn.Close();
                        }
                    }

                    //寫入圖檔
                    SaveImgFromBase64(sign_form_image, sign_form_image_name, upload_time);
                    SaveImgFromBase64(sign_field_image, sign_field_image_name, upload_time);

                }


                writeLog("ref", "sendUploadData", scan_item + "_" + driver_code + "_" + check_number + "_" + scan_date + "_" + area_arrive_code + "_" + platform + "_" + car_number + "_" + sowage_rate + "_" + area + "_" + exception_option + "_" + arrive_option, "");
            }
            catch (Exception e)
            {
                resultcode = false;
                error_msg = e.Message;
                writeLog("err", "sendUploadData", scan_item + "_" + driver_code + "_" + check_number + "_" + scan_date + "_" + area_arrive_code + "_" + platform + "_" + car_number + "_" + sowage_rate + "_" + area + "_" + exception_option + "_" + arrive_option, e.Message);
            }
            finally
            {
            }


            //將欄位名稱轉成小寫
            if (dtAppRequest != null && dtAppRequest.Rows.Count > 0)
            {
                foreach (DataColumn column in dtAppRequest.Columns)
                {
                    column.ColumnName = column.ColumnName.ToLower();
                }
            }

        }

        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, serverdate = serverdate }, Formatting.Indented);
        return strAppRequest;
    }
    #endregion

    #region 託運單掃讀資料上傳(v2)
    /// <summary>
    /// 託運單掃讀資料上傳(v2)
    /// </summary>
    /// <returns></returns>
    [WebMethod]
    public string sendUploadData2(string scan_item, string driver_code, string check_number, string scan_date, string area_arrive_code, string platform, string car_number, string sowage_rate, string area, string exception_option, string arrive_option, string ship_mode, string goods_type, string pieces, string weight, string runs, string sign_form_image, string sign_field_image)
    {
        writeLog("call", "sendUploadData2", scan_item + "_" + driver_code + "_" + check_number + "_" + scan_date + "_" + area_arrive_code + "_" + platform + "_" + car_number + "_" + sowage_rate + "_" + area + "_" + exception_option + "_" + arrive_option, "");
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dtAppRequest = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;

        if (scan_item.Equals(""))
        {
            resultcode = false;
            error_msg = "作業類別未傳入";
        }
        else if (driver_code.Equals(""))
        {
            resultcode = false;
            error_msg = "司機代碼未傳入";
        }
        else if (check_number.Equals(""))
        {
            resultcode = false;
            error_msg = "託運單號未傳入";

        }
        else if (!(scan_item.Equals("1") || scan_item.Equals("2") || scan_item.Equals("3") || scan_item.Equals("4") || scan_item.Equals("5") || scan_item.Equals("6") || scan_item.Equals("7")))
        {
            resultcode = false;
            error_msg = "作業類別錯誤";
        }
        else if (scan_item.Equals("4") && (sign_form_image.Equals("") || sign_field_image.Equals("")))
        {
            resultcode = false;
            error_msg = "簽單圖檔未傳入";
        }

        else
        {


            try
            {
                //集貨掃讀、缷集、發送
                if (scan_item.Equals("5") || scan_item.Equals("6") || scan_item.Equals("7"))
                {
                    using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                    {
                        StringBuilder sql = new StringBuilder();
                        sql.Append(" insert into ttDeliveryScanLog (driver_code, check_number, scan_item, scan_date, area_arrive_code, platform, car_number, sowage_rate, area, exception_option,pieces,weight,runs,ship_mode,goods_type) values (@driver_code, @check_number, @scan_item, @scan_date, @area_arrive_code, @platform, @car_number, @sowage_rate, @area, @exception_option ,@pieces,@weight,@runs,@ship_mode, @goods_type)");

                        using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                        {
                            conn.Open();
                            command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                            command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                            command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                            command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                            command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;
                            command.Parameters.Add("@platform", SqlDbType.VarChar).Value = platform;
                            command.Parameters.Add("@car_number", SqlDbType.VarChar).Value = car_number;
                            command.Parameters.Add("@sowage_rate", SqlDbType.VarChar).Value = sowage_rate;
                            command.Parameters.Add("@area", SqlDbType.VarChar).Value = area;
                            command.Parameters.Add("@ship_mode", SqlDbType.NVarChar, 2).Value = ship_mode;
                            command.Parameters.Add("@goods_type", SqlDbType.NVarChar, 2).Value = goods_type;
                            command.Parameters.Add("@pieces", SqlDbType.Int).Value = (pieces == "") ? 0 : Convert.ToInt32(pieces);
                            command.Parameters.Add("@weight", SqlDbType.Float).Value = (weight == "") ? 0 : Convert.ToDouble(weight);
                            command.Parameters.Add("@runs", SqlDbType.Int).Value = (runs == "") ? 0 : Convert.ToInt32(runs);
                            command.Parameters.Add("@exception_option", SqlDbType.VarChar).Value = exception_option;

                            SqlDataAdapter da = new SqlDataAdapter(command);
                            da.SelectCommand.CommandTimeout = db_Timeout;
                            da.Fill(dtAppRequest);
                            conn.Close();

                        }
                    }
                }
                //到著掃讀
                else if (scan_item.Equals("1"))
                {

                    using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                    {
                        StringBuilder sql = new StringBuilder();
                        sql.Append(" insert into ttDeliveryScanLog (driver_code, check_number, scan_item, scan_date, area_arrive_code, platform, car_number, sowage_rate, area, exception_option) values (@driver_code, @check_number, @scan_item, @scan_date, @area_arrive_code, @platform, @car_number, @sowage_rate, @area, @exception_option)");

                        using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                        {
                            conn.Open();
                            command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                            command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                            command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                            command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                            command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;
                            command.Parameters.Add("@platform", SqlDbType.VarChar).Value = platform;
                            command.Parameters.Add("@car_number", SqlDbType.VarChar).Value = car_number;
                            command.Parameters.Add("@sowage_rate", SqlDbType.VarChar).Value = sowage_rate;
                            command.Parameters.Add("@area", SqlDbType.VarChar).Value = area;
                            command.Parameters.Add("@exception_option", SqlDbType.VarChar).Value = exception_option;

                            SqlDataAdapter da = new SqlDataAdapter(command);
                            da.SelectCommand.CommandTimeout = db_Timeout;
                            da.Fill(dtAppRequest);
                            conn.Close();

                        }
                    }
                }

                //配送掃讀
                else if (scan_item.Equals("2"))
                {

                    using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                    {
                        StringBuilder sql = new StringBuilder();
                        sql.Append(" insert into ttDeliveryScanLog (driver_code, check_number, scan_item, scan_date, exception_option, area_arrive_code) values (@driver_code, @check_number, @scan_item, @scan_date, @exception_option, @area_arrive_code)");

                        using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                        {
                            conn.Open();
                            command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                            command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                            command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                            command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                            command.Parameters.Add("@exception_option", SqlDbType.VarChar).Value = exception_option;
                            command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;

                            SqlDataAdapter da = new SqlDataAdapter(command);
                            da.SelectCommand.CommandTimeout = db_Timeout;
                            da.Fill(dtAppRequest);
                            conn.Close();

                        }
                    }


                }

                //配達掃讀
                else if (scan_item.Equals("3"))
                {

                    //DateTime now = System.DateTime.Now; //string drawString = time.ToString("yyyy/MM/dd HH:mm");
                    //string upload_time = now.ToString("yyyy/MM/dd HH:mm");

                    //託運單圖檔名
                    //string sign_form_image_name = driver_code + "_" + check_number + "_" + now.ToString("yyyyMMddHHmmss") + ".jpg";
                    //託運單簽名欄位圖檔名
                    //string sign_field_image_name = "sign" + "_" + check_number  + "_" + now.ToString("yyyyMMddHHmmss") + ".jpg";

                    using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                    {
                        StringBuilder sql = new StringBuilder();
                        sql.Append(" insert into ttDeliveryScanLog (driver_code, check_number, scan_item, scan_date, arrive_option, area_arrive_code) values (@driver_code, @check_number, @scan_item, @scan_date, @arrive_option, @area_arrive_code)");

                        using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                        {
                            conn.Open();
                            command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                            command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                            command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                            command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                            command.Parameters.Add("@arrive_option", SqlDbType.VarChar).Value = arrive_option;
                            //command.Parameters.Add("@sign_form_image", SqlDbType.VarChar).Value = sign_form_image_name;
                            //command.Parameters.Add("@sign_field_image", SqlDbType.VarChar).Value = sign_field_image_name;
                            command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;

                            SqlDataAdapter da = new SqlDataAdapter(command);
                            da.SelectCommand.CommandTimeout = db_Timeout;
                            da.Fill(dtAppRequest);
                            conn.Close();
                        }
                    }

                    //寫入圖檔
                    //SaveImgFromBase64(sign_form_image, sign_form_image_name, upload_time);
                    //SaveImgFromBase64(sign_field_image, sign_field_image_name, upload_time);

                }

                //簽單掃讀
                else if (scan_item.Equals("4"))
                {

                    DateTime now = System.DateTime.Now; //string drawString = time.ToString("yyyy/MM/dd HH:mm");
                    string upload_time = now.ToString("yyyy/MM/dd HH:mm");

                    //託運單圖檔名
                    string sign_form_image_name = driver_code + "_" + check_number + "_" + now.ToString("yyyyMMddHHmmss") + ".jpg";
                    //託運單簽名欄位圖檔名
                    string sign_field_image_name = "sign" + "_" + check_number + "_" + now.ToString("yyyyMMddHHmmss") + ".jpg";

                    using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                    {
                        StringBuilder sql = new StringBuilder();
                        sql.Append(" insert into ttDeliveryScanLog (driver_code, check_number, scan_item, scan_date, arrive_option, sign_form_image, sign_field_image, area_arrive_code) values (@driver_code, @check_number, @scan_item, @scan_date, @arrive_option, @sign_form_image, @sign_field_image, @area_arrive_code)");

                        using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                        {
                            conn.Open();
                            command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                            command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                            command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                            command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                            command.Parameters.Add("@arrive_option", SqlDbType.VarChar).Value = arrive_option;
                            command.Parameters.Add("@sign_form_image", SqlDbType.VarChar).Value = sign_form_image_name;
                            command.Parameters.Add("@sign_field_image", SqlDbType.VarChar).Value = sign_field_image_name;
                            command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;

                            SqlDataAdapter da = new SqlDataAdapter(command);
                            da.SelectCommand.CommandTimeout = db_Timeout;
                            da.Fill(dtAppRequest);
                            conn.Close();
                        }
                    }

                    //寫入圖檔
                    SaveImgFromBase64(sign_form_image, sign_form_image_name, upload_time);
                    SaveImgFromBase64(sign_field_image, sign_field_image_name, upload_time);

                }


                writeLog("ref", "sendUploadData2", scan_item + "_" + driver_code + "_" + check_number + "_" + scan_date + "_" + area_arrive_code + "_" + platform + "_" + car_number + "_" + sowage_rate + "_" + area + "_" + exception_option + "_" + arrive_option, "");
            }
            catch (Exception e)
            {
                resultcode = false;
                error_msg = e.Message;
                writeLog("err", "sendUploadData2", scan_item + "_" + driver_code + "_" + check_number + "_" + scan_date + "_" + area_arrive_code + "_" + platform + "_" + car_number + "_" + sowage_rate + "_" + area + "_" + exception_option + "_" + arrive_option, e.Message);
            }
            finally
            {
            }


            //將欄位名稱轉成小寫
            if (dtAppRequest != null && dtAppRequest.Rows.Count > 0)
            {
                foreach (DataColumn column in dtAppRequest.Columns)
                {
                    column.ColumnName = column.ColumnName.ToLower();
                }
            }

        }

        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, serverdate = serverdate }, Formatting.Indented);
        return strAppRequest;
    }
    #endregion

    #region 託運單掃讀資料上傳(v3)
    /// <summary>
    /// 託運單掃讀資料上傳(v3)
    /// </summary>
    /// <returns></returns>
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string sendUploadData3(string scan_item, string driver_code, string check_number, string scan_date, string area_arrive_code, string platform, string car_number, string sowage_rate, string area, string exception_option, string arrive_option, string ship_mode, string goods_type, string pieces, string weight, string runs, string plates, string sign_form_image, string sign_field_image)
    {
        writeLog("call", "sendUploadData3", scan_item + "_" + driver_code + "_" + check_number + "_" + scan_date + "_" + area_arrive_code + "_" + platform + "_" + car_number + "_" + sowage_rate + "_" + area + "_" + exception_option + "_" + arrive_option + "_" + ship_mode + "_" + goods_type + "_" + pieces + "_" + weight + "_" + runs + "_" + plates, "");
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dtAppRequest = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;

        if (scan_item.Equals(""))
        {
            resultcode = false;
            error_msg = "作業類別未傳入";
        }
        else if (driver_code.Equals(""))
        {
            resultcode = false;
            error_msg = "司機代碼未傳入";
        }
        else if (check_number.Equals(""))
        {
            resultcode = false;
            error_msg = "託運單號未傳入";

        }
        else if (!(scan_item.Equals("1") || scan_item.Equals("2") || scan_item.Equals("3") || scan_item.Equals("4") || scan_item.Equals("5") || scan_item.Equals("6") || scan_item.Equals("7")))
        {
            resultcode = false;
            error_msg = "作業類別錯誤";
        }
        else if (scan_item.Equals("4") && (sign_form_image.Equals("") || sign_field_image.Equals("")) && (driver_code.Length > 1 && driver_code.Substring(0, 1) != "Z"))
        {
            resultcode = false;
            error_msg = "簽單圖檔未傳入";
        }

        else
        {
            int _pieces = (String.IsNullOrEmpty(pieces) || !IsNumeric(pieces)) ? 0 : Convert.ToInt32(pieces);
            double _weight = (String.IsNullOrEmpty(weight) || !IsNumeric(weight)) ? 0 : Convert.ToDouble(weight);
            int _runs = (String.IsNullOrEmpty(runs) || !IsNumeric(runs)) ? 0 : Convert.ToInt32(runs);
            int _plates = (String.IsNullOrEmpty(plates) || !IsNumeric(plates)) ? 0 : Convert.ToInt32(plates);


            try
            {
                //集貨掃讀、卸集、發送
                if (scan_item.Equals("5") || scan_item.Equals("6") || scan_item.Equals("7"))
                {
                    using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                    {
                        StringBuilder sql = new StringBuilder();
                        sql.Append(" insert into ttDeliveryScanLog (driver_code, check_number, scan_item, scan_date, area_arrive_code, platform, car_number, sowage_rate, area, exception_option,pieces,weight,runs,ship_mode,goods_type , plates) values (@driver_code, @check_number, @scan_item, @scan_date, @area_arrive_code, @platform, @car_number, @sowage_rate, @area, @exception_option ,@pieces,@weight,@runs,@ship_mode, @goods_type , @plates)");

                        using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                        {
                            conn.Open();
                            command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                            command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                            command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                            command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                            command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;
                            command.Parameters.Add("@platform", SqlDbType.VarChar).Value = platform;
                            command.Parameters.Add("@car_number", SqlDbType.VarChar).Value = car_number;
                            command.Parameters.Add("@sowage_rate", SqlDbType.VarChar).Value = sowage_rate;
                            command.Parameters.Add("@area", SqlDbType.VarChar).Value = area;
                            command.Parameters.Add("@ship_mode", SqlDbType.NVarChar, 2).Value = ship_mode;
                            command.Parameters.Add("@goods_type", SqlDbType.NVarChar, 2).Value = goods_type;
                            command.Parameters.Add("@pieces", SqlDbType.Int).Value = _pieces;
                            command.Parameters.Add("@weight", SqlDbType.Float).Value = _weight;
                            command.Parameters.Add("@runs", SqlDbType.Int).Value = _runs;
                            command.Parameters.Add("@plates", SqlDbType.Int).Value = _plates;
                            command.Parameters.Add("@exception_option", SqlDbType.VarChar).Value = exception_option;


                            SqlDataAdapter da = new SqlDataAdapter(command);
                            da.SelectCommand.CommandTimeout = db_Timeout;
                            da.Fill(dtAppRequest);
                            conn.Close();

                        }
                    }
                }
                //到著掃讀
                else if (scan_item.Equals("1"))
                {

                    using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                    {
                        StringBuilder sql = new StringBuilder();
                        sql.Append(" insert into ttDeliveryScanLog (driver_code, check_number, scan_item, scan_date, area_arrive_code, platform, car_number, sowage_rate, area, exception_option, plates) values (@driver_code, @check_number, @scan_item, @scan_date, @area_arrive_code, @platform, @car_number, @sowage_rate, @area, @exception_option, @plates)");

                        using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                        {
                            conn.Open();
                            command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                            command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                            command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                            command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                            command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;
                            command.Parameters.Add("@platform", SqlDbType.VarChar).Value = platform;
                            command.Parameters.Add("@car_number", SqlDbType.VarChar).Value = car_number;
                            command.Parameters.Add("@sowage_rate", SqlDbType.VarChar).Value = sowage_rate;
                            command.Parameters.Add("@area", SqlDbType.VarChar).Value = area;
                            command.Parameters.Add("@exception_option", SqlDbType.VarChar).Value = exception_option;
                            command.Parameters.Add("@plates", SqlDbType.Int).Value = _plates;

                            SqlDataAdapter da = new SqlDataAdapter(command);
                            da.SelectCommand.CommandTimeout = db_Timeout;
                            da.Fill(dtAppRequest);
                            conn.Close();
                        }
                    }
                }

                //配送掃讀
                else if (scan_item.Equals("2"))
                {

                    using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                    {
                        StringBuilder sql = new StringBuilder();
                        sql.Append(" insert into ttDeliveryScanLog (driver_code, check_number, scan_item, scan_date, exception_option, area_arrive_code, plates  ) values (@driver_code, @check_number, @scan_item, @scan_date, @exception_option, @area_arrive_code , @plates)");

                        using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                        {
                            conn.Open();
                            command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                            command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                            command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                            command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                            command.Parameters.Add("@exception_option", SqlDbType.VarChar).Value = exception_option;
                            command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;
                            command.Parameters.Add("@plates", SqlDbType.Int).Value = _plates;

                            SqlDataAdapter da = new SqlDataAdapter(command);
                            da.SelectCommand.CommandTimeout = db_Timeout;
                            da.Fill(dtAppRequest);
                            conn.Close();

                        }
                    }
                }

                //配達掃讀
                else if (scan_item.Equals("3"))
                {

                    //DateTime now = System.DateTime.Now; //string drawString = time.ToString("yyyy/MM/dd HH:mm");
                    //string upload_time = now.ToString("yyyy/MM/dd HH:mm");

                    //託運單圖檔名
                    //string sign_form_image_name = driver_code + "_" + check_number + "_" + now.ToString("yyyyMMddHHmmss") + ".jpg";
                    //託運單簽名欄位圖檔名
                    //string sign_field_image_name = "sign" + "_" + check_number  + "_" + now.ToString("yyyyMMddHHmmss") + ".jpg";

                    using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                    {
                        StringBuilder sql = new StringBuilder();
                        sql.Append(" insert into ttDeliveryScanLog (driver_code, check_number, scan_item, scan_date, arrive_option, area_arrive_code, plates) values (@driver_code, @check_number, @scan_item, @scan_date, @arrive_option, @area_arrive_code ,@plates)");

                        using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                        {
                            conn.Open();
                            command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                            command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                            command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                            command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                            command.Parameters.Add("@arrive_option", SqlDbType.VarChar).Value = arrive_option;
                            //command.Parameters.Add("@sign_form_image", SqlDbType.VarChar).Value = sign_form_image_name;
                            //command.Parameters.Add("@sign_field_image", SqlDbType.VarChar).Value = sign_field_image_name;
                            command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;
                            command.Parameters.Add("@plates", SqlDbType.Int).Value = _plates;
                            SqlDataAdapter da = new SqlDataAdapter(command);
                            da.SelectCommand.CommandTimeout = db_Timeout;
                            da.Fill(dtAppRequest);
                            conn.Close();
                            #region 原單退回
                            if (arrive_option == "A")
                            {
                                using (SqlCommand cmd = new SqlCommand())
                                {
                                    cmd.Parameters.AddWithValue("@check_number", check_number);
                                    cmd.CommandText = "dbo.uspWeb_ExecReturn";
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    try
                                    {
                                        dbAdapter.execNonQuery(cmd);
                                    }
                                    catch (Exception e)
                                    {
                                        resultcode = false;
                                        error_msg = e.Message;
                                        writeLog("err", "sendUploadData3", scan_item + "_" + driver_code + "_" + check_number + "_" + scan_date + "_" + area_arrive_code + "_" + platform + "_" + car_number + "_" + sowage_rate + "_" + area + "_" + exception_option + "_" + arrive_option + "_" + ship_mode + "_" + goods_type + "_" + pieces + "_" + weight + "_" + runs + "_" + plates, e.Message);
                                    }
                                }
                            }
                            #endregion
                        }
                    }

                    //寫入圖檔
                    //SaveImgFromBase64(sign_form_image, sign_form_image_name, upload_time);
                    //SaveImgFromBase64(sign_field_image, sign_field_image_name, upload_time);

                }

                //簽單掃讀
                else if (scan_item.Equals("4") && (sign_form_image != "" || sign_field_image != ""))
                {

                    DateTime now = System.DateTime.Now; //string drawString = time.ToString("yyyy/MM/dd HH:mm");
                    string upload_time = now.ToString("yyyy/MM/dd HH:mm");

                    //託運單圖檔名
                    string sign_form_image_name = driver_code + "_" + check_number + "_" + now.ToString("yyyyMMddHHmmss") + ".jpg";
                    //託運單簽名欄位圖檔名
                    string sign_field_image_name = "sign" + "_" + check_number + "_" + now.ToString("yyyyMMddHHmmss") + ".jpg";

                    using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                    {
                        StringBuilder sql = new StringBuilder();
                        sql.Append(" insert into ttDeliveryScanLog (driver_code, check_number, scan_item, scan_date, arrive_option, sign_form_image, sign_field_image, area_arrive_code , plates) values (@driver_code, @check_number, @scan_item, @scan_date, @arrive_option, @sign_form_image, @sign_field_image, @area_arrive_code , @plates)");

                        using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                        {
                            conn.Open();
                            command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                            command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                            command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                            command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                            command.Parameters.Add("@arrive_option", SqlDbType.VarChar).Value = arrive_option;
                            command.Parameters.Add("@sign_form_image", SqlDbType.VarChar).Value = sign_form_image_name;
                            command.Parameters.Add("@sign_field_image", SqlDbType.VarChar).Value = sign_field_image_name;
                            command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;
                            command.Parameters.Add("@plates", SqlDbType.Int).Value = _plates;

                            SqlDataAdapter da = new SqlDataAdapter(command);
                            da.SelectCommand.CommandTimeout = db_Timeout;
                            da.Fill(dtAppRequest);
                            conn.Close();
                        }
                    }

                    //寫入圖檔
                    SaveImgFromBase64(sign_form_image, sign_form_image_name, upload_time);
                    SaveImgFromBase64(sign_field_image, sign_field_image_name, upload_time);

                }


                writeLog("ref", "sendUploadData3", scan_item + "_" + driver_code + "_" + check_number + "_" + scan_date + "_" + area_arrive_code + "_" + platform + "_" + car_number + "_" + sowage_rate + "_" + area + "_" + exception_option + "_" + arrive_option + "_" + ship_mode + "_" + goods_type + "_" + pieces + "_" + weight + "_" + runs + "_" + plates, "");
            }
            catch (Exception e)
            {
                resultcode = false;
                error_msg = e.Message;
                writeLog("err", "sendUploadData3", scan_item + "_" + driver_code + "_" + check_number + "_" + scan_date + "_" + area_arrive_code + "_" + platform + "_" + car_number + "_" + sowage_rate + "_" + area + "_" + exception_option + "_" + arrive_option + "_" + ship_mode + "_" + goods_type + "_" + pieces + "_" + weight + "_" + runs + "_" + plates, e.Message);
            }
            finally
            {
            }

            //將欄位名稱轉成小寫
            if (dtAppRequest != null && dtAppRequest.Rows.Count > 0)
            {
                foreach (DataColumn column in dtAppRequest.Columns)
                {
                    column.ColumnName = column.ColumnName.ToLower();
                }
            }

        }

        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, serverdate = serverdate }, Formatting.Indented);
        return strAppRequest;
    }
    #endregion

    #region 託運單掃讀資料上傳(v4)
    /// <summary>
    /// 託運單掃讀資料上傳(v4)
    /// </summary>
    /// <returns></returns>
    [WebMethod]
    public string sendUploadData4(string scan_item, string driver_code, string check_number, string scan_date, string area_arrive_code, string platform, string car_number, string sowage_rate, string area, string exception_option, string arrive_option, string ship_mode, string goods_type, string pieces, string weight, string runs, string plates, string sign_form_image, string sign_field_image, string coupon, string cash, string receive_option = "")
    {
        string UserAgent = this.Context.Request.UserAgent;
        string IP = this.Context.Request.UserHostAddress;
        Guid g = Guid.NewGuid();
        var sessionid = g.ToString();
        writeLog("call", "sendUploadData4", "scan_item:" + scan_item + " driver_code:" + driver_code + " check_number:" + check_number + " scan_date:" + scan_date + " area_arrive_code:" + area_arrive_code + " platform:" + platform + " car_number:" + car_number + " sowage_rate:" + sowage_rate + " area:" + area + " exception_option:" + exception_option + " arrive_option:" + arrive_option + " ship_mode:" + ship_mode + " goods_type:" + goods_type + " pieces:" + pieces + " weight:" + weight + " runs:" + runs + " plates:" + plates + " coupon:" + coupon + " cash:" + cash + " receive_option:" + receive_option, "");
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dtAppRequest = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;

        //預計 2022-07 準備下線 配達 1.0
        bool canUseDelivery1 = true;
        if (scan_item.Equals("3") || scan_item.Equals("4"))
        {
            if (int.Parse(DateTime.Now.ToString("yyyyMMdd")) >= 20220701)
            {
                canUseDelivery1 = false;

                writeLog("setdata", "setDeliveryWhiteUser", "", "");
                setDeliveryWhiteUser();

                for (int i = 0; i < listDeliveryWhiteUser.Count; i++)
                {
                    if (driver_code.ToUpper().Equals(listDeliveryWhiteUser[i].driver_code.ToUpper()))
                    {
                        canUseDelivery1 = true;
                    }
                }
            }
        }

        //
        //指定版號 禁止使用
        bool isBlackVersion = false;
        if (1 == 2)
        {
            if (int.Parse(DateTime.Now.ToString("yyyyMMdd")) >= 20220701)
            {
                isBlackVersion = false;
                setLongTimeNoCheck();

                for (int i = 0; i < listLongTimeNoCheckUser.Count; i++)
                {
                    if (driver_code.ToUpper().Equals(listLongTimeNoCheckUser[i].driver_code.ToUpper()))
                    {
                        isBlackVersion = true;
                    }
                }
            }
        }

        var status = string.Empty;

        if (scan_item.Equals(""))
        {
            resultcode = false;
            error_msg = "作業類別未傳入";
        }
        else if (driver_code.Equals(""))
        {
            resultcode = false;
            error_msg = "司機代碼未傳入";
        }
        else if (check_number.Equals(""))
        {
            resultcode = false;
            error_msg = "託運單號未傳入";

        }
        else if (!(scan_item.Equals("1") || scan_item.Equals("2") || scan_item.Equals("3") || scan_item.Equals("4") || scan_item.Equals("5") || scan_item.Equals("6") || scan_item.Equals("7")))
        {
            resultcode = false;
            error_msg = "作業類別錯誤";
        }
        else if (scan_item.Equals("4") && (sign_form_image.Equals("") || sign_field_image.Equals("")) && (driver_code.Length > 1 && driver_code.Substring(0, 1) != "Z"))
        {
            resultcode = false;
            error_msg = "簽單圖檔未傳入";
        }
        else if (canUseDelivery1 == false && (scan_item.Equals("3") || scan_item.Equals("4")))
        {
            resultcode = false;
            status = "無權限使用";
            error_msg = "無權限使用配達1.0";
        }
        else if (isBlackVersion == true && (scan_item.Equals("5") || scan_item.Equals("3")))
        {
            resultcode = true;
            status = "APP版本太舊或是不明";
            error_msg = "禁止使用!! APP版本太舊或是不明 , 請先登出帳號檢查版本";
            string checkNumber = "--";
            Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter2 = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
            //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
            //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
            timeConverter2.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
            string strAppRequest2 = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, status = status, checkNumber = checkNumber, serverdate = serverdate }, Formatting.Indented);
            return strAppRequest2;

        }
        else
        {
            int _pieces = (String.IsNullOrEmpty(pieces) || !IsNumeric(pieces)) ? 0 : Convert.ToInt32(pieces);
            double _weight = (String.IsNullOrEmpty(weight) || !IsNumeric(weight)) ? 0 : Convert.ToDouble(weight);
            int _runs = (String.IsNullOrEmpty(runs) || !IsNumeric(runs)) ? 0 : Convert.ToInt32(runs);
            int _plates = (String.IsNullOrEmpty(plates) || !IsNumeric(plates)) ? 0 : Convert.ToInt32(plates);
            int _coupon = (String.IsNullOrEmpty(coupon) || !IsNumeric(coupon)) ? 0 : Convert.ToInt32(coupon);
            int _cash = (String.IsNullOrEmpty(cash) || !IsNumeric(cash)) ? 0 : Convert.ToInt32(cash);
            //var status = string.Empty;
            var consignment = GettcDeliveryRequestsBycheck_number(check_number);

            if (IsMOMOCheckNumber(check_number) && scan_item == "5" && consignment == null)
            {
                JubFuAPPWebServiceLog(sessionid, "sendUploadData4", "error_msg", "MOMO集貨;主表無單號");
                //error_msg = "MOMO集貨;主表無單號";
                saveErrScanLog("sendUploadData4", "MOMO集貨;主表無單號", sessionid, scan_item, driver_code, check_number, scan_date, area_arrive_code, platform, car_number, sowage_rate, area, exception_option, arrive_option, ship_mode, goods_type, pieces, weight, runs, plates, sign_form_image, sign_field_image, coupon, cash, receive_option = "");

            }
            else
            {
                if (consignment == null)
                {
                    JubFuAPPWebServiceLog(sessionid, "sendUploadData4", "error_msg", "查無單號");
                    resultcode = true;
                    error_msg = "查不到此筆單號，請與發送站所進行確認。";
                    status = "查無單號";
                    saveErrScanLog("sendUploadData4", error_msg, sessionid, scan_item, driver_code, check_number, scan_date, area_arrive_code, platform, car_number, sowage_rate, area, exception_option, arrive_option, ship_mode, goods_type, pieces, weight, runs, plates, sign_form_image, sign_field_image, coupon, cash, receive_option = "");
                    return JsonConvert.SerializeObject(new { checkNumber = check_number, resultcode = resultcode, resultdesc = error_msg, serverdate = serverdate, status = status }, Formatting.Indented); ;

                }
                if (IsFSEDriver(driver_code) && !scan_item.Equals("4"))
                {

                    if (IsNormalDelivery(consignment))
                    {
                        JubFuAPPWebServiceLog(sessionid, "sendUploadData4", "error_msg", "此筆已正常配達(或已產生退貨單號)");
                        resultcode = true;
                        error_msg = "此筆已正常配達(或已產生退貨單號)";
                        status = "正常配達(或已產生退貨單號)";
                        saveErrScanLog("sendUploadData4", error_msg, sessionid, scan_item, driver_code, check_number, scan_date, area_arrive_code, platform, car_number, sowage_rate, area, exception_option, arrive_option, ship_mode, goods_type, pieces, weight, runs, plates, sign_form_image, sign_field_image, coupon, cash, receive_option = "");
                        return JsonConvert.SerializeObject(new { checkNumber = check_number, resultcode = resultcode, resultdesc = error_msg, serverdate = serverdate, status = status }, Formatting.Indented); ;
                    }
                    else if (IsCancelled(consignment))
                    {
                        JubFuAPPWebServiceLog(sessionid, "sendUploadData4", "error_msg", "此筆已銷單");
                        resultcode = true;
                        error_msg = "此筆已銷單";
                        status = "已銷單";
                        saveErrScanLog("sendUploadData4", error_msg, sessionid, scan_item, driver_code, check_number, scan_date, area_arrive_code, platform, car_number, sowage_rate, area, exception_option, arrive_option, ship_mode, goods_type, pieces, weight, runs, plates, sign_form_image, sign_field_image, coupon, cash, receive_option = "");
                        return JsonConvert.SerializeObject(new { checkNumber = check_number, resultcode = resultcode, resultdesc = error_msg, serverdate = serverdate, status = status }, Formatting.Indented); ;
                    }
                }
            }
            try
            {
                ////集貨掃讀判斷司機站點(全速配)
                //if (scan_item.Equals("5") && IsFSEDriver(driver_code))
                //{
                //    //判斷是否同站所及是否為第一筆集貨貨態
                //    if (!IsSameStation(driver_code, check_number) && !IsFirstPickUpScanLog(check_number))
                //    {
                //        JubFuAPPWebServiceLog(sessionid, "sendUploadData4", "error_msg", "貨單與司機站所不同");
                //        resultcode = true;
                //        error_msg = "貨單與司機站所不同";
                //        status = "貨單與司機站所不同";
                //        #region 存入另一張表
                //        try
                //        {
                //            using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                //            {
                //                StringBuilder sql = new StringBuilder();
                //                sql.Append(" insert into ttDeliveryScanLog_ErrorScanLog (driver_code, check_number, scan_item, scan_date, area_arrive_code, platform, car_number, sowage_rate, area, exception_option,pieces,weight,runs,ship_mode,goods_type , plates, receive_option,error_msg) values (@driver_code, @check_number, @scan_item, @scan_date, @area_arrive_code, @platform, @car_number, @sowage_rate, @area, @exception_option ,@pieces,@weight,@runs,@ship_mode, @goods_type , @plates, @receive_option, @error_msg)");

                //                using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                //                {
                //                    conn.Open();
                //                    command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                //                    command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                //                    command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                //                    command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                //                    command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;
                //                    command.Parameters.Add("@platform", SqlDbType.VarChar).Value = platform;
                //                    command.Parameters.Add("@car_number", SqlDbType.VarChar).Value = car_number;
                //                    command.Parameters.Add("@sowage_rate", SqlDbType.VarChar).Value = sowage_rate;
                //                    command.Parameters.Add("@area", SqlDbType.VarChar).Value = area;
                //                    command.Parameters.Add("@ship_mode", SqlDbType.NVarChar, 2).Value = ship_mode;
                //                    command.Parameters.Add("@goods_type", SqlDbType.NVarChar, 2).Value = goods_type;
                //                    command.Parameters.Add("@pieces", SqlDbType.Int).Value = _pieces;
                //                    command.Parameters.Add("@weight", SqlDbType.Float).Value = _weight;
                //                    command.Parameters.Add("@runs", SqlDbType.Int).Value = _runs;
                //                    command.Parameters.Add("@plates", SqlDbType.Int).Value = _plates;
                //                    command.Parameters.Add("@exception_option", SqlDbType.VarChar).Value = exception_option;
                //                    command.Parameters.Add("@receive_option", SqlDbType.VarChar).Value = receive_option;
                //                    command.Parameters.Add("@error_msg", SqlDbType.VarChar).Value = error_msg;

                //                    command.ExecuteNonQuery();
                //                    conn.Close();
                //                }
                //            }
                //        }
                //        catch (Exception e)
                //        {
                //            JubFuAPPWebServiceLog(sessionid, "sendUploadData4", "Exception Message", e.Message);
                //            JubFuAPPWebServiceLog(sessionid, "sendUploadData4", "Exception StackTrace", e.StackTrace);
                //            JubFuAPPWebServiceLog(sessionid, "sendUploadData4", "Exception Source", e.Source);
                //        }
                //        #endregion
                //        return JsonConvert.SerializeObject(new { checkNumber = check_number, resultcode = resultcode, resultdesc = error_msg, serverdate = serverdate, status = status }, Formatting.Indented); ;
                //    }
                //    else
                //    {
                //        using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                //        {
                //            StringBuilder sql = new StringBuilder();
                //            sql.Append(" insert into ttDeliveryScanLog (driver_code, check_number, scan_item, scan_date, area_arrive_code, platform, car_number, sowage_rate, area, exception_option,pieces,weight,runs,ship_mode,goods_type , plates, receive_option) values (@driver_code, @check_number, @scan_item, @scan_date, @area_arrive_code, @platform, @car_number, @sowage_rate, @area, @exception_option ,@pieces,@weight,@runs,@ship_mode, @goods_type , @plates, @receive_option)");

                //            using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                //            {
                //                conn.Open();
                //                command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                //                command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                //                command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                //                command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                //                command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;
                //                command.Parameters.Add("@platform", SqlDbType.VarChar).Value = platform;
                //                command.Parameters.Add("@car_number", SqlDbType.VarChar).Value = car_number;
                //                command.Parameters.Add("@sowage_rate", SqlDbType.VarChar).Value = sowage_rate;
                //                command.Parameters.Add("@area", SqlDbType.VarChar).Value = area;
                //                command.Parameters.Add("@ship_mode", SqlDbType.NVarChar, 2).Value = ship_mode;
                //                command.Parameters.Add("@goods_type", SqlDbType.NVarChar, 2).Value = goods_type;
                //                command.Parameters.Add("@pieces", SqlDbType.Int).Value = _pieces;
                //                command.Parameters.Add("@weight", SqlDbType.Float).Value = _weight;
                //                command.Parameters.Add("@runs", SqlDbType.Int).Value = _runs;
                //                command.Parameters.Add("@plates", SqlDbType.Int).Value = _plates;
                //                command.Parameters.Add("@exception_option", SqlDbType.VarChar).Value = exception_option;
                //                command.Parameters.Add("@receive_option", SqlDbType.VarChar).Value = receive_option;


                //                SqlDataAdapter da = new SqlDataAdapter(command);
                //                da.SelectCommand.CommandTimeout = db_Timeout;
                //                da.Fill(dtAppRequest);
                //                conn.Close();

                //            }
                //        }
                //    }
                //}

                //集貨掃讀、卸集、發送
                //else if (scan_item.Equals("5") || scan_item.Equals("6") || scan_item.Equals("7"))
                if (scan_item.Equals("5") || scan_item.Equals("6") || scan_item.Equals("7"))
                {
                    using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                    {
                        StringBuilder sql = new StringBuilder();
                        sql.Append(" insert into ttDeliveryScanLog (driver_code, check_number, scan_item, scan_date, area_arrive_code, platform, car_number, sowage_rate, area, exception_option,pieces,weight,runs,ship_mode,goods_type , plates, receive_option) values (@driver_code, @check_number, @scan_item, @scan_date, @area_arrive_code, @platform, @car_number, @sowage_rate, @area, @exception_option ,@pieces,@weight,@runs,@ship_mode, @goods_type , @plates, @receive_option)");

                        using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                        {
                            conn.Open();
                            command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                            command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                            command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                            command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                            command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;
                            command.Parameters.Add("@platform", SqlDbType.VarChar).Value = platform;
                            command.Parameters.Add("@car_number", SqlDbType.VarChar).Value = car_number;
                            command.Parameters.Add("@sowage_rate", SqlDbType.VarChar).Value = sowage_rate;
                            command.Parameters.Add("@area", SqlDbType.VarChar).Value = area;
                            command.Parameters.Add("@ship_mode", SqlDbType.NVarChar, 2).Value = ship_mode;
                            command.Parameters.Add("@goods_type", SqlDbType.NVarChar, 2).Value = goods_type;
                            command.Parameters.Add("@pieces", SqlDbType.Int).Value = _pieces;
                            command.Parameters.Add("@weight", SqlDbType.Float).Value = _weight;
                            command.Parameters.Add("@runs", SqlDbType.Int).Value = _runs;
                            command.Parameters.Add("@plates", SqlDbType.Int).Value = _plates;
                            command.Parameters.Add("@exception_option", SqlDbType.VarChar).Value = exception_option;
                            command.Parameters.Add("@receive_option", SqlDbType.VarChar).Value = receive_option;


                            SqlDataAdapter da = new SqlDataAdapter(command);
                            da.SelectCommand.CommandTimeout = db_Timeout;
                            da.Fill(dtAppRequest);
                            conn.Close();

                        }
                    }
                }
                //到著掃讀
                else if (scan_item.Equals("1"))
                {

                    using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                    {
                        StringBuilder sql = new StringBuilder();
                        sql.Append(" insert into ttDeliveryScanLog (driver_code, check_number, scan_item, scan_date, area_arrive_code, platform, car_number, sowage_rate, area, exception_option, plates) values (@driver_code, @check_number, @scan_item, @scan_date, @area_arrive_code, @platform, @car_number, @sowage_rate, @area, @exception_option, @plates)");

                        using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                        {
                            conn.Open();
                            command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                            command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                            command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                            command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                            command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;
                            command.Parameters.Add("@platform", SqlDbType.VarChar).Value = platform;
                            command.Parameters.Add("@car_number", SqlDbType.VarChar).Value = car_number;
                            command.Parameters.Add("@sowage_rate", SqlDbType.VarChar).Value = sowage_rate;
                            command.Parameters.Add("@area", SqlDbType.VarChar).Value = area;
                            command.Parameters.Add("@exception_option", SqlDbType.VarChar).Value = exception_option;
                            command.Parameters.Add("@plates", SqlDbType.Int).Value = _plates;

                            SqlDataAdapter da = new SqlDataAdapter(command);
                            da.SelectCommand.CommandTimeout = db_Timeout;
                            da.Fill(dtAppRequest);
                            conn.Close();
                        }
                    }
                }

                //配送掃讀
                else if (scan_item.Equals("2"))
                {
                    using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                    {
                        StringBuilder sql = new StringBuilder();
                        sql.Append(" insert into ttDeliveryScanLog (driver_code, check_number, scan_item, scan_date, exception_option, area_arrive_code, plates  ) values (@driver_code, @check_number, @scan_item, @scan_date, @exception_option, @area_arrive_code , @plates)");

                        using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                        {
                            conn.Open();
                            command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                            command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                            command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                            command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                            command.Parameters.Add("@exception_option", SqlDbType.VarChar).Value = exception_option;
                            command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;
                            command.Parameters.Add("@plates", SqlDbType.Int).Value = _plates;

                            SqlDataAdapter da = new SqlDataAdapter(command);
                            da.SelectCommand.CommandTimeout = db_Timeout;
                            da.Fill(dtAppRequest);
                            conn.Close();
                        }
                    }
                }

                //配達掃讀
                else if (scan_item.Equals("3"))
                {

                    //DateTime now = System.DateTime.Now; //string drawString = time.ToString("yyyy/MM/dd HH:mm");
                    //string upload_time = now.ToString("yyyy/MM/dd HH:mm");

                    //託運單圖檔名
                    //string sign_form_image_name = driver_code + "_" + check_number + "_" + now.ToString("yyyyMMddHHmmss") + ".jpg";
                    //託運單簽名欄位圖檔名
                    //string sign_field_image_name = "sign" + "_" + check_number  + "_" + now.ToString("yyyyMMddHHmmss") + ".jpg";

                    if (IsFSEDriver(driver_code) && !IsLatestScanItem(check_number, "2"))
                    {
                        JubFuAPPWebServiceLog(sessionid, "sendUploadData4", "error_msg", "此票尚未配送，無法完成配達");
                        resultcode = true;
                        error_msg = "此票尚未配送，無法完成配達";
                        status = "尚未配送";
                        saveErrScanLog("sendUploadData4", error_msg, sessionid, scan_item, driver_code, check_number, scan_date, area_arrive_code, platform, car_number, sowage_rate, area, exception_option, arrive_option, ship_mode, goods_type, pieces, weight, runs, plates, sign_form_image, sign_field_image, coupon, cash, receive_option = "");
                        return JsonConvert.SerializeObject(new { checkNumber = check_number, resultcode = resultcode, resultdesc = error_msg, serverdate = serverdate, status = status }, Formatting.Indented); ;
                    }

                    using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                    {
                        StringBuilder sql = new StringBuilder();
                        sql.Append(" insert into ttDeliveryScanLog (driver_code, check_number, scan_item, scan_date, arrive_option, area_arrive_code, plates, VoucherMoney, CashMoney) values (@driver_code, @check_number, @scan_item, @scan_date, @arrive_option, @area_arrive_code ,@plates,@VoucherMoney, @CashMoney)");

                        using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                        {
                            conn.Open();
                            command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                            command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                            command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                            command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                            command.Parameters.Add("@arrive_option", SqlDbType.VarChar).Value = arrive_option;
                            //command.Parameters.Add("@sign_form_image", SqlDbType.VarChar).Value = sign_form_image_name;
                            //command.Parameters.Add("@sign_field_image", SqlDbType.VarChar).Value = sign_field_image_name;
                            command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;
                            command.Parameters.Add("@plates", SqlDbType.Int).Value = _plates;
                            command.Parameters.Add("@VoucherMoney", SqlDbType.Int).Value = _coupon;  //振興券金額
                            command.Parameters.Add("@CashMoney", SqlDbType.Int).Value = _cash;       //現金金額
                            SqlDataAdapter da = new SqlDataAdapter(command);
                            da.SelectCommand.CommandTimeout = db_Timeout;
                            da.Fill(dtAppRequest);
                            conn.Close();
                            #region 原單退回
                            if (arrive_option == "c" || arrive_option == "C")
                            {
                                using (SqlCommand cmd = new SqlCommand())
                                {
                                    cmd.Parameters.AddWithValue("@VoucherMoney", _coupon);
                                    cmd.Parameters.AddWithValue("@CashMoney", _cash);
                                    cmd.Parameters.AddWithValue("@check_number", check_number);
                                    cmd.CommandText = @" update tcDeliveryRequests set VoucherMoney=@VoucherMoney ,CashMoney=@CashMoney where check_number =@check_number and print_date >= DATEADD (MONTH,-6 ,getdate())";
                                    try
                                    {
                                        dbAdapter.execNonQuery(cmd);
                                    }
                                    catch (Exception e)
                                    {
                                        resultcode = false;
                                        error_msg = e.Message;
                                        writeLog("err", "sendUploadData4", "scan_item:" + scan_item + " driver_code:" + driver_code + " check_number:" + check_number + " scan_date:" + scan_date + " area_arrive_code:" + area_arrive_code + " platform:" + platform + " car_number:" + car_number + " sowage_rate:" + sowage_rate + " area:" + area + " exception_option:" + exception_option + " arrive_option:" + arrive_option + " ship_mode:" + ship_mode + " goods_type:" + goods_type + " pieces:" + pieces + " weight:" + weight + " runs:" + runs + " plates:" + plates + " coupon:" + coupon + " cash:" + cash, e.Message);
                                    }
                                }
                            }
                            else if (arrive_option == "A")
                            {
                                using (SqlCommand cmd = new SqlCommand())
                                {
                                    cmd.Parameters.AddWithValue("@check_number", check_number);
                                    cmd.CommandText = "dbo.uspWeb_ExecReturn";
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    try
                                    {
                                        dbAdapter.execNonQuery(cmd);
                                    }
                                    catch (Exception e)
                                    {
                                        resultcode = false;
                                        error_msg = e.Message;
                                        writeLog("err", "sendUploadData4", "scan_item:" + scan_item + " driver_code:" + driver_code + " check_number:" + check_number + " scan_date:" + scan_date + " area_arrive_code:" + area_arrive_code + " platform:" + platform + " car_number:" + car_number + " sowage_rate:" + sowage_rate + " area:" + area + " exception_option:" + exception_option + " arrive_option:" + arrive_option + " ship_mode:" + ship_mode + " goods_type:" + goods_type + " pieces:" + pieces + " weight:" + weight + " runs:" + runs + " plates:" + plates + " coupon:" + coupon + " cash:" + cash, e.Message);
                                    }
                                }
                            }

                            #endregion
                        }
                    }

                    //寫入圖檔
                    //SaveImgFromBase64(sign_form_image, sign_form_image_name, upload_time);
                    //SaveImgFromBase64(sign_field_image, sign_field_image_name, upload_time);

                }

                //簽單掃讀
                else if (scan_item.Equals("4") && (sign_form_image != "" || sign_field_image != ""))
                {

                    DateTime now = System.DateTime.Now; //string drawString = time.ToString("yyyy/MM/dd HH:mm");
                    string upload_time = now.ToString("yyyy/MM/dd HH:mm");

                    //託運單圖檔名
                    string sign_form_image_name = driver_code + "_" + check_number + "_" + now.ToString("yyyyMMddHHmmss") + ".jpg";
                    //託運單簽名欄位圖檔名
                    string sign_field_image_name = "sign" + "_" + check_number + "_" + now.ToString("yyyyMMddHHmmss") + ".jpg";

                    using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                    {
                        StringBuilder sql = new StringBuilder();
                        sql.Append(" insert into ttDeliveryScanLog (driver_code, check_number, scan_item, scan_date, arrive_option, sign_form_image, sign_field_image, area_arrive_code , plates) values (@driver_code, @check_number, @scan_item, @scan_date, @arrive_option, @sign_form_image, @sign_field_image, @area_arrive_code , @plates)");

                        using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                        {
                            conn.Open();
                            command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                            command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                            command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                            command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                            command.Parameters.Add("@arrive_option", SqlDbType.VarChar).Value = arrive_option;
                            command.Parameters.Add("@sign_form_image", SqlDbType.VarChar).Value = sign_form_image_name;
                            command.Parameters.Add("@sign_field_image", SqlDbType.VarChar).Value = sign_field_image_name;
                            command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;
                            command.Parameters.Add("@plates", SqlDbType.Int).Value = _plates;

                            SqlDataAdapter da = new SqlDataAdapter(command);
                            da.SelectCommand.CommandTimeout = db_Timeout;
                            da.Fill(dtAppRequest);
                            conn.Close();
                        }
                    }

                    //寫入圖檔
                    SaveImgFromBase64(sign_form_image, sign_form_image_name, upload_time);
                    SaveImgFromBase64(sign_field_image, sign_field_image_name, upload_time);

                }


                writeLog("ref", "sendUploadData4", "scan_item:" + scan_item + " driver_code:" + driver_code + " check_number:" + check_number + " scan_date:" + scan_date + " area_arrive_code:" + area_arrive_code + " platform:" + platform + " car_number:" + car_number + " sowage_rate:" + sowage_rate + " area:" + area + " exception_option:" + exception_option + " arrive_option:" + arrive_option + " ship_mode:" + ship_mode + " goods_type:" + goods_type + " pieces:" + pieces + " weight:" + weight + " runs:" + runs + " plates:" + plates + " coupon:" + coupon + " cash:" + cash, "");
            }
            catch (Exception e)
            {
                resultcode = false;
                error_msg = e.Message;
                writeLog("err", "sendUploadData4", "scan_item:" + scan_item + " driver_code:" + driver_code + " check_number:" + check_number + " scan_date:" + scan_date + " area_arrive_code:" + area_arrive_code + " platform:" + platform + " car_number:" + car_number + " sowage_rate:" + sowage_rate + " area:" + area + " exception_option:" + exception_option + " arrive_option:" + arrive_option + " ship_mode:" + ship_mode + " goods_type:" + goods_type + " pieces:" + pieces + " weight:" + weight + " runs:" + runs + " plates:" + plates + " coupon:" + coupon + " cash:" + cash, e.Message);
            }
            finally
            {
            }

            //將欄位名稱轉成小寫
            if (dtAppRequest != null && dtAppRequest.Rows.Count > 0)
            {
                foreach (DataColumn column in dtAppRequest.Columns)
                {
                    column.ColumnName = column.ColumnName.ToLower();
                }
            }
        }

        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, serverdate = serverdate }, Formatting.Indented);
        return strAppRequest;
    }
    #endregion

    #region 託運單掃讀資料上傳(v5)
    /// <summary>
    /// 託運單掃讀資料上傳(v5)
    /// </summary>
    /// <returns></returns>
    [WebMethod]
    public string sendUploadData5(string scan_item, string driver_code, string request_id, string check_number, string scan_date, string area_arrive_code, string platform, string car_number, string sowage_rate, string area, string exception_option, string arrive_option, string ship_mode, string goods_type, string pieces, string weight, string runs, string plates, string sign_field_image, string coupon, string cash, int signType, string receive_option = "")
    {
        string UserAgent = this.Context.Request.UserAgent;
        string IP = this.Context.Request.UserHostAddress;
        Guid g = Guid.NewGuid();
        var sessionid = g.ToString();
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dtAppRequest = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;

        //託運單圖檔名
        string sign_form_image_name = string.Empty;
        //託運單簽名欄位圖檔名
        string sign_field_image_name = string.Empty;


        //託運單圖檔名
        string sign_form_image_url = string.Empty;
        //託運單簽名欄位圖檔名
        string sign_field_image_url = string.Empty;

        //指定版號 禁止使用
        bool isBlackVersion = false;
        if (1 == 2)
        {
            if (int.Parse(DateTime.Now.ToString("yyyyMMdd")) >= 20220701)
            {
                isBlackVersion = false;
                setLongTimeNoCheck();

                for (int i = 0; i < listLongTimeNoCheckUser.Count; i++)
                {
                    if (driver_code.ToUpper().Equals(listLongTimeNoCheckUser[i].driver_code.ToUpper()))
                    {
                        isBlackVersion = true;
                    }
                }
            }
        }

        var status = string.Empty;
        try
        {
            JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "Start", "OK");
            JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "UserAgent", UserAgent);
            JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "IP", IP);
            string parameter = JsonConvert.SerializeObject(new { scan_item, driver_code, request_id, check_number, scan_date, area_arrive_code, platform, car_number, sowage_rate, area, exception_option, arrive_option, ship_mode, goods_type, pieces, weight, runs, plates, sign_field_image, coupon, cash, receive_option });
            JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "parameter", parameter);

            if (scan_item.Equals(""))
            {
                resultcode = false;
                error_msg = "作業類別未傳入";
                JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "error_msg", "作業類別未傳入");
                return JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg }, Formatting.Indented);

            }
            else if (driver_code.Equals(""))
            {
                resultcode = false;
                error_msg = "司機代碼未傳入";
                JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "error_msg", "司機代碼未傳入");
                return JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg }, Formatting.Indented);
            }
            else if (check_number.Equals(""))
            {
                resultcode = false;
                error_msg = "託運單號未傳入";
                JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "error_msg", "託運單號未傳入");
                return JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg }, Formatting.Indented);
            }
            else if (!(scan_item.Equals("3") || scan_item.Equals("4")))
            {
                resultcode = false;
                error_msg = "作業類別錯誤";
                JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "error_msg", "作業類別錯誤");
                return JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg }, Formatting.Indented);
            }
            else if (scan_item.Equals("4") && (sign_field_image.Equals("")) && (driver_code.Length > 1 && driver_code.Substring(0, 1) != "Z"))
            {
                resultcode = false;
                error_msg = "簽單圖檔未傳入";
                JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "error_msg", "簽單圖檔未傳入");
                return JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg }, Formatting.Indented);
            }
            else if (isBlackVersion == true)
            {
                resultcode = true;
                error_msg = "禁止使用!! APP版本太舊或是不明 , 請先登出帳號檢查版本";
                status = "APP版本太舊或是不明";
                JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "error_msg", error_msg);
                return JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, status = status, checkNumber = "--" }, Formatting.Indented);
            }
            else
            {
                int _pieces = (String.IsNullOrEmpty(pieces) || !IsNumeric(pieces)) ? 0 : Convert.ToInt32(pieces);
                double _weight = (String.IsNullOrEmpty(weight) || !IsNumeric(weight)) ? 0 : Convert.ToDouble(weight);
                int _runs = (String.IsNullOrEmpty(runs) || !IsNumeric(runs)) ? 0 : Convert.ToInt32(runs);
                int _plates = (String.IsNullOrEmpty(plates) || !IsNumeric(plates)) ? 0 : Convert.ToInt32(plates);
                int _coupon = (String.IsNullOrEmpty(coupon) || !IsNumeric(coupon)) ? 0 : Convert.ToInt32(coupon);
                int _cash = (String.IsNullOrEmpty(cash) || !IsNumeric(cash)) ? 0 : Convert.ToInt32(cash);
                //var status = string.Empty;

                var consignment = GettcDeliveryRequestsBycheck_number(check_number);

                //配達掃讀
                if (scan_item.Equals("3"))
                {
                    if (consignment == null)
                    {
                        JubFuAPPWebServiceLog(sessionid, "sendUploadData4", "error_msg", "查無單號");
                        resultcode = true;
                        error_msg = "查不到此筆單號，請與發送站所進行確認。";
                        status = "查無單號";
                        saveErrScanLog("sendUploadData5", error_msg, sessionid, scan_item, driver_code, check_number, scan_date, area_arrive_code, platform, car_number, sowage_rate, area, exception_option, arrive_option, ship_mode, goods_type, pieces, weight, runs, plates, "", "", coupon, cash, receive_option = "");
                        return JsonConvert.SerializeObject(new { checkNumber = check_number, resultcode = resultcode, resultdesc = error_msg, serverdate = serverdate, status = status }, Formatting.Indented); ;
                    }
                    if (IsNormalDelivery(consignment))
                    {
                        JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "error_msg", "此筆已正常配達(或已產生退貨單號)");
                        resultcode = true;
                        error_msg = "此筆已正常配達(或已產生退貨單號)";
                        status = "正常配達(或已產生退貨單號)";
                        saveErrScanLog("sendUploadData5", error_msg, sessionid, scan_item, driver_code, check_number, scan_date, area_arrive_code, platform, car_number, sowage_rate, area, exception_option, arrive_option, ship_mode, goods_type, pieces, weight, runs, plates, "", "", coupon, cash, receive_option = "");
                        return JsonConvert.SerializeObject(new { checkNumber = check_number, resultcode = resultcode, resultdesc = error_msg, serverdate = serverdate, status = status }, Formatting.Indented); ;
                    }
                    else if (IsCancelled(consignment))
                    {
                        JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "error_msg", "此筆已銷單");
                        resultcode = true;
                        error_msg = "此筆已銷單";
                        status = "已銷單";
                        saveErrScanLog("sendUploadData5", error_msg, sessionid, scan_item, driver_code, check_number, scan_date, area_arrive_code, platform, car_number, sowage_rate, area, exception_option, arrive_option, ship_mode, goods_type, pieces, weight, runs, plates, "", "", coupon, cash, receive_option = "");
                        return JsonConvert.SerializeObject(new { checkNumber = check_number, resultcode = resultcode, resultdesc = error_msg, serverdate = serverdate, status = status }, Formatting.Indented); ;
                    }
                    else if (!IsLatestScanItem(check_number, "2"))
                    {
                        JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "error_msg", "此票尚未配送，無法完成配達");
                        resultcode = true;
                        error_msg = "此票尚未配送，無法完成配達";
                        status = "尚未配送";
                        saveErrScanLog("sendUploadData5", error_msg, sessionid, scan_item, driver_code, check_number, scan_date, area_arrive_code, platform, car_number, sowage_rate, area, exception_option, arrive_option, ship_mode, goods_type, pieces, weight, runs, plates, "", "", coupon, cash, receive_option = "");
                        return JsonConvert.SerializeObject(new { checkNumber = check_number, resultcode = resultcode, resultdesc = error_msg, serverdate = serverdate, status = status }, Formatting.Indented); ;
                    }


                    //原本是 2022-06-30 要上線的配異簡訊
                    //現在突然不上線了
                    //突然又要上線了
                    if (int.Parse(DateTime.Now.ToString("yyyyMMdd")) >= 20220701 || driver_code.ToString().ToUpper() == "Z010006")
                    {
                        string sms_log_type = "sendUploadData5-SMS";
                        string sms_log_action = "check_number:" + check_number
                                                        + " ; scan_item:" + scan_item + " ; arrive_option:" + arrive_option
                                                        + " ; driver_code:" + driver_code;
                        string sms_log_msg = "";

                        string smsText = "";
                        //準備發送配異簡訊
                        if (scan_item.Equals("3"))
                        {
                            arrive_option = arrive_option.Trim();

                            if (arrive_option != "3" && arrive_option != "")
                            {
                                bool IsEverSendSms = CheckIsEverSendSmsByCheckNumber(check_number, 0);

                                if (IsEverSendSms == false)
                                {
                                    string mobile = GetCheckNumberMobile(check_number);

                                    if (mobile != "")
                                    {
                                        //2022-11-09 緊急進版
                                        //只有 1 客戶不在 / 26 聯絡不上 需要發簡訊
                                        if (arrive_option == "1" || arrive_option == "26")
                                        {
                                            sms_log_msg = "配異發送簡訊成功 ; mobile:" + mobile;
                                            smsText = SendSms(check_number, mobile, scan_item, arrive_option, driver_code);
                                            sms_log_msg += " ; " + smsText;
                                        }
                                        else
                                        {
                                            sms_log_msg = "配異發送簡訊失敗 ; Error:指定不發的arrive_option-" + arrive_option;
                                        }
                                    }
                                    else
                                    {
                                        sms_log_msg = "配異發送簡訊失敗 ; Error:配達收件人沒有提供手機";
                                    }
                                }
                                else
                                {
                                    sms_log_msg = "配異發送簡訊失敗 ; Error:同一check_number今日已發送過簡訊";
                                }
                            }
                            else
                            {
                                sms_log_msg = "配異發送簡訊失敗 ; Error:正常配達不用發送簡訊";
                            }
                        }

                        //萬一沒發簡訊 不用紀錄LOG
                        if (smsText != "")
                        {
                            JubFuAPPWebServiceLog_SMS(sessionid, sms_log_type, check_number, sms_log_action, sms_log_msg);
                        }
                    }




                    using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                    {
                        StringBuilder sql = new StringBuilder();
                        sql.Append(" insert into ttDeliveryScanLog (driver_code, check_number, scan_item, scan_date, arrive_option, area_arrive_code, plates, VoucherMoney, CashMoney) values (@driver_code, @check_number, @scan_item, @scan_date, @arrive_option, @area_arrive_code ,@plates,@VoucherMoney, @CashMoney)");

                        using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                        {
                            string DeliveryRequestsparameter = JsonConvert.SerializeObject(new { driver_code, check_number, scan_item, scan_date, arrive_option, area_arrive_code, _plates, _coupon, _cash });
                            JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "InsterttDeliveryScanLogsparameter_3", DeliveryRequestsparameter);


                            conn.Open();
                            command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                            command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                            command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                            command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                            command.Parameters.Add("@arrive_option", SqlDbType.VarChar).Value = arrive_option;
                            command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;
                            command.Parameters.Add("@plates", SqlDbType.Int).Value = _plates;
                            command.Parameters.Add("@VoucherMoney", SqlDbType.Int).Value = _coupon;  //振興券金額
                            command.Parameters.Add("@CashMoney", SqlDbType.Int).Value = _cash;       //現金金額



                            SqlDataAdapter da = new SqlDataAdapter(command);
                            da.SelectCommand.CommandTimeout = db_Timeout;
                            da.Fill(dtAppRequest);
                            conn.Close();

                            JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "InsterttDeliveryScanLogsparameter_3", "End");


                            #region 原單退回
                            if (arrive_option.ToUpper() == "C")
                            {
                                using (SqlCommand cmd = new SqlCommand())
                                {
                                    cmd.Parameters.AddWithValue("@VoucherMoney", _coupon);
                                    cmd.Parameters.AddWithValue("@CashMoney", _cash);
                                    cmd.Parameters.AddWithValue("@check_number", check_number);
                                    cmd.CommandText = @" update tcDeliveryRequests set VoucherMoney=@VoucherMoney ,CashMoney=@CashMoney where check_number =@check_number and print_date >= DATEADD (MONTH,-6 ,getdate())";
                                    try
                                    {
                                        string updateDeliveryRequestsparameter = JsonConvert.SerializeObject(new { _coupon, _cash, check_number });
                                        JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "updatetcDeliveryRequestsparameter arrive_option=C", updateDeliveryRequestsparameter);
                                        dbAdapter.execNonQuery(cmd);
                                        JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "updatetcDeliveryRequestssparameter arrive_option=C", "End");
                                    }
                                    catch (Exception e)
                                    {
                                        resultcode = false;
                                        error_msg = e.Message;
                                        JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "error_msg", e.Message);
                                        return JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg }, Formatting.Indented);
                                    }
                                }
                            }
                            else if (arrive_option.ToUpper() == "A")
                            {
                                using (SqlCommand cmd = new SqlCommand())
                                {
                                    cmd.Parameters.AddWithValue("@check_number", check_number);
                                    cmd.CommandText = "dbo.uspWeb_ExecReturn";
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    try
                                    {
                                        JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "updateDeliveryRequestsparameter arrive_option=A", "dbo.uspWeb_ExecReturn  " + check_number);
                                        dbAdapter.execNonQuery(cmd);
                                        JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "updateDeliveryRequestsparameter arrive_option=A", "End");
                                    }
                                    catch (Exception e)
                                    {
                                        resultcode = false;
                                        error_msg = e.Message;
                                        JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "error_msg", e.Message);
                                        return JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg }, Formatting.Indented);
                                    }
                                }
                            }

                            #endregion
                        }
                    }

                }

                //簽單掃讀
                else if (scan_item.Equals("4") && (sign_field_image != ""))
                {

                    //託運單圖檔名
                    sign_form_image_name = driver_code + "_" + check_number + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".jpg";
                    //託運單簽名欄位圖檔名
                    sign_field_image_name = "sign" + "_" + check_number + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".png";


                    //託運單圖檔名
                    sign_form_image_url = ImageUrl + sign_form_image_name;
                    //託運單簽名欄位圖檔名
                    sign_field_image_url = ImageUrl + sign_field_image_name;

                    DateTime now = System.DateTime.Now; //string drawString = time.ToString("yyyy/MM/dd HH:mm");
                    string upload_time = now.ToString("yyyy/MM/dd HH:mm");


                    switch (signType)
                    {
                        case 1:
                        case 2:

                            JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "sign_field_image", sign_field_image);
                            //寫入簽名圖檔
                            // SaveImgFromBase64(sign_field_image, sign_field_image_name, upload_time);

                            Base64ToPNG(sign_field_image, sign_field_image_name);
                            JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "sign_field_image", "End");

                            //更新簽名檔
                            using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                            {
                                StringBuilder sql = new StringBuilder();
                                sql.Append(" insert into ttDeliveryScanLog (driver_code, check_number, scan_item, scan_date, arrive_option, sign_field_image, area_arrive_code , plates) values (@driver_code, @check_number, @scan_item, @scan_date, @arrive_option, @sign_field_image, @area_arrive_code , @plates)");

                                using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                                {
                                    string updateDeliveryRequestsparameter = JsonConvert.SerializeObject(new { driver_code, check_number, scan_item, scan_date, arrive_option, sign_field_image_name, area_arrive_code, _plates });
                                    JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "InsterttDeliveryScanLogsparameter_4", updateDeliveryRequestsparameter);
                                    conn.Open();
                                    command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                                    command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                                    command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                                    command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                                    command.Parameters.Add("@arrive_option", SqlDbType.VarChar).Value = arrive_option;
                                    // command.Parameters.Add("@sign_form_image", SqlDbType.VarChar).Value = sign_form_image_name;
                                    command.Parameters.Add("@sign_field_image", SqlDbType.VarChar).Value = sign_field_image_name;
                                    command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;
                                    command.Parameters.Add("@plates", SqlDbType.Int).Value = _plates;

                                    SqlDataAdapter da = new SqlDataAdapter(command);
                                    da.SelectCommand.CommandTimeout = db_Timeout;
                                    da.Fill(dtAppRequest);
                                    conn.Close();


                                    JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "InsterttDeliveryScanLogsparameter_4", "End");
                                }

                            }


                            JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "sign_form_image", sign_form_image_name);
                            var URL = GetSimpleReportUrl("NonPaperDeliverySignReceipt");


                            try
                            {
                                var stream = PostInner(URL, "request_ids=" + request_id + "&sign_field_image_url=" + sign_field_image_url);
                                using (Bitmap bitmap = new Bitmap(stream))
                                {

                                    if (bitmap != null)
                                    {
                                        bitmap.Save(HttpContext.Current.Server.MapPath("~/PHOTO/" + sign_form_image_name), ImageFormat.Jpeg);
                                    }
                                    stream.Flush();
                                    stream.Close();
                                }
                            }
                            catch (Exception e)
                            {
                                sign_form_image_name = "";
                                JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "sign_form_image", "Error：" + e.Message);
                            }

                            JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "sign_form_image", "End");

                            //簽單存入SignImage表
                            JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "InstertSignImageParameter", "Start");
                            StringBuilder sql2 = new StringBuilder();
                            sql2.Append(@" INSERT INTO SignImage (DriverCode, CheckNumber, SignType, SignFormImage, SignFieldImage, cdate) 
                                VALUES  (@driver_code, @check_number, @signType, @sign_form_image, @sign_field_image, GETDATE() )");
                            using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                            {
                                using (SqlCommand command = new SqlCommand(sql2.ToString(), conn))
                                {
                                    string updateDeliveryRequestsparameter = JsonConvert.SerializeObject(new { driver_code, check_number, scan_item, signType, sign_form_image_name, sign_field_image_name });
                                    JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "InstertSignImageParameter", updateDeliveryRequestsparameter);
                                    conn.Open();
                                    command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                                    command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                                    command.Parameters.Add("@signType", SqlDbType.VarChar).Value = signType;
                                    command.Parameters.Add("@sign_form_image", SqlDbType.VarChar).Value = sign_form_image_name;
                                    command.Parameters.Add("@sign_field_image", SqlDbType.VarChar).Value = sign_field_image_name;

                                    SqlDataAdapter da = new SqlDataAdapter(command);
                                    da.SelectCommand.CommandTimeout = db_Timeout;
                                    da.Fill(dtAppRequest);
                                    conn.Close();


                                    JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "InstertSignImageParameter", "End");
                                }
                            }
                            //更新整張託運單圖
                            JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "Updatetcsign_form_image", "Strat");
                            UpdatetcttDeliveryScanLogsign_form_imageBYsign_field_image(check_number, sign_field_image_name, sign_form_image_name);
                            JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "Updatetcsign_form_image", "End");
                            break;
                        case 3:
                            JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "sign_field_image", sign_field_image);
                            //寫入簽名圖檔
                            SaveImgFromBase64(sign_field_image, sign_field_image_name, upload_time);
                            SaveImgFromBase64(sign_field_image, sign_form_image_name, upload_time);
                            //Base64ToPNG(sign_field_image, sign_field_image_name);
                            JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "sign_field_image", "End");

                            //更新簽名檔
                            using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                            {
                                StringBuilder sql = new StringBuilder();
                                sql.Append(" insert into ttDeliveryScanLog (driver_code, check_number, scan_item, scan_date, arrive_option, sign_field_image,sign_form_image, area_arrive_code , plates) values (@driver_code, @check_number, @scan_item, @scan_date, @arrive_option, @sign_field_image,@sign_form_image,@area_arrive_code , @plates)");

                                using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                                {
                                    string updateDeliveryRequestsparameter = JsonConvert.SerializeObject(new { driver_code, check_number, scan_item, scan_date, arrive_option, sign_field_image_name, area_arrive_code, _plates });
                                    JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "InsterttDeliveryScanLogsparameter_4", updateDeliveryRequestsparameter);
                                    conn.Open();
                                    command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                                    command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                                    command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                                    command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                                    command.Parameters.Add("@arrive_option", SqlDbType.VarChar).Value = arrive_option;
                                    command.Parameters.Add("@sign_form_image", SqlDbType.VarChar).Value = sign_form_image_name;//只會有一張完整所以都填一樣
                                    command.Parameters.Add("@sign_field_image", SqlDbType.VarChar).Value = sign_field_image_name;
                                    command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;
                                    command.Parameters.Add("@plates", SqlDbType.Int).Value = _plates;

                                    SqlDataAdapter da = new SqlDataAdapter(command);
                                    da.SelectCommand.CommandTimeout = db_Timeout;
                                    da.Fill(dtAppRequest);
                                    conn.Close();


                                    JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "InsterttDeliveryScanLogsparameter_4", "End");
                                }

                            }
                            //簽單存入SignImage表
                            JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "InstertSignImageParameter", "Start");
                            StringBuilder sql3 = new StringBuilder();
                            sql3.Append(@" INSERT INTO SignImage (DriverCode, CheckNumber, SignType, SignFormImage, SignFieldImage, cdate) 
                                VALUES  (@driver_code, @check_number, @signType, @sign_form_image, @sign_field_image, GETDATE() )");
                            using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                            {
                                using (SqlCommand command = new SqlCommand(sql3.ToString(), conn))
                                {
                                    string updateDeliveryRequestsparameter = JsonConvert.SerializeObject(new { driver_code, check_number, scan_item, signType, sign_form_image_name, sign_field_image_name });
                                    JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "InstertSignImageParameter", updateDeliveryRequestsparameter);
                                    conn.Open();
                                    command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                                    command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                                    command.Parameters.Add("@signType", SqlDbType.VarChar).Value = signType;
                                    command.Parameters.Add("@sign_form_image", SqlDbType.VarChar).Value = sign_form_image_name;
                                    command.Parameters.Add("@sign_field_image", SqlDbType.VarChar).Value = sign_field_image_name;

                                    SqlDataAdapter da = new SqlDataAdapter(command);
                                    da.SelectCommand.CommandTimeout = db_Timeout;
                                    da.Fill(dtAppRequest);
                                    conn.Close();
                                    JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "InstertSignImageParameter", "End");
                                }
                            }

                            break;
                        default:
                            return JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = "系統異常", serverdate = serverdate, sign_form_image_url, sign_field_image_url }, Formatting.Indented);


                    }


                }
            }

            Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
            //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
            //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
            timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
            string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, serverdate = serverdate, sign_form_image_url, sign_field_image_url }, Formatting.Indented);

            JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "strAppRequest", strAppRequest);
            JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "End", "OK");

            return strAppRequest;
        }
        catch (Exception e)
        {
            JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "Exception Message", e.Message);
            JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "Exception StackTrace", e.StackTrace);
            JubFuAPPWebServiceLog(sessionid, "sendUploadData5", "Exception Source", e.Source);

            string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = e.Message, serverdate = serverdate }, Formatting.Indented);
            return strAppRequest;
        }
    }
    #endregion

    #region 託運單掃讀資料上傳(v8)
    /// <summary>
    /// 託運單掃讀資料上傳(v8)
    /// </summary>
    /// <returns></returns>
    [WebMethod]
    public string sendUploadData8(string scan_item, string driver_code, string check_number, string scan_date, string area_arrive_code, string platform, string car_number, string sowage_rate, string area, string exception_option, string arrive_option, string ship_mode, string goods_type, string pieces, string weight, string runs, string plates, string sign_form_image, string sign_field_image, string coupon, string cash, string receive_option = "", string Work_Station_Scode = "", string Work_Send_Code = "", string Work_Delivery_Code = "")
    {
        string UserAgent = this.Context.Request.UserAgent;
        string IP = this.Context.Request.UserHostAddress;
        Guid g = Guid.NewGuid();
        var sessionid = g.ToString();
        writeLog("call", "sendUploadData8", "scan_item:" + scan_item + " driver_code:" + driver_code + " check_number:" + check_number + " scan_date:" + scan_date + " area_arrive_code:" + area_arrive_code + " platform:" + platform + " car_number:" + car_number + " sowage_rate:" + sowage_rate + " area:" + area + " exception_option:" + exception_option + " arrive_option:" + arrive_option + " ship_mode:" + ship_mode + " goods_type:" + goods_type + " pieces:" + pieces + " weight:" + weight + " runs:" + runs + " plates:" + plates + " coupon:" + coupon + " cash:" + cash + " receive_option:" + receive_option, "");
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dtAppRequest = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;

        //預計 2022-07 準備下線 配達 1.0
        bool canUseDelivery1 = true;
        if (scan_item.Equals("3") || scan_item.Equals("4"))
        {
            if (int.Parse(DateTime.Now.ToString("yyyyMMdd")) >= 20220701)
            {
                canUseDelivery1 = false;

                writeLog("setdata", "setDeliveryWhiteUser", "", "");
                setDeliveryWhiteUser();

                for (int i = 0; i < listDeliveryWhiteUser.Count; i++)
                {
                    if (driver_code.ToUpper().Equals(listDeliveryWhiteUser[i].driver_code.ToUpper()))
                    {
                        canUseDelivery1 = true;
                    }
                }
            }
        }

        //
        //指定版號 禁止使用
        bool isBlackVersion = false;
        if (1 == 2)
        {
            if (int.Parse(DateTime.Now.ToString("yyyyMMdd")) >= 20220701)
            {
                isBlackVersion = false;
                setLongTimeNoCheck();

                for (int i = 0; i < listLongTimeNoCheckUser.Count; i++)
                {
                    if (driver_code.ToUpper().Equals(listLongTimeNoCheckUser[i].driver_code.ToUpper()))
                    {
                        isBlackVersion = true;
                    }
                }
            }
        }

        var status = string.Empty;

        if (scan_item.Equals(""))
        {
            resultcode = false;
            error_msg = "作業類別未傳入";
        }
        else if (driver_code.Equals(""))
        {
            resultcode = false;
            error_msg = "司機代碼未傳入";
        }
        else if (check_number.Equals(""))
        {
            resultcode = false;
            error_msg = "託運單號未傳入";

        }
        else if (!(scan_item.Equals("1") || scan_item.Equals("2") || scan_item.Equals("3") || scan_item.Equals("4") || scan_item.Equals("5") || scan_item.Equals("6") || scan_item.Equals("7")))
        {
            resultcode = false;
            error_msg = "作業類別錯誤";
        }
        else if (scan_item.Equals("4") && (sign_form_image.Equals("") || sign_field_image.Equals("")) && (driver_code.Length > 1 && driver_code.Substring(0, 1) != "Z"))
        {
            resultcode = false;
            error_msg = "簽單圖檔未傳入";
        }
        else if (canUseDelivery1 == false && (scan_item.Equals("3") || scan_item.Equals("4")))
        {
            resultcode = false;
            status = "無權限使用";
            error_msg = "無權限使用配達1.0";
        }
        else if (isBlackVersion == true && (scan_item.Equals("5") || scan_item.Equals("3")))
        {
            resultcode = true;
            status = "APP版本太舊或是不明";
            error_msg = "禁止使用!! APP版本太舊或是不明 , 請先登出帳號檢查版本";
            string checkNumber = "--";
            Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter2 = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
            //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
            //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
            timeConverter2.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
            string strAppRequest2 = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, status = status, checkNumber = checkNumber, serverdate = serverdate }, Formatting.Indented);
            return strAppRequest2;

        }
        else
        {
            int _pieces = (String.IsNullOrEmpty(pieces) || !IsNumeric(pieces)) ? 0 : Convert.ToInt32(pieces);
            double _weight = (String.IsNullOrEmpty(weight) || !IsNumeric(weight)) ? 0 : Convert.ToDouble(weight);
            int _runs = (String.IsNullOrEmpty(runs) || !IsNumeric(runs)) ? 0 : Convert.ToInt32(runs);
            int _plates = (String.IsNullOrEmpty(plates) || !IsNumeric(plates)) ? 0 : Convert.ToInt32(plates);
            int _coupon = (String.IsNullOrEmpty(coupon) || !IsNumeric(coupon)) ? 0 : Convert.ToInt32(coupon);
            int _cash = (String.IsNullOrEmpty(cash) || !IsNumeric(cash)) ? 0 : Convert.ToInt32(cash);
            //var status = string.Empty;
            var consignment = GettcDeliveryRequestsBycheck_number(check_number);

            if (IsMOMOCheckNumber(check_number) && scan_item == "5" && consignment == null)
            {
                JubFuAPPWebServiceLog(sessionid, "sendUploadData8", "error_msg", "MOMO集貨;主表無單號");
                //error_msg = "MOMO集貨;主表無單號";
                saveErrScanLog("sendUploadData8", "MOMO集貨;主表無單號", sessionid, scan_item, driver_code, check_number, scan_date, area_arrive_code, platform, car_number, sowage_rate, area, exception_option, arrive_option, ship_mode, goods_type, pieces, weight, runs, plates, sign_form_image, sign_field_image, coupon, cash, receive_option = "");

            }
            else
            {
                if (consignment == null)
                {
                    JubFuAPPWebServiceLog(sessionid, "sendUploadData8", "error_msg", "查無單號");
                    resultcode = true;
                    error_msg = "查不到此筆單號，請與發送站所進行確認。";
                    status = "查無單號";
                    saveErrScanLog("sendUploadData8", error_msg, sessionid, scan_item, driver_code, check_number, scan_date, area_arrive_code, platform, car_number, sowage_rate, area, exception_option, arrive_option, ship_mode, goods_type, pieces, weight, runs, plates, sign_form_image, sign_field_image, coupon, cash, receive_option = "");
                    return JsonConvert.SerializeObject(new { checkNumber = check_number, resultcode = resultcode, resultdesc = error_msg, serverdate = serverdate, status = status }, Formatting.Indented); ;

                }
                if (IsFSEDriver(driver_code) && !scan_item.Equals("4"))
                {

                    if (IsNormalDelivery(consignment))
                    {
                        JubFuAPPWebServiceLog(sessionid, "sendUploadData8", "error_msg", "此筆已正常配達(或已產生退貨單號)");
                        resultcode = true;
                        error_msg = "此筆已正常配達(或已產生退貨單號)";
                        status = "正常配達(或已產生退貨單號)";
                        saveErrScanLog("sendUploadData8", error_msg, sessionid, scan_item, driver_code, check_number, scan_date, area_arrive_code, platform, car_number, sowage_rate, area, exception_option, arrive_option, ship_mode, goods_type, pieces, weight, runs, plates, sign_form_image, sign_field_image, coupon, cash, receive_option = "");
                        return JsonConvert.SerializeObject(new { checkNumber = check_number, resultcode = resultcode, resultdesc = error_msg, serverdate = serverdate, status = status }, Formatting.Indented); ;
                    }
                    else if (IsCancelled(consignment))
                    {
                        JubFuAPPWebServiceLog(sessionid, "sendUploadData8", "error_msg", "此筆已銷單");
                        resultcode = true;
                        error_msg = "此筆已銷單";
                        status = "已銷單";
                        saveErrScanLog("sendUploadData8", error_msg, sessionid, scan_item, driver_code, check_number, scan_date, area_arrive_code, platform, car_number, sowage_rate, area, exception_option, arrive_option, ship_mode, goods_type, pieces, weight, runs, plates, sign_form_image, sign_field_image, coupon, cash, receive_option = "");
                        return JsonConvert.SerializeObject(new { checkNumber = check_number, resultcode = resultcode, resultdesc = error_msg, serverdate = serverdate, status = status }, Formatting.Indented); ;
                    }
                }
            }
            try
            {
                //集貨掃讀、卸集、發送
                //else if (scan_item.Equals("5") || scan_item.Equals("6") || scan_item.Equals("7"))
                if (scan_item.Equals("5") || scan_item.Equals("6") || scan_item.Equals("7"))
                {
                    using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                    {
                        StringBuilder sql = new StringBuilder();
                        sql.Append(" insert into ttDeliveryScanLog (driver_code, check_number, scan_item, scan_date, area_arrive_code, platform, car_number, sowage_rate, area, exception_option,pieces,weight,runs,ship_mode,goods_type , plates, receive_option) values (@driver_code, @check_number, @scan_item, @scan_date, @area_arrive_code, @platform, @car_number, @sowage_rate, @area, @exception_option ,@pieces,@weight,@runs,@ship_mode, @goods_type , @plates, @receive_option)");

                        using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                        {
                            conn.Open();
                            command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                            command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                            command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                            command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                            command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;
                            command.Parameters.Add("@platform", SqlDbType.VarChar).Value = platform;
                            command.Parameters.Add("@car_number", SqlDbType.VarChar).Value = car_number;
                            command.Parameters.Add("@sowage_rate", SqlDbType.VarChar).Value = sowage_rate;
                            command.Parameters.Add("@area", SqlDbType.VarChar).Value = area;
                            command.Parameters.Add("@ship_mode", SqlDbType.NVarChar, 2).Value = ship_mode;
                            command.Parameters.Add("@goods_type", SqlDbType.NVarChar, 2).Value = goods_type;
                            command.Parameters.Add("@pieces", SqlDbType.Int).Value = _pieces;
                            command.Parameters.Add("@weight", SqlDbType.Float).Value = _weight;
                            command.Parameters.Add("@runs", SqlDbType.Int).Value = _runs;
                            command.Parameters.Add("@plates", SqlDbType.Int).Value = _plates;
                            command.Parameters.Add("@exception_option", SqlDbType.VarChar).Value = exception_option;
                            command.Parameters.Add("@receive_option", SqlDbType.VarChar).Value = receive_option;


                            SqlDataAdapter da = new SqlDataAdapter(command);
                            da.SelectCommand.CommandTimeout = db_Timeout;
                            da.Fill(dtAppRequest);
                            conn.Close();

                        }
                    }

                    using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                    {
                        StringBuilder sql = new StringBuilder();
                        sql.Append(" insert into ttDeliveryScanLog2 (driver_code, check_number, scan_item, scan_date, area_arrive_code, platform, car_number, sowage_rate, area, exception_option,pieces,weight,runs,ship_mode,goods_type , plates, receive_option,Work_Station_Scode,Work_Send_Code,Work_Delivery_Code) values (@driver_code, @check_number, @scan_item, @scan_date, @area_arrive_code, @platform, @car_number, @sowage_rate, @area, @exception_option ,@pieces,@weight,@runs,@ship_mode, @goods_type , @plates, @receive_option,@Work_Station_Scode,@Work_Send_Code,@Work_Delivery_Code)");

                        using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                        {
                            conn.Open();
                            command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                            command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                            command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                            command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                            command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;
                            command.Parameters.Add("@platform", SqlDbType.VarChar).Value = platform;
                            command.Parameters.Add("@car_number", SqlDbType.VarChar).Value = car_number;
                            command.Parameters.Add("@sowage_rate", SqlDbType.VarChar).Value = sowage_rate;
                            command.Parameters.Add("@area", SqlDbType.VarChar).Value = area;
                            command.Parameters.Add("@ship_mode", SqlDbType.NVarChar, 2).Value = ship_mode;
                            command.Parameters.Add("@goods_type", SqlDbType.NVarChar, 2).Value = goods_type;
                            command.Parameters.Add("@pieces", SqlDbType.Int).Value = _pieces;
                            command.Parameters.Add("@weight", SqlDbType.Float).Value = _weight;
                            command.Parameters.Add("@runs", SqlDbType.Int).Value = _runs;
                            command.Parameters.Add("@plates", SqlDbType.Int).Value = _plates;
                            command.Parameters.Add("@exception_option", SqlDbType.VarChar).Value = exception_option;
                            command.Parameters.Add("@receive_option", SqlDbType.VarChar).Value = receive_option;

                            command.Parameters.Add("@Work_Station_Scode", SqlDbType.VarChar).Value = Work_Station_Scode;
                            command.Parameters.Add("@Work_Send_Code", SqlDbType.VarChar).Value = Work_Send_Code;
                            command.Parameters.Add("@Work_Delivery_Code", SqlDbType.VarChar).Value = Work_Delivery_Code;

                            SqlDataAdapter da = new SqlDataAdapter(command);
                            da.SelectCommand.CommandTimeout = db_Timeout;
                            da.Fill(dtAppRequest);
                            conn.Close();

                        }
                    }
                }
                //到著掃讀
                else if (scan_item.Equals("1"))
                {

                    using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                    {
                        StringBuilder sql = new StringBuilder();
                        sql.Append(" insert into ttDeliveryScanLog (driver_code, check_number, scan_item, scan_date, area_arrive_code, platform, car_number, sowage_rate, area, exception_option, plates) values (@driver_code, @check_number, @scan_item, @scan_date, @area_arrive_code, @platform, @car_number, @sowage_rate, @area, @exception_option, @plates)");

                        using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                        {
                            conn.Open();
                            command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                            command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                            command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                            command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                            command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;
                            command.Parameters.Add("@platform", SqlDbType.VarChar).Value = platform;
                            command.Parameters.Add("@car_number", SqlDbType.VarChar).Value = car_number;
                            command.Parameters.Add("@sowage_rate", SqlDbType.VarChar).Value = sowage_rate;
                            command.Parameters.Add("@area", SqlDbType.VarChar).Value = area;
                            command.Parameters.Add("@exception_option", SqlDbType.VarChar).Value = exception_option;
                            command.Parameters.Add("@plates", SqlDbType.Int).Value = _plates;

                            SqlDataAdapter da = new SqlDataAdapter(command);
                            da.SelectCommand.CommandTimeout = db_Timeout;
                            da.Fill(dtAppRequest);
                            conn.Close();
                        }
                    }

                    using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                    {
                        StringBuilder sql = new StringBuilder();
                        sql.Append(" insert into ttDeliveryScanLog2 (driver_code, check_number, scan_item, scan_date, area_arrive_code, platform, car_number, sowage_rate, area, exception_option, plates,Work_Station_Scode,Work_Send_Code,Work_Delivery_Code) values (@driver_code, @check_number, @scan_item, @scan_date, @area_arrive_code, @platform, @car_number, @sowage_rate, @area, @exception_option, @plates,@Work_Station_Scode,@Work_Send_Code,@Work_Delivery_Code)");

                        using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                        {
                            conn.Open();
                            command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                            command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                            command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                            command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                            command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;
                            command.Parameters.Add("@platform", SqlDbType.VarChar).Value = platform;
                            command.Parameters.Add("@car_number", SqlDbType.VarChar).Value = car_number;
                            command.Parameters.Add("@sowage_rate", SqlDbType.VarChar).Value = sowage_rate;
                            command.Parameters.Add("@area", SqlDbType.VarChar).Value = area;
                            command.Parameters.Add("@exception_option", SqlDbType.VarChar).Value = exception_option;
                            command.Parameters.Add("@plates", SqlDbType.Int).Value = _plates;

                            command.Parameters.Add("@Work_Station_Scode", SqlDbType.VarChar).Value = Work_Station_Scode;
                            command.Parameters.Add("@Work_Send_Code", SqlDbType.VarChar).Value = Work_Send_Code;
                            command.Parameters.Add("@Work_Delivery_Code", SqlDbType.VarChar).Value = Work_Delivery_Code;

                            SqlDataAdapter da = new SqlDataAdapter(command);
                            da.SelectCommand.CommandTimeout = db_Timeout;
                            da.Fill(dtAppRequest);
                            conn.Close();
                        }
                    }
                }

                //配送掃讀
                else if (scan_item.Equals("2"))
                {
                    using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                    {
                        StringBuilder sql = new StringBuilder();
                        sql.Append(" insert into ttDeliveryScanLog (driver_code, check_number, scan_item, scan_date, exception_option, area_arrive_code, plates  ) values (@driver_code, @check_number, @scan_item, @scan_date, @exception_option, @area_arrive_code , @plates)");

                        using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                        {
                            conn.Open();
                            command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                            command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                            command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                            command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                            command.Parameters.Add("@exception_option", SqlDbType.VarChar).Value = exception_option;
                            command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;
                            command.Parameters.Add("@plates", SqlDbType.Int).Value = _plates;

                            SqlDataAdapter da = new SqlDataAdapter(command);
                            da.SelectCommand.CommandTimeout = db_Timeout;
                            da.Fill(dtAppRequest);
                            conn.Close();
                        }
                    }

                    using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                    {
                        StringBuilder sql = new StringBuilder();
                        sql.Append(" insert into ttDeliveryScanLog2 (driver_code, check_number, scan_item, scan_date, exception_option, area_arrive_code, plates ,Work_Station_Scode,Work_Send_Code,Work_Delivery_Code ) values (@driver_code, @check_number, @scan_item, @scan_date, @exception_option, @area_arrive_code , @plates,@Work_Station_Scode,@Work_Send_Code,@Work_Delivery_Code)");

                        using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                        {
                            conn.Open();
                            command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                            command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                            command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                            command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                            command.Parameters.Add("@exception_option", SqlDbType.VarChar).Value = exception_option;
                            command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;
                            command.Parameters.Add("@plates", SqlDbType.Int).Value = _plates;

                            command.Parameters.Add("@Work_Station_Scode", SqlDbType.VarChar).Value = Work_Station_Scode;
                            command.Parameters.Add("@Work_Send_Code", SqlDbType.VarChar).Value = Work_Send_Code;
                            command.Parameters.Add("@Work_Delivery_Code", SqlDbType.VarChar).Value = Work_Delivery_Code;

                            SqlDataAdapter da = new SqlDataAdapter(command);
                            da.SelectCommand.CommandTimeout = db_Timeout;
                            da.Fill(dtAppRequest);
                            conn.Close();
                        }
                    }
                }

                //配達掃讀
                else if (scan_item.Equals("3"))
                {

                    //DateTime now = System.DateTime.Now; //string drawString = time.ToString("yyyy/MM/dd HH:mm");
                    //string upload_time = now.ToString("yyyy/MM/dd HH:mm");

                    //託運單圖檔名
                    //string sign_form_image_name = driver_code + "_" + check_number + "_" + now.ToString("yyyyMMddHHmmss") + ".jpg";
                    //託運單簽名欄位圖檔名
                    //string sign_field_image_name = "sign" + "_" + check_number  + "_" + now.ToString("yyyyMMddHHmmss") + ".jpg";

                    if (IsFSEDriver(driver_code) && !IsLatestScanItem(check_number, "2"))
                    {
                        JubFuAPPWebServiceLog(sessionid, "sendUploadData8", "error_msg", "此票尚未配送，無法完成配達");
                        resultcode = true;
                        error_msg = "此票尚未配送，無法完成配達";
                        status = "尚未配送";
                        saveErrScanLog("sendUploadData8", error_msg, sessionid, scan_item, driver_code, check_number, scan_date, area_arrive_code, platform, car_number, sowage_rate, area, exception_option, arrive_option, ship_mode, goods_type, pieces, weight, runs, plates, sign_form_image, sign_field_image, coupon, cash, receive_option = "");
                        return JsonConvert.SerializeObject(new { checkNumber = check_number, resultcode = resultcode, resultdesc = error_msg, serverdate = serverdate, status = status }, Formatting.Indented); ;
                    }

                    using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                    {
                        StringBuilder sql = new StringBuilder();
                        sql.Append(" insert into ttDeliveryScanLog (driver_code, check_number, scan_item, scan_date, arrive_option, area_arrive_code, plates, VoucherMoney, CashMoney) values (@driver_code, @check_number, @scan_item, @scan_date, @arrive_option, @area_arrive_code ,@plates,@VoucherMoney, @CashMoney)");

                        using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                        {
                            conn.Open();
                            command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                            command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                            command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                            command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                            command.Parameters.Add("@arrive_option", SqlDbType.VarChar).Value = arrive_option;
                            //command.Parameters.Add("@sign_form_image", SqlDbType.VarChar).Value = sign_form_image_name;
                            //command.Parameters.Add("@sign_field_image", SqlDbType.VarChar).Value = sign_field_image_name;
                            command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;
                            command.Parameters.Add("@plates", SqlDbType.Int).Value = _plates;
                            command.Parameters.Add("@VoucherMoney", SqlDbType.Int).Value = _coupon;  //振興券金額
                            command.Parameters.Add("@CashMoney", SqlDbType.Int).Value = _cash;       //現金金額
                            SqlDataAdapter da = new SqlDataAdapter(command);
                            da.SelectCommand.CommandTimeout = db_Timeout;
                            da.Fill(dtAppRequest);
                            conn.Close();
                            #region 原單退回
                            if (arrive_option == "c" || arrive_option == "C")
                            {
                                using (SqlCommand cmd = new SqlCommand())
                                {
                                    cmd.Parameters.AddWithValue("@VoucherMoney", _coupon);
                                    cmd.Parameters.AddWithValue("@CashMoney", _cash);
                                    cmd.Parameters.AddWithValue("@check_number", check_number);
                                    cmd.CommandText = @" update tcDeliveryRequests set VoucherMoney=@VoucherMoney ,CashMoney=@CashMoney where check_number =@check_number and print_date >= DATEADD (MONTH,-6 ,getdate())";
                                    try
                                    {
                                        dbAdapter.execNonQuery(cmd);
                                    }
                                    catch (Exception e)
                                    {
                                        resultcode = false;
                                        error_msg = e.Message;
                                        writeLog("err", "sendUploadData8", "scan_item:" + scan_item + " driver_code:" + driver_code + " check_number:" + check_number + " scan_date:" + scan_date + " area_arrive_code:" + area_arrive_code + " platform:" + platform + " car_number:" + car_number + " sowage_rate:" + sowage_rate + " area:" + area + " exception_option:" + exception_option + " arrive_option:" + arrive_option + " ship_mode:" + ship_mode + " goods_type:" + goods_type + " pieces:" + pieces + " weight:" + weight + " runs:" + runs + " plates:" + plates + " coupon:" + coupon + " cash:" + cash, e.Message);
                                    }
                                }
                            }
                            else if (arrive_option == "A")
                            {
                                using (SqlCommand cmd = new SqlCommand())
                                {
                                    cmd.Parameters.AddWithValue("@check_number", check_number);
                                    cmd.CommandText = "dbo.uspWeb_ExecReturn";
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    try
                                    {
                                        dbAdapter.execNonQuery(cmd);
                                    }
                                    catch (Exception e)
                                    {
                                        resultcode = false;
                                        error_msg = e.Message;
                                        writeLog("err", "sendUploadData8", "scan_item:" + scan_item + " driver_code:" + driver_code + " check_number:" + check_number + " scan_date:" + scan_date + " area_arrive_code:" + area_arrive_code + " platform:" + platform + " car_number:" + car_number + " sowage_rate:" + sowage_rate + " area:" + area + " exception_option:" + exception_option + " arrive_option:" + arrive_option + " ship_mode:" + ship_mode + " goods_type:" + goods_type + " pieces:" + pieces + " weight:" + weight + " runs:" + runs + " plates:" + plates + " coupon:" + coupon + " cash:" + cash, e.Message);
                                    }
                                }
                            }

                            #endregion
                        }
                    }

                    using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                    {
                        StringBuilder sql = new StringBuilder();
                        sql.Append(" insert into ttDeliveryScanLog2 (driver_code, check_number, scan_item, scan_date, arrive_option, area_arrive_code, plates, VoucherMoney, CashMoney,Work_Station_Scode,Work_Send_Code,Work_Delivery_Code) values (@driver_code, @check_number, @scan_item, @scan_date, @arrive_option, @area_arrive_code ,@plates,@VoucherMoney, @CashMoney,@Work_Station_Scode,@Work_Send_Code,@Work_Delivery_Code)");

                        using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                        {
                            conn.Open();
                            command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                            command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                            command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                            command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                            command.Parameters.Add("@arrive_option", SqlDbType.VarChar).Value = arrive_option;
                            //command.Parameters.Add("@sign_form_image", SqlDbType.VarChar).Value = sign_form_image_name;
                            //command.Parameters.Add("@sign_field_image", SqlDbType.VarChar).Value = sign_field_image_name;
                            command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;
                            command.Parameters.Add("@plates", SqlDbType.Int).Value = _plates;
                            command.Parameters.Add("@VoucherMoney", SqlDbType.Int).Value = _coupon;  //振興券金額
                            command.Parameters.Add("@CashMoney", SqlDbType.Int).Value = _cash;       //現金金額

                            command.Parameters.Add("@Work_Station_Scode", SqlDbType.VarChar).Value = Work_Station_Scode;
                            command.Parameters.Add("@Work_Send_Code", SqlDbType.VarChar).Value = Work_Send_Code;
                            command.Parameters.Add("@Work_Delivery_Code", SqlDbType.VarChar).Value = Work_Delivery_Code;

                            SqlDataAdapter da = new SqlDataAdapter(command);
                            da.SelectCommand.CommandTimeout = db_Timeout;
                            da.Fill(dtAppRequest);
                            conn.Close();
                            #region 原單退回
                            if (arrive_option == "c" || arrive_option == "C")
                            {
                                using (SqlCommand cmd = new SqlCommand())
                                {
                                    cmd.Parameters.AddWithValue("@VoucherMoney", _coupon);
                                    cmd.Parameters.AddWithValue("@CashMoney", _cash);
                                    cmd.Parameters.AddWithValue("@check_number", check_number);
                                    cmd.CommandText = @" update tcDeliveryRequests set VoucherMoney=@VoucherMoney ,CashMoney=@CashMoney where check_number =@check_number and print_date >= DATEADD (MONTH,-6 ,getdate())";
                                    try
                                    {
                                        dbAdapter.execNonQuery(cmd);
                                    }
                                    catch (Exception e)
                                    {
                                        resultcode = false;
                                        error_msg = e.Message;
                                        writeLog("err", "sendUploadData8", "scan_item:" + scan_item + " driver_code:" + driver_code + " check_number:" + check_number + " scan_date:" + scan_date + " area_arrive_code:" + area_arrive_code + " platform:" + platform + " car_number:" + car_number + " sowage_rate:" + sowage_rate + " area:" + area + " exception_option:" + exception_option + " arrive_option:" + arrive_option + " ship_mode:" + ship_mode + " goods_type:" + goods_type + " pieces:" + pieces + " weight:" + weight + " runs:" + runs + " plates:" + plates + " coupon:" + coupon + " cash:" + cash, e.Message);
                                    }
                                }
                            }
                            else if (arrive_option == "A")
                            {
                                using (SqlCommand cmd = new SqlCommand())
                                {
                                    cmd.Parameters.AddWithValue("@check_number", check_number);
                                    cmd.CommandText = "dbo.uspWeb_ExecReturn";
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    try
                                    {
                                        dbAdapter.execNonQuery(cmd);
                                    }
                                    catch (Exception e)
                                    {
                                        resultcode = false;
                                        error_msg = e.Message;
                                        writeLog("err", "sendUploadData8", "scan_item:" + scan_item + " driver_code:" + driver_code + " check_number:" + check_number + " scan_date:" + scan_date + " area_arrive_code:" + area_arrive_code + " platform:" + platform + " car_number:" + car_number + " sowage_rate:" + sowage_rate + " area:" + area + " exception_option:" + exception_option + " arrive_option:" + arrive_option + " ship_mode:" + ship_mode + " goods_type:" + goods_type + " pieces:" + pieces + " weight:" + weight + " runs:" + runs + " plates:" + plates + " coupon:" + coupon + " cash:" + cash, e.Message);
                                    }
                                }
                            }

                            #endregion
                        }
                    }

                    //寫入圖檔
                    //SaveImgFromBase64(sign_form_image, sign_form_image_name, upload_time);
                    //SaveImgFromBase64(sign_field_image, sign_field_image_name, upload_time);

                }

                //簽單掃讀
                else if (scan_item.Equals("4") && (sign_form_image != "" || sign_field_image != ""))
                {

                    DateTime now = System.DateTime.Now; //string drawString = time.ToString("yyyy/MM/dd HH:mm");
                    string upload_time = now.ToString("yyyy/MM/dd HH:mm");

                    //託運單圖檔名
                    string sign_form_image_name = driver_code + "_" + check_number + "_" + now.ToString("yyyyMMddHHmmss") + ".jpg";
                    //託運單簽名欄位圖檔名
                    string sign_field_image_name = "sign" + "_" + check_number + "_" + now.ToString("yyyyMMddHHmmss") + ".jpg";

                    using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                    {
                        StringBuilder sql = new StringBuilder();
                        sql.Append(" insert into ttDeliveryScanLog (driver_code, check_number, scan_item, scan_date, arrive_option, sign_form_image, sign_field_image, area_arrive_code , plates) values (@driver_code, @check_number, @scan_item, @scan_date, @arrive_option, @sign_form_image, @sign_field_image, @area_arrive_code , @plates)");

                        using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                        {
                            conn.Open();
                            command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                            command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                            command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                            command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                            command.Parameters.Add("@arrive_option", SqlDbType.VarChar).Value = arrive_option;
                            command.Parameters.Add("@sign_form_image", SqlDbType.VarChar).Value = sign_form_image_name;
                            command.Parameters.Add("@sign_field_image", SqlDbType.VarChar).Value = sign_field_image_name;
                            command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;
                            command.Parameters.Add("@plates", SqlDbType.Int).Value = _plates;

                            SqlDataAdapter da = new SqlDataAdapter(command);
                            da.SelectCommand.CommandTimeout = db_Timeout;
                            da.Fill(dtAppRequest);
                            conn.Close();
                        }
                    }

                    using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                    {
                        StringBuilder sql = new StringBuilder();
                        sql.Append(" insert into ttDeliveryScanLog2 (driver_code, check_number, scan_item, scan_date, arrive_option, sign_form_image, sign_field_image, area_arrive_code , plates,Work_Station_Scode,Work_Send_Code,Work_Delivery_Code) values (@driver_code, @check_number, @scan_item, @scan_date, @arrive_option, @sign_form_image, @sign_field_image, @area_arrive_code , @plates,@Work_Station_Scode,@Work_Send_Code,@Work_Delivery_Code)");

                        using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                        {
                            conn.Open();
                            command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                            command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                            command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                            command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                            command.Parameters.Add("@arrive_option", SqlDbType.VarChar).Value = arrive_option;
                            command.Parameters.Add("@sign_form_image", SqlDbType.VarChar).Value = sign_form_image_name;
                            command.Parameters.Add("@sign_field_image", SqlDbType.VarChar).Value = sign_field_image_name;
                            command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;
                            command.Parameters.Add("@plates", SqlDbType.Int).Value = _plates;

                            command.Parameters.Add("@Work_Station_Scode", SqlDbType.VarChar).Value = Work_Station_Scode;
                            command.Parameters.Add("@Work_Send_Code", SqlDbType.VarChar).Value = Work_Send_Code;
                            command.Parameters.Add("@Work_Delivery_Code", SqlDbType.VarChar).Value = Work_Delivery_Code;

                            SqlDataAdapter da = new SqlDataAdapter(command);
                            da.SelectCommand.CommandTimeout = db_Timeout;
                            da.Fill(dtAppRequest);
                            conn.Close();
                        }
                    }

                    //寫入圖檔
                    SaveImgFromBase64(sign_form_image, sign_form_image_name, upload_time);
                    SaveImgFromBase64(sign_field_image, sign_field_image_name, upload_time);

                }


                writeLog("ref", "sendUploadData8", "scan_item:" + scan_item + " driver_code:" + driver_code + " check_number:" + check_number + " scan_date:" + scan_date + " area_arrive_code:" + area_arrive_code + " platform:" + platform + " car_number:" + car_number + " sowage_rate:" + sowage_rate + " area:" + area + " exception_option:" + exception_option + " arrive_option:" + arrive_option + " ship_mode:" + ship_mode + " goods_type:" + goods_type + " pieces:" + pieces + " weight:" + weight + " runs:" + runs + " plates:" + plates + " coupon:" + coupon + " cash:" + cash, "");
            }
            catch (Exception e)
            {
                resultcode = false;
                error_msg = e.Message;
                writeLog("err", "sendUploadData8", "scan_item:" + scan_item + " driver_code:" + driver_code + " check_number:" + check_number + " scan_date:" + scan_date + " area_arrive_code:" + area_arrive_code + " platform:" + platform + " car_number:" + car_number + " sowage_rate:" + sowage_rate + " area:" + area + " exception_option:" + exception_option + " arrive_option:" + arrive_option + " ship_mode:" + ship_mode + " goods_type:" + goods_type + " pieces:" + pieces + " weight:" + weight + " runs:" + runs + " plates:" + plates + " coupon:" + coupon + " cash:" + cash, e.Message);
            }
            finally
            {
            }

            //將欄位名稱轉成小寫
            if (dtAppRequest != null && dtAppRequest.Rows.Count > 0)
            {
                foreach (DataColumn column in dtAppRequest.Columns)
                {
                    column.ColumnName = column.ColumnName.ToLower();
                }
            }
        }

        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, serverdate = serverdate }, Formatting.Indented);
        return strAppRequest;
    }
    #endregion

    #region 託運單掃讀資料上傳(v9)
    /// <summary>
    /// 託運單掃讀資料上傳(v9)
    /// </summary>
    /// <returns></returns>
    [WebMethod]
    public string sendUploadData9(string scan_item, string driver_code, string request_id, string check_number, string scan_date, string area_arrive_code, string platform, string car_number, string sowage_rate, string area, string exception_option, string arrive_option, string ship_mode, string goods_type, string pieces, string weight, string runs, string plates, string sign_field_image, string coupon, string cash, int signType, string receive_option = "", string Work_Station_Scode = "", string Work_Send_Code = "", string Work_Delivery_Code = "")
    {
        string UserAgent = this.Context.Request.UserAgent;
        string IP = this.Context.Request.UserHostAddress;
        Guid g = Guid.NewGuid();
        var sessionid = g.ToString();
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dtAppRequest = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;

        //託運單圖檔名
        string sign_form_image_name = string.Empty;
        //託運單簽名欄位圖檔名
        string sign_field_image_name = string.Empty;


        //託運單圖檔名
        string sign_form_image_url = string.Empty;
        //託運單簽名欄位圖檔名
        string sign_field_image_url = string.Empty;

        //指定版號 禁止使用
        bool isBlackVersion = false;
        if (1 == 2)
        {
            if (int.Parse(DateTime.Now.ToString("yyyyMMdd")) >= 20220701)
            {
                isBlackVersion = false;
                setLongTimeNoCheck();

                for (int i = 0; i < listLongTimeNoCheckUser.Count; i++)
                {
                    if (driver_code.ToUpper().Equals(listLongTimeNoCheckUser[i].driver_code.ToUpper()))
                    {
                        isBlackVersion = true;
                    }
                }
            }
        }

        var status = string.Empty;
        try
        {
            JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "Start", "OK");
            JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "UserAgent", UserAgent);
            JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "IP", IP);
            string parameter = JsonConvert.SerializeObject(new { scan_item, driver_code, request_id, check_number, scan_date, area_arrive_code, platform, car_number, sowage_rate, area, exception_option, arrive_option, ship_mode, goods_type, pieces, weight, runs, plates, sign_field_image, coupon, cash, receive_option });
            JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "parameter", parameter);

            if (scan_item.Equals(""))
            {
                resultcode = false;
                error_msg = "作業類別未傳入";
                JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "error_msg", "作業類別未傳入");
                return JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg }, Formatting.Indented);

            }
            else if (driver_code.Equals(""))
            {
                resultcode = false;
                error_msg = "司機代碼未傳入";
                JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "error_msg", "司機代碼未傳入");
                return JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg }, Formatting.Indented);
            }
            else if (check_number.Equals(""))
            {
                resultcode = false;
                error_msg = "託運單號未傳入";
                JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "error_msg", "託運單號未傳入");
                return JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg }, Formatting.Indented);
            }
            else if (!(scan_item.Equals("3") || scan_item.Equals("4")))
            {
                resultcode = false;
                error_msg = "作業類別錯誤";
                JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "error_msg", "作業類別錯誤");
                return JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg }, Formatting.Indented);
            }
            else if (scan_item.Equals("4") && (sign_field_image.Equals("")) && (driver_code.Length > 1 && driver_code.Substring(0, 1) != "Z"))
            {
                resultcode = false;
                error_msg = "簽單圖檔未傳入";
                JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "error_msg", "簽單圖檔未傳入");
                return JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg }, Formatting.Indented);
            }
            else if (isBlackVersion == true)
            {
                resultcode = true;
                error_msg = "禁止使用!! APP版本太舊或是不明 , 請先登出帳號檢查版本";
                status = "APP版本太舊或是不明";
                JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "error_msg", error_msg);
                return JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, status = status, checkNumber = "--" }, Formatting.Indented);
            }
            else
            {
                int _pieces = (String.IsNullOrEmpty(pieces) || !IsNumeric(pieces)) ? 0 : Convert.ToInt32(pieces);
                double _weight = (String.IsNullOrEmpty(weight) || !IsNumeric(weight)) ? 0 : Convert.ToDouble(weight);
                int _runs = (String.IsNullOrEmpty(runs) || !IsNumeric(runs)) ? 0 : Convert.ToInt32(runs);
                int _plates = (String.IsNullOrEmpty(plates) || !IsNumeric(plates)) ? 0 : Convert.ToInt32(plates);
                int _coupon = (String.IsNullOrEmpty(coupon) || !IsNumeric(coupon)) ? 0 : Convert.ToInt32(coupon);
                int _cash = (String.IsNullOrEmpty(cash) || !IsNumeric(cash)) ? 0 : Convert.ToInt32(cash);
                //var status = string.Empty;

                var consignment = GettcDeliveryRequestsBycheck_number(check_number);

                //配達掃讀
                if (scan_item.Equals("3"))
                {
                    if (consignment == null)
                    {
                        JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "error_msg", "查無單號");
                        resultcode = true;
                        error_msg = "查不到此筆單號，請與發送站所進行確認。";
                        status = "查無單號";
                        saveErrScanLog("sendUploadData5", error_msg, sessionid, scan_item, driver_code, check_number, scan_date, area_arrive_code, platform, car_number, sowage_rate, area, exception_option, arrive_option, ship_mode, goods_type, pieces, weight, runs, plates, "", "", coupon, cash, receive_option = "");
                        return JsonConvert.SerializeObject(new { checkNumber = check_number, resultcode = resultcode, resultdesc = error_msg, serverdate = serverdate, status = status }, Formatting.Indented); ;
                    }
                    if (IsNormalDelivery(consignment))
                    {
                        JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "error_msg", "此筆已正常配達(或已產生退貨單號)");
                        resultcode = true;
                        error_msg = "此筆已正常配達(或已產生退貨單號)";
                        status = "正常配達(或已產生退貨單號)";
                        saveErrScanLog("sendUploadData5", error_msg, sessionid, scan_item, driver_code, check_number, scan_date, area_arrive_code, platform, car_number, sowage_rate, area, exception_option, arrive_option, ship_mode, goods_type, pieces, weight, runs, plates, "", "", coupon, cash, receive_option = "");
                        return JsonConvert.SerializeObject(new { checkNumber = check_number, resultcode = resultcode, resultdesc = error_msg, serverdate = serverdate, status = status }, Formatting.Indented); ;
                    }
                    else if (IsCancelled(consignment))
                    {
                        JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "error_msg", "此筆已銷單");
                        resultcode = true;
                        error_msg = "此筆已銷單";
                        status = "已銷單";
                        saveErrScanLog("sendUploadData9", error_msg, sessionid, scan_item, driver_code, check_number, scan_date, area_arrive_code, platform, car_number, sowage_rate, area, exception_option, arrive_option, ship_mode, goods_type, pieces, weight, runs, plates, "", "", coupon, cash, receive_option = "");
                        return JsonConvert.SerializeObject(new { checkNumber = check_number, resultcode = resultcode, resultdesc = error_msg, serverdate = serverdate, status = status }, Formatting.Indented); ;
                    }
                    else if (!IsLatestScanItem(check_number, "2"))
                    {
                        JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "error_msg", "此票尚未配送，無法完成配達");
                        resultcode = true;
                        error_msg = "此票尚未配送，無法完成配達";
                        status = "尚未配送";
                        saveErrScanLog("sendUploadData9", error_msg, sessionid, scan_item, driver_code, check_number, scan_date, area_arrive_code, platform, car_number, sowage_rate, area, exception_option, arrive_option, ship_mode, goods_type, pieces, weight, runs, plates, "", "", coupon, cash, receive_option = "");
                        return JsonConvert.SerializeObject(new { checkNumber = check_number, resultcode = resultcode, resultdesc = error_msg, serverdate = serverdate, status = status }, Formatting.Indented); ;
                    }


                    //原本是 2022-06-30 要上線的配異簡訊
                    //現在突然不上線了
                    //突然又要上線了
                    if (int.Parse(DateTime.Now.ToString("yyyyMMdd")) >= 20220701 || driver_code.ToString().ToUpper() == "Z010006")
                    {
                        string sms_log_type = "sendUploadData9-SMS";
                        string sms_log_action = "check_number:" + check_number
                                                        + " ; scan_item:" + scan_item + " ; arrive_option:" + arrive_option
                                                        + " ; driver_code:" + driver_code;
                        string sms_log_msg = "";

                        string smsText = "";
                        //準備發送配異簡訊
                        if (scan_item.Equals("3"))
                        {
                            arrive_option = arrive_option.Trim();

                            if (arrive_option != "3" && arrive_option != "")
                            {
                                bool IsEverSendSms = CheckIsEverSendSmsByCheckNumber(check_number, 0);

                                if (IsEverSendSms == false)
                                {
                                    string mobile = GetCheckNumberMobile(check_number);

                                    if (mobile != "")
                                    {
                                        //2022-11-09 緊急進版
                                        //只有 1 客戶不在 / 26 聯絡不上 需要發簡訊
                                        if (arrive_option == "1" || arrive_option == "26")
                                        {
                                            sms_log_msg = "配異發送簡訊成功 ; mobile:" + mobile;
                                            smsText = SendSms(check_number, mobile, scan_item, arrive_option, driver_code);
                                            sms_log_msg += " ; " + smsText;
                                        }
                                        else
                                        {
                                            sms_log_msg = "配異發送簡訊失敗 ; Error:指定不發的arrive_option-" + arrive_option;
                                        }
                                    }
                                    else
                                    {
                                        sms_log_msg = "配異發送簡訊失敗 ; Error:配達收件人沒有提供手機";
                                    }
                                }
                                else
                                {
                                    sms_log_msg = "配異發送簡訊失敗 ; Error:同一check_number今日已發送過簡訊";
                                }
                            }
                            else
                            {
                                sms_log_msg = "配異發送簡訊失敗 ; Error:正常配達不用發送簡訊";
                            }
                        }

                        //萬一沒發簡訊 不用紀錄LOG
                        if (smsText != "")
                        {
                            JubFuAPPWebServiceLog_SMS(sessionid, sms_log_type, check_number, sms_log_action, sms_log_msg);
                        }
                    }




                    using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                    {
                        StringBuilder sql = new StringBuilder();
                        sql.Append(" insert into ttDeliveryScanLog (driver_code, check_number, scan_item, scan_date, arrive_option, area_arrive_code, plates, VoucherMoney, CashMoney) values (@driver_code, @check_number, @scan_item, @scan_date, @arrive_option, @area_arrive_code ,@plates,@VoucherMoney, @CashMoney)");

                        using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                        {
                            string DeliveryRequestsparameter = JsonConvert.SerializeObject(new { driver_code, check_number, scan_item, scan_date, arrive_option, area_arrive_code, _plates, _coupon, _cash });
                            JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "InsterttDeliveryScanLogsparameter_3", DeliveryRequestsparameter);


                            conn.Open();
                            command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                            command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                            command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                            command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                            command.Parameters.Add("@arrive_option", SqlDbType.VarChar).Value = arrive_option;
                            command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;
                            command.Parameters.Add("@plates", SqlDbType.Int).Value = _plates;
                            command.Parameters.Add("@VoucherMoney", SqlDbType.Int).Value = _coupon;  //振興券金額
                            command.Parameters.Add("@CashMoney", SqlDbType.Int).Value = _cash;       //現金金額



                            SqlDataAdapter da = new SqlDataAdapter(command);
                            da.SelectCommand.CommandTimeout = db_Timeout;
                            da.Fill(dtAppRequest);
                            conn.Close();

                            JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "InsterttDeliveryScanLogsparameter_3", "End");


                            #region 原單退回
                            if (arrive_option.ToUpper() == "C")
                            {
                                using (SqlCommand cmd = new SqlCommand())
                                {
                                    cmd.Parameters.AddWithValue("@VoucherMoney", _coupon);
                                    cmd.Parameters.AddWithValue("@CashMoney", _cash);
                                    cmd.Parameters.AddWithValue("@check_number", check_number);
                                    cmd.CommandText = @" update tcDeliveryRequests set VoucherMoney=@VoucherMoney ,CashMoney=@CashMoney where check_number =@check_number and print_date >= DATEADD (MONTH,-6 ,getdate())";
                                    try
                                    {
                                        string updateDeliveryRequestsparameter = JsonConvert.SerializeObject(new { _coupon, _cash, check_number });
                                        JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "updatetcDeliveryRequestsparameter arrive_option=C", updateDeliveryRequestsparameter);
                                        dbAdapter.execNonQuery(cmd);
                                        JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "updatetcDeliveryRequestssparameter arrive_option=C", "End");
                                    }
                                    catch (Exception e)
                                    {
                                        resultcode = false;
                                        error_msg = e.Message;
                                        JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "error_msg", e.Message);
                                        return JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg }, Formatting.Indented);
                                    }
                                }
                            }
                            else if (arrive_option.ToUpper() == "A")
                            {
                                using (SqlCommand cmd = new SqlCommand())
                                {
                                    cmd.Parameters.AddWithValue("@check_number", check_number);
                                    cmd.CommandText = "dbo.uspWeb_ExecReturn";
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    try
                                    {
                                        JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "updateDeliveryRequestsparameter arrive_option=A", "dbo.uspWeb_ExecReturn  " + check_number);
                                        dbAdapter.execNonQuery(cmd);
                                        JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "updateDeliveryRequestsparameter arrive_option=A", "End");
                                    }
                                    catch (Exception e)
                                    {
                                        resultcode = false;
                                        error_msg = e.Message;
                                        JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "error_msg", e.Message);
                                        return JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg }, Formatting.Indented);
                                    }
                                }
                            }

                            #endregion
                        }
                    }

                    using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                    {
                        StringBuilder sql = new StringBuilder();
                        sql.Append(" insert into ttDeliveryScanLog2 (driver_code, check_number, scan_item, scan_date, arrive_option, area_arrive_code, plates, VoucherMoney, CashMoney,Work_Station_Scode,Work_Send_Code,Work_Delivery_Code) values (@driver_code, @check_number, @scan_item, @scan_date, @arrive_option, @area_arrive_code ,@plates,@VoucherMoney, @CashMoney,@Work_Station_Scode,@Work_Send_Code,@Work_Delivery_Code)");

                        using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                        {
                            string DeliveryRequestsparameter = JsonConvert.SerializeObject(new { driver_code, check_number, scan_item, scan_date, arrive_option, area_arrive_code, _plates, _coupon, _cash });
                            JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "InsterttDeliveryScanLogsparameter_3", DeliveryRequestsparameter);


                            conn.Open();
                            command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                            command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                            command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                            command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                            command.Parameters.Add("@arrive_option", SqlDbType.VarChar).Value = arrive_option;
                            command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;
                            command.Parameters.Add("@plates", SqlDbType.Int).Value = _plates;
                            command.Parameters.Add("@VoucherMoney", SqlDbType.Int).Value = _coupon;  //振興券金額
                            command.Parameters.Add("@CashMoney", SqlDbType.Int).Value = _cash;       //現金金額


                            command.Parameters.Add("@Work_Station_Scode", SqlDbType.VarChar).Value = Work_Station_Scode;
                            command.Parameters.Add("@Work_Send_Code", SqlDbType.VarChar).Value = Work_Send_Code;
                            command.Parameters.Add("@Work_Delivery_Code", SqlDbType.VarChar).Value = Work_Delivery_Code;

                            SqlDataAdapter da = new SqlDataAdapter(command);
                            da.SelectCommand.CommandTimeout = db_Timeout;
                            da.Fill(dtAppRequest);
                            conn.Close();

                            JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "InsterttDeliveryScanLogsparameter_3", "End");


                            #region 原單退回
                            if (arrive_option.ToUpper() == "C")
                            {
                                using (SqlCommand cmd = new SqlCommand())
                                {
                                    cmd.Parameters.AddWithValue("@VoucherMoney", _coupon);
                                    cmd.Parameters.AddWithValue("@CashMoney", _cash);
                                    cmd.Parameters.AddWithValue("@check_number", check_number);
                                    cmd.CommandText = @" update tcDeliveryRequests set VoucherMoney=@VoucherMoney ,CashMoney=@CashMoney where check_number =@check_number and print_date >= DATEADD (MONTH,-6 ,getdate())";
                                    try
                                    {
                                        string updateDeliveryRequestsparameter = JsonConvert.SerializeObject(new { _coupon, _cash, check_number });
                                        JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "updatetcDeliveryRequestsparameter arrive_option=C", updateDeliveryRequestsparameter);
                                        dbAdapter.execNonQuery(cmd);
                                        JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "updatetcDeliveryRequestssparameter arrive_option=C", "End");
                                    }
                                    catch (Exception e)
                                    {
                                        resultcode = false;
                                        error_msg = e.Message;
                                        JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "error_msg", e.Message);
                                        return JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg }, Formatting.Indented);
                                    }
                                }
                            }
                            else if (arrive_option.ToUpper() == "A")
                            {
                                using (SqlCommand cmd = new SqlCommand())
                                {
                                    cmd.Parameters.AddWithValue("@check_number", check_number);
                                    cmd.CommandText = "dbo.uspWeb_ExecReturn";
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    try
                                    {
                                        JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "updateDeliveryRequestsparameter arrive_option=A", "dbo.uspWeb_ExecReturn  " + check_number);
                                        dbAdapter.execNonQuery(cmd);
                                        JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "updateDeliveryRequestsparameter arrive_option=A", "End");
                                    }
                                    catch (Exception e)
                                    {
                                        resultcode = false;
                                        error_msg = e.Message;
                                        JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "error_msg", e.Message);
                                        return JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg }, Formatting.Indented);
                                    }
                                }
                            }

                            #endregion
                        }
                    }

                }

                //簽單掃讀
                else if (scan_item.Equals("4") && (sign_field_image != ""))
                {

                    //託運單圖檔名
                    sign_form_image_name = driver_code + "_" + check_number + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".jpg";
                    //託運單簽名欄位圖檔名
                    sign_field_image_name = "sign" + "_" + check_number + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".png";


                    //託運單圖檔名
                    sign_form_image_url = ImageUrl + sign_form_image_name;
                    //託運單簽名欄位圖檔名
                    sign_field_image_url = ImageUrl + sign_field_image_name;

                    DateTime now = System.DateTime.Now; //string drawString = time.ToString("yyyy/MM/dd HH:mm");
                    string upload_time = now.ToString("yyyy/MM/dd HH:mm");


                    switch (signType)
                    {
                        case 1:
                        case 2:

                            JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "sign_field_image", sign_field_image);
                            //寫入簽名圖檔
                            // SaveImgFromBase64(sign_field_image, sign_field_image_name, upload_time);

                            Base64ToPNG(sign_field_image, sign_field_image_name);
                            JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "sign_field_image", "End");

                            //更新簽名檔
                            using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                            {
                                StringBuilder sql = new StringBuilder();
                                sql.Append(" insert into ttDeliveryScanLog (driver_code, check_number, scan_item, scan_date, arrive_option, sign_field_image, area_arrive_code , plates) values (@driver_code, @check_number, @scan_item, @scan_date, @arrive_option, @sign_field_image, @area_arrive_code , @plates)");

                                using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                                {
                                    string updateDeliveryRequestsparameter = JsonConvert.SerializeObject(new { driver_code, check_number, scan_item, scan_date, arrive_option, sign_field_image_name, area_arrive_code, _plates });
                                    JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "InsterttDeliveryScanLogsparameter_4", updateDeliveryRequestsparameter);
                                    conn.Open();
                                    command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                                    command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                                    command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                                    command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                                    command.Parameters.Add("@arrive_option", SqlDbType.VarChar).Value = arrive_option;
                                    // command.Parameters.Add("@sign_form_image", SqlDbType.VarChar).Value = sign_form_image_name;
                                    command.Parameters.Add("@sign_field_image", SqlDbType.VarChar).Value = sign_field_image_name;
                                    command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;
                                    command.Parameters.Add("@plates", SqlDbType.Int).Value = _plates;

                                    SqlDataAdapter da = new SqlDataAdapter(command);
                                    da.SelectCommand.CommandTimeout = db_Timeout;
                                    da.Fill(dtAppRequest);
                                    conn.Close();


                                    JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "InsterttDeliveryScanLogsparameter_4", "End");
                                }

                            }

                            using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                            {
                                StringBuilder sql = new StringBuilder();
                                sql.Append(" insert into ttDeliveryScanLog2 (driver_code, check_number, scan_item, scan_date, arrive_option, sign_field_image, area_arrive_code , plates,Work_Station_Scode,Work_Send_Code,Work_Delivery_Code) values (@driver_code, @check_number, @scan_item, @scan_date, @arrive_option, @sign_field_image, @area_arrive_code , @plates,@Work_Station_Scode,@Work_Send_Code,@Work_Delivery_Code)");

                                using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                                {
                                    string updateDeliveryRequestsparameter = JsonConvert.SerializeObject(new { driver_code, check_number, scan_item, scan_date, arrive_option, sign_field_image_name, area_arrive_code, _plates });
                                    JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "InsterttDeliveryScanLogsparameter_4", updateDeliveryRequestsparameter);
                                    conn.Open();
                                    command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                                    command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                                    command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                                    command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                                    command.Parameters.Add("@arrive_option", SqlDbType.VarChar).Value = arrive_option;
                                    // command.Parameters.Add("@sign_form_image", SqlDbType.VarChar).Value = sign_form_image_name;
                                    command.Parameters.Add("@sign_field_image", SqlDbType.VarChar).Value = sign_field_image_name;
                                    command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;
                                    command.Parameters.Add("@plates", SqlDbType.Int).Value = _plates;

                                    command.Parameters.Add("@Work_Station_Scode", SqlDbType.VarChar).Value = Work_Station_Scode;
                                    command.Parameters.Add("@Work_Send_Code", SqlDbType.VarChar).Value = Work_Send_Code;
                                    command.Parameters.Add("@Work_Delivery_Code", SqlDbType.VarChar).Value = Work_Delivery_Code;

                                    SqlDataAdapter da = new SqlDataAdapter(command);
                                    da.SelectCommand.CommandTimeout = db_Timeout;
                                    da.Fill(dtAppRequest);
                                    conn.Close();


                                    JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "InsterttDeliveryScanLogsparameter_4", "End");
                                }

                            }


                            JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "sign_form_image", sign_form_image_name);
                            var URL = GetSimpleReportUrl("NonPaperDeliverySignReceipt");


                            try
                            {
                                var stream = PostInner(URL, "request_ids=" + request_id + "&sign_field_image_url=" + sign_field_image_url);
                                using (Bitmap bitmap = new Bitmap(stream))
                                {

                                    if (bitmap != null)
                                    {
                                        bitmap.Save(HttpContext.Current.Server.MapPath("~/PHOTO/" + sign_form_image_name), ImageFormat.Jpeg);
                                    }
                                    stream.Flush();
                                    stream.Close();
                                }
                            }
                            catch (Exception e)
                            {
                                sign_form_image_name = "";
                                JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "sign_form_image", "Error：" + e.Message);
                            }

                            JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "sign_form_image", "End");

                            //簽單存入SignImage表
                            JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "InstertSignImageParameter", "Start");
                            StringBuilder sql2 = new StringBuilder();
                            sql2.Append(@" INSERT INTO SignImage (DriverCode, CheckNumber, SignType, SignFormImage, SignFieldImage, cdate) 
                                VALUES  (@driver_code, @check_number, @signType, @sign_form_image, @sign_field_image, GETDATE() )");
                            using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                            {
                                using (SqlCommand command = new SqlCommand(sql2.ToString(), conn))
                                {
                                    string updateDeliveryRequestsparameter = JsonConvert.SerializeObject(new { driver_code, check_number, scan_item, signType, sign_form_image_name, sign_field_image_name });
                                    JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "InstertSignImageParameter", updateDeliveryRequestsparameter);
                                    conn.Open();
                                    command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                                    command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                                    command.Parameters.Add("@signType", SqlDbType.VarChar).Value = signType;
                                    command.Parameters.Add("@sign_form_image", SqlDbType.VarChar).Value = sign_form_image_name;
                                    command.Parameters.Add("@sign_field_image", SqlDbType.VarChar).Value = sign_field_image_name;

                                    SqlDataAdapter da = new SqlDataAdapter(command);
                                    da.SelectCommand.CommandTimeout = db_Timeout;
                                    da.Fill(dtAppRequest);
                                    conn.Close();


                                    JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "InstertSignImageParameter", "End");
                                }
                            }
                            //更新整張託運單圖
                            JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "Updatetcsign_form_image", "Strat");
                            UpdatetcttDeliveryScanLogsign_form_imageBYsign_field_image(check_number, sign_field_image_name, sign_form_image_name);
                            JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "Updatetcsign_form_image", "End");
                            break;
                        case 3:
                            JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "sign_field_image", sign_field_image);
                            //寫入簽名圖檔
                            SaveImgFromBase64(sign_field_image, sign_field_image_name, upload_time);
                            SaveImgFromBase64(sign_field_image, sign_form_image_name, upload_time);
                            //Base64ToPNG(sign_field_image, sign_field_image_name);
                            JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "sign_field_image", "End");

                            //更新簽名檔
                            using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                            {
                                StringBuilder sql = new StringBuilder();
                                sql.Append(" insert into ttDeliveryScanLog (driver_code, check_number, scan_item, scan_date, arrive_option, sign_field_image,sign_form_image, area_arrive_code , plates) values (@driver_code, @check_number, @scan_item, @scan_date, @arrive_option, @sign_field_image,@sign_form_image,@area_arrive_code , @plates)");

                                using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                                {
                                    string updateDeliveryRequestsparameter = JsonConvert.SerializeObject(new { driver_code, check_number, scan_item, scan_date, arrive_option, sign_field_image_name, area_arrive_code, _plates });
                                    JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "InsterttDeliveryScanLogsparameter_4", updateDeliveryRequestsparameter);
                                    conn.Open();
                                    command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                                    command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                                    command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                                    command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                                    command.Parameters.Add("@arrive_option", SqlDbType.VarChar).Value = arrive_option;
                                    command.Parameters.Add("@sign_form_image", SqlDbType.VarChar).Value = sign_form_image_name;//只會有一張完整所以都填一樣
                                    command.Parameters.Add("@sign_field_image", SqlDbType.VarChar).Value = sign_field_image_name;
                                    command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;
                                    command.Parameters.Add("@plates", SqlDbType.Int).Value = _plates;

                                    SqlDataAdapter da = new SqlDataAdapter(command);
                                    da.SelectCommand.CommandTimeout = db_Timeout;
                                    da.Fill(dtAppRequest);
                                    conn.Close();


                                    JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "InsterttDeliveryScanLogsparameter_4", "End");
                                }

                            }

                            using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                            {
                                StringBuilder sql = new StringBuilder();
                                sql.Append(" insert into ttDeliveryScanLog2 (driver_code, check_number, scan_item, scan_date, arrive_option, sign_field_image,sign_form_image, area_arrive_code , plates,Work_Station_Scode,Work_Send_Code,Work_Delivery_Code) values (@driver_code, @check_number, @scan_item, @scan_date, @arrive_option, @sign_field_image,@sign_form_image,@area_arrive_code , @plates,@Work_Station_Scode,@Work_Send_Code,@Work_Delivery_Code)");

                                using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                                {
                                    string updateDeliveryRequestsparameter = JsonConvert.SerializeObject(new { driver_code, check_number, scan_item, scan_date, arrive_option, sign_field_image_name, area_arrive_code, _plates });
                                    JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "InsterttDeliveryScanLogsparameter_4", updateDeliveryRequestsparameter);
                                    conn.Open();
                                    command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                                    command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                                    command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                                    command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                                    command.Parameters.Add("@arrive_option", SqlDbType.VarChar).Value = arrive_option;
                                    command.Parameters.Add("@sign_form_image", SqlDbType.VarChar).Value = sign_form_image_name;//只會有一張完整所以都填一樣
                                    command.Parameters.Add("@sign_field_image", SqlDbType.VarChar).Value = sign_field_image_name;
                                    command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;
                                    command.Parameters.Add("@plates", SqlDbType.Int).Value = _plates;

                                    command.Parameters.Add("@Work_Station_Scode", SqlDbType.VarChar).Value = Work_Station_Scode;
                                    command.Parameters.Add("@Work_Send_Code", SqlDbType.VarChar).Value = Work_Send_Code;
                                    command.Parameters.Add("@Work_Delivery_Code", SqlDbType.VarChar).Value = Work_Delivery_Code;

                                    SqlDataAdapter da = new SqlDataAdapter(command);
                                    da.SelectCommand.CommandTimeout = db_Timeout;
                                    da.Fill(dtAppRequest);
                                    conn.Close();


                                    JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "InsterttDeliveryScanLogsparameter_4", "End");
                                }

                            }
                            //簽單存入SignImage表
                            JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "InstertSignImageParameter", "Start");
                            StringBuilder sql3 = new StringBuilder();
                            sql3.Append(@" INSERT INTO SignImage (DriverCode, CheckNumber, SignType, SignFormImage, SignFieldImage, cdate) 
                                VALUES  (@driver_code, @check_number, @signType, @sign_form_image, @sign_field_image, GETDATE() )");
                            using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                            {
                                using (SqlCommand command = new SqlCommand(sql3.ToString(), conn))
                                {
                                    string updateDeliveryRequestsparameter = JsonConvert.SerializeObject(new { driver_code, check_number, scan_item, signType, sign_form_image_name, sign_field_image_name });
                                    JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "InstertSignImageParameter", updateDeliveryRequestsparameter);
                                    conn.Open();
                                    command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                                    command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                                    command.Parameters.Add("@signType", SqlDbType.VarChar).Value = signType;
                                    command.Parameters.Add("@sign_form_image", SqlDbType.VarChar).Value = sign_form_image_name;
                                    command.Parameters.Add("@sign_field_image", SqlDbType.VarChar).Value = sign_field_image_name;

                                    SqlDataAdapter da = new SqlDataAdapter(command);
                                    da.SelectCommand.CommandTimeout = db_Timeout;
                                    da.Fill(dtAppRequest);
                                    conn.Close();
                                    JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "InstertSignImageParameter", "End");
                                }
                            }

                            break;
                        default:
                            return JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = "系統異常", serverdate = serverdate, sign_form_image_url, sign_field_image_url }, Formatting.Indented);


                    }


                }
            }

            Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
            //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
            //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
            timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
            string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, serverdate = serverdate, sign_form_image_url, sign_field_image_url }, Formatting.Indented);

            JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "strAppRequest", strAppRequest);
            JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "End", "OK");

            return strAppRequest;
        }
        catch (Exception e)
        {
            JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "Exception Message", e.Message);
            JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "Exception StackTrace", e.StackTrace);
            JubFuAPPWebServiceLog(sessionid, "sendUploadData9", "Exception Source", e.Source);

            string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = e.Message, serverdate = serverdate }, Formatting.Indented);
            return strAppRequest;
        }
    }
    #endregion

    /// <summary>
    /// APP手機訊息推撥
    /// </summary>
    /// <param name="work_Station_Scode">作業站所</param>
    /// <param name="work_Send_code">集區</param>
    /// <param name="work_Delivery_Code">配區</param>
    /// <param name="driver_code">帳號</param>
    /// <param name="TITLE">推播表頭</param>
    /// <param name="sendValue">文字訊息</param>
    [WebMethod(Description = "APP手機訊息推撥", EnableSession = true)]
    public void AppMsgPush(string work_Station_Scode, string work_Send_code, string work_Delivery_Code, string driver_code, string TITLE = "行動派遣", string sendValue = "")
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        string str = "";

        //作業站所、集區、配區、帳號 判定
        using (SqlCommand cmd = new SqlCommand())
        {
            cmd.Parameters.AddWithValue("@work_Station_Scode", work_Station_Scode);
            cmd.Parameters.AddWithValue("@work_Send_code", work_Send_code);
            cmd.Parameters.AddWithValue("@work_Delivery_Code", work_Delivery_Code);
            cmd.Parameters.AddWithValue("@driver_code", driver_code);
            cmd.CommandText = @"
select driver_code,mobiletoken,mobiletype,Work_Station_Scode,Work_Send_Code,Work_Delivery_Code  
from tbDrivers 
where 1=1 
and  (@driver_code = '' or driver_code = @driver_code)
and  (@work_Station_Scode = '' or work_Station_Scode = @work_Station_Scode)
and  (@work_Send_code = '' or work_Send_code = @work_Send_code)
and  (@work_Delivery_Code = '' or work_Delivery_Code = @work_Delivery_Code)";
            dt = dbAdapter.getDataTable(cmd);
        }

        System.Collections.Hashtable Res = new System.Collections.Hashtable();
        if (dt.Rows.Count > 0)
        {
            Res.Add("Result", true);
            Res.Add("ReturnMsg", "");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var value = "";
                if (sendValue == "")
                {

                    value = string.Format("您有新任務，請至行動派遣查看。");
                }
                else
                {
                    value = sendValue;
                }

                if (TITLE == "")
                {
                    TITLE = "行動派遣";
                }

                using (SqlCommand cmd = new SqlCommand())
                {
                    int request_id = 0;
                    //cmd.CommandText = strSQL;
                    cmd.Parameters.AddWithValue("@user_no", dt.Rows[i]["driver_code"].ToString());
                    cmd.Parameters.AddWithValue("@mobiletoken", dt.Rows[i]["mobiletoken"].ToString().ToString());
                    cmd.Parameters.AddWithValue("@mobiletype", dt.Rows[i]["mobiletype"].ToString().ToString());
                    cmd.Parameters.AddWithValue("@title", TITLE);
                    cmd.Parameters.AddWithValue("@msg", value);
                    cmd.Parameters.AddWithValue("@cdt", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    cmd.CommandText = dbAdapter.genInsertComm("ttPushMsg", true, cmd);

                    if (int.TryParse(dbAdapter.getScalarBySQL(cmd).ToString(), out request_id))
                    {
                        if (dt.Rows[i]["mobiletoken"].ToString() != "")
                        {
                            HttpWebRequest tRequest;
                            tRequest = (HttpWebRequest)WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                            tRequest.Method = "post";
                            tRequest.ContentType = "application/json";

                            Byte[] byteArray = null;

                            #region 推撥TOKEN
                            string SERVER_API_KEY = "";
                            string SENDER_ID = "";
                            SERVER_API_KEY = chimeiAdmin_Info.ANDROID_SERVER_API_KEY;//
                            SENDER_ID = chimeiAdmin_Info.ANDROID_SENDER_ID;//
                                                                           //TITLE = "新指派任務";
                            var data = new
                            {
                                registration_ids = new string[] { dt.Rows[i]["mobiletoken"].ToString() },
                                data = new
                                {
                                    body = value,
                                    title = TITLE,
                                    sound = "Enabled",
                                    news_id = request_id
                                },

                            };

                            var serializer = new JavaScriptSerializer();
                            var json = serializer.Serialize(data);
                            byteArray = Encoding.UTF8.GetBytes(json);
                            tRequest.Headers.Add(string.Format("Authorization: key={0}", SERVER_API_KEY));
                            tRequest.Headers.Add(string.Format("Sender: id={0}", SENDER_ID));
                            tRequest.ContentLength = byteArray.Length;
                            #endregion

                            try
                            {
                                using (Stream dataStream = tRequest.GetRequestStream())
                                {
                                    dataStream.Write(byteArray, 0, byteArray.Length);
                                    using (HttpWebResponse tResponse = (HttpWebResponse)tRequest.GetResponse())
                                    {
                                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                                        {
                                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                                            {
                                                String sResponseFromServer = tReader.ReadToEnd();
                                                str = sResponseFromServer;
                                            }
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Res["Result"] = false;
                                Res["ReturnMsg"] = ex.ToString();
                                DoContextResponse(DoHashTableToString(Res));
                            }
                        }
                        else
                        {
                            Res["Result"] = true;
                            Res["ReturnMsg"] = "";
                            DoContextResponse(DoHashTableToString(Res));
                        }
                    }
                }

                //using (SqlCommand cmd = new SqlCommand())
                //{
                //    if (TableName == "")
                //    {
                //        TableName = "DriverSendPrepare";
                //    }
                //    cmd.Parameters.AddWithValue("@driver_code", driver_code);
                //    cmd.Parameters.AddWithValue("@sdt", DateTime.Now.AddMinutes(-5).ToString("yyyy-MM-dd HH:mm:ss"));
                //    cmd.Parameters.AddWithValue("@edt", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                //    cmd.CommandText = string.Format(@"select * from {0} where driver_code = @driver_code and cdate>=@sdt and cdate<=@edt", TableName);
                //    dt1 = dbAdapter.getDataTable(cmd);
                //}
                //if (dt1.Rows.Count > 0)
                //{
                //    var value = "";
                //    if (sendValue == "")
                //    {

                //         value = string.Format("您有 {0} 條新任務，請至行動派遣查看。", dt1.Rows.Count);
                //    }
                //    else {

                //         value = sendValue;
                //    }
                //    int request_id = 0;
                //    //string strSQL = "";
                //    //strSQL += string.Format(@"Insert Into ttPushMsg (user_no, mobiletoken, mobiletype, title, msg, cdt) values ('{0}','{1}','{2}','{3}','{4}','{5}'); "
                //    //                      , dt.Rows[i]["driver_code"].ToString()
                //    //                      , dt.Rows[i]["mobiletoken"].ToString().ToString()
                //    //                      , dt.Rows[i]["mobiletype"].ToString().ToString()
                //    //                      , "新指派任務"
                //    //                      , string.Format("託運單號：{0} 指派給您，請至配送派遣查看。", check_number)
                //    //                      , DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
                //    //                      );
                //    using (SqlCommand cmd = new SqlCommand())
                //    {
                //        //cmd.CommandText = strSQL;
                //        cmd.Parameters.AddWithValue("@user_no", dt.Rows[i]["driver_code"].ToString());
                //        cmd.Parameters.AddWithValue("@mobiletoken", dt.Rows[i]["mobiletoken"].ToString().ToString());
                //        cmd.Parameters.AddWithValue("@mobiletype", dt.Rows[i]["mobiletype"].ToString().ToString());
                //        cmd.Parameters.AddWithValue("@title", "新指派任務");                               //建立人員
                //        cmd.Parameters.AddWithValue("@msg", value);                                          //建立時間
                //        cmd.Parameters.AddWithValue("@cdt", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));                               //更新人員
                //        cmd.CommandText = dbAdapter.genInsertComm("ttPushMsg", true, cmd);

                //        if (int.TryParse(dbAdapter.getScalarBySQL(cmd).ToString(), out request_id))
                //        {
                //            WebRequest tRequest;
                //            tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                //            tRequest.Method = "post";
                //            tRequest.ContentType = "application/json";

                //            Byte[] byteArray = null;

                //            #region 推撥TOKEN
                //            string SERVER_API_KEY = "";
                //            string SENDER_ID = "";
                //            string TITLE = "";

                //            SERVER_API_KEY = chimeiAdmin_Info.ANDROID_SERVER_API_KEY;//
                //            SENDER_ID = chimeiAdmin_Info.ANDROID_SENDER_ID;//
                //            TITLE = "新指派任務";


                //            var data = new
                //            {
                //                registration_ids = new string[] { dt.Rows[i]["mobiletoken"].ToString().ToString() },
                //                data = new
                //                {
                //                    body = value,
                //                    title = TITLE,
                //                    sound = "Enabled",
                //                    news_id = request_id
                //                },

                //            };

                //            var serializer = new JavaScriptSerializer();
                //            var json = serializer.Serialize(data);
                //            byteArray = Encoding.UTF8.GetBytes(json);
                //            tRequest.Headers.Add(string.Format("Authorization: key={0}", SERVER_API_KEY));
                //            tRequest.Headers.Add(string.Format("Sender: id={0}", SENDER_ID));
                //            tRequest.ContentLength = byteArray.Length;
                //            #endregion

                //            try
                //            {
                //                using (Stream dataStream = tRequest.GetRequestStream())
                //                {
                //                    dataStream.Write(byteArray, 0, byteArray.Length);
                //                    using (WebResponse tResponse = tRequest.GetResponse())
                //                    {
                //                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                //                        {
                //                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                //                            {
                //                                String sResponseFromServer = tReader.ReadToEnd();
                //                                str = sResponseFromServer;
                //                            }
                //                        }
                //                    }
                //                }
                //            }
                //            catch (Exception ex)
                //            {
                //                Res["Result"] = false;
                //                Res["ReturnMsg"] = ex.ToString();
                //                DoContextResponse(DoHashTableToString(Res));
                //            }
                //        }
                //    }
                //}
            }
            Res["Result"] = true;
            Res["ReturnMsg"] = str;

            DoContextResponse(DoHashTableToString(Res));
        }
        else
        {
            Res.Add("Result", true);
            Res.Add("ReturnMsg", "");
            DoContextResponse(DoHashTableToString(Res));
        }

    }

    #region delivery request 資料修改
    /// <summary>
    /// 掃讀集貨前先選擇貨件種類
    /// </summary>
    /// <returns></returns>
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string SelectPickUpType(string checkNumber, string pickUpType)
    {
        bool resultCode = true;
        string errorMessage = string.Empty;

        using (SqlConnection connection = new SqlConnection(db_junfu_sql_conn_str))
        {
            string sql = "update tcDeliveryRequests set pick_up_good_type = @pickUpType where check_number = @checkNumber";
            using (SqlCommand command = new SqlCommand(sql, connection))
            {
                command.Parameters.AddWithValue("@pickUpType", pickUpType);
                command.Parameters.AddWithValue("@checkNumber", checkNumber);

                try
                {
                    connection.Open();
                    int rowAffected = command.ExecuteNonQuery();
                    if (rowAffected == 0)
                    {
                        errorMessage = "託運單號不存在資料庫中";
                    }
                }
                catch (Exception ex)
                {
                    resultCode = false;
                    errorMessage = ex.Message;
                }
            }
        }

        string response = JsonConvert.SerializeObject(new { resultcode = resultCode, resultdesc = errorMessage });
        return response;
    }

    /// <summary>
    /// 存運價和集貨袋種
    /// </summary>
    /// <param name="checkNumber"></param>
    /// <param name="freight"></param>
    /// <param name="pickUpType"></param>
    /// <returns></returns>
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string SaveFreightAndType(string checkNumber, int freight, string pickUpType)
    {
        bool resultCode = true;
        string errorMessage = string.Empty;

        using (SqlConnection connection = new SqlConnection(db_junfu_sql_conn_str))
        {
            string sql = "update tcDeliveryRequests set pick_up_good_type = @pickUpType, freight = @freight where check_number = @checkNumber";
            using (SqlCommand command = new SqlCommand(sql, connection))
            {
                command.Parameters.AddWithValue("@pickUpType", pickUpType);
                command.Parameters.AddWithValue("@checkNumber", checkNumber);
                command.Parameters.AddWithValue("@freight", freight);

                try
                {
                    connection.Open();
                    int rowAffected = command.ExecuteNonQuery();
                    if (rowAffected == 0)
                    {
                        errorMessage = "託運單號不存在資料庫中";
                    }
                }
                catch (Exception ex)
                {
                    resultCode = false;
                    errorMessage = ex.Message;
                }
            }
        }

        string response = JsonConvert.SerializeObject(new { resultcode = resultCode, resultdesc = errorMessage });
        return response;
    }

    /// <summary>
    /// 修改付款方式
    /// </summary>
    /// <param name="checkNumber"></param>
    /// <param name="paidMethod"></param>
    /// <returns></returns>
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string UpdatePaidMethod(string checkNumber, string paidMethod)
    {
        bool resultCode = true;
        string errorMessage = string.Empty;

        using (SqlConnection connection = new SqlConnection(db_junfu_sql_conn_str))
        {
            string sql = "update tcDeliveryRequests set payment_method = @paidMethod where check_number = @checkNumber";
            using (SqlCommand command = new SqlCommand(sql, connection))
            {
                command.Parameters.AddWithValue("@paidMethod", paidMethod);
                command.Parameters.AddWithValue("@checkNumber", checkNumber);

                try
                {
                    connection.Open();
                    int rowAffected = command.ExecuteNonQuery();
                    if (rowAffected == 0)
                    {
                        errorMessage = "託運單號不存在資料庫中";
                    }
                }
                catch (Exception ex)
                {
                    resultCode = false;
                    errorMessage = ex.Message;
                }
            }
        }

        string response = JsonConvert.SerializeObject(new { resultcode = resultCode, resultdesc = errorMessage });
        return response;
    }

    #endregion


    public class Data
    {
        public string checknumber { get; set; }
        public string scanname { get; set; }
    }
    /// <summary>
    /// 取得司機當日上傳資訊
    /// </summary>
    /// <param name="driver_code"></param>
    /// <returns></returns>
    [WebMethod]
    public string getUploadData(string driver_code)
    {
        writeLog("call", "getUploadData", driver_code, "");
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dt = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;
        List<Data> uploaddatas = new List<Data>();

        if (driver_code.Equals(""))
        {
            resultcode = false;
            error_msg = "司機工號未傳入";
        }
        else
        {

            try
            {
                using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                {

                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = conn;
                        command.CommandText = string.Format(@"SELECT check_number, scan_item,
                                                              CASE scan_item WHEN '1' THEN '到著' WHEN '2' THEN '配送' WHEN '3' THEN '配達'  WHEN '4' THEN '簽單' WHEN '5' THEN '集貨' WHEN '6' THEN '卸集' WHEN '7' THEN '發送'   END AS scan_name
                                                              FROM ttDeliveryScanLog where check_number <> '' and driver_code = @driver_code and Convert(nvarchar, scan_date,111) = @scan_date
                                                              ORDER BY scan_date");
                        command.Parameters.Add("@driver_code", SqlDbType.NVarChar, 10).Value = driver_code;
                        command.Parameters.Add("@scan_date", SqlDbType.DateTime).Value = System.DateTime.Now.ToString("yyyy/MM/dd");
                        conn.Open();


                        SqlDataAdapter da = new SqlDataAdapter(command);
                        da.SelectCommand.CommandTimeout = db_Timeout;
                        da.Fill(dt);

                        if (dt != null && dt.Rows.Count > 0)
                        {
                            for (int i = 0; i <= dt.Rows.Count - 1; i++)
                            {
                                Data uploaddata = new Data();
                                uploaddata.checknumber = dt.Rows[i]["check_number"].ToString();
                                uploaddata.scanname = dt.Rows[i]["scan_name"].ToString();
                                uploaddatas.Add(uploaddata);
                            }
                        }
                        else
                        {
                            resultcode = false;
                            error_msg = "查無上傳資料";
                        }

                        conn.Close();

                    }
                }
                writeLog("ref", "getUploadData", driver_code, "");
            }
            catch (Exception e)
            {
                resultcode = false;
                error_msg = e.Message;
                writeLog("err", "getUploadData", driver_code, e.Message);
            }
            finally
            {
            }


        }

        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, uploaddatas = uploaddatas }, Formatting.Indented);
        return strAppRequest;
    }

    [WebMethod]
    public string checkUloadData(string scan_item, string check_number, string scan_date)
    {
        writeLog("call", "checkUloadData", scan_item + "_" + check_number + "_" + scan_date, "");
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dt = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;

        if (scan_item.Equals(""))
        {
            resultcode = false;
            error_msg = "作業類別未傳入";
        }
        else if (check_number.Equals(""))
        {
            resultcode = false;
            error_msg = "託運單號未傳入";
        }
        else if (scan_date.Equals(""))
        {
            resultcode = false;
            error_msg = "掃讀時間未傳入";
        }
        else
        {

            try
            {
                using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                {

                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = conn;
                        command.CommandText = string.Format(@"SELECT count(*) as cnt FROM ttDeliveryScanLog where scan_item = @scan_item  and  check_number =@check_number  and scan_date= @scan_date
                                                              and cdate >= CONVERT(varchar,DATEADD (MONTH,-3 ,getdate()),121)");
                        command.Parameters.Add("@scan_item", SqlDbType.Char, 1).Value = scan_item;
                        command.Parameters.Add("@check_number", SqlDbType.NVarChar, 20).Value = check_number;
                        command.Parameters.Add("@scan_date", SqlDbType.DateTime).Value = scan_date;
                        conn.Open();


                        SqlDataAdapter da = new SqlDataAdapter(command);
                        da.SelectCommand.CommandTimeout = db_Timeout;
                        da.Fill(dt);

                        if (dt != null && dt.Rows.Count > 0)
                        {
                            int cnt = Convert.ToInt32(dt.Rows[0]["cnt"]);
                            if (cnt > 0)
                            {
                                resultcode = true;
                                error_msg = "";
                            }
                            else
                            {
                                resultcode = false;
                                using (SqlCommand command2 = new SqlCommand())
                                {
                                    command2.Connection = conn;
                                    command2.CommandText = string.Format(@"select top 1  app_log  from ttAppLog where app_type = @app_type  and  app_memo like '%'+@check_number+'%'  
                                                                         and cdate >= CONVERT(varchar,DATEADD (MONTH,-3 ,getdate()),121)");
                                    command2.Parameters.Add("@app_type", SqlDbType.VarChar, 10).Value = "err";
                                    command2.Parameters.Add("@check_number", SqlDbType.VarChar, 20).Value = check_number;
                                    DataTable dt2 = new DataTable();
                                    SqlDataAdapter da2 = new SqlDataAdapter(command2);
                                    da2.SelectCommand.CommandTimeout = db_Timeout;
                                    da2.Fill(dt2);
                                    if (dt2 != null && dt2.Rows.Count > 0)
                                    {
                                        error_msg = "上傳失敗記錄：" + dt2.Rows[0]["app_log"].ToString();
                                    }
                                    else
                                    {
                                        error_msg = "查無上傳資料";
                                    }

                                }
                            }
                        }
                        else
                        {
                            resultcode = false;
                            error_msg = "查無上傳資料";
                        }

                        conn.Close();

                    }
                }
                writeLog("ref", "checkUloadData", scan_item + "_" + check_number + "_" + scan_date, "");
            }
            catch (Exception e)
            {
                resultcode = false;
                error_msg = e.Message;
                writeLog("err", "checkUloadData", scan_item + "_" + check_number + "_" + scan_date, e.Message);
            }
        }

        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, serverdate = serverdate }, Formatting.Indented);
        return strAppRequest;
    }

    /// <summary>
    /// 6. 取得所有站所
    /// </summary>
    /// <returns></returns>
    [WebMethod]
    public string getSupplierStation()
    {
        writeLog("call", "getSupplierStation", "", "");
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dt = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;

        try
        {
            using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
            {

                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = conn;
                    command.CommandText = string.Format(@"select supplier_code , supplier_name  from tbSuppliers where active_flag  = 1  order by supplier_code ");
                    conn.Open();

                    SqlDataAdapter da = new SqlDataAdapter(command);
                    da.SelectCommand.CommandTimeout = db_Timeout;
                    da.Fill(dt);

                    if (dt == null && dt.Rows.Count == 0)
                    {
                        resultcode = false;
                        error_msg = "查無站所資料";
                    }

                    conn.Close();

                }
            }
            writeLog("ref", "getSupplierStation", "", "");
        }
        catch (Exception e)
        {
            resultcode = false;
            error_msg = e.Message;
            writeLog("err", "getSupplierStation", "", e.Message);
        }


        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, stations = dt }, Formatting.Indented);
        return strAppRequest;
    }



    /// <summary>
    /// 回傳座標位置
    /// </summary>
    /// <param name="driver_id">司機編號</param>
    /// <param name="X">經度(wgx)</param>
    /// <param name="Y">緯度(wgx)</param>
    /// <returns></returns>
    [WebMethod]
    public string sendXYLocation(string driver_code, string X, string Y)
    {
        writeLog("call", "sendXYLocation", driver_code + " " + X + " " + Y, "");
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dt = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;
        double x;
        double y;

        if (driver_code.Equals(""))
        {
            resultcode = false;
            error_msg = "司機工號未傳入";
        }
        else if (X.Equals("") || !double.TryParse(X, out x))
        {
            resultcode = false;
            error_msg = "請輸X座標";
        }
        else if (Y.Equals("") || !double.TryParse(Y, out y))
        {
            resultcode = false;
            error_msg = "請輸Y座標";
        }
        else
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                {

                    using (SqlCommand command = new SqlCommand())
                    {
                        command.Connection = conn;
                        command.CommandText = "uspApp_SendLocation";
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@X", X);
                        command.Parameters.AddWithValue("@Y", Y);
                        command.Parameters.AddWithValue("@driver_code", driver_code);
                        conn.Open();

                        SqlDataAdapter da = new SqlDataAdapter(command);
                        da.SelectCommand.CommandTimeout = db_Timeout;
                        da.Fill(dt);


                        if (dt != null && dt.Rows.Count > 0)
                        {
                            if (Convert.ToBoolean(dt.Rows[0][0]) == false)
                            {
                                resultcode = false;
                            }
                            error_msg = dt.Rows[0][1].ToString();

                        }
                        else
                        {
                            resultcode = false;
                            error_msg = "回傳座標位置失敗";
                        }

                        conn.Close();

                    }
                }
                writeLog("ref", "sendXYLocation", driver_code + " " + X + " " + Y, "");
            }
            catch (Exception e)
            {
                resultcode = false;
                error_msg = e.Message;
                writeLog("err", "sendXYLocation", driver_code + " " + X + " " + Y, e.Message);
            }
        }



        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg }, Formatting.Indented);
        return strAppRequest;
    }

    /// <summary>
    /// 回傳幹線運輸起/迄時間
    /// </summary>
    /// <param name="car_license">車牌</param>
    /// <param name="tel">司機電話</param>
    /// <param name="driver_name">司機姓名</param>
    /// <param name="sdate">開始時間</param>
    /// <param name="edate">結束時間</param>
    /// <returns></returns>
    [WebMethod]
    public string send_dispatch(string car_license, string tel, string driver_name, string sdate, string edate)
    {
        writeLog("call", "send_dispatch", "car_license:" + car_license + "; tel: " + tel + "; driver_name:" + driver_name + "; sdate:" + sdate + "; edate:" + edate, "");
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dt = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;

        if (car_license.Equals(""))
        {
            resultcode = false;
            error_msg = "車牌未傳入";
        }
        //else if (tel.Equals(""))
        //{
        //    resultcode = false;
        //    error_msg = "司機電話未傳入";
        //}
        else if (sdate.Equals("") && edate.Equals(""))
        {
            resultcode = false;
            error_msg = "請輸入開始派遣或結束派遣時間";
        }
        else
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Parameters.Add("@car_license", SqlDbType.NVarChar, 10).Value = car_license;
                    cmd.Parameters.Add("@tel", SqlDbType.NVarChar, 10).Value = tel;
                    cmd.Parameters.Add("@driver_name", SqlDbType.NVarChar, 10).Value = driver_name;
                    if (sdate != "")
                    {
                        cmd.Parameters.Add("@sdate", SqlDbType.DateTime).Value = sdate;
                    }
                    else
                    {
                        cmd.Parameters.Add("@edate", SqlDbType.DateTime).Value = edate;
                    }
                    cmd.CommandText = dbAdapter.SQLdosomething("ttTrunkTransportation", cmd, "insert");

                    try
                    {
                        dbAdapter.execNonQuery(cmd);
                    }
                    catch (Exception e)
                    {
                        resultcode = false;
                        error_msg = e.Message;
                        writeLog("err", "send_dispatch", "car_license:" + car_license + "; tel: " + tel + "; driver_name:" + driver_name + "; sdate:" + sdate + "; edate:" + edate, e.Message);
                    }
                }
                writeLog("ref", "send_dispatch", "car_license:" + car_license + "; tel: " + tel + "; driver_name:" + driver_name + "; sdate:" + sdate + "; edate:" + edate, "");
            }
            catch (Exception e)
            {
                resultcode = false;
                error_msg = e.Message;
                writeLog("err", "send_dispatch", "car_license:" + car_license + "; tel: " + tel + "; driver_name:" + driver_name + "; sdate:" + sdate + "; edate:" + edate, e.Message);
            }
        }

        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg }, Formatting.Indented);
        return strAppRequest;
    }

    /// <summary>
    /// 取得派遣任務總表
    /// </summary>
    /// <param name="car_license">車牌編號</param>
    /// <returns></returns>
    [WebMethod]
    public string get_dispatchstatistics(string car_license)
    {
        writeLog("call", "get_dispatchstatistics", "car_license:" + car_license, "");
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dt = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;
        List<DispatchStatistics> _items = new List<DispatchStatistics>();


        if (car_license.Equals(""))
        {
            resultcode = false;
            error_msg = "車牌未傳入";
        }
        else
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Parameters.Add("@car_license", SqlDbType.NVarChar, 10).Value = car_license;
                    cmd.CommandText = string.Format(@"Select b.code_id 'task_type' , b.code_name 'type_name', COALESCE(a.cnt,0)   'cnt'
                                                        From tbItemCodes b with(nolock) 
                                                        LEFT OUTER JOIN (select task_type, count(*) cnt  from ttDispatchTask where REPLACE ( car_license,'-','')  =  REPLACE(@car_license, '-','') group  by task_type) a on  a.task_type = b.code_id 
                                                        Where  b.code_bclass = '6' and b.code_sclass = 'dispatch_type'");
                    try
                    {
                        dt = dbAdapter.getDataTable(cmd);
                        _items = DataTableExtensions.ToList<DispatchStatistics>(dt).ToList();
                    }
                    catch (Exception e)
                    {
                        resultcode = false;
                        error_msg = e.Message;
                        writeLog("err", "get_dispatchstatistics", "car_license:" + car_license, e.Message);
                    }
                }
                writeLog("ref", "get_dispatchstatistics", "car_license:" + car_license, "");
            }
            catch (Exception e)
            {
                resultcode = false;
                error_msg = e.Message;
                writeLog("err", "get_dispatchstatistics", "car_license:" + car_license, e.Message);
            }
        }

        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, tasks = _items }, Formatting.Indented);
        return strAppRequest;
    }

    /// <summary>
    /// 取得派遣任務明細表
    /// </summary>
    /// <param name="car_license">車牌編號</param>
    /// <param name="task_type">派遣類型</param>
    /// <returns></returns>
    [WebMethod]
    public string get_dispatchtask(string car_license, string task_type)
    {
        writeLog("call", "get_dispatchtask", "car_license:" + car_license + ", task_type:" + task_type, "");
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dt = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;
        List<DispatchTask> _items = new List<DispatchTask>();


        if (car_license.Equals(""))
        {
            resultcode = false;
            error_msg = "車牌未傳入";
        }
        else if (task_type.Equals(""))
        {
            resultcode = false;
            error_msg = "派遣類型未傳入";
        }
        else
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Parameters.Add("@car_license", SqlDbType.NVarChar, 10).Value = car_license;
                    cmd.Parameters.Add("@task_type", SqlDbType.NVarChar, 10).Value = task_type;
                    cmd.CommandText = string.Format(@"select a.t_id  'task_id', a.task_type , b.code_name 'type_name', CONVERT(varchar(100), a.task_date, 111) 'task_date' , a.supplier_no, ISNULL(c.supplier_shortname,'')  'supplier_shortname' ,
                                                        a.driver , a.car_license , isnull(a.memo,'') 'memo'
                                                        from ttDispatchTask a with(nolock) 
                                                        left join tbItemCodes b with(nolock) on  a.task_type   = b.code_id  and b.code_bclass = '6' and b.code_sclass = 'dispatch_type'
                                                        left join tbSuppliers c with(nolock) on a.supplier_no = c.supplier_no  and c.active_flag = 1 
                                                        where REPLACE ( car_license,'-','')  =  REPLACE(@car_license, '-','') 
                                                        and a.task_type =@task_type");

                    try
                    {
                        dt = dbAdapter.getDataTable(cmd);
                        _items = DataTableExtensions.ToList<DispatchTask>(dt).ToList();
                    }
                    catch (Exception e)
                    {
                        resultcode = false;
                        error_msg = e.Message;
                        writeLog("err", "get_dispatchtask", "car_license:" + car_license + ", task_type:" + task_type, e.Message);
                    }
                }
                writeLog("ref", "get_dispatchtask", "car_license:" + car_license + ", task_type:" + task_type, "");
            }
            catch (Exception e)
            {
                resultcode = false;
                error_msg = e.Message;
                writeLog("err", "get_dispatchtask", "car_license:" + car_license + ", task_type:" + task_type, e.Message);
            }
        }

        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, tasks = _items }, Formatting.Indented);
        return strAppRequest;
    }

    /// <summary>
    /// 取得派遣任務的託運單明細
    /// </summary>
    /// <param name="car_license">派遣任務編號</param>
    /// <returns></returns>
    [WebMethod]
    public string get_dispatchtask_detail(string task_id)
    {
        writeLog("call", "get_dispatchtask_detail", "task_id:" + task_id, "");
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dt = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;
        List<DispatchTaskDetail> _items = new List<DispatchTaskDetail>();


        if (task_id.Equals("") || !IsNumeric(task_id))
        {
            resultcode = false;
            error_msg = "請輸入正確的任務編號";
        }
        else
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.Parameters.Add("@t_id", SqlDbType.Int).Value = task_id;
                    cmd.CommandText = string.Format(@"Select a.t_id 'task_id', E.code_name 'pricing_name' , CONVERT(varchar(100), B.print_date, 120) 'print_date' , B.check_number  , B.receive_contact ,
	                                                           --CASE WHEN B.receive_by_arrive_site_flag = '1' then B.arrive_address else B.receive_city +  B.receive_area + B.receive_address end 'receive_address',
	                                                           B.send_contact , B.send_city + B.send_area + B.send_address 'send_address' ,
	                                                           B.pieces , B.plates , B.cbm ,
	                                                           B.customer_code , D.customer_shortname ,
	                                                           B.supplier_code, F.supplier_name 'supplier_name', B.area_arrive_code , G.supplier_name 'area_arrive_name',
	                                                           info.status_5, info.status_6
                                                        from ttDispatchTaskDetail A with(nolock) 
                                                        left join tcDeliveryRequests B with(nolock) on A.request_id = B.request_id 
                                                        left join tbCustomers D With(Nolock) on D.customer_code = B.customer_code 
                                                        left join tbItemCodes E with(nolock) on E.code_bclass = '1' and E.code_sclass = 'PM' and  B.pricing_type = E.code_id 
                                                        left join tbSuppliers F with(nolock) on B.supplier_code  = F.supplier_code  and F.active_flag = 1 
                                                        left join tbSuppliers G with(nolock) on B.area_arrive_code = G.supplier_code  and G.active_flag = 1 
                                                        CROSS APPLY dbo.fu_GetDeliveryStatus(B.check_number) info
                                                        Where a.t_id= @t_id
                                                        Order by B.check_number ");
                    try
                    {
                        dt = dbAdapter.getDataTable(cmd);
                        _items = DataTableExtensions.ToList<DispatchTaskDetail>(dt).ToList();
                    }
                    catch (Exception e)
                    {
                        resultcode = false;
                        error_msg = e.Message;
                        writeLog("err", "get_dispatchtask_detail", "task_id:" + task_id, e.Message);
                    }
                }
                writeLog("ref", "get_dispatchtask_detail", "task_id:" + task_id, "");
            }
            catch (Exception e)
            {
                resultcode = false;
                error_msg = e.Message;
                writeLog("err", "get_dispatchtask_detail", "task_id:" + task_id, e.Message);
            }
        }

        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, tasks = _items }, Formatting.Indented);
        return strAppRequest;
    }


    /// <summary>
    /// 派遣任務查詢(已上傳)
    /// </summary>
    /// <param name="car_license">車牌編號</param>
    /// <param name="task_type">派遣類型</param>
    /// <returns></returns>
    [WebMethod]
    public string get_uploaded_dispatchtask(string car_license, string sdate, string edate)
    {
        writeLog("call", "get_uploaded_dispatchtask", "car_license:" + car_license + ", sdate:" + sdate + ", edate:" + edate, "");
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dt = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;
        List<DispatchTask> _items = new List<DispatchTask>();


        if (car_license.Equals(""))
        {
            resultcode = false;
            error_msg = "車牌未傳入";
        }

        else
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    string strWhere = string.Empty;
                    cmd.Parameters.Add("@car_license", SqlDbType.NVarChar, 10).Value = car_license;
                    if (sdate != "" && edate != "")
                    {
                        cmd.Parameters.Add("@sdate", SqlDbType.DateTime).Value = sdate;
                        cmd.Parameters.Add("@edate", SqlDbType.DateTime).Value = Convert.ToDateTime(edate).AddDays(1).ToString("yyyy/MM/dd HH:mm:ss");
                        strWhere = " and A.task_date >= @sdate  and A.task_date <@edate ";
                    }

                    cmd.CommandText = string.Format(@"select a.t_id  'task_id', a.task_type , b.code_name 'type_name', CONVERT(varchar(100), a.task_date, 111) 'task_date' , a.supplier_no, ISNULL(c.supplier_shortname,'')  'supplier_shortname'   , 
                                                        a.driver , a.car_license , isnull(a.memo,'') 'memo'
                                                        from ttDispatchTask a with(nolock) 
                                                        left join tbItemCodes b with(nolock) on  a.task_type   = b.code_id  and b.code_bclass = '6' and b.code_sclass = 'dispatch_type'
                                                        left join tbSuppliers c with(nolock) on a.supplier_no = c.supplier_no  and c.active_flag = 1 
                                                        where REPLACE ( car_license,'-','')  =  REPLACE(@car_license, '-','') AND a.status = 1 
                                                        {0}", strWhere);

                    try
                    {
                        dt = dbAdapter.getDataTable(cmd);
                        _items = DataTableExtensions.ToList<DispatchTask>(dt).ToList();
                    }
                    catch (Exception e)
                    {
                        resultcode = false;
                        error_msg = e.Message;
                        writeLog("err", "get_uploaded_dispatchtask", "car_license:" + car_license + ", sdate:" + sdate + ", edate:" + edate, e.Message);
                    }
                }
                writeLog("ref", "get_uploaded_dispatchtask", "car_license:" + car_license + ", sdate:" + sdate + ", edate:" + edate, "");
            }
            catch (Exception e)
            {
                resultcode = false;
                error_msg = e.Message;
                writeLog("err", "get_uploaded_dispatchtask", "car_license:" + car_license + ", sdate:" + sdate + ", edate:" + edate, e.Message);
            }
        }

        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, cnt = _items.Count, tasks = _items }, Formatting.Indented);
        return strAppRequest;
    }

    /// <summary>
    /// 取得棧板使用的配達區分
    /// </summary>
    /// <returns></returns>
    [WebMethod(Description = "取得棧板使用的配達區分")]
    public string GetPalletArriveOption()
    {
        writeLog("call", "GetPalletArriveOption", "", "");
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dt = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;

        try
        {
            using (SqlCommand cmd = new SqlCommand())
            {

                cmd.CommandText = string.Format(@"SELECT code_id as arrive_option_code , code_name as arrive_option_name
                                                  FROM tbItemCodes
                                                  WHERE (code_bclass = '5') AND (code_sclass = 'AO') AND (active_flag = 1) and code_id = '10' or seq between 99 and 108 ");

                try
                {
                    dt = dbAdapter.getDataTable(cmd);
                    if (dt == null && dt.Rows.Count == 0)
                    {
                        resultcode = false;
                        error_msg = "查無資料";
                    }
                }
                catch (Exception e)
                {
                    resultcode = false;
                    error_msg = e.Message;
                    writeLog("err", "GetPalletArriveOption", "", e.Message);
                }
            }
            writeLog("ref", "GetPalletArriveOption", "", "");
        }
        catch (Exception e)
        {
            resultcode = false;
            error_msg = e.Message;
            writeLog("err", "GetPalletArriveOption", "", e.Message);
        }


        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, arriveOptions = dt }, Formatting.Indented);
        return strAppRequest;
    }

    /// <summary>
    /// 取得異常狀態
    /// </summary>
    /// <returns></returns>
    [WebMethod(Description = "取得異常狀態")]
    public string GetExceptionOption()
    {
        writeLog("call", "GetExceptionOption", "", "");
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dt = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;

        try
        {
            using (SqlCommand cmd = new SqlCommand())
            {

                cmd.CommandText = string.Format(@"SELECT code_id as exception_option_code , code_name as exception_option_name
                                                  FROM tbItemCodes
                                                  WHERE (code_bclass = '5') AND (code_sclass = 'EO') AND (active_flag = 1) ");

                try
                {
                    dt = dbAdapter.getDataTable(cmd);
                    if (dt == null && dt.Rows.Count == 0)
                    {
                        resultcode = false;
                        error_msg = "查無資料";
                    }
                }
                catch (Exception e)
                {
                    resultcode = false;
                    error_msg = e.Message;
                    writeLog("err", "GetExceptionOption", "", e.Message);
                }
            }
            writeLog("ref", "GetExceptionOption", "", "");
        }
        catch (Exception e)
        {
            resultcode = false;
            error_msg = e.Message;
            writeLog("err", "GetExceptionOption", "", e.Message);
        }


        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, exceptionOptions = dt }, Formatting.Indented);
        return strAppRequest;
    }


    /// <summary>
    /// 取得所有倉房
    /// </summary>
    /// <returns></returns>
    [WebMethod]
    public string get_warehouse()
    {
        writeLog("call", "get_warehouse", "", "");
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dt = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;

        try
        {
            using (SqlCommand cmd = new SqlCommand())
            {

                cmd.CommandText = string.Format(@"SELECT code_id as supplier_code , code_name as supplier_name
                                                  FROM tbItemCodes
                                                  WHERE (code_bclass = '1') AND (code_sclass = 'warehouse') AND (active_flag = 1) ");

                try
                {
                    dt = dbAdapter.getDataTable(cmd);
                    if (dt == null && dt.Rows.Count == 0)
                    {
                        resultcode = false;
                        error_msg = "查無倉房資料";
                    }
                }
                catch (Exception e)
                {
                    resultcode = false;
                    error_msg = e.Message;
                    writeLog("err", "get_warehouse", "", e.Message);
                }
            }
            writeLog("ref", "get_warehouse", "", "");
        }
        catch (Exception e)
        {
            resultcode = false;
            error_msg = e.Message;
            writeLog("err", "get_warehouse", "", e.Message);
        }


        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, stations = dt }, Formatting.Indented);
        return strAppRequest;
    }


    #region 版本確認及更新
    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    [WebMethod]
    public string getAppVersion(string app_id)
    {
        writeLog("call", "getAppVersion", app_id, "");
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dtAppRequest = new DataTable();
        Boolean resultcode = true; string error_msg = string.Empty;
        if (app_id.Equals(""))
        {
            resultcode = false;
            error_msg = "app_id未傳入";
        }
        else
        {

            try
            {
                using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                {
                    StringBuilder sql = new StringBuilder();
                    sql.Append(" select top 1 * from ttAppVersion where app_id=@app_id");

                    using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                    {
                        conn.Open();
                        command.Parameters.Add("@app_id", SqlDbType.VarChar).Value = app_id;

                        SqlDataAdapter da = new SqlDataAdapter(command);
                        da.SelectCommand.CommandTimeout = db_Timeout;
                        da.Fill(dtAppRequest);
                        conn.Close();

                    }
                }
            }
            catch (Exception e)
            {
                resultcode = false;
                error_msg = e.Message;
                writeLog("err", "getAppVersion", app_id, e.Message);
            }
            finally
            {
            }
        }

        //將欄位名稱轉成小寫
        foreach (DataColumn column in dtAppRequest.Columns)
        {
            column.ColumnName = column.ColumnName.ToLower();
        }

        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, ttVersion = JsonConvert.SerializeObject(dtAppRequest, Formatting.Indented, timeConverter) }, Formatting.Indented);
        return strAppRequest;
    }
    #endregion


    #region 給APP確認是否有網路連線
    /// <summary>
    /// 給APP確認是否有網路連線 : checkNetwrok
    /// </summary>
    /// <param name="user_name">使用者帳號</param>
    /// <returns></returns>
    [WebMethod]
    public string checkNetwork(string user_id)
    {
        HttpContext.Current.Response.AppendHeader("Access-Control-Allow-Origin", "*");
        HttpContext.Current.Response.AppendHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

        //writeLog("call", "checkNetwork", user_id, "");
        string strCheck = string.Empty;
        var login = new { status = "0" };
        if (user_id != "")
            login = new { status = "1" };

        strCheck = JsonConvert.SerializeObject(login, Formatting.Indented);
        return strCheck;
    }
    #endregion

    [WebMethod(Description = "依據縣市區域取得區配代碼")]
    public void GetttArriveSitesSupplierCode(string post_city, string post_area)
    {
        //Context.Response.ContentType = "application/json";
        HttpContext.Current.Response.ContentEncoding = Encoding.GetEncoding("utf-8");
        HttpContext.Current.Response.Write(TtArriveSitesSupplierCode.GetttArriveSitesSupplierCode(post_city, post_area));
        return;
    }

    #region 寫入登入時間tbDrivers
    /// <summary>
    /// 寫入登入時間tbDrivers
    /// </summary>
    /// <param name="driver_code"></param>
    protected void writeLoginDate(string driver_code)
    {
        writeLog("call", "writeLoginDate", driver_code, "");

        try
        {
            using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
            {

                string sql = "update tbDrivers set login_date = getdate() where driver_code = @driver_code ";

                using (SqlCommand command = new SqlCommand(sql, conn))
                {
                    conn.Open();

                    command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                    command.ExecuteNonQuery();

                }
                conn.Close();

            }

            writeLog("ref", "writeLoginDate", driver_code, "");
        }
        catch (Exception e)
        {
            writeLog("err_err", "writeLoginDate", driver_code, e.Message);
        }
        finally
        {
        }

    }

    /// <summary>
    /// 寫入登入時間tbDrivers
    /// </summary>
    /// <param name="driver_code"></param>
    /// <param name="device"></param>
    /// <param name="product"></param>
    /// <param name="brand"></param>
    /// <param name="versionCode"></param>
    protected void writeLoginDate(string driver_code, string device, string product, string brand, string versionCode)
    {
        writeLog("call", "writeLoginDate", driver_code, "");
        device = device.Length > 50 ? device.Substring(0, 50) : device;
        product = product.Length > 50 ? product.Substring(0, 50) : product;
        brand = brand.Length > 50 ? brand.Substring(0, 50) : brand;
        versionCode = versionCode.Length > 50 ? versionCode.Substring(0, 50) : versionCode;
        try
        {
            using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
            {

                string sql = @"update tbDrivers set login_date = getdate()
                                , Device = @device, Product = @product, Brand = @brand, VersionCode = @versionCode
                                    where driver_code = @driver_code ";

                using (SqlCommand command = new SqlCommand(sql, conn))
                {
                    conn.Open();

                    command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                    command.Parameters.Add("@device", SqlDbType.NVarChar).Value = device;
                    command.Parameters.Add("@product", SqlDbType.NVarChar).Value = product;
                    command.Parameters.Add("@brand", SqlDbType.NVarChar).Value = brand;
                    command.Parameters.Add("@versionCode", SqlDbType.NVarChar).Value = versionCode;
                    command.ExecuteNonQuery();

                }
                conn.Close();

            }

            writeLog("ref", "writeLoginDate", driver_code, "");
        }
        catch (Exception e)
        {
            writeLog("err_err", "writeLoginDate", driver_code, e.Message);
        }
        finally
        {
        }

    }

    /// <summary>
    /// 寫入登入時間tbDrivers
    /// </summary>
    /// <param name="driver_code"></param>
    /// <param name="device"></param>
    /// <param name="product"></param>
    /// <param name="brand"></param>
    /// <param name="versionCode"></param>
    protected void writeLoginDate(string driver_code, string device, string product, string brand, string versionCode, string mobiletoken = "", string mobiletype = "")
    {
        writeLog("call", "writeLoginDate", driver_code, "");
        device = device.Length > 50 ? device.Substring(0, 50) : device;
        product = product.Length > 50 ? product.Substring(0, 50) : product;
        brand = brand.Length > 50 ? brand.Substring(0, 50) : brand;
        versionCode = versionCode.Length > 50 ? versionCode.Substring(0, 50) : versionCode;
        try
        {
            using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
            {

                string sql = @"update tbDrivers 
set 
login_date = getdate(), 
Device = @device,
Product = @product,
Brand = @brand,
VersionCode = @versionCode,
mobiletoken = @mobiletoken,
mobiletype = @mobiletype  

                                    where driver_code = @driver_code ";

                using (SqlCommand command = new SqlCommand(sql, conn))
                {
                    conn.Open();

                    command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                    command.Parameters.Add("@device", SqlDbType.NVarChar).Value = device;
                    command.Parameters.Add("@product", SqlDbType.NVarChar).Value = product;
                    command.Parameters.Add("@brand", SqlDbType.NVarChar).Value = brand;
                    command.Parameters.Add("@versionCode", SqlDbType.NVarChar).Value = versionCode;

                    command.Parameters.Add("@mobiletoken", SqlDbType.VarChar).Value = mobiletoken;
                    command.Parameters.Add("@mobiletype", SqlDbType.VarChar).Value = mobiletype;
                    command.ExecuteNonQuery();

                }
                conn.Close();

            }

            writeLog("ref", "writeLoginDate", driver_code, "");
        }
        catch (Exception e)
        {
            writeLog("err_err", "writeLoginDate", driver_code, e.Message);
        }
        finally
        {
        }

    }
    #endregion

    #region 執行歷程寫log到資料庫ttAppLog
    /// <summary>
    /// 
    /// </summary> 
    protected void writeLog(string type, string fun_name, string memo, string log)
    {
        try
        {
            using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
            {

                string sql = "Insert into ttAppLog (app_project, app_fun_name, app_type, app_memo, app_log) values (@app_project, @app_fun_name, @app_type, @app_memo, @app_log)";

                using (SqlCommand command = new SqlCommand(sql, conn))
                {
                    conn.Open();

                    command.Parameters.Add("@app_project", SqlDbType.VarChar).Value = project_name;
                    command.Parameters.Add("@app_fun_name", SqlDbType.VarChar).Value = fun_name;
                    command.Parameters.Add("@app_type", SqlDbType.VarChar).Value = type;
                    command.Parameters.Add("@app_memo", SqlDbType.VarChar).Value = memo;
                    command.Parameters.Add("@app_log", SqlDbType.VarChar).Value = log;
                    command.ExecuteNonQuery();

                }
                conn.Close();

            }

        }
        catch (Exception e)
        {
            //這邊例外, 一樣會寫DB, 但不會再第二次catch了.
            if (!fun_name.Equals("writeLog"))
                writeLog("err_err", "writeLog", memo, e.Message);
        }
        finally
        {
        }

    }
    #endregion

    #region Base64轉成實體照片
    /// 以下這個區塊為將Base64字串轉換為實體圖片......Start------------------------------------------------------
    /// SaveImgFromBase64 : Base64轉成實體照片
    /// BufferToImage:將 Byte 陣列轉換為 Image。
    /// ImageToBuffer: 將 Image 轉換為 Byte 陣列。
    /// <summary>
    /// Base64轉成實體照片
    /// </summary>
    /// <param name="strBase64"></param>
    /// <param name="strFileName"></param>
    /// <param name="strTime"></param>
    private void SaveImgFromBase64(string strBase64, string strFileName, string strTime)
    {
        writeLog("call", "SaveImgFromBase64", strFileName + "_" + strTime, "");
        try
        {
            //傳入的strTime的格式為 103/07/12 01:02
            //DateTime time = DateTime.Parse(strTime);
            DateTime time = DateTime.Now;
            //將日期轉為西元年的datetime
            //time = time.AddYears(1911);
            strBase64 = strBase64.Substring(strBase64.IndexOf(",") + 1);
            Bitmap img = BufferToImage(Convert.FromBase64String(strBase64));
            using (Graphics g = Graphics.FromImage(img))
            {
                string drawString = time.ToString("yyyy/MM/dd HH:mm:ss");

                g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;

                // Create font and brush.
                Font drawFont = new Font("Arial", 20);
                SolidBrush drawBrush = new SolidBrush(Color.FromArgb(255, 0, 0));
                //SolidBrush drawBrush = new SolidBrush(Color.FromArgb(60, 255, 255, 0));


                SizeF stringSize = new SizeF();
                stringSize = g.MeasureString(drawString, drawFont);

                // Create point for upper-left corner of drawing.
                PointF drawPoint = new PointF(0, 0);
                g.DrawString(drawString, drawFont, drawBrush, drawPoint); // requires font, brush etc
            }

            if (strFileName.ToLower().EndsWith(".jpg") == false && strFileName.ToLower().EndsWith(".png") == false)
            {
                strFileName += ".jpg";
            }
            byte[] aryByteImg = ImageToBuffer(img, System.Drawing.Imaging.ImageFormat.Jpeg);
            using (FileStream _FileStream = new FileStream(HttpContext.Current.Server.MapPath("~/PHOTO/" + strFileName), FileMode.Create, FileAccess.Write))
            {
                _FileStream.Write(aryByteImg, 0, aryByteImg.Length);
                _FileStream.Close();
            }
        }
        catch (Exception ex)
        {
            //throw ex;
            writeLog("err", "SaveImgFromBase64", strFileName + "_" + strTime, ex.Message);
        }
    }
    #endregion

    #region 將 Base64 陣列轉換為 PNG

    protected static void Base64ToPNG(string Base64, string FileName)
    {
        byte[] imageBytes = Convert.FromBase64String(Base64);
        using (MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length))
        {
            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
            image.Save(HttpContext.Current.Server.MapPath("~/PHOTO/" + FileName), System.Drawing.Imaging.ImageFormat.Png);
        }
    }


    #endregion

    #region 將 Byte 陣列轉換為 Image
    /// <summary>
    /// 
    /// </summary>
    /// <param name="Buffer">Byte 陣列。</param>        
    protected static Bitmap BufferToImage(byte[] Buffer)
    {

        if (Buffer == null || Buffer.Length == 0) { return null; }
        byte[] data = null;
        Image oImage = null;
        Bitmap oBitmap = null;
        //建立副本
        data = (byte[])Buffer.Clone();
        try
        {
            MemoryStream oMemoryStream = new MemoryStream(Buffer);
            //設定資料流位置
            oMemoryStream.Position = 0;
            oImage = System.Drawing.Image.FromStream(oMemoryStream);
            //建立副本
            oBitmap = new Bitmap(oImage);
        }
        catch
        {
            throw;
        }
        return oBitmap;
    }
    #endregion

    #region 將 Image 轉換為 Byte 陣列
    /// <summary>
    /// 將 Image 轉換為 Byte 陣列。
    /// </summary>
    /// <param name="Image">Image 。</param>
    /// <param name="imageFormat">指定影像格式。</param>        
    protected static byte[] ImageToBuffer(Bitmap Image, System.Drawing.Imaging.ImageFormat imageFormat)
    {
        if (Image == null) { return null; }
        byte[] data = null;
        using (MemoryStream oMemoryStream = new MemoryStream())
        {
            //建立副本
            using (Bitmap oBitmap = new Bitmap(Image))
            {
                //儲存圖片到 MemoryStream 物件，並且指定儲存影像之格式
                oBitmap.Save(oMemoryStream, imageFormat);
                //設定資料流位置
                oMemoryStream.Position = 0;
                //設定 buffer 長度
                data = new byte[oMemoryStream.Length];
                //將資料寫入 buffer
                oMemoryStream.Read(data, 0, Convert.ToInt32(oMemoryStream.Length));
                //將所有緩衝區的資料寫入資料流
                oMemoryStream.Flush();
            }
        }
        return data;
    }
    #endregion

    #region WebService 推播回傳訊息
    public class sendFcmMsg
    {
        //"{\"multicast_id\":6731310868968694564,\"success\":0,\"failure\":1,\"canonical_ids\":0,\"results\":[{\"error\":\"MissingRegistration\"}]}"
        public long multicast_id { get; set; }
        public int success { get; set; }
        public int failure { get; set; }
        public int canonical_ids { get; set; }
        public Result[] results { get; set; }
    }
    public class Result
    {
        public string error { get; set; }
    }
    #endregion
    /// <summary>
    /// 找等待綁定的單號
    /// </summary>
    private PostCollectOnDelivery_Condition CheckPostCollectionMoneyPending(string check_number)
    {
        try
        {
            using (var cn = new SqlConnection(db_junfu_sql_conn_str))
            {
                string sql = @"

SELECT *
  FROM [dbo].[PostCollectOnDelivery]
  WHERE check_number = @check_number AND PostId is null

";
                return cn.QueryFirstOrDefault<PostCollectOnDelivery_Condition>(sql, new { check_number });
            }
        }
        catch (Exception e)
        {
            return null;
        }
    }



    /// <summary>
    /// 綁定郵局代收貨款號
    /// </summary>
    private bool DoPostCollectionMoneyNoBinding(Post_Condition data)
    {
        bool s = true;
        data.UpdateTime = DateTime.Now;
        try
        {
            //更新 代收郵局表
            using (var cn = new SqlConnection(db_junfu_sql_conn_str))
            {
                string sql = @"
UPDATE [dbo].[PostCollectOnDelivery]
  SET PostId=@PostId,UpdateTime = @UpdateTime
  WHERE  check_number=@check_number

";
                cn.Execute(sql, data);
            }

            //更新郵局表
            using (var cn = new SqlConnection(db_junfu_sql_conn_str))
            {
                string sql = @"

INSERT INTO [dbo].[Post]
           (
[PostId]
           ,[request_id]
           ,[check_number]
           ,[Weight]
         
)
     VALUES
           (
@PostId
           ,@request_id
           ,@check_number
           ,@Weight
         
)
";
                cn.Execute(sql, data);
            }
        }
        catch (Exception e)
        {
            s = false;
        }
        return s;
    }
    /// <summary>
    /// 解除綁定郵局代收貨款號
    /// </summary>
    private bool DelPostCollectionMoneyNoBinding(Post_Condition data)
    {
        bool s = true;

        data.UpdateTime = DateTime.Now;
        data.IsDel = true;//刪除狀態

        PostCollectOnDelivery_Condition _postCollectOnDelivery_Condition = new PostCollectOnDelivery_Condition();
        _postCollectOnDelivery_Condition.check_number = data.check_number;
        _postCollectOnDelivery_Condition.PostId = data.PostId;
        _postCollectOnDelivery_Condition.UpdateTime = DateTime.Now;



        try
        {
            //解除
            using (var cn = new SqlConnection(db_junfu_sql_conn_str))
            {
                string sql = @"
  UPDATE [dbo].[Post]
  SET IsDel=@IsDel,UpdateTime = @UpdateTime
  WHERE PostId=@PostId AND check_number=@check_number
";
                cn.Execute(sql, data);
            }
            //刪掉綁定
            using (var cn = new SqlConnection(db_junfu_sql_conn_str))
            {
                string sql = @"
  UPDATE [dbo].[PostCollectOnDelivery]
  SET PostId = NULL,UpdateTime = @UpdateTime
  WHERE PostId=@PostId AND check_number=@check_number
";
                cn.Execute(sql, _postCollectOnDelivery_Condition);
            }
            //刪掉SCANLOG
            //刪掉綁定
            using (var cn = new SqlConnection(db_junfu_sql_conn_str))
            {
                string sql = @"
   DELETE [dbo].[ttDeliveryScanLog]
    WHERE [check_number] = @check_number AND [tracking_number] = @tracking_number

";
                cn.Execute(sql, new { check_number = _postCollectOnDelivery_Condition.check_number, tracking_number = _postCollectOnDelivery_Condition.PostId });
            }

            //刪掉SCANLOG2
            using (var cn = new SqlConnection(db_junfu_sql_conn_str))
            {
                string sql = @"
   DELETE [dbo].[ttDeliveryScanLog2]
    WHERE [check_number] = @check_number AND [tracking_number] = @tracking_number

";
                cn.Execute(sql, new { check_number = _postCollectOnDelivery_Condition.check_number, tracking_number = _postCollectOnDelivery_Condition.PostId });
            }


        }
        catch (Exception e)
        {
            s = false;
        }
        return s;
    }


    private tcDeliveryRequests_Condition GettcDeliveryRequestsBycheck_number(string check_number)
    {
        tcDeliveryRequests_Condition tcDeliveryRequests_Condition = new tcDeliveryRequests_Condition();

        tcDeliveryRequests_Condition = null;


        try
        {
            using (var cn = new SqlConnection(db_junfu_sql_conn_str))
            {
                string sql = @"select top 1 * from tcDeliveryRequests with (nolock) where check_number = @check_number order by request_id desc";
                tcDeliveryRequests_Condition = cn.QueryFirstOrDefault<tcDeliveryRequests_Condition>(sql, new { check_number });
            }
        }
        catch (Exception e)
        {
            tcDeliveryRequests_Condition = null;
        }

        return tcDeliveryRequests_Condition;
    }
    private List<ttDeliveryScanLog> GetttDeliveryScanLogListBycheck_number(string check_number)
    {
        List<ttDeliveryScanLog> ttDeliveryScanLogs = new List<ttDeliveryScanLog>();
        ttDeliveryScanLogs = null;


        try
        {
            using (var cn = new SqlConnection(db_junfu_sql_conn_str))
            {
                string sql = @"select * from ttDeliveryScanLog with (nolock) where check_number = @check_number";
                ttDeliveryScanLogs = cn.Query<ttDeliveryScanLog>(sql, new { check_number }).ToList();
            }
        }
        catch (Exception e)
        {
            ttDeliveryScanLogs = null;
        }

        return ttDeliveryScanLogs;
    }

    private void UpdatetcttDeliveryScanLogsign_form_imageBYsign_field_image(string check_number, string sign_field_image_name, string sign_form_image_name)
    {

        using (var cn = new SqlConnection(db_junfu_sql_conn_str))
        {
            string sql =
                @"
  UPDATE [dbo].[ttDeliveryScanLog]
  SET sign_form_image= @sign_form_image_name
  WHERE sign_field_image = @sign_field_image_name AND check_number = @check_number
                ";
            cn.Execute(sql, new { check_number, sign_field_image_name, sign_form_image_name });
        }

        using (var cn = new SqlConnection(db_junfu_sql_conn_str))
        {
            string sql =
                @"
  UPDATE [dbo].[ttDeliveryScanLog2]
  SET sign_form_image= @sign_form_image_name
  WHERE sign_field_image = @sign_field_image_name AND check_number = @check_number
                ";
            cn.Execute(sql, new { check_number, sign_field_image_name, sign_form_image_name });
        }

    }


    private void JubFuAPPWebServiceLog(string sessionid, string type, string action, string message)
    {
        try
        {
            using (var cn = new SqlConnection(db_junfu_sql_conn_str))
            {
                string sql = @"INSERT INTO [JubFuAPPWebServiceLog](sessionid,type,action,message,cdate) VALUES (@sessionid,@type,@action,@message,@cdate)";
                cn.Execute(sql, new { sessionid, type, action, message = new DbString { Value = message, Length = -1 }, cdate = DateTime.Now });
            }
        }
        catch (Exception e)
        {

        }
    }

    private void JubFuAPPWebServiceLog_SMS(string sessionid, string type, string check_number, string action, string message)
    {
        try
        {
            using (var cn = new SqlConnection(db_junfu_sql_conn_str))
            {
                string sql = @"INSERT INTO [JubFuAPPWebServiceLog_SMS](sessionid,type,check_number,action,message,cdate) VALUES (@sessionid,@type,@check_number,@action,@message,@cdate)";
                cn.Execute(sql, new { sessionid, type, check_number, action, message, cdate = DateTime.Now });
            }
        }
        catch (Exception e)
        {

        }
    }


    static bool IsNumeric(object Expression)
    {
        bool isNum;
        double retNum;
        isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);

        return isNum;
    }


    /// <summary>
    /// 取得逆物流任務清單
    /// </summary>
    /// <param name="driver_code">司機帳號</param>
    /// <param name="status">取件狀態("":全部　"0":未取  "1":已取)</param>
    /// <returns></returns>
    [WebMethod]
    public string get_return_delivery(string driver_code, string status)
    {
        writeLog("call", "get_return_delivery", "driver_code:" + driver_code + "; status:" + status.ToString(), "");
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dt = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;
        List<ReturnRelivery> _items = new List<ReturnRelivery>();

        if (driver_code.Equals(""))
        {
            resultcode = false;
            error_msg = "司機帳號未傳入";
        }
        else
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    string wherestr = string.Empty;
                    if (status == "0")
                    {
                        wherestr += " and ISNULL(c.seq,0) =0";
                    }
                    else if (status == "1")
                    {
                        wherestr += " and ISNULL(c.seq,0) >0";
                    }
                    cmd.Parameters.AddWithValue("@driver_code", driver_code);
                    //cmd.CommandText = string.Format(@"Select A.request_id, A.check_number, A.order_number, A.receive_customer_code, A.receive_tel1 'receive_tel',
                    //                                      A.receive_contact,A.receive_city+A.receive_area +A.receive_address 'receive_address',A.send_contact, 
                    //                                      A.send_tel, A.send_city+A.send_area+A.send_address 'send_address',A.invoice_desc 'memo'
                    //                                      from tcDeliveryRequests  a with(nolock) 
                    //                                      --inner join tbDrivers b on A.area_arrive_code  = B.station  and B.driver_code = @driver_code
                    //                                      inner join tbDrivers b on B.driver_code = 'Z010002' and B.station = (select station_code  from ttArriveSitesScattered where post_city =A.send_city  and post_area =A.send_area )
                    //                                      left join ttReturnDeliveryPickupLog c with(nolock)  on A.check_number = c.check_number
                    //                                      where DeliveryType  = 'R' 
                    //                                      {0}
                    //                                      order by A.print_date", wherestr);

                    cmd.CommandText = string.Format(@"
                                                      Select A.request_id, A.check_number, A.order_number, A.receive_customer_code, A.receive_tel1 'receive_tel',
                                                            A.receive_contact,A.receive_city+A.receive_area +A.receive_address 'receive_address',A.send_contact, 
                                                            A.send_tel, A.send_city+A.send_area+A.send_address 'send_address',A.invoice_desc 'memo'
                                                            from tcDeliveryRequests  a with(nolock) 
                                                            LEFT JOIN ttArriveSitesScattered eta With(nolock) on A.send_city = eta.post_city and A.send_area = eta.post_area
                                                            LEFT JOIN tbStation sta1 With(nolock) on sta1.station_scode  = eta.station_code	
                                                            inner join tbDrivers b on A.area_arrive_code  = B.station  and B.driver_code = @driver_code and B.station = sta1.station_scode
                                                            --inner join tbDrivers b on B.driver_code = 'Z010002' and B.station = (select station_code  from ttArriveSitesScattered where post_city =A.send_city  and post_area =A.send_area )
                                                            left join ttReturnDeliveryPickupLog c with(nolock)  on A.check_number = c.check_number
                                                          where 1=1 {0}
                                                          and DeliveryType  = 'R' 
                                                          order by A.print_date", wherestr);


                    try
                    {
                        dt = dbAdapter.getDataTable(cmd);
                        _items = DataTableExtensions.ToList<ReturnRelivery>(dt).ToList();
                    }
                    catch (Exception e)
                    {
                        resultcode = false;
                        error_msg = e.Message;
                        writeLog("err", "get_return_delivery", "driver_code:" + driver_code + "; status:" + status.ToString(), e.Message);
                    }
                }
                writeLog("ref", "get_return_delivery", "driver_code:" + driver_code + "; status:" + status.ToString(), "");
            }
            catch (Exception e)
            {
                resultcode = false;
                error_msg = e.Message;
                writeLog("err", "get_return_delivery", "driver_code:" + driver_code + "; status:" + status.ToString(), e.Message);
            }
        }

        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, tasks = _items }, Formatting.Indented);
        return strAppRequest;
    }


    /// <summary>
    /// 取得逆物流未取件數
    /// </summary>
    /// <param name="driver_code">司機帳號</param>
    /// <returns></returns>
    [WebMethod]
    public string get_return_unpickcnt(string driver_code)
    {
        writeLog("call", "get_return_unpickcnt", "driver_code:" + driver_code, "");
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dt = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;
        int unpickcnt = 0;

        if (driver_code.Equals(""))
        {
            resultcode = false;
            error_msg = "司機帳號未傳入";
        }
        else
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    string wherestr = string.Empty;

                    cmd.Parameters.AddWithValue("@driver_code", driver_code);
                    cmd.CommandText = string.Format(@"Select count(A.request_id)
                                                       from tcDeliveryRequests  a with(nolock) 
                                                       LEFT JOIN ttArriveSitesScattered eta With(nolock) on A.send_city = eta.post_city and A.send_area = eta.post_area
                                                       LEFT JOIN tbStation sta1 With(nolock) on sta1.station_scode  = eta.station_code	
                                                       inner join tbDrivers b on A.area_arrive_code  = B.station  and B.driver_code = @driver_code and B.station = sta1.station_scode
                                                       left join ttReturnDeliveryPickupLog c with(nolock)  on A.check_number = c.check_number
                                                       where  ISNULL(c.seq,0) =0 and DeliveryType  = 'R'", wherestr);


                    try
                    {
                        dt = dbAdapter.getDataTable(cmd);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            unpickcnt = Convert.ToInt32(dt.Rows[0][0]);
                        }
                    }
                    catch (Exception e)
                    {
                        resultcode = false;
                        error_msg = e.Message;
                        writeLog("err", "get_return_unpickcnt", "driver_code:" + driver_code, e.Message);
                    }
                }
                writeLog("ref", "get_return_unpickcnt", "driver_code:" + driver_code, "");
            }
            catch (Exception e)
            {
                resultcode = false;
                error_msg = e.Message;
                writeLog("err", "get_return_unpickcnt", "driver_code:" + driver_code, e.Message);
            }
        }

        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, unpickcnt = unpickcnt }, Formatting.Indented);
        return strAppRequest;
    }


    [WebMethod]
    /// <summary>
    /// 逆物流取件代碼
    /// </summary>
    /// <returns></returns>
    public string get_return_code()
    {
        writeLog("call", "get_return_code", "", "");
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dt = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;

        try
        {
            using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
            {

                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = conn;
                    command.CommandText = string.Format(@"Select code_id , code_name , 
                                                          CASE WHEN code_id in(1) THEN 0 else 0 END  'picture',--in(1,4)
                                                          CASE WHEN code_id in(2) THEN 1 else 0 END  'date'
                                                          from tbItemCodes with(nolock) where code_bclass = '5' and  code_sclass = 'RO'
														  and active_flag = 1");
                    conn.Open();

                    SqlDataAdapter da = new SqlDataAdapter(command);
                    da.SelectCommand.CommandTimeout = db_Timeout;
                    da.Fill(dt);

                    if (dt == null && dt.Rows.Count == 0)
                    {
                        resultcode = false;
                        error_msg = "查無逆物流取件代碼";
                    }

                    conn.Close();

                }
            }
            writeLog("ref", "get_return_code", "", "");
        }
        catch (Exception e)
        {
            resultcode = false;
            error_msg = e.Message;
            writeLog("err", "get_return_code", "", e.Message);
        }


        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, codes = dt }, Formatting.Indented);
        return strAppRequest;
    }


    [WebMethod]
    /// <summary>
    /// 逆物流 狀態編碼
    /// </summary>
    /// <returns></returns>
    public string get_re_code()
    {
        writeLog("call", "get_re_code", "", "");
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dt = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;

        try
        {
            using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
            {

                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = conn;
                    command.CommandText = string.Format(@"Select code_id , code_name , 
                                                          CASE WHEN code_id in(1,2,3,4,5,6,7,8,9) THEN 0 else 0 END  'picture',
                                                          CASE WHEN code_id in(1) THEN 1 else 0 END  'date'
                                                          from tbItemCodes with(nolock) where code_bclass = '5' and  code_sclass = 'RE'
														  and active_flag = 1");
                    conn.Open();

                    SqlDataAdapter da = new SqlDataAdapter(command);
                    da.SelectCommand.CommandTimeout = db_Timeout;
                    da.Fill(dt);

                    if (dt == null && dt.Rows.Count == 0)
                    {
                        resultcode = false;
                        error_msg = "查無逆物流狀態編碼";
                    }

                    conn.Close();

                }
            }
            writeLog("ref", "get_re_code", "", "");
        }
        catch (Exception e)
        {
            resultcode = false;
            error_msg = e.Message;
            writeLog("err", "get_re_code", "", e.Message);
        }


        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, codes = dt }, Formatting.Indented);
        return strAppRequest;
    }

    #region 退貨取件
    /// <summary>
    /// 退貨取件
    /// </summary>
    /// <returns></returns>
    [WebMethod]
    public string send_pickup_data(string scan_item, string driver_code, string check_number, string scan_date, string sign_form_image, string sign_field_image, string next_pickup_time)
    {
        writeLog("call", "send_pickup_data", scan_item + "_" + driver_code + "_" + check_number + "_" + scan_date + "_", "");
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dtAppRequest = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;
        DateTime _next_pickup_time = new DateTime();

        if (scan_item.Equals(""))
        {
            resultcode = false;
            error_msg = "作業類別未傳入";
        }
        else if (driver_code.Equals(""))
        {
            resultcode = false;
            error_msg = "司機代碼未傳入";
        }
        else if (check_number.Equals(""))
        {
            resultcode = false;
            error_msg = "託運單號未傳入";

        }
        else if (!(scan_item.Equals("1") || scan_item.Equals("2") || scan_item.Equals("3") || scan_item.Equals("4") || scan_item.Equals("5")))
        {
            resultcode = false;
            error_msg = "作業類別錯誤";
        }
        //else if ((scan_item.Equals("1") || scan_item.Equals("4")) &&   (sign_form_image.Equals("") && sign_field_image.Equals("")) && (driver_code.Length > 1 && driver_code.Substring(0, 1) != "Z"))
        //{
        //    resultcode = false;
        //    error_msg = "簽單圖檔未傳入";
        //}
        else if (scan_item.Equals("3") && next_pickup_time.Equals(""))
        {
            resultcode = false;
            error_msg = "再取請傳入約定再取時間";
        }
        else if (scan_item.Equals("3") && !DateTime.TryParse(next_pickup_time, out _next_pickup_time))
        {
            resultcode = false;
            error_msg = "約定再取日期時間錯誤";

        }

        else
        {
            try
            {
                DateTime now = System.DateTime.Now; //string drawString = time.ToString("yyyy/MM/dd HH:mm");
                string upload_time = now.ToString("yyyy/MM/dd HH:mm");

                //託運單圖檔名
                string sign_form_image_name = sign_form_image != "" ? driver_code + "_" + check_number + "_" + now.ToString("yyyyMMddHHmmss") + ".jpg" : "";
                //託運單簽名欄位圖檔名
                string sign_field_image_name = sign_field_image != "" ? "sign" + "_" + check_number + "_" + now.ToString("yyyyMMddHHmmss") + ".jpg" : "";

                using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                {
                    StringBuilder sql = new StringBuilder();
                    sql.Append(" insert into ttReturnDeliveryPickupLog ([check_number],[scan_item],[scan_date],[driver_code],[sign_form_image],[sign_field_image], [next_pickup_time]) values (@check_number, @scan_item, @scan_date, @driver_code, @sign_form_image, @sign_field_image, @next_pickup_time)");

                    using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                    {
                        conn.Open();
                        command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                        command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                        command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                        command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                        command.Parameters.Add("@sign_form_image", SqlDbType.VarChar).Value = sign_form_image_name;
                        command.Parameters.Add("@sign_field_image", SqlDbType.VarChar).Value = sign_field_image_name;

                        if (scan_item == "3")
                        {
                            command.Parameters.Add("@next_pickup_time", SqlDbType.DateTime).Value = _next_pickup_time;
                        }
                        else
                        {
                            command.Parameters.Add("@next_pickup_time", SqlDbType.DateTime).Value = (object)DBNull.Value;
                        }

                        SqlDataAdapter da = new SqlDataAdapter(command);
                        da.SelectCommand.CommandTimeout = db_Timeout;
                        da.Fill(dtAppRequest);
                        conn.Close();
                    }
                }

                //寫入圖檔
                if (sign_form_image != "")
                {
                    SaveImgFromBase64(sign_form_image, sign_form_image_name, upload_time);
                }

                if (sign_field_image != "")
                {
                    SaveImgFromBase64(sign_field_image, sign_field_image_name, upload_time);
                }



                writeLog("ref", "send_pickup_data", scan_item + "_" + driver_code + "_" + check_number + "_" + scan_date + "_", "");
            }
            catch (Exception e)
            {
                resultcode = false;
                error_msg = e.Message;
                writeLog("err", "send_pickup_data", scan_item + "_" + driver_code + "_" + check_number + "_" + scan_date + "_", e.Message);
            }
            finally
            {
            }

            //將欄位名稱轉成小寫
            if (dtAppRequest != null && dtAppRequest.Rows.Count > 0)
            {
                foreach (DataColumn column in dtAppRequest.Columns)
                {
                    column.ColumnName = column.ColumnName.ToLower();
                }
            }

        }

        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, serverdate = serverdate }, Formatting.Indented);
        return strAppRequest;
    }

    /// <summary>
    /// 逆物流 未完成任務掃讀
    /// </summary>
    /// <param name="scan_item">作業類別代碼(1:已取、2:未取)</param>
    /// <param name="driver_code">司機編號</param>
    /// <param name="check_number">託運單號</param>
    /// <param name="scan_date">掃讀日期</param>
    /// <param name="sign_form_image">兩張圖片檔之一</param>
    /// <param name="sign_field_image">兩張圖片檔之二</param>
    /// <param name="next_pickup_time">約定在配時間</param>
    /// <param name="re_code">狀態編碼(1:另約時間、2:電聯不上、3:資料有誤、4:無件可取、5:超大超重、6:同業已取、7:商品未到、8:不退、9:公司休息)</param>
    /// <returns></returns>
    [WebMethod]
    public string send_pickup_data1(string scan_item, string driver_code, string check_number, string scan_date, string sign_form_image, string sign_field_image, string next_pickup_time, string re_code)
    {
        writeLog("call", "send_pickup_data1", scan_item + "_" + driver_code + "_" + check_number + "_" + scan_date + "_", "");
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dtAppRequest = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;
        DateTime _next_pickup_time = new DateTime();

        if (scan_item.Equals(""))
        {
            resultcode = false;
            error_msg = "作業類別未傳入";
        }
        else if (driver_code.Equals(""))
        {
            resultcode = false;
            error_msg = "司機代碼未傳入";
        }
        else if (check_number.Equals(""))
        {
            resultcode = false;
            error_msg = "託運單號未傳入";

        }
        else if (!(scan_item.Equals("1") || scan_item.Equals("2")))//|| scan_item.Equals("3") || scan_item.Equals("4") || scan_item.Equals("5"))
        {
            resultcode = false;
            error_msg = "作業類別錯誤";
        }
        //else if ((scan_item.Equals("1") || scan_item.Equals("4")) &&   (sign_form_image.Equals("") && sign_field_image.Equals("")) && (driver_code.Length > 1 && driver_code.Substring(0, 1) != "Z"))
        //{
        //    resultcode = false;
        //    error_msg = "簽單圖檔未傳入";
        //}
        //else if (scan_item.Equals("3") && next_pickup_time.Equals(""))
        //{
        //    resultcode = false;
        //    error_msg = "再取請傳入約定再取時間";
        //}
        //else if (scan_item.Equals("3") && !DateTime.TryParse(next_pickup_time, out _next_pickup_time))
        //{
        //    resultcode = false;
        //    error_msg = "約定再取日期時間錯誤";

        //}


        else if (scan_item.Equals("2") && re_code.Equals("1") && next_pickup_time.Equals(""))
        {
            resultcode = false;
            error_msg = "再取請傳入另約時間";
        }
        else if (scan_item.Equals("2") && re_code.Equals("1") && !DateTime.TryParse(next_pickup_time, out _next_pickup_time))
        {
            resultcode = false;
            error_msg = "另約時間日期時間錯誤";
        }

        else
        {
            try
            {
                DateTime now = System.DateTime.Now; //string drawString = time.ToString("yyyy/MM/dd HH:mm");
                string upload_time = now.ToString("yyyy/MM/dd HH:mm");

                //託運單圖檔名
                string sign_form_image_name = sign_form_image != "" ? driver_code + "_" + check_number + "_" + now.ToString("yyyyMMddHHmmss") + ".jpg" : "";
                //託運單簽名欄位圖檔名
                string sign_field_image_name = sign_field_image != "" ? "sign" + "_" + check_number + "_" + now.ToString("yyyyMMddHHmmss") + ".jpg" : "";

                using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                {
                    StringBuilder sql = new StringBuilder();
                    sql.Append(" insert into ttReturnDeliveryPickupLog ([check_number],[scan_item],[scan_date],[driver_code],[sign_form_image],[sign_field_image], [next_pickup_time],[re_code]) values (@check_number, @scan_item, @scan_date, @driver_code, @sign_form_image, @sign_field_image, @next_pickup_time,@re_code)");

                    using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                    {
                        conn.Open();
                        command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                        command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                        command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                        command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                        command.Parameters.Add("@sign_form_image", SqlDbType.VarChar).Value = sign_form_image_name;
                        command.Parameters.Add("@sign_field_image", SqlDbType.VarChar).Value = sign_field_image_name;

                        if (scan_item == "2" && re_code == "1")
                        {
                            command.Parameters.Add("@next_pickup_time", SqlDbType.DateTime).Value = _next_pickup_time;
                        }
                        else
                        {
                            command.Parameters.Add("@next_pickup_time", SqlDbType.DateTime).Value = (object)DBNull.Value;
                        }

                        if (re_code == "" || re_code == "0")
                        {
                            command.Parameters.Add("@re_code", SqlDbType.VarChar).Value = (object)DBNull.Value;
                        }
                        else
                        {
                            command.Parameters.Add("@re_code", SqlDbType.VarChar).Value = re_code;
                        }


                        SqlDataAdapter da = new SqlDataAdapter(command);
                        da.SelectCommand.CommandTimeout = db_Timeout;
                        da.Fill(dtAppRequest);
                        conn.Close();
                    }
                }

                //寫入圖檔
                if (sign_form_image != "")
                {
                    SaveImgFromBase64(sign_form_image, sign_form_image_name, upload_time);
                }

                if (sign_field_image != "")
                {
                    SaveImgFromBase64(sign_field_image, sign_field_image_name, upload_time);
                }



                writeLog("ref", "send_pickup_data", scan_item + "_" + driver_code + "_" + check_number + "_" + scan_date + "_", "");
            }
            catch (Exception e)
            {
                resultcode = false;
                error_msg = e.Message;
                writeLog("err", "send_pickup_data", scan_item + "_" + driver_code + "_" + check_number + "_" + scan_date + "_", e.Message);
            }
            finally
            {
            }

            //將欄位名稱轉成小寫
            if (dtAppRequest != null && dtAppRequest.Rows.Count > 0)
            {
                foreach (DataColumn column in dtAppRequest.Columns)
                {
                    column.ColumnName = column.ColumnName.ToLower();
                }
            }

        }

        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, serverdate = serverdate }, Formatting.Indented);
        return strAppRequest;
    }
    #endregion


    /// <summary>
    /// 盤點作業
    /// </summary>
    /// <param name="station">站所代碼</param>
    /// <param name="driver_code">司機代碼</param>
    /// <param name="scan_date">掃讀時間</param>
    /// <param name="check_number">貨號</param>
    /// <returns></returns>
    [WebMethod]
    public string send_inventory(string station, string driver_code, string scan_date, string check_number)
    {
        writeLog("call", "send_inventory", "station:" + station + "; driver_code: " + driver_code + "; scan_date:" + scan_date + "; check_number:" + check_number, "");
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dt = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;
        DateTime _scan_date;

        //if (station.Equals(""))
        //{
        //    resultcode = false;
        //    error_msg = "站所代碼未傳入";
        //}
        //else 
        if (driver_code.Equals(""))
        {
            resultcode = false;
            error_msg = "司機代碼未傳入";
        }
        else if (scan_date.Equals(""))
        {
            resultcode = false;
            error_msg = "掃讀時間未傳入";
        }
        else if (!DateTime.TryParse(scan_date, out _scan_date))
        {
            resultcode = false;
            error_msg = "掃讀時間格式有誤";
        }
        else if (check_number.Equals(""))
        {
            resultcode = false;
            error_msg = "貨號未傳入";
        }
        else
        {

            try
            {
                string[] check_numbers = check_number.Replace("，", ",").Split(',');
                if (check_numbers != null && check_numbers.Length > 0)
                {
                    for (int i = 0; i <= check_numbers.Length - 1; i++)
                    {
                        if (check_numbers[i] != "")
                        {
                            using (SqlCommand cmd = new SqlCommand())
                            {
                                cmd.Parameters.AddWithValue("@station_code", station);
                                cmd.Parameters.AddWithValue("@driver_code", driver_code);
                                cmd.Parameters.AddWithValue("@scanning_dt", _scan_date);
                                cmd.Parameters.AddWithValue("@check_number", check_numbers[i]);
                                cmd.CommandText = dbAdapter.SQLdosomething("ttInventory", cmd, "insert");

                                try
                                {
                                    dbAdapter.execNonQuery(cmd);
                                }
                                catch (Exception e)
                                {
                                    resultcode = false;
                                    error_msg = e.Message;
                                    writeLog("err", "send_inventory", "station:" + station + "; driver_code: " + driver_code + "; scan_date:" + scan_date + "; check_number:" + check_number, e.Message);
                                }
                            }
                        }
                    }
                }

                writeLog("ref", "send_inventory", "station:" + station + "; driver_code: " + driver_code + "; scan_date:" + scan_date + "; check_number:" + check_number, "");
            }
            catch (Exception e)
            {
                resultcode = false;
                error_msg = e.Message;
                writeLog("err", "send_inventory", "station:" + station + "; driver_code: " + driver_code + "; scan_date:" + scan_date + "; check_number:" + check_number, e.Message);
            }
        }

        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg }, Formatting.Indented);
        return strAppRequest;
    }

    /// <summary>
    /// 盤點作業 (新)
    /// </summary>
    /// <param name="station">站所代碼</param>
    /// <param name="driver_code">司機代碼</param>
    /// <param name="scan_date">掃讀時間</param>
    /// <param name="check_number">貨號</param>
    /// <param name="wgs_x"></param>
    /// <param name="wgs_y"></param>
    /// <returns></returns>
    [WebMethod]
    public string send_inventory_v2(string station, string driver_code, string scan_date, string check_number, string wgs_x, string wgs_y)
    {


        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dt = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;
        DateTime _scan_date = DateTime.Now;
        string strAppRequest;
        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();

        string[] check_numbers = check_number.Replace("，", ",").Split(',');

        if (driver_code.Equals(""))
        {
            resultcode = false;
            error_msg = "司機代碼未傳入";
        }
        else if (scan_date.Equals(""))
        {
            resultcode = false;
            error_msg = "掃讀時間未傳入";
        }
        else if (!DateTime.TryParse(scan_date, out _scan_date))
        {
            resultcode = false;
            error_msg = "掃讀時間格式有誤";
        }
        else if (check_number.Equals(""))
        {
            resultcode = false;
            error_msg = "貨號未傳入";
        }

        if (resultcode == false)
        {
            //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
            //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
            timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
            strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg }, Formatting.Indented);
            return strAppRequest;
        }

        if (check_numbers != null && check_numbers.Length > 0)
        {

            for (int i = 0; i <= check_numbers.Length - 1; i++)
            {
                if (check_numbers[i] != "")
                {
                    if (check_numbers[i].Length >= 10)
                    {
                        if (check_numbers[i].Substring(0, 7) == "0000000" && (check_numbers[i].Substring(7, 1) == "1" || check_numbers[i].Substring(7, 1) == "2" || check_numbers[i].Substring(7, 1) == "3" ||
                            check_numbers[i].Substring(7, 1) == "4" || check_numbers[i].Substring(7, 1) == "5" || check_numbers[i].Substring(7, 1) == "6" ||
                            check_numbers[i].Substring(7, 1) == "7" || check_numbers[i].Substring(7, 1) == "8" || check_numbers[i].Substring(7, 1) == "9"))
                        {
                            InsertAsset_Management(check_numbers[i], "5", station, driver_code, wgs_x, wgs_y);
                        }
                        else if (check_numbers[i].Substring(0, 8) == "00000000")
                        {
                            //writeLog("call", "send_inventory_v2", "station:" + station + "; driver_code: " + driver_code + "; scan_date:" + scan_date + "; check_number:" + check_numbers[i] + " ; wgs_x:" + wgs_x + " ; wgs_y:" + wgs_y, "");

                            CageMana_InsertCageRecord_DbMain(check_numbers[i], "5", station, driver_code, wgs_x, wgs_y);

                        }
                        else
                        {
                            using (SqlCommand cmd = new SqlCommand())
                            {
                                cmd.Parameters.AddWithValue("@station_code", station);
                                cmd.Parameters.AddWithValue("@driver_code", driver_code);
                                cmd.Parameters.AddWithValue("@scanning_dt", _scan_date);
                                cmd.Parameters.AddWithValue("@check_number", check_numbers[i]);
                                cmd.CommandText = dbAdapter.SQLdosomething("ttInventory", cmd, "insert");

                                try
                                {
                                    dbAdapter.execNonQuery(cmd);
                                }
                                catch (Exception e)
                                {
                                    resultcode = false;
                                    error_msg = e.Message;
                                    writeLog("err", "send_inventory", "station:" + station + "; driver_code: " + driver_code + "; scan_date:" + scan_date + "; check_number:" + check_number, e.Message);
                                }
                            }
                        }
                    }
                    else
                    {
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Parameters.AddWithValue("@station_code", station);
                            cmd.Parameters.AddWithValue("@driver_code", driver_code);
                            cmd.Parameters.AddWithValue("@scanning_dt", _scan_date);
                            cmd.Parameters.AddWithValue("@check_number", check_numbers[i]);
                            cmd.CommandText = dbAdapter.SQLdosomething("ttInventory", cmd, "insert");

                            try
                            {
                                dbAdapter.execNonQuery(cmd);
                            }
                            catch (Exception e)
                            {
                                resultcode = false;
                                error_msg = e.Message;
                                writeLog("err", "send_inventory", "station:" + station + "; driver_code: " + driver_code + "; scan_date:" + scan_date + "; check_number:" + check_number, e.Message);
                            }
                        }
                    }
                }

                System.Threading.Thread.Sleep(50);
            }
        }


        writeLog("ref", "send_inventory_v2", "station:" + station + "; driver_code: " + driver_code + "; scan_date:" + scan_date + "; check_number:" + check_numbers, "");


        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg }, Formatting.Indented);
        return strAppRequest;



    }

    /// <summary>
    /// 盤點作業 (新) V3
    /// </summary>
    /// <param name="station">站所代碼</param>
    /// <param name="driver_code">司機代碼</param>
    /// <param name="scan_date">掃讀時間</param>
    /// <param name="check_number">貨號</param>
    /// <param name="wgs_x"></param>
    /// <param name="wgs_y"></param>
    /// <param name="scan_item"></param>
    /// <returns></returns>
    [WebMethod]
    public string send_inventory_v3(string station, string driver_code, string scan_date, string check_number, string wgs_x, string wgs_y, string scan_item)
    {
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dt = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;
        DateTime _scan_date = DateTime.Now;
        string strAppRequest;
        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();

        string[] check_numbers = check_number.Replace("，", ",").Split(',');

        if (driver_code.Equals(""))
        {
            resultcode = false;
            error_msg = "司機代碼未傳入";
        }
        else if (scan_date.Equals(""))
        {
            resultcode = false;
            error_msg = "掃讀時間未傳入";
        }
        else if (!DateTime.TryParse(scan_date, out _scan_date))
        {
            resultcode = false;
            error_msg = "掃讀時間格式有誤";
        }
        else if (check_number.Equals(""))
        {
            resultcode = false;
            error_msg = "貨號未傳入";
        }
        else if (scan_item.Equals(""))
        {
            resultcode = false;
            error_msg = "代碼未傳入";
        }
        else if (!scan_item.Equals("1") && !scan_item.Equals("2"))
        {
            resultcode = false;
            error_msg = "代碼不正確";
        }

        if (resultcode == false)
        {
            //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
            //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
            timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
            strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg }, Formatting.Indented);
            return strAppRequest;
        }

        if (check_numbers != null && check_numbers.Length > 0)
        {

            for (int i = 0; i <= check_numbers.Length - 1; i++)
            {
                if (check_numbers[i] != "")
                {
                    if (check_numbers[i].Length >= 10)
                    {
                        if (check_numbers[i].Substring(0, 7) == "0000000" && (check_numbers[i].Substring(7, 1) == "1" || check_numbers[i].Substring(7, 1) == "2" || check_numbers[i].Substring(7, 1) == "3" ||
                            check_numbers[i].Substring(7, 1) == "4" || check_numbers[i].Substring(7, 1) == "5" || check_numbers[i].Substring(7, 1) == "6" ||
                            check_numbers[i].Substring(7, 1) == "7" || check_numbers[i].Substring(7, 1) == "8" || check_numbers[i].Substring(7, 1) == "9"))
                        {
                            InsertAsset_Management(check_numbers[i], "5", station, driver_code, wgs_x, wgs_y);
                        }
                        else if (check_numbers[i].Substring(0, 8) == "00000000")
                        {
                            //writeLog("call", "send_inventory_v2", "station:" + station + "; driver_code: " + driver_code + "; scan_date:" + scan_date + "; check_number:" + check_numbers[i] + " ; wgs_x:" + wgs_x + " ; wgs_y:" + wgs_y, "");

                            CageMana_InsertCageRecord_DbMain(check_numbers[i], "5", station, driver_code, wgs_x, wgs_y);

                        }
                        else
                        {
                            using (SqlCommand cmd = new SqlCommand())
                            {
                                cmd.Parameters.AddWithValue("@station_code", station);
                                cmd.Parameters.AddWithValue("@driver_code", driver_code);
                                cmd.Parameters.AddWithValue("@scanning_dt", _scan_date);
                                cmd.Parameters.AddWithValue("@check_number", check_numbers[i]);
                                cmd.Parameters.AddWithValue("@scan_item", scan_item);
                                cmd.CommandText = dbAdapter.SQLdosomething("ttInventory", cmd, "insert");

                                try
                                {
                                    dbAdapter.execNonQuery(cmd);
                                }
                                catch (Exception e)
                                {
                                    resultcode = false;
                                    error_msg = e.Message;
                                    writeLog("err", "send_inventory", "station:" + station + "; driver_code: " + driver_code + "; scan_date:" + scan_date + "; check_number:" + check_number + "; scan_item:" + scan_item, e.Message);
                                }
                            }
                        }
                    }
                    else
                    {
                        using (SqlCommand cmd = new SqlCommand())
                        {
                            cmd.Parameters.AddWithValue("@station_code", station);
                            cmd.Parameters.AddWithValue("@driver_code", driver_code);
                            cmd.Parameters.AddWithValue("@scanning_dt", _scan_date);
                            cmd.Parameters.AddWithValue("@check_number", check_numbers[i]);
                            cmd.Parameters.AddWithValue("@scan_item", scan_item);
                            cmd.CommandText = dbAdapter.SQLdosomething("ttInventory", cmd, "insert");

                            try
                            {
                                dbAdapter.execNonQuery(cmd);
                            }
                            catch (Exception e)
                            {
                                resultcode = false;
                                error_msg = e.Message;
                                writeLog("err", "send_inventory", "station:" + station + "; driver_code: " + driver_code + "; scan_date:" + scan_date + "; check_number:" + check_number + "; scan_item:" + scan_item, e.Message);
                            }
                        }
                    }
                }

                System.Threading.Thread.Sleep(50);
            }
        }


        writeLog("ref", "send_inventory_v3", "station:" + station + "; driver_code: " + driver_code + "; scan_date:" + scan_date + "; check_number:" + check_numbers + "; scan_item:" + scan_item, "");


        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg }, Formatting.Indented);
        return strAppRequest;
    }

    /// <summary>
    /// 盤點清單
    /// </summary>
    /// <param name="driver_code"></param>
    /// <param name="sdate"></param>
    /// <param name="edate"></param>
    /// <returns></returns>
    [WebMethod]
    public string get_inventory_data(string driver_code, string sdate, string edate)
    {
        writeLog("call", "get_inventory_data", "driver_code:" + driver_code + "; sdate:" + sdate + "; edate:" + edate, "");
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dt = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;
        DateTime _sdate;
        DateTime _edate;
        List<Inventory> _items = new List<Inventory>();

        if (driver_code.Equals(""))
        {
            resultcode = false;
            error_msg = "司機帳號未傳入";
        }
        else
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    string wherestr = string.Empty;
                    if (DateTime.TryParse(sdate, out _sdate) && DateTime.TryParse(edate, out _edate))
                    {
                        cmd.Parameters.AddWithValue("@sdate", _sdate);
                        cmd.Parameters.AddWithValue("@edate", _edate.AddDays(1));
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@sdate", DateTime.Today);
                        cmd.Parameters.AddWithValue("@edate", DateTime.Today.AddDays(1));
                    }
                    wherestr += " and A.scanning_dt >=@sdate and  A.scanning_dt < @edate ";
                    cmd.Parameters.AddWithValue("@driver_code", driver_code);
                    cmd.CommandText = string.Format(@"WITH scan AS(
                                                        SELECT A.Inventory_ID 'id', A.station_code, A.driver_code, CONVERT(varchar(100), A.scanning_dt, 120)  'scan_date', 
                                                        A.check_number, B.station_name , C.driver_name ,
                                                        ROW_NUMBER() OVER (PARTITION BY A.check_number ORDER BY A.scanning_dt DESC) AS rn 
                                                        FROM ttInventory A with(nolock) 
                                                        left join tbStation  B with(nolock) on A.station_code = B.station_scode
                                                        left join tbDrivers C with(nolock) on A.driver_code = C.driver_code 
                                                        where 1=1 {0})

                                                        select * from scan where rn= 1 order by scan_date", wherestr);

                    try
                    {
                        dt = dbAdapter.getDataTable(cmd);
                        _items = DataTableExtensions.ToList<Inventory>(dt).ToList();
                    }
                    catch (Exception e)
                    {
                        resultcode = false;
                        error_msg = e.Message;
                        writeLog("err", "get_inventory_data", "driver_code:" + driver_code + "; sdate:" + sdate + "; edate:" + edate, e.Message);
                    }
                }
                writeLog("ref", "get_inventory_data", "driver_code:" + driver_code + "; sdate:" + sdate + "; edate:" + edate, "");
            }
            catch (Exception e)
            {
                resultcode = false;
                error_msg = e.Message;
                writeLog("err", "get_inventory_data", "driver_code:" + driver_code + "; sdate:" + sdate + "; edate:" + edate, e.Message);
            }
        }

        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, data = _items }, Formatting.Indented);
        return strAppRequest;
    }




    /// <summary>
    /// 聯運轉出
    /// </summary>
    /// <param name="station">站所代碼</param>
    /// <param name="driver_code">司機代碼</param>
    /// <param name="scan_date">掃讀時間</param>
    /// <param name="check_number">貨號</param>
    /// <returns></returns>
    [WebMethod]
    public string send_transfer(string station, string driver_code, string scan_date, string check_number)
    {
        string UserAgent = this.Context.Request.UserAgent;
        string IP = this.Context.Request.UserHostAddress;
        Guid g = Guid.NewGuid();
        var sessionid = g.ToString();
        JubFuAPPWebServiceLog(sessionid, "send_transfer", "Start", "OK");
        JubFuAPPWebServiceLog(sessionid, "send_transfer", "UserAgent", UserAgent);
        JubFuAPPWebServiceLog(sessionid, "send_transfer", "IP", IP);
        string parameter = JsonConvert.SerializeObject(new { station, driver_code, scan_date, check_number });
        JubFuAPPWebServiceLog(sessionid, "send_transfer", "parameter", parameter);

        writeLog("call", "send_transfer", "station:" + station + "; driver_code: " + driver_code + "; scan_date:" + scan_date + "; check_number:" + check_number, "");
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dt = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;
        DateTime _scan_date;

        string Transfer = "";

        //if (station.Equals(""))
        //{
        //    resultcode = false;
        //    error_msg = "站所代碼未傳入";
        //}
        //else 
        if (driver_code.Equals(""))
        {
            resultcode = false;
            error_msg = "司機代碼未傳入";
        }
        else if (scan_date.Equals(""))
        {
            resultcode = false;
            error_msg = "掃讀時間未傳入";
        }
        else if (!DateTime.TryParse(scan_date, out _scan_date))
        {
            resultcode = false;
            error_msg = "掃讀時間格式有誤";
        }
        else if (check_number.Equals(""))
        {
            resultcode = false;
            error_msg = "貨號未傳入";
        }
        else
        {
            try
            {
                switch (driver_code.ToUpper())
                {
                    case "PP990": station = "29"; Transfer = "2"; break;
                }


                string[] check_numbers = check_number.Replace("，", ",").Split(',');
                //排除超過正常單號的字串
                check_numbers = check_numbers.Where(x => x.Length <= 20 && !string.IsNullOrEmpty(x)).ToArray();


                if (check_numbers != null && check_numbers.Length > 0)
                {
                    for (int i = 0; i <= check_numbers.Length - 1; i++)
                    {
                        if (check_numbers[i] != "")
                        {

                            string InsertttTransferLogParameter = JsonConvert.SerializeObject(
                                new
                                {
                                    station_code = station,
                                    driver_code = driver_code,
                                    scanning_dt = _scan_date,
                                    check_number = check_numbers[i],
                                    Transfer = Transfer
                                });

                            using (SqlCommand cmd = new SqlCommand())
                            {
                                cmd.Parameters.AddWithValue("@station_code", station);
                                cmd.Parameters.AddWithValue("@driver_code", driver_code);
                                cmd.Parameters.AddWithValue("@scanning_dt", _scan_date);
                                cmd.Parameters.AddWithValue("@check_number", check_numbers[i]);
                                cmd.Parameters.AddWithValue("@Transfer", Transfer);
                                cmd.CommandText = dbAdapter.SQLdosomething("ttTransferLog", cmd, "insert");

                                try
                                {
                                    JubFuAPPWebServiceLog(sessionid, "send_transfer", "InsertttTransferLogStart", InsertttTransferLogParameter);
                                    dbAdapter.execNonQuery(cmd);
                                    JubFuAPPWebServiceLog(sessionid, "send_transfer", "InsertttTransferLogEnd", "End");
                                }
                                catch (Exception e)
                                {
                                    resultcode = false;
                                    error_msg = e.Message;
                                    writeLog("err", "send_transfer", "station:" + station + "; driver_code: " + driver_code + "; scan_date:" + scan_date + "; check_number:" + check_number, e.Message);
                                }
                            }
                            if (driver_code.ToUpper() == "PP990")
                            {
                                string InsertttDeliveryScanLogParameter = JsonConvert.SerializeObject(
                                new
                                {
                                    driver_code = driver_code,
                                    check_number = driver_code,
                                    scan_item = "7",
                                    scan_date = _scan_date,
                                    area_arrive_code = station,
                                    platform = "",
                                    car_number = "",
                                    sowage_rate = "",
                                    area = "",
                                    pieces = 1,
                                    runs = 0,
                                    plates = 0,
                                    exception_option = ""

                                });

                                using (SqlCommand cmd = new SqlCommand())
                                {
                                    cmd.Parameters.AddWithValue("@driver_code", driver_code);
                                    cmd.Parameters.AddWithValue("@check_number", check_numbers[i]);
                                    cmd.Parameters.AddWithValue("@scan_item", "7");
                                    cmd.Parameters.AddWithValue("@scan_date", _scan_date);
                                    cmd.Parameters.AddWithValue("@area_arrive_code", station);
                                    cmd.Parameters.AddWithValue("@platform", "");
                                    cmd.Parameters.AddWithValue("@car_number", "");
                                    cmd.Parameters.AddWithValue("@sowage_rate", "");
                                    cmd.Parameters.AddWithValue("@area", "");
                                    cmd.Parameters.AddWithValue("@pieces", 1);
                                    cmd.Parameters.AddWithValue("@weight", 0);
                                    cmd.Parameters.AddWithValue("@runs", 0);
                                    cmd.Parameters.AddWithValue("@plates", 0);
                                    cmd.Parameters.AddWithValue("@exception_option", "");

                                    cmd.CommandText = dbAdapter.SQLdosomething("ttDeliveryScanLog", cmd, "insert");

                                    try
                                    {
                                        JubFuAPPWebServiceLog(sessionid, "send_transfer", "InsertttDeliveryScanLog", InsertttDeliveryScanLogParameter);
                                        dbAdapter.execNonQuery(cmd);
                                        JubFuAPPWebServiceLog(sessionid, "send_transfer", "InsertttDeliveryScanLog", "End");
                                    }
                                    catch (Exception e)
                                    {
                                        resultcode = false;
                                        error_msg = e.Message;
                                        writeLog("err", "send_transfer", "station:" + station + "; driver_code: " + driver_code + "; scan_date:" + scan_date + "; check_number:" + check_number, e.Message);
                                    }
                                }


                            }
                        }
                    }
                }
                writeLog("ref", "send_transfer", "station:" + station + "; driver_code: " + driver_code + "; scan_date:" + scan_date + "; check_number:" + check_number, "");
            }
            catch (Exception e)
            {
                resultcode = false;
                error_msg = e.Message;
                writeLog("err", "send_transfer", "station:" + station + "; driver_code: " + driver_code + "; scan_date:" + scan_date + "; check_number:" + check_number, e.Message);
            }
        }

        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg }, Formatting.Indented);
        JubFuAPPWebServiceLog(sessionid, "send_transfer", "strAppRequest", strAppRequest);
        JubFuAPPWebServiceLog(sessionid, "send_transfer", "End", "OK");
        return strAppRequest;
    }


    /// <summary>
    /// 聯運轉出清單
    /// </summary>
    /// <param name="driver_code"></param>
    /// <param name="sdate"></param>
    /// <param name="edate"></param>
    /// <returns></returns>
    [WebMethod]
    public string get_transfer_data(string driver_code, string sdate, string edate)
    {
        writeLog("call", "get_transfer_data", "driver_code:" + driver_code + "; sdate:" + sdate + "; edate:" + edate, "");
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dt = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;
        DateTime _sdate;
        DateTime _edate;
        List<Transfer> _items = new List<Transfer>();

        if (driver_code.Equals(""))
        {
            resultcode = false;
            error_msg = "司機帳號未傳入";
        }
        else
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    string wherestr = string.Empty;
                    if (DateTime.TryParse(sdate, out _sdate) && DateTime.TryParse(edate, out _edate))
                    {
                        cmd.Parameters.AddWithValue("@sdate", _sdate);
                        cmd.Parameters.AddWithValue("@edate", _edate.AddDays(1));
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@sdate", DateTime.Today);
                        cmd.Parameters.AddWithValue("@edate", DateTime.Today.AddDays(1));
                    }
                    wherestr += " and A.scanning_dt >=@sdate and  A.scanning_dt < @edate ";
                    cmd.Parameters.AddWithValue("@driver_code", driver_code);
                    cmd.CommandText = string.Format(@"WITH scan AS(
                                                        SELECT A.TransferID 'id', A.station_code, A.driver_code, CONVERT(varchar(100), A.scanning_dt, 120)  'scan_date', 
                                                        A.check_number, B.station_name , C.driver_name ,
                                                        ROW_NUMBER() OVER (PARTITION BY A.check_number ORDER BY A.scanning_dt DESC) AS rn 
                                                        FROM ttTransferLog A with(nolock) 
                                                        left join tbStation  B with(nolock) on A.station_code = B.station_scode
                                                        left join tbDrivers C with(nolock) on A.driver_code = C.driver_code 
                                                        where 1=1 {0})

                                                        select * from scan where rn= 1 order by scan_date", wherestr);

                    try
                    {
                        dt = dbAdapter.getDataTable(cmd);
                        _items = DataTableExtensions.ToList<Transfer>(dt).ToList();
                    }
                    catch (Exception e)
                    {
                        resultcode = false;
                        error_msg = e.Message;
                        writeLog("err", "get_transfer_data", "driver_code:" + driver_code + "; sdate:" + sdate + "; edate:" + edate, e.Message);
                    }
                }
                writeLog("ref", "get_transfer_data", "driver_code:" + driver_code + "; sdate:" + sdate + "; edate:" + edate, "");
            }
            catch (Exception e)
            {
                resultcode = false;
                error_msg = e.Message;
                writeLog("err", "get_transfer_data", "driver_code:" + driver_code + "; sdate:" + sdate + "; edate:" + edate, e.Message);
            }
        }

        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, data = _items }, Formatting.Indented);
        return strAppRequest;
    }


    [WebMethod(Description = "補救用的WebService，若某天異常可輸入日期以讀取已存在此機的log檔寫入資料庫")]
    public string OneDayUpdateDB(int station, string logName)       //補救用，從本機寫入資料庫，一次一天(一個檔案)
    {
        UpdateFromLogToDB(station, logName);
        return "執行完畢";
    }

    [WebMethod(Description = "下載大園倉丈量機檔案與寫入資料庫")]
    public string DaYuanDataCarryer()
    {
        CbmCarryAndUpdateFromLogToDB(29);
        return "執行完畢";
    }

    [WebMethod(Description = "下載台中倉丈量機檔案與寫入資料庫")]
    public string TaiChungDataCarryer()
    {
        CbmCarryAndUpdateFromLogToDB(49);
        return "執行完畢";
    }

    [WebMethod(Description = "OnlyForTest")]
    public string TestCarryer()
    {
        CbmCarryAndUpdateFromLogToDB(99);
        return "執行完畢";
    }

    private string UpdateFromLogToDB(int station, string logName)
    {
        string pathLocal = "";
        switch (station)
        {
            case 29:
                pathLocal = localPathDaYuan;
                break;
            case 49:
                pathLocal = localPathTaiChung;
                break;
            case 99:                                //**
                pathLocal = @"E:\Local\DaYuan\";
                break;
        }

        List<string> detailDataList = new List<string>();           //List會有三層，第一層依檔案不同，第二層依每個檔案內的行數，第三層依每行檔案的標點

        detailDataList.Add(GetLogDataDetail(logName, pathLocal));

        List<List<string>> detailColumnList = new List<List<string>>();

        for (var i = 0; i < detailDataList.Count(); i++)
        {
            detailColumnList.Add(CbmColumnDetail(detailDataList[i]));
        }

        List<List<CbmEntity>> finalList = new List<List<CbmEntity>>();

        for (var i = 0; i < detailColumnList.Count(); i++)
        {
            List<CbmEntity> emptyList = new List<CbmEntity>();
            finalList.Add(emptyList);

            for (var j = 0; j < detailColumnList[i].Count(); j++)
            {
                finalList[i].Add(CbmFinalDetail(detailColumnList[i][j]));
            }
        }

        foreach (var i in finalList)
        {
            foreach (var j in i)
            {
                if (j.CheckNumber != "資料異常" & j.CheckNumber != "空白" & j.CheckNumber != null & j.CheckNumber != "MULTRD" & j.CheckNumber != "")
                {
                    bool isInDeliveryRequests = false;
                    string connstr_check = db_junfu_sql_conn_str;
                    SqlConnection conn_check = new SqlConnection(connstr_check);
                    conn_check.Open();
                    string sql_check = @"select check_number from tcDeliveryRequests where check_number = '" + j.CheckNumber + "'";
                    SqlCommand cmd_check = new SqlCommand(sql_check, conn_check);
                    SqlDataReader reader_check = cmd_check.ExecuteReader();
                    if (reader_check.HasRows)
                    {
                        isInDeliveryRequests = true;
                    }
                    conn_check.Close();

                    if (isInDeliveryRequests)
                    {
                        string constr = db_junfu_sql_conn_str;
                        SqlConnection conn = new SqlConnection(constr);
                        conn.Open();
                        string sqlStr = @"update tcDeliveryRequests
                            set cbmLength = " + j.Length + @", cbmWidth = " + j.Width + @", cbmHeight = " + j.Height + @", cbmCont = " + j.Cont + @", pic_path = '" + j.PicPath + "'" +
                                    @"where check_number = '" + j.CheckNumber + "'";
                        SqlCommand cmd = new SqlCommand(sqlStr, conn);
                        SqlDataReader reader = cmd.ExecuteReader();
                        conn.Close();

                        string constr_trans = db_junfu_trans_conn_str;
                        SqlConnection conn_trans = new SqlConnection(constr_trans);
                        conn_trans.Open();
                        string sqlStr_trans = @"insert into cbm_update_log(check_number,cbmLength,cbmWidth,cbmHeight,cbmCont,cdate,data_from,pic_path)values(" + j.CheckNumber + "," + j.Length + "," + j.Width + "," + j.Height + "," + j.Cont + ",'" + DateTime.Now.ToString("u").Trim('z').Trim('Z') + "'," + station.ToString() + ",'" + j.PicPath + "')";
                        SqlCommand cmd_trans = new SqlCommand(sqlStr_trans, conn_trans);
                        SqlDataReader reader_trans = cmd_trans.ExecuteReader();
                        conn_trans.Close();
                    }

                }
            }
        }
        return "執行完畢";
    }

    private string CbmCarryAndUpdateFromLogToDB(int station)  //大園倉=29,台中倉=49, Local測試用 = 99
    {
        string pathS3 = "";
        switch (station)
        {
            case 29:
                pathS3 = S3PathDaYuan;
                break;
            case 49:
                pathS3 = S3PathTaiChung;
                break;
            case 99:                                //**
                pathS3 = S3PathDaYuan;
                break;
        }
        string pathLocal = "";
        switch (station)
        {
            case 29:
                pathLocal = localPathDaYuan;
                break;
            case 49:
                pathLocal = localPathTaiChung;
                break;
            case 99:                                //**
                pathLocal = @"E:\Local\DaYuan\";
                break;
        }
        List<string> list = UpdateFiles(pathS3, pathLocal);
        List<string> detailDataList = new List<string>();           //List會有三層，第一層依檔案不同，第二層依每個檔案內的行數，第三層依每行檔案的標點
        foreach (var i in list)
        {
            detailDataList.Add(GetLogDataDetail(i, pathLocal));
        }

        List<List<string>> detailColumnList = new List<List<string>>();

        for (var i = 0; i < detailDataList.Count(); i++)
        {
            detailColumnList.Add(CbmColumnDetail(detailDataList[i]));
        }

        List<List<CbmEntity>> finalList = new List<List<CbmEntity>>();

        for (var i = 0; i < detailColumnList.Count(); i++)
        {
            List<CbmEntity> emptyList = new List<CbmEntity>();
            finalList.Add(emptyList);

            for (var j = 0; j < detailColumnList[i].Count(); j++)
            {
                finalList[i].Add(CbmFinalDetail(detailColumnList[i][j]));
            }
        }

        foreach (var i in finalList)
        {
            foreach (var j in i)
            {
                if (j.CheckNumber != "資料異常" & j.CheckNumber != "空白" & j.CheckNumber != null & j.CheckNumber != "MULTRD" & j.CheckNumber != "" & (j.Length != 0 & j.Height != 0 & j.Width != 0))
                {
                    bool isInDeliveryRequests = false;
                    string connstr_check = db_junfu_sql_conn_str;
                    SqlConnection conn_check = new SqlConnection(connstr_check);
                    conn_check.Open();
                    string sql_check = @"select check_number from tcDeliveryRequests where check_number = '" + j.CheckNumber + "'";
                    SqlCommand cmd_check = new SqlCommand(sql_check, conn_check);
                    SqlDataReader reader_check = cmd_check.ExecuteReader();
                    if (reader_check.HasRows)
                    {
                        isInDeliveryRequests = true;
                    }
                    conn_check.Close();

                    if (isInDeliveryRequests)
                    {
                        string constr = db_junfu_sql_conn_str;
                        SqlConnection conn = new SqlConnection(constr);
                        conn.Open();
                        string sqlStr = @"update tcDeliveryRequests
                        set cbmLength = " + j.Length + @", cbmWidth = " + j.Width + @", cbmHeight = " + j.Height + @", cbmCont = " + j.Cont + @", pic_path = '" + j.PicPath + @"'" +
                        @" where request_id in (select top 1 request_id from tcDeliveryRequests where check_number = '" + j.CheckNumber + @"')";
                        SqlCommand cmd = new SqlCommand(sqlStr, conn);
                        SqlDataReader reader = cmd.ExecuteReader();
                        conn.Close();

                        string constr_trans = db_junfu_trans_conn_str;
                        SqlConnection conn_trans = new SqlConnection(constr_trans);
                        conn_trans.Open();
                        string sqlStr_trans = @"insert into cbm_update_log(check_number,cbmLength,cbmWidth,cbmHeight,cbmCont,cdate,data_from,pic_path)values(" + j.CheckNumber + "," + j.Length + "," + j.Width + "," + j.Height + "," + j.Cont + ",'" + DateTime.Now.ToString("u").Trim('z').Trim('Z') + "'," + station.ToString() + ",'" + j.PicPath + "')";
                        SqlCommand cmd_trans = new SqlCommand(sqlStr_trans, conn_trans);
                        SqlDataReader reader_trans = cmd_trans.ExecuteReader();
                        conn_trans.Close();
                    }

                }
            }
        }
        return "執行完畢";
    }

    private CbmEntity CbmFinalDetail(string str)
    {
        string[] stringDetail = str.Split(';', ' ', '-', ',');
        CbmEntity result = new CbmEntity();

        try
        {

            if (str == "")
            {
                result = new CbmEntity()
                {
                    CheckNumber = "空白",
                    Height = 0,
                    Width = 0,
                    Length = 0,
                    PicPath = "",
                    IsNormal = false
                };
            }
            else if (stringDetail.Length == 12)
            {
                if (stringDetail[0] != "NOREAD" & stringDetail[3] != "NOINDEX")
                {
                    result = new CbmEntity()
                    {
                        CheckNumber = stringDetail[0],
                        Height = int.Parse(stringDetail[2]),
                        Width = int.Parse(stringDetail[3]),
                        Length = int.Parse(stringDetail[4]),
                        Cont = decimal.Parse(stringDetail[5]),
                        PicPath = stringDetail[6] + "-" + stringDetail[7] + "-" + "08.jpg",
                        IsNormal = true
                    };
                }
            }
            else
            {
                result = new CbmEntity()
                {
                    CheckNumber = "資料異常",
                    Height = 0,
                    Width = 0,
                    Length = 0,
                    PicPath = "",
                    IsNormal = false
                };
            }
            return result;
        }
        catch (Exception e)
        {
            return new CbmEntity()
            {
                CheckNumber = "空白",
                Height = 0,
                Width = 0,
                Length = 0,
                PicPath = "",
                IsNormal = false
            };
        }
    }
    private List<string> CbmColumnDetail(string context)
    {
        List<string> result = new List<string>();
        var stringList = context.Split('$');
        for (var j = 0; j < stringList.Count(); j++)
        {
            result.Add(stringList[j]);
        }
        return result;
    }
    private string GetLogDataDetail(string dataName, string pathLocal)
    {
        string folderName = pathLocal + dataName;

        string line;

        string result = "";

        StreamReader file = new StreamReader(folderName);
        while ((line = file.ReadLine()) != null)
        {
            result += line.Trim() + "$";         //加上每一行的分界號(自訂)
        }

        file.Close();
        return result;
    }

    private List<string> GetAllLogDataName(string pathLocal)
    {
        List<string> myList = new List<string>();
        string folderName = pathLocal;

        foreach (string fname in Directory.GetFileSystemEntries(folderName, "*.csv"))
        {
            var splitList = fname.Split('\\', '.');
            string s = splitList[splitList.Count() - 2];
            myList.Add(s);
        }
        return myList;
    }

    private FileInfo[] GetFileList(string path)
    {
        FileInfo[] FileList = null;
        if (Directory.Exists(path))
        {
            DirectoryInfo di = new DirectoryInfo(path);
            FileList = di.GetFiles();
        }
        return FileList;
    }

    private List<string> UpdateFiles(string From, string To)
    {
        FileInfo[] fileFrom = GetFileList(From);
        FileInfo[] fileTo = GetFileList(To);
        List<string> fileFromList = new List<string>();
        List<string> fileToList = new List<string>();
        List<string> carryFileList = new List<string>();
        List<int> allDayList = new List<int>();

        foreach (var i in fileTo)
        {
            if (i.Name.IndexOf(".csv") != -1)
            {
                string[] splitName = i.Name.Split('.');
                if (splitName[1] == ".csv")
                {
                    allDayList.Add(int.Parse(splitName[0]));
                }
            }
        }

        foreach (var i in fileFrom)
        {
            fileFromList.Add(i.Name);
        }

        foreach (var i in fileTo)
        {
            fileToList.Add(i.Name);
        }

        var newFileList = fileFromList.Except(fileToList).ToList();

        if (newFileList.Count() != 0)
        {
            foreach (var i in newFileList)
            {
                string fileName = From + i;
                string desFileName = To + i;
                File.Copy(fileName, desFileName, true);
                carryFileList.Add(i);
                System.Threading.Thread.Sleep(500);
            }
        }

        return carryFileList;
    }

    [WebMethod(Description = "將丈量機掃發送之貨號資料寫入資料庫")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string InsertDeliveryScanLogFromCBM(DateTime scanDate, string driverCode, string checkNumber)
    {
        bool resultCode = true;
        string errorMessage = string.Format("沒有問題");
        Guid g = Guid.NewGuid();
        var sessionid = g.ToString();
        var consignment = GettcDeliveryRequestsBycheck_number(checkNumber);
        if (consignment == null)
        {
            errorMessage = "查無單號";
            #region 存入另一張表
            try
            {
                using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                {
                    StringBuilder sql = new StringBuilder();
                    sql.Append("insert into ttDeliveryScanLog_ErrorScanLog (driver_code, check_number, scan_item, scan_date,error_msg,cdate) values (@driver_code, @check_number, '7' , @scan_date, @error_msg,@cdate)");

                    using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                    {
                        conn.Open();
                        command.Parameters.AddWithValue("@check_number", checkNumber);
                        command.Parameters.AddWithValue("@driver_code", driverCode);
                        command.Parameters.AddWithValue("@error_msg", errorMessage);
                        command.Parameters.AddWithValue("@scan_date", scanDate);
                        command.Parameters.AddWithValue("@cdate", DateTime.Now);

                        command.ExecuteNonQuery();
                        conn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                JubFuAPPWebServiceLog(sessionid, "sendUploadData4", "Exception Message", e.Message);
                JubFuAPPWebServiceLog(sessionid, "sendUploadData4", "Exception StackTrace", e.StackTrace);
                JubFuAPPWebServiceLog(sessionid, "sendUploadData4", "Exception Source", e.Source);
            }
            #endregion
            return JsonConvert.SerializeObject(new { resultcode = resultCode, resultdesc = errorMessage });
        }
        if (IsNormalDelivery(consignment))
        {
            errorMessage = "此筆已正常配達(或已產生退貨單號)，無法發送(丈量機)";
            #region 存入另一張表
            try
            {
                using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                {
                    StringBuilder sql = new StringBuilder();
                    sql.Append("insert into ttDeliveryScanLog_ErrorScanLog (driver_code, check_number, scan_item, scan_date,error_msg,cdate) values (@driver_code, @check_number, '7' , @scan_date, @error_msg,@cdate)");

                    using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                    {
                        conn.Open();
                        command.Parameters.AddWithValue("@check_number", checkNumber);
                        command.Parameters.AddWithValue("@driver_code", driverCode);
                        command.Parameters.AddWithValue("@error_msg", errorMessage);
                        command.Parameters.AddWithValue("@scan_date", scanDate);
                        command.Parameters.AddWithValue("@cdate", DateTime.Now);

                        command.ExecuteNonQuery();
                        conn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                JubFuAPPWebServiceLog(sessionid, "sendUploadData4", "Exception Message", e.Message);
                JubFuAPPWebServiceLog(sessionid, "sendUploadData4", "Exception StackTrace", e.StackTrace);
                JubFuAPPWebServiceLog(sessionid, "sendUploadData4", "Exception Source", e.Source);
            }
            #endregion
            return JsonConvert.SerializeObject(new { resultcode = resultCode, resultdesc = errorMessage });
        }
        else if (IsCancelled(consignment))
        {
            errorMessage = "此筆已銷單，無法發送(丈量機)";
            #region 存入另一張表
            try
            {
                using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
                {
                    StringBuilder sql = new StringBuilder();
                    sql.Append("insert into ttDeliveryScanLog_ErrorScanLog (driver_code, check_number, scan_item, scan_date,error_msg,cdate) values (@driver_code, @check_number, '7' , @scan_date, @error_msg,@cdate)");

                    using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                    {
                        conn.Open();
                        command.Parameters.AddWithValue("@check_number", checkNumber);
                        command.Parameters.AddWithValue("@driver_code", driverCode);
                        command.Parameters.AddWithValue("@error_msg", errorMessage);
                        command.Parameters.AddWithValue("@scan_date", scanDate);
                        command.Parameters.AddWithValue("@cdate", DateTime.Now);

                        command.ExecuteNonQuery();
                        conn.Close();
                    }
                }
            }
            catch (Exception e)
            {
                JubFuAPPWebServiceLog(sessionid, "sendUploadData4", "Exception Message", e.Message);
                JubFuAPPWebServiceLog(sessionid, "sendUploadData4", "Exception StackTrace", e.StackTrace);
                JubFuAPPWebServiceLog(sessionid, "sendUploadData4", "Exception Source", e.Source);
            }
            #endregion
            return JsonConvert.SerializeObject(new { resultcode = resultCode, resultdesc = errorMessage });
        }

        using (SqlConnection connection = new SqlConnection(db_junfu_sql_conn_str))
        {
            string sql = "insert into ttDeliveryScanLog (driver_code, check_number, scan_item, scan_date) values (@driverCode, @checkNumber, '7' , @scan_date)";

            DataTable ttDeliveryScanLog = new DataTable("ttDeliveryScanLog");

            using (SqlCommand command = new SqlCommand(sql, connection))
            {
                command.Parameters.AddWithValue("@checkNumber", checkNumber);
                command.Parameters.AddWithValue("@driverCode", driverCode);
                command.Parameters.AddWithValue("@scan_date", scanDate);

                try
                {
                    connection.Open();
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);
                    sqlDataAdapter.Fill(ttDeliveryScanLog);
                    connection.Close();
                }
                catch (Exception e)
                {
                    resultCode = false;
                    errorMessage = e.Message;
                }
            }
        }
        string response = JsonConvert.SerializeObject(new { resultcode = resultCode, resultdesc = errorMessage });
        return response;
    }

    [WebMethod(Description = "新增或更新車行")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string SaveCompany(string code_name, bool active_flag = false, decimal seq = 0)
    {
        bool resultCode = true;
        string errorMessage = string.Format("沒有問題");

        try
        {
            if (seq == 0)
            {
                using (SqlConnection connection = new SqlConnection(db_junfu_sql_conn_str))
                {
                    string sql = @"
                    declare @code_id int;
                    set @code_id = (select top 1 code_id from tbItemCodes where code_bclass = '6' and code_sclass = 'CD' and code_id<> '99' order by cast(code_id as int) desc);
                    insert into tbItemCodes (code_bclass, code_sclass ,code_id, code_name, active_flag, memo) values ('6', 'CD', @code_id + 1, @code_name, 1, '車行')";

                    DataTable tbItemCodes = new DataTable("tbItemCodes");

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@code_name", code_name);
                        connection.Open();
                        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);
                        sqlDataAdapter.Fill(tbItemCodes);
                        connection.Close();
                    }
                }
            }
            else
            {
                using (SqlConnection connection = new SqlConnection(db_junfu_sql_conn_str))
                {
                    string sql = @"
                    update tbItemCodes set code_name = @code_name, active_flag = @active_flag where seq = @seq";

                    DataTable tbItemCodes = new DataTable("tbItemCodes");

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@code_name", code_name);
                        command.Parameters.AddWithValue("@active_flag", active_flag);
                        command.Parameters.AddWithValue("@seq", seq);
                        connection.Open();
                        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);
                        sqlDataAdapter.Fill(tbItemCodes);
                        connection.Close();
                    }
                }
            }
        }
        catch (Exception e)
        {
            resultCode = false;
            errorMessage = e.Message;
        }
        string response = JsonConvert.SerializeObject(new { resultcode = resultCode, resultdesc = errorMessage });
        return response;
    }

    [WebMethod(Description = "新增或更新費用類別-支出")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string SaveExpenditureFeeType(string code_name, bool active_flag = false, decimal seq = 0)
    {
        bool resultCode = true;
        string errorMessage = string.Format("沒有問題");

        try
        {
            if (seq == 0)
            {
                using (SqlConnection connection = new SqlConnection(db_junfu_sql_conn_str))
                {
                    string sql = @"
                    declare @code_id int;
                    set @code_id = (select top 1 code_id from tbItemCodes where code_bclass = '6' and code_sclass = 'fee_type' and code_id<> '99' order by cast(code_id as int) desc);
                    insert into tbItemCodes (code_bclass, code_sclass ,code_id, code_name, active_flag, memo) values ('6', 'fee_type', @code_id + 1, @code_name, 1, '費用類別-支出')";

                    DataTable tbItemCodes = new DataTable("tbItemCodes");

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@code_name", code_name);
                        connection.Open();
                        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);
                        sqlDataAdapter.Fill(tbItemCodes);
                        connection.Close();
                    }
                }
            }
            else
            {
                using (SqlConnection connection = new SqlConnection(db_junfu_sql_conn_str))
                {
                    string sql = @"
                    update tbItemCodes set code_name = @code_name, active_flag = @active_flag where seq = @seq";

                    DataTable tbItemCodes = new DataTable("tbItemCodes");

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@code_name", code_name);
                        command.Parameters.AddWithValue("@active_flag", active_flag);
                        command.Parameters.AddWithValue("@seq", seq);
                        connection.Open();
                        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);
                        sqlDataAdapter.Fill(tbItemCodes);
                        connection.Close();
                    }
                }
            }
        }
        catch (Exception e)
        {
            resultCode = false;
            errorMessage = e.Message;
        }
        string response = JsonConvert.SerializeObject(new { resultcode = resultCode, resultdesc = errorMessage });
        return response;
    }

    [WebMethod(Description = "新增或更新費用類別-收入")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string SaveIncomeFeeType(string code_name, bool active_flag = false, decimal seq = 0)
    {
        bool resultCode = true;
        string errorMessage = string.Format("沒有問題");

        try
        {
            if (seq == 0)
            {
                using (SqlConnection connection = new SqlConnection(db_junfu_sql_conn_str))
                {
                    string sql = @"
                    declare @code_id int;
                    set @code_id = (select top 1 code_id from tbItemCodes where code_bclass = '6' and code_sclass = 'fee_type' and code_id<> '99' order by cast(code_id as int) desc);
                    insert into tbItemCodes (code_bclass, code_sclass ,code_id, code_name, active_flag, memo) values ('6', 'fee_type', @code_id + 1, @code_name, 1, '費用類別-收入')";

                    DataTable tbItemCodes = new DataTable("tbItemCodes");

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@code_name", code_name);
                        connection.Open();
                        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);
                        sqlDataAdapter.Fill(tbItemCodes);
                        connection.Close();
                    }
                }
            }
            else
            {
                using (SqlConnection connection = new SqlConnection(db_junfu_sql_conn_str))
                {
                    string sql = @"
                    update tbItemCodes set code_name = @code_name, active_flag = @active_flag where seq = @seq";

                    DataTable tbItemCodes = new DataTable("tbItemCodes");

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@code_name", code_name);
                        command.Parameters.AddWithValue("@active_flag", active_flag);
                        command.Parameters.AddWithValue("@seq", seq);
                        connection.Open();
                        SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);
                        sqlDataAdapter.Fill(tbItemCodes);
                        connection.Close();
                    }
                }
            }
        }
        catch (Exception e)
        {
            resultCode = false;
            errorMessage = e.Message;
        }
        string response = JsonConvert.SerializeObject(new { resultcode = resultCode, resultdesc = errorMessage });
        return response;
    }

    [WebMethod(Description = "將丈量機圖片路徑寫入資料庫")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string UpdateDeliveryRequestsFromCBM(int station, string checkNumber, string picPath) //大園倉 = 29 , 台中倉 = 49
    {
        bool resultCode = true;
        string errorMessage = string.Format("沒有問題");

        string column = "";
        if (station == 29)
        {
            column = "catch_cbm_pic_path_from_S3";
        }
        else if (station == 49)
        {
            column = "catch_taichung_cbm_pic_path_from_S3";
        }

        using (SqlConnection connection = new SqlConnection(db_junfu_sql_conn_str))
        {
            string sql = string.Format("update tcDeliveryRequests set {0} = '{1}' where check_number = '{2}'", column, picPath, checkNumber);

            DataTable tcDeliveryRequests = new DataTable("tcDeliveryRequests");

            using (SqlCommand command = new SqlCommand(sql, connection))
            {
                try
                {
                    connection.Open();
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);
                    sqlDataAdapter.Fill(tcDeliveryRequests);
                    connection.Close();
                }
                catch (Exception e)
                {
                    resultCode = false;
                    errorMessage = e.Message;
                }
            }
        }
        string response = JsonConvert.SerializeObject(new { resultcode = resultCode, resultdesc = errorMessage });
        return response;
    }

    [WebMethod(Description = "將掃配達的貨號資料寫入資料庫")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string InsertDeliveryScanLog(string areaArriveCode, string driverCode, string arriveOption, string checkNumber)
    {
        bool resultCode = true;
        string errorMessage = string.Format("沒有問題");

        using (SqlConnection connection = new SqlConnection(db_junfu_sql_conn_str))
        {
            string sql = "insert into ttDeliveryScanLog (driver_code, check_number, scan_item, scan_date, area_arrive_code, arrive_option, write_off_type) values (@driverCode, @checkNumber, '3' , GETDATE(), @areaArriveCode, @arriveOption,1 )";

            DataTable ttDeliveryScanLog = new DataTable("ttDeliveryScanLog");

            using (SqlCommand command = new SqlCommand(sql, connection))
            {
                command.Parameters.AddWithValue("@checkNumber", checkNumber);
                command.Parameters.AddWithValue("@areaArriveCode", areaArriveCode);
                command.Parameters.AddWithValue("@driverCode", driverCode);
                command.Parameters.AddWithValue("@arriveOption", arriveOption);

                try
                {
                    connection.Open();
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);
                    sqlDataAdapter.Fill(ttDeliveryScanLog);
                    connection.Close();
                }
                catch (Exception e)
                {
                    resultCode = false;
                    errorMessage = e.Message;
                }
            }
        }
        string response = JsonConvert.SerializeObject(new { resultcode = resultCode, resultdesc = errorMessage });
        return response;
    }

    [WebMethod(Description = "將掃配達且正常配交的貨號資料寫入資料庫")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string InsertDeliveryScanLogFromGuoYang(int pieces, DateTime scanDate, string driverCode, string checkNumber)
    {
        bool resultCode = true;
        string errorMessage = string.Format("沒有問題");

        using (SqlConnection connection = new SqlConnection(db_junfu_sql_conn_str))
        {
            string sql = "insert into ttDeliveryScanLog (driver_code, check_number, scan_item, scan_date,  arrive_option, pieces) values (@driverCode, @checkNumber, '3' , @scanDate, '3',@pieces )";

            DataTable ttDeliveryScanLog = new DataTable("ttDeliveryScanLog");

            using (SqlCommand command = new SqlCommand(sql, connection))
            {
                command.Parameters.AddWithValue("@checkNumber", checkNumber);
                command.Parameters.AddWithValue("@pieces", pieces);
                command.Parameters.AddWithValue("@driverCode", driverCode);
                command.Parameters.AddWithValue("@scanDate", scanDate);

                try
                {
                    connection.Open();
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);
                    sqlDataAdapter.Fill(ttDeliveryScanLog);
                    connection.Close();
                }
                catch (Exception e)
                {
                    resultCode = false;
                    errorMessage = e.Message;
                }
            }
        }
        string response = JsonConvert.SerializeObject(new { resultcode = resultCode, resultdesc = errorMessage });
        return response;
    }

    [WebMethod(Description = "在發送總表將貨號標註為已銷號")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string UpdateDeliveryRequest(string checkNumber)
    {
        bool resultCode = true;
        string errorMessage = string.Empty;

        using (SqlConnection connection = new SqlConnection(db_junfu_sql_conn_str))
        {
            string sql1 = "update tcDeliveryRequests set is_write_off = 1 where check_number = @checkNumber";

            DataTable tcDeliveryRequests = new DataTable("tcDeliveryRequests");

            using (SqlCommand command = new SqlCommand(sql1, connection))
            {
                command.Parameters.AddWithValue("@checkNumber", checkNumber);

                try
                {
                    connection.Open();
                    SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(command);
                    sqlDataAdapter.Fill(tcDeliveryRequests);

                }
                catch (Exception e)
                {
                    resultCode = false;
                    errorMessage = e.Message;
                }
            }
        }
        string response = JsonConvert.SerializeObject(new { resultcode = resultCode, resultdesc = errorMessage });
        return response;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string SaveFreightBatch(string[] checkNumber, int[] freight)
    {
        bool resultCode = true;
        string errorMessage = string.Empty;

        using (SqlConnection connection = new SqlConnection(db_junfu_sql_conn_str))
        {
            string sql = string.Empty;
            for (int i = 0; i < checkNumber.Length; i++)
            {
                sql += " update tcDeliveryRequests set freight = " + freight[i] +
                    "where check_number = '" + checkNumber[i] + "'";
            }
            using (SqlCommand command = new SqlCommand(sql, connection))
            {
                try
                {
                    connection.Open();
                    int rowAffected = command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    resultCode = false;
                    errorMessage = ex.Message;
                }
            }
        }

        string response = JsonConvert.SerializeObject(new { resultcode = resultCode, resultdesc = errorMessage });
        return response;
    }

    [WebMethod(Description = "新增或更新ItemCodeMapping部門對應支出項目")]
    public bool UpdateJfDeptFeeTypeMappingCode(int mappingId, int aId, int bId, bool active)
    {
        if (mappingId == 0) // insert
        {
            using (SqlConnection connection = new SqlConnection(db_junfu_sql_conn_str))
            {
                string sql = @"INSERT INTO [dbo].[tbItemMappingCodes] ([mapping_bclass] ,[mapping_sclass] ,[mapping_a_id] ,[mapping_b_id] ,[active_flag] ,[memo]) 
                    VALUES('1', 'jf_dept_fee_type', @aid, @bid, @active, '車老闆收支項目權責部門')";

                using (SqlCommand cmd = new SqlCommand(sql, connection))
                {
                    cmd.Parameters.AddWithValue("@aid", aId);
                    cmd.Parameters.AddWithValue("@bid", bId);
                    cmd.Parameters.AddWithValue("@active", active);

                    try
                    {
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                }
            }
        }
        else  // update
        {
            using (SqlConnection connection = new SqlConnection(db_junfu_sql_conn_str))
            {
                string sql = @"UPDATE [dbo].[tbItemMappingCodes] SET [mapping_a_id] = @aid,
                        [mapping_b_id] = @bid ,[active_flag] = @active 
                        WHERE mapping_id = @id";

                using (SqlCommand cmd = new SqlCommand(sql, connection))
                {
                    cmd.Parameters.AddWithValue("@id", mappingId);
                    cmd.Parameters.AddWithValue("@aid", aId);
                    cmd.Parameters.AddWithValue("@bid", bId);
                    cmd.Parameters.AddWithValue("@active", active);

                    try
                    {
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception)
                    {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    [WebMethod(Description = "更新預購袋使用數量")]
    public bool UpdateCheckNumberSetting(string customerCode, int count)
    {
        using (SqlConnection connection = new SqlConnection(db_junfu_sql_conn_str))
        {
            string sql = @"update Customer_checknumber_setting set used_count = used_count + @count  where customer_code = @customer_code and is_active = 1";

            using (SqlCommand cmd = new SqlCommand(sql, connection))
            {
                cmd.Parameters.AddWithValue("@count", count);
                cmd.Parameters.AddWithValue("@customer_code", customerCode);

                try
                {
                    connection.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        return true;
    }


    private string GetSimpleReportUrl(string reportName, string exportType = "IMAGE", string imageType = "PNG")
    {
        var preUrl = string.Format("{0}?/{1}/{2}", ReportUrl, ReportFolder, reportName);

        string returnUrl = string.Format("{0}&rs:Format={1}", preUrl, exportType);

        if (exportType.ToUpper() == "IMAGE")
        {
            returnUrl = string.Format("{0}&rc:OutputFormat={1}&rc:DpiX=300&rc:DpiY=300", returnUrl, imageType);
        }

        return returnUrl;
    }

    private Stream Post(string url, Dictionary<string, string[]> reportParams)
    {
        var queryStringList = new List<string>();

        foreach (string key in reportParams.Keys)
        {
            var values = reportParams[key];

            if (values != null)
            {
                if (values.Length == 0)
                {
                    queryStringList.Add(string.Format("{0}=", key));
                }

                foreach (string value in values)
                {
                    queryStringList.Add(string.Format("{0}={1}", key, value));
                }
            }
        }

        string queryString = string.Join("&", queryStringList);

        return PostInner(url, queryString);
    }

    /// <summary>
    /// post資料部分
    /// </summary>
    /// <param name="url"></param>
    /// <param name="queryString"></param>
    /// <returns></returns>
    [WebMethod]
    private Stream PostInner(string url, string queryString)
    {
        HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
        req.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 6.0; zh-TW; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1";
        req.Method = "POST";
        req.ContentType = "application/x-www-form-urlencoded";
        req.Credentials = new NetworkCredential(ReportAccount, ReportPassword);
        req.KeepAlive = true;
        req.Accept = "*/*";


        //取得postData
        byte[] byteData = Encoding.UTF8.GetBytes(queryString);
        req.ContentLength = byteData.Length;
        Stream requestStream = req.GetRequestStream();
        requestStream.Write(byteData, 0, byteData.Length);
        requestStream.Close();

        HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
        Stream dataStream = resp.GetResponseStream();

        return dataStream;
    }

    /// <summary>
    /// 是否為正常配達
    /// </summary>
    /// <param name="checkNumber"></param>
    /// <returns></returns>
    //[WebMethod]
    //public bool IsNormalDelivery(string checkNumber)
    //{

    //    var requestInfo = GettcDeliveryRequestsBycheck_number(checkNumber);
    //    if (requestInfo == null)
    //    {

    //    }
    //    else
    //    {
    //        if (requestInfo.latest_scan_item == "3" && requestInfo.latest_arrive_option == "3")
    //        {
    //            var scanList = GetttDeliveryScanLogListBycheck_number(checkNumber);
    //            if (scanList.Count > 0)
    //            {
    //                return true;
    //            }
    //        }
    //    }
    //    return false;
    //}

    /// <summary>
    /// 是否為正常配達(或已產生退貨單號)
    /// </summary>
    /// <param name="consignment"></param>
    /// <returns></returns>
    [WebMethod]
    public bool IsNormalDelivery(tcDeliveryRequests_Condition consignment)
    {
        var NormalDelivery = new List<string> { "3", "49" };
        if (consignment.latest_scan_item == "3" && NormalDelivery.Contains(consignment.latest_arrive_option))
        {
            var scanList = GetttDeliveryScanLogListBycheck_number(consignment.check_number);
            if (scanList.Count > 0)
            {
                return true;
            }
        }
        return false;
    }

    /// <summary>
    /// 是否為銷單
    /// </summary>
    /// <param name="checkNumber"></param>
    /// <returns></returns>
    //[WebMethod]
    //public bool IsCancelled(string checkNumber)
    //{
    //    var requestInfo = GettcDeliveryRequestsBycheck_number(checkNumber);
    //    if (requestInfo == null)
    //    {

    //    }
    //    else
    //    {
    //        if (requestInfo.cancel_date != null)
    //        {
    //            return true;
    //        }
    //    }
    //    return false;
    //}
    /// <summary>
    /// 是否為銷單
    /// </summary>
    /// <param name="consignment"></param>
    /// <returns></returns>
    [WebMethod]
    public bool IsCancelled(tcDeliveryRequests_Condition consignment)
    {
        if (consignment.cancel_date != null)
        {
            return true;
        }
        return false;
    }
    /// <summary>
    /// 貨單與司機站點是否相同
    /// </summary>
    /// <param name="driverCode"></param>
    /// <param name="checkNumber"></param>
    /// <returns></returns>
    //[WebMethod]
    //public bool IsSameStation(string driverCode, string checkNumber)
    //{
    //    var requestInfo = GettcDeliveryRequestsBycheck_number(checkNumber);
    //    var driverInfo = GetDriversInfoByDriverCode(driverCode);
    //    if (requestInfo == null || driverInfo == null)
    //    {

    //    }
    //    else
    //    {
    //        requestInfo.send_station_scode = requestInfo.send_station_scode.Trim() ?? "";
    //        requestInfo.area_arrive_code = requestInfo.area_arrive_code.Trim() ?? "";
    //        driverInfo.station = driverInfo.station.Trim() ?? "";
    //        if (requestInfo.send_station_scode == driverInfo.station)
    //        {
    //            return true;
    //        }
    //        else if (requestInfo.send_station_scode == "" || driverInfo.station == "")
    //        {
    //            return true;
    //        }
    //        else
    //        {
    //            return false;
    //        }

    //    }
    //    return true;
    //}
    /// <summary>
    /// 貨單與司機站點是否相同
    /// </summary>
    /// <param name="driverCode"></param>
    /// <param name="consignment"></param>
    /// <returns></returns>
    [WebMethod]
    public bool IsSameStation(string driverCode, tcDeliveryRequests_Condition consignment)
    {
        var driverInfo = GetDriversInfoByDriverCode(driverCode);
        if (driverInfo == null)
        {

        }
        else
        {
            consignment.send_station_scode = consignment.send_station_scode.Trim() ?? "";
            consignment.area_arrive_code = consignment.area_arrive_code.Trim() ?? "";
            driverInfo.station = driverInfo.station.Trim() ?? "";
            if (consignment.send_station_scode == driverInfo.station)
            {
                return true;
            }
            else if (consignment.send_station_scode == "" || driverInfo.station == "")
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        return true;
    }
    /// <summary>
    /// 是否為第一筆集貨資料
    /// </summary>
    /// <param name="checkNumber"></param>
    /// <returns></returns>
    //[WebMethod]
    //public bool IsFirstPickUpScanLog(string checkNumber)
    //{
    //    var requestInfo = GettcDeliveryRequestsBycheck_number(checkNumber);
    //    var scanLogs = GetttDeliveryScanLogListBycheck_number(checkNumber);
    //    if (requestInfo == null || scanLogs == null)
    //    {

    //    }
    //    else
    //    {
    //        foreach (var log in scanLogs)
    //        {
    //            if (log.scan_item == "5" && requestInfo.ship_date != null)
    //            {
    //                return false;
    //            }
    //        }
    //    }
    //    return true;
    //}
    /// <summary>
    /// 是否為第一筆集貨資料
    /// </summary>
    /// <param name="checkNumber"></param>
    /// <returns></returns>
    [WebMethod]
    public bool IsFirstPickUpScanLog(tcDeliveryRequests_Condition consignment)
    {
        var scanLogs = GetttDeliveryScanLogListBycheck_number(consignment.check_number);
        if (scanLogs == null)
        {

        }
        else
        {
            foreach (var log in scanLogs)
            {
                if (log.scan_item == "5" && consignment.ship_date != null)
                {
                    return false;
                }
            }
        }
        return true;
    }
    /// <summary>
    /// 是否為全速配司機
    /// </summary>
    /// <param name="driverCode"></param>
    /// <returns></returns>
    [WebMethod]
    public bool IsFSEDriver(string driverCode)
    {
        if (driverCode.Length < 1)
        {
            return false;
        }
        var driverFSEHeader = new List<string>() { "z", "p" };
        return driverFSEHeader.Contains(driverCode.ToLower().Substring(0, 1));
    }
    /// <summary>
    /// 取得司機基本資料
    /// </summary>
    /// <param name="driver_code"></param>
    /// <returns></returns>
    private tbDrivers GetDriversInfoByDriverCode(string driver_code)
    {
        tbDrivers tbDrivers = new tbDrivers();
        tbDrivers = null;

        try
        {
            using (var cn = new SqlConnection(db_junfu_sql_conn_str))
            {
                string sql = @"
                select top 1 
	                   tbD.[driver_id]
                      ,tbD.[driver_code]
                      ,tbD.[driver_name]
                      ,tbD.[driver_mobile]
                      ,tbD.[supplier_code]
                      ,tbD.[emp_code]
                      ,tbD.[site_code]
                      ,tbD.[area]
                      ,tbD.[login_password]
                      ,tbD.[login_date]
                      ,tbD.[active_flag]
                      ,tbD.[external_driver]
                      ,tbD.[push_id]
                      ,tbD.[wgs_x]
                      ,tbD.[wgs_y]
                      ,CASE WHEN　ISNULL(tbD.[station],'') = '' THEN tbS.[station_scode] ELSE tbD.[station] END AS [station]
                      ,tbD.[cdate]
                      ,tbD.[cuser]
                      ,tbD.[udate]
                      ,tbD.[uuser]
                      ,tbD.[office]
                      ,tbD.[CollectionNo]
                      ,tbD.[driver_type]
                      ,tbD.[driver_type_code]
                      ,tbD.[checkout_vendor] from tbDrivers tbD with (nolock)
                LEFT JOIN tbEmps tbE with (nolock) ON tbD.driver_code = tbE.driver_code
                LEFT JOIN tbStation tbS with (nolock) ON tbE.station = tbS.id

                where tbD.driver_code = @driver_code
                order by driver_id desc";
                tbDrivers = cn.QueryFirstOrDefault<tbDrivers>(sql, new { driver_code });
            }
        }
        catch (Exception e)
        {
            tbDrivers = null;
        }
        return tbDrivers;
    }



    /// <summary>
    /// 依駐區取得集區代碼 
    /// </summary>
    /// <param name="station_scode">駐區代碼</param>
    /// <returns></returns>
    private void GetCollectStation(string station_scode)
    {

        System.Collections.Hashtable Res = new System.Collections.Hashtable();
        System.Web.Script.Serialization.JavaScriptSerializer javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        if (station_scode == "")
        {
            Res.Add("syscode", "001");
            Res.Add("sysmsg", "駐區代碼為空!!");
            DoContextResponse(DoHashTableToString(Res));
        }
        else
        {
            DataTable dtemp = new DataTable();

            //司機、協力商dt
            using (SqlCommand cmd1 = new SqlCommand())
            {
                cmd1.Parameters.AddWithValue("@station_scode", station_scode);
                cmd1.CommandText = @"
   select distinct convert(int,A.sup)  as sup from (
  select SendMD as sup from JunFuTrans..[orgArea] where  station_scode = @station_scode and isnull(SendMD,'') <>'' group by SendMD
  union
  select SendSD as sup from JunFuTrans..[orgArea] where  station_scode = @station_scode and isnull(SendSD,'') <>''  group by SendSD
  ) A order by convert(int,A.sup) ";
                dtemp = dbAdapter.getDataTable(cmd1);
                if (dtemp.Rows.Count <= 0)
                {
                    Res.Add("syscode", "001");
                    Res.Add("sysmsg", "無資料。");
                    Res.Add("DATA", dtemp);
                    DoContextResponse(DoHashTableToString(Res));
                    return;
                }
                else
                {
                    Res.Add("syscode", "000");
                    Res.Add("sysmsg", "");
                    Res.Add("DATA", dtemp);
                    DoContextResponse(DoHashTableToString(Res));
                    return;
                }
            }
        }

    }

    /// <summary>
    /// 依駐區取得配區代碼 
    /// </summary>
    /// <param name="station_scode">駐區代碼</param>
    /// <returns></returns>
    private void GetDeliveryStation(string station_scode)
    {

        System.Collections.Hashtable Res = new System.Collections.Hashtable();
        System.Web.Script.Serialization.JavaScriptSerializer javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        if (station_scode == "")
        {
            Res.Add("syscode", "001");
            Res.Add("sysmsg", "駐區代碼為空!!");
            DoContextResponse(DoHashTableToString(Res));
        }
        else
        {
            DataTable dtemp = new DataTable();

            //司機、協力商dt
            using (SqlCommand cmd1 = new SqlCommand())
            {
                cmd1.Parameters.AddWithValue("@station_scode", station_scode);
                cmd1.CommandText = @"
   select distinct convert(int,A.sup)  as sup from (
  select md_no as sup from JunFuTrans..[orgArea] where  station_scode = @station_scode and isnull(md_no,'') <>'' group by md_no
  union
  select sd_no as sup from JunFuTrans..[orgArea] where  station_scode = @station_scode and isnull(sd_no,'') <>''  group by sd_no
  ) A order by convert(int,A.sup) ";
                dtemp = dbAdapter.getDataTable(cmd1);
                if (dtemp.Rows.Count <= 0)
                {
                    Res.Add("syscode", "001");
                    Res.Add("sysmsg", "無資料。");
                    Res.Add("DATA", dtemp);
                    DoContextResponse(DoHashTableToString(Res));
                    return;
                }
                else
                {
                    Res.Add("syscode", "000");
                    Res.Add("sysmsg", "");
                    Res.Add("DATA", dtemp);
                    DoContextResponse(DoHashTableToString(Res));
                    return;
                }
            }
        }

    }

    /// <summary>
    /// 取得駐區清單 
    /// </summary>
    /// <param name="station_scode">駐區代碼</param>
    /// <returns></returns>
    private void GetStation()
    {

        System.Collections.Hashtable Res = new System.Collections.Hashtable();
        System.Web.Script.Serialization.JavaScriptSerializer javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

        DataTable dtemp = new DataTable();

        using (SqlCommand cmd1 = new SqlCommand())
        {
            cmd1.CommandText = @"SELECT 
       [station_scode]
      ,[station_name]

  FROM [tbStation]";
            dtemp = dbAdapter.getDataTable(cmd1);
            if (dtemp.Rows.Count <= 0)
            {
                Res.Add("syscode", "001");
                Res.Add("sysmsg", "無資料。");
                Res.Add("DATA", dtemp);
                DoContextResponse(DoHashTableToString(Res));
                return;
            }
            else
            {
                Res.Add("syscode", "000");
                Res.Add("sysmsg", "");
                Res.Add("DATA", dtemp);
                DoContextResponse(DoHashTableToString(Res));
                return;
            }
        }
    }

    /// <summary>
    /// 紀錄司機最新 駐點、集區、配區 代號
    /// </summary>
    /// <param name="driver_code">司機代號</param>
    /// <param name="station_scode">駐點簡碼</param>
    /// <param name="Collect">集區代碼</param>
    /// <param name="Delivery">配區代碼</param>
    private void CreatDriverDetail(string driver_code, string station_scode, string Collect, string Delivery)
    {
        System.Collections.Hashtable Res = new System.Collections.Hashtable();
        System.Web.Script.Serialization.JavaScriptSerializer javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

        try
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                //司機代號
                cmd.Parameters.AddWithValue("@driver_code", driver_code);
                //駐點簡碼
                cmd.Parameters.AddWithValue("@station_scode", station_scode);
                //集區代碼
                cmd.Parameters.AddWithValue("@Collect", Collect);
                //配區代碼
                cmd.Parameters.AddWithValue("@Delivery", Delivery);
                //寫入時間
                cmd.Parameters.AddWithValue("@cdate", DateTime.Now);
                cmd.CommandText = dbAdapter.genInsertComm("ttAppDriverDetail", true, cmd);
                dbAdapter.execNonQuery(cmd);
            }
            Res.Add("syscode", "000");
            Res.Add("sysmsg", "");
            DoContextResponse(DoHashTableToString(Res));
            return;
        }
        catch (Exception ex)
        {
            Res.Add("syscode", "007");
            Res.Add("sysmsg", ex.Message);
            DoContextResponse(DoHashTableToString(Res));
            return;
        }
    }

    /// <summary>
    /// 紀錄司機最新 駐點、集區、配區 代號
    /// </summary>
    /// <param name="driver_code">司機代號</param>
    /// <param name="station_scode">駐點簡碼</param>
    /// <param name="Collect">集區代碼</param>
    /// <param name="Delivery">配區代碼</param>
    private void GetDriverDetail(string driver_code)
    {
        System.Collections.Hashtable Res = new System.Collections.Hashtable();
        System.Web.Script.Serialization.JavaScriptSerializer javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        DataTable dt = new DataTable();
        try
        {
            using (SqlCommand cmd = new SqlCommand())
            {
                //司機代號
                cmd.Parameters.AddWithValue("@driver_code", driver_code);
                cmd.CommandText = @"select top 1 * from ttAppDriverDetail where driver_code = @driver_code order by cdate desc";
                cmd.CommandText = dbAdapter.genInsertComm("ttAppDriverDetail", true, cmd);
                dbAdapter.execNonQuery(cmd);
            }
            Res.Add("syscode", "000");
            Res.Add("sysmsg", "");
            Res.Add("DATA", dt);
            DoContextResponse(DoHashTableToString(Res));
            return;
        }
        catch (Exception ex)
        {
            Res.Add("syscode", "007");
            Res.Add("sysmsg", ex.Message);
            DoContextResponse(DoHashTableToString(Res));
            return;
        }
    }


    /// <summary>
    /// 判斷是否有貨態
    /// </summary>
    /// <param name="checkNumber"></param>
    /// <param name="scan_item"></param>
    /// <returns></returns>
    [WebMethod]
    public bool HasScanItem(string checkNumber, string scan_item)
    {
        var list = GetttDeliveryScanLogListBycheck_number(checkNumber);
        if (list != null && list.Count() > 0)
        {
            if (list.Select(x => x.scan_item).Contains(scan_item))
            {
                return true;
            }
        }
        return false;
    }

    [WebMethod]
    public bool IsLatestScanItem(string checkNumber, string latest_scan_item)
    {
        var info = GettcDeliveryRequestsBycheck_number(checkNumber);
        if (info != null)
        {
            if (info.latest_scan_item == latest_scan_item)
            {
                return true;
            }
        }
        return false;
    }

    [WebMethod]
    public bool IsMOMOCheckNumber(string checkNumber)
    {
        List<string> momoCheck = new List<string> { "201", "202" };
        if (checkNumber.Length == 12)
        {
            if (momoCheck.Contains(checkNumber.Substring(0, 3)))
            {
                return true;
            }
        }
        return false;
    }
    /// <summary>
    /// 錯誤掃讀記錄存入errlog表中
    /// </summary>
    /// <param name="actoin"></param>
    /// <param name="error_msg"></param>
    /// <param name="sessionid"></param>
    /// <param name="scan_item"></param>
    /// <param name="driver_code"></param>
    /// <param name="check_number"></param>
    /// <param name="scan_date"></param>
    /// <param name="area_arrive_code"></param>
    /// <param name="platform"></param>
    /// <param name="car_number"></param>
    /// <param name="sowage_rate"></param>
    /// <param name="area"></param>
    /// <param name="exception_option"></param>
    /// <param name="arrive_option"></param>
    /// <param name="ship_mode"></param>
    /// <param name="goods_type"></param>
    /// <param name="pieces"></param>
    /// <param name="weight"></param>
    /// <param name="runs"></param>
    /// <param name="plates"></param>
    /// <param name="sign_form_image"></param>
    /// <param name="sign_field_image"></param>
    /// <param name="coupon"></param>
    /// <param name="cash"></param>
    /// <param name="receive_option"></param>
    private void saveErrScanLog(string actoin, string error_msg, string sessionid, string scan_item, string driver_code, string check_number, string scan_date, string area_arrive_code, string platform, string car_number, string sowage_rate, string area, string exception_option, string arrive_option, string ship_mode, string goods_type, string pieces, string weight, string runs, string plates, string sign_form_image, string sign_field_image, string coupon, string cash, string receive_option = "", string station_scode = "", string Collect = "", string Delivery = "")
    {
        int _pieces = (String.IsNullOrEmpty(pieces) || !IsNumeric(pieces)) ? 0 : Convert.ToInt32(pieces);
        double _weight = (String.IsNullOrEmpty(weight) || !IsNumeric(weight)) ? 0 : Convert.ToDouble(weight);
        int _runs = (String.IsNullOrEmpty(runs) || !IsNumeric(runs)) ? 0 : Convert.ToInt32(runs);
        int _plates = (String.IsNullOrEmpty(plates) || !IsNumeric(plates)) ? 0 : Convert.ToInt32(plates);
        int _coupon = (String.IsNullOrEmpty(coupon) || !IsNumeric(coupon)) ? 0 : Convert.ToInt32(coupon);
        int _cash = (String.IsNullOrEmpty(cash) || !IsNumeric(cash)) ? 0 : Convert.ToInt32(cash);
        try
        {
            using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
            {
                StringBuilder sql = new StringBuilder();
                sql.Append(@" insert into ttDeliveryScanLog_ErrorScanLog (driver_code, check_number, scan_item, scan_date, area_arrive_code, platform, car_number, sowage_rate, area, exception_option,pieces,weight,runs,ship_mode,goods_type , plates, receive_option,error_msg,arrive_option,VoucherMoney,CashMoney,cdate) 
                            values (@driver_code, @check_number, @scan_item, @scan_date, @area_arrive_code, @platform, @car_number, @sowage_rate, @area, @exception_option ,@pieces,@weight,@runs,@ship_mode, @goods_type , @plates, @receive_option, @error_msg, @arrive_option, @VoucherMoney, @CashMoney, @cdate)");

                using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                {
                    conn.Open();
                    command.Parameters.Add("@driver_code", SqlDbType.VarChar).Value = driver_code;
                    command.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;
                    command.Parameters.Add("@scan_item", SqlDbType.VarChar).Value = scan_item;
                    command.Parameters.Add("@scan_date", SqlDbType.VarChar).Value = scan_date;
                    command.Parameters.Add("@area_arrive_code", SqlDbType.VarChar).Value = area_arrive_code;
                    command.Parameters.Add("@platform", SqlDbType.VarChar).Value = platform;
                    command.Parameters.Add("@car_number", SqlDbType.VarChar).Value = car_number;
                    command.Parameters.Add("@sowage_rate", SqlDbType.VarChar).Value = sowage_rate;
                    command.Parameters.Add("@area", SqlDbType.VarChar).Value = area;
                    command.Parameters.Add("@ship_mode", SqlDbType.NVarChar, 2).Value = ship_mode;
                    command.Parameters.Add("@goods_type", SqlDbType.NVarChar, 2).Value = goods_type;
                    command.Parameters.Add("@pieces", SqlDbType.Int).Value = _pieces;
                    command.Parameters.Add("@weight", SqlDbType.Float).Value = _weight;
                    command.Parameters.Add("@runs", SqlDbType.Int).Value = _runs;
                    command.Parameters.Add("@plates", SqlDbType.Int).Value = _plates;
                    command.Parameters.Add("@exception_option", SqlDbType.VarChar).Value = exception_option;
                    command.Parameters.Add("@receive_option", SqlDbType.VarChar).Value = receive_option;
                    command.Parameters.Add("@error_msg", SqlDbType.VarChar).Value = error_msg;
                    command.Parameters.Add("@arrive_option", SqlDbType.VarChar).Value = arrive_option;
                    command.Parameters.Add("@VoucherMoney", SqlDbType.Int).Value = _coupon;  //振興券金額
                    command.Parameters.Add("@CashMoney", SqlDbType.Int).Value = _cash;       //現金金額
                    command.Parameters.Add("@cdate", SqlDbType.DateTime).Value = DateTime.Now;       //現金金額
                    command.ExecuteNonQuery();
                    conn.Close();
                }
            }
        }
        catch (Exception e)
        {
            JubFuAPPWebServiceLog(sessionid, actoin, "Exception Message", e.Message);
            JubFuAPPWebServiceLog(sessionid, actoin, "Exception StackTrace", e.StackTrace);
            JubFuAPPWebServiceLog(sessionid, actoin, "Exception Source", e.Source);
        }
    }


    /// <summary>
    /// APP端取得 集貨分區/配達分區的代碼清單
    /// </summary>
    /// <param name="strType">"RO" 集貨分區   "AO" 配達分區 </param>
    /// <returns></returns>
    [WebMethod]
    public string getDeliveryType(string strType)
    {
        string strSQLCMD = "";

        strType = strType.ToUpper();
        if (strType != "RO" && strType != "AO" && strType != "STATION")
        {
            strType = "RO";
        }


        if (strType.Equals("RO"))
        {
            strSQLCMD = @"
Select	(
			case 
				when code_id = '20' then 1
				when code_id = '7' then  2
				when code_id = '18' then 3
				when code_id = '17' then 4
				when code_id = '12' then 5
				when code_id = '6' then  6
				when code_id = '16' then 7
				when code_id = '22' then 8
				when code_id = '24' then 9
				when code_id = '8' then  10
				when code_id = '9' then  11
				when code_id = '23' then 12
				else 50
			End
		) as idx ,
		code_id , code_name , 
		left(code_id + '  ',2) + '.' + code_name as choice
From tbItemCodes with (nolock)
Where code_bclass = '5' 
and code_sclass = 'RO'
and code_id in ('20','7','18','17','12','6','16','22','24','8','9','23')
and active_flag = '1'
order by idx
";
        }
        else if (strType.Equals("AO"))
        {
            strSQLCMD = @"
Select	(
			case 
				when code_id = '3' then 1
				when code_id = '1' then  2
				when code_id = '2' then 3
				when code_id = '4' then 4
				when code_id = '5' then 5
				when code_id = '6' then  6
				when code_id = '11' then 7
				when code_id = '26' then 8
				when code_id = '21' then 9
                when code_id = '7' then 10
                when code_id = '8' then 11
				else 50
			End
		) as idx ,
		code_id , code_name , 
		left(code_id + '  ',2) + '.' + code_name as choice
From tbItemCodes with (nolock)
Where code_bclass = '5' 
and code_sclass = 'AO'
and code_id in ('3','1','2','4','5','6','11','26','21','7','8')
and active_flag = '1'
order by idx
";
        }
        else if (strType.Equals("STATION"))
        {
            strSQLCMD = @"
Select (
			case 
                when station_scode in ('28','48') then 0
				when left(BusinessDistrict,3) = '北二區' then 2
				when left(BusinessDistrict,2) = '北區'   then 1
				when left(BusinessDistrict,3) = '桃竹區' then 3
				else 9 
			End
		) as idx , 
		station_scode as code_id , 
		station_name as code_name , 
		station_scode + '.' + station_name as choice 
From tbStation with (nolock)
Where active_flag = 1 
and left( station_scode , 1 ) <> '9'
and right( station_scode , 1 ) <> '9'
and len(station_scode) = 2
Order by idx 
";
        }

        writeLog("call", "getDeliveryType", strType, "");
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dt = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;

        try
        {
            using (SqlCommand cmd = new SqlCommand())
            {

                cmd.CommandText = strSQLCMD;

                try
                {
                    dt = dbAdapter.getDataTable(cmd);
                    if (dt == null && dt.Rows.Count == 0)
                    {
                        resultcode = false;
                        error_msg = "查無分區資料";
                    }
                }
                catch (Exception e)
                {
                    resultcode = false;
                    error_msg = e.Message;
                    writeLog("err", "getDeliveryType", "", e.Message);
                }
            }
            writeLog("ref", "getDeliveryType", "", "");
        }
        catch (Exception e)
        {
            resultcode = false;
            error_msg = e.Message;
            writeLog("err", "getDeliveryType", "", e.Message);
        }


        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, choicelist = dt }, Formatting.Indented);
        return strAppRequest;
    }


    /// <summary>
    /// 站所enum
    /// </summary>
    //enum StationType
    //{
    //    /// <summary>
    //    /// 集貨站所
    //    /// </summary>
    //    [EnumMember(Value = "send_station_scode")]
    //    send_station_scode = 1,
    //    /// <summary>
    //    /// 到著站所
    //    /// </summary>
    //    [EnumMember(Value = "area_arrive_code")]
    //    area_arrive_code = 2
    //}



    [WebMethod(Description = "月結袋綁定API")]
    /// <summary>
    /// 月結袋綁定API
    /// </summary>
    /// <param name="checkNumber"></param>
    /// <param name="bagNo"></param>
    /// <param name="driverCode"></param>
    /// <param name="bind"> 大於0:綁定,小於等於0:解綁</param>
    /// <returns></returns>
    public string BindBagNo(string checkNumber, string bagNo, string driverCode, int bind)
    {
        bool isBind = bind > 0 ? true : false;
        string UserAgent = this.Context.Request.UserAgent;
        string IP = this.Context.Request.UserHostAddress;
        Guid g = Guid.NewGuid();
        var sessionid = g.ToString();
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dtAppRequest = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;
        string success_msg = string.Empty;
        JubFuAPPWebServiceLog(sessionid, "BindBagNo", "Start", "OK");
        JubFuAPPWebServiceLog(sessionid, "BindBagNo", "UserAgent", UserAgent);
        JubFuAPPWebServiceLog(sessionid, "BindBagNo", "IP", IP);
        string parameter = JsonConvert.SerializeObject(new { checkNumber, bagNo, driverCode });
        JubFuAPPWebServiceLog(sessionid, "BindBagNo", "parameter", parameter);
        var consignment = GettcDeliveryRequestsBycheck_number(checkNumber);
        var bagBinding = GetBagBindingByCheckNumber(checkNumber);
        if (driverCode.Equals(""))
        {
            resultcode = false;
            error_msg = "司機代碼未傳入";
            JubFuAPPWebServiceLog(sessionid, "BindBagNo", "error_msg", error_msg);
            return JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg }, Formatting.Indented);
        }
        else if (checkNumber.Equals(""))
        {
            resultcode = false;
            error_msg = "託運單號未傳入";
            JubFuAPPWebServiceLog(sessionid, "BindBagNo", "error_msg", error_msg);
            return JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg }, Formatting.Indented);

        }
        else if (bagNo.Equals("") && isBind)
        {
            resultcode = false;
            error_msg = "袋號未傳入";
            JubFuAPPWebServiceLog(sessionid, "BindBagNo", "error_msg", error_msg);
            return JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg }, Formatting.Indented);

        }

        if (consignment == null)
        {
            JubFuAPPWebServiceLog(sessionid, "BindBagNo", "error_msg", "查無單號");
            resultcode = true;
            error_msg = "查不到此筆單號，請與發送站所進行確認。";
            JubFuAPPWebServiceLog(sessionid, "BindBagNo", "error_msg", error_msg);
            return JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg }, Formatting.Indented); ;

        }
        else if (IsNormalDelivery(consignment))
        {
            resultcode = true;
            error_msg = "此筆已正常配達(或已產生退貨單號)";
            JubFuAPPWebServiceLog(sessionid, "BindBagNo", "error_msg", error_msg);
            return JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg }, Formatting.Indented); ;
        }
        else if (IsCancelled(consignment))
        {
            resultcode = true;
            error_msg = "此筆已銷單";
            JubFuAPPWebServiceLog(sessionid, "BindBagNo", "error_msg", error_msg);
            return JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg }, Formatting.Indented); ;
        }
        if (bagBinding == null && isBind)
        {
            using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
            {
                StringBuilder sql = new StringBuilder();
                var requestId = consignment.request_id;
                sql.Append(@" 
                    BEGIN TRY  
                         insert into BagNoBinding (RequestId,CheckNumber,BagNo,DriverCode,IsBind,cdate) values (@requestId,@checkNumber,@bagNo,@driverCode,1,getdate())
                         insert into CBMDetailLog (ComeFrom,CheckNumber,CBM,CreateUser) values ('4',@checkNumber,@bagNo,@driverCode)
                    END TRY  
                    BEGIN CATCH  
                         update BagNoBinding set BagNo = @bagNo,IsBind = 1,udate = getdate() where RequestId = @requestId
                         insert into CBMDetailLog (ComeFrom,CheckNumber,CBM,CreateUser) values ('4',@checkNumber,@bagNo,@driverCode)
                    END CATCH  
                    ");

                using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                {
                    parameter = JsonConvert.SerializeObject(new { requestId, checkNumber, bagNo, driverCode, bind });
                    JubFuAPPWebServiceLog(sessionid, "BindBagNo", "InsertBagNoBinding", parameter);
                    conn.Open();
                    command.Parameters.Add("@requestId", SqlDbType.BigInt).Value = requestId;
                    command.Parameters.Add("@checkNumber", SqlDbType.NVarChar).Value = checkNumber;
                    command.Parameters.Add("@bagNo", SqlDbType.NVarChar).Value = bagNo;
                    command.Parameters.Add("@driverCode", SqlDbType.NVarChar).Value = driverCode;

                    try
                    {
                        SqlDataAdapter da = new SqlDataAdapter(command);
                        da.SelectCommand.CommandTimeout = db_Timeout;
                        da.Fill(dtAppRequest);
                        JubFuAPPWebServiceLog(sessionid, "BindBagNo", "InsertBagNoBinding", "InstertSuccess");
                    }
                    catch (Exception e)
                    {
                        JubFuAPPWebServiceLog(sessionid, "BindBagNo", "InsertBagNoBinding", "InstertFail errmsg:" + e.Message);
                    }

                    conn.Close();
                }

            }
        }
        else if (bagBinding != null && isBind)
        {
            using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
            {
                StringBuilder sql = new StringBuilder();
                var id = bagBinding.id;
                var requestId = consignment.request_id;
                sql.Append(@"update BagNoBinding set BagNo = @bagNo,IsBind = 1,udate = getdate() where id = @id");
                sql.Append("\n insert into CBMDetailLog(ComeFrom, CheckNumber, CBM, CreateUser) values('4', @checkNumber, @bagNo, @driverCode)");


                using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                {
                    parameter = JsonConvert.SerializeObject(new { id, checkNumber, bagNo, driverCode, bind });
                    JubFuAPPWebServiceLog(sessionid, "BindBagNo", "UpdateBagNoBinding", parameter);
                    conn.Open();
                    command.Parameters.Add("@id", SqlDbType.BigInt).Value = id;
                    command.Parameters.Add("@bagNo", SqlDbType.NVarChar).Value = bagNo;
                    command.Parameters.Add("@checkNumber", SqlDbType.NVarChar).Value = checkNumber;
                    command.Parameters.Add("@driverCode", SqlDbType.NVarChar).Value = driverCode;

                    try
                    {
                        SqlDataAdapter da = new SqlDataAdapter(command);
                        da.SelectCommand.CommandTimeout = db_Timeout;
                        da.Fill(dtAppRequest);
                        JubFuAPPWebServiceLog(sessionid, "BindBagNo", "UpdateBagNoBinding", "UpdateSuccess");
                    }
                    catch (Exception e)
                    {
                        JubFuAPPWebServiceLog(sessionid, "BindBagNo", "UpdateBagNoBinding", "UpdateFail errmsg:" + e.Message);
                    }
                    conn.Close();

                }

            }
        }
        else if (bagBinding != null && !isBind)
        {
            using (SqlConnection conn = new SqlConnection(db_junfu_sql_conn_str))
            {
                StringBuilder sql = new StringBuilder();
                var id = bagBinding.id;
                sql.Append("update BagNoBinding set IsBind = 0,udate = getdate() where id = @id");
                sql.Append("\n insert into CBMDetailLog(ComeFrom, CheckNumber, CBM, CreateUser) values('5', @checkNumber, @bagNo, @driverCode)");

                using (SqlCommand command = new SqlCommand(sql.ToString(), conn))
                {
                    parameter = JsonConvert.SerializeObject(new { id, checkNumber, bagNo, driverCode, bind });
                    JubFuAPPWebServiceLog(sessionid, "BindBagNo", "UpdateBagNoBinding", parameter);
                    conn.Open();
                    command.Parameters.Add("@id", SqlDbType.BigInt).Value = id;
                    command.Parameters.Add("@bagNo", SqlDbType.NVarChar).Value = bagNo;
                    command.Parameters.Add("@checkNumber", SqlDbType.NVarChar).Value = checkNumber;
                    command.Parameters.Add("@driverCode", SqlDbType.NVarChar).Value = driverCode;


                    try
                    {
                        SqlDataAdapter da = new SqlDataAdapter(command);
                        da.SelectCommand.CommandTimeout = db_Timeout;
                        da.Fill(dtAppRequest);
                        JubFuAPPWebServiceLog(sessionid, "BindBagNo", "UpdateBagNoBinding", "UpdateSuccess");
                    }
                    catch (Exception e)
                    {
                        JubFuAPPWebServiceLog(sessionid, "BindBagNo", "UpdateBagNoBinding", "UpdateFail errmsg:" + e.Message);
                    }
                    conn.Close();

                    JubFuAPPWebServiceLog(sessionid, "BindBagNo", "UpdateBagNoBinding", "UpdateSuccess");
                }
            }
        }
        resultcode = true;
        success_msg = isBind ? string.Format("單號{0}已成功綁定月結袋", checkNumber) : string.Format("單號{0}已成功解除綁定", checkNumber);
        JubFuAPPWebServiceLog(sessionid, "bindBagNo", "error_msg", success_msg);
        return JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = success_msg }, Formatting.Indented); ;
    }

    /// <summary>
    /// 取得箱號袋號
    /// </summary>
    /// <returns></returns>
    [WebMethod(Description = "取得箱號袋號")]
    public string GetContainerCode()
    {
        writeLog("call", "GetContainerCode", "", "");
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dt = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;

        try
        {
            using (SqlCommand cmd = new SqlCommand())
            {

                cmd.CommandText = string.Format(@"SELECT ContainerCode ,ContainerName
                                                  FROM ContainerCode
                                                  WHERE IsActive = 1 ");

                try
                {
                    dt = dbAdapter.getDataTable(cmd);
                    if (dt == null && dt.Rows.Count == 0)
                    {
                        resultcode = false;
                        error_msg = "查無資料";
                    }
                }
                catch (Exception e)
                {
                    resultcode = false;
                    error_msg = e.Message;
                    writeLog("err", "GetContainerCode", "", e.Message);
                }
            }
            writeLog("ref", "GetContainerCode", "", "");
        }
        catch (Exception e)
        {
            resultcode = false;
            error_msg = e.Message;
            writeLog("err", "GetContainerCode", "", e.Message);
        }


        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, arriveOptions = dt }, Formatting.Indented);
        return strAppRequest;
    }

    /// <summary>
    /// 取得代號綁定資料
    /// </summary>
    /// <param name="checkNumber"></param>
    /// <returns></returns>
    private BagNoBinding GetBagBindingByCheckNumber(string checkNumber)
    {
        BagNoBinding bagNoBinding = new BagNoBinding();

        try
        {
            using (var cn = new SqlConnection(db_junfu_sql_conn_str))
            {
                string sql = @"select top 1 * from BagNoBinding with (nolock) where CheckNumber = @checkNumber order by id desc";
                bagNoBinding = cn.QueryFirstOrDefault<BagNoBinding>(sql, new { checkNumber });
            }
        }
        catch (Exception e)
        {
            bagNoBinding = null;
        }

        return bagNoBinding;
    }

    [WebMethod(Description = "取得配達1.0 可使用USER")]
    public string GetDeliveryWhiteUser()
    {
        writeLog("call", "GetDeliveryWhiteUser", "", "");
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        Boolean resultcode = true;
        string error_msg = string.Empty;

        writeLog("setdata", "GetDeliveryWhiteUser", "", "");
        setDeliveryWhiteUser();

        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        string strAppRequest = JsonConvert.SerializeObject(new
        {
            resultcode = resultcode,
            resultdesc = error_msg,
            whiteuser = listDeliveryWhiteUser
        }, Formatting.Indented);
        return strAppRequest;
    }

    private List<DeliveryWhiteUser> listDeliveryWhiteUser = new List<DeliveryWhiteUser>();

    private void setDeliveryWhiteUser()
    {
        /*
目前會用到配達1.0的帳號
Z010506 Momo入倉工號
        */
        //listDeliveryWhiteUser.Add(new DeliveryWhiteUser() { driver_code = "Z010506", driver_name = "Momo入倉工號" });
        listDeliveryWhiteUser.Add(new DeliveryWhiteUser() { driver_code = "Z010563", driver_name = "Momo入倉工號" });
        listDeliveryWhiteUser.Add(new DeliveryWhiteUser() { driver_code = "PP992", driver_name = "北轉大量退貨" });
        listDeliveryWhiteUser.Add(new DeliveryWhiteUser() { driver_code = "PP990", driver_name = "總公司" });
        listDeliveryWhiteUser.Add(new DeliveryWhiteUser() { driver_code = "PP994", driver_name = "五股大量退貨" });
        //listDeliveryWhiteUser.Add(new DeliveryWhiteUser() { driver_code = "Z010006", driver_name = "工程師測試帳號" });
    }


    private List<DeliveryBlackUser> listLongTimeNoCheckUser = new List<DeliveryBlackUser>();

    private void setLongTimeNoCheck()
    {
        try
        {
            using (var cn = new SqlConnection(db_junfu_sql_conn_str))
            {
                string sql = @"
Select upper(driver_code) as driver_code , driver_name 
From tbDrivers with (nolock)
Where VersionCode not in (
						Select Distinct Top 4 VersionCode
						From tbDrivers with (nolock)
						Where VersionCode is not null
						Order by VersionCode desc
						)
or VersionCode is null
--and driver_code in ( Select Distinct driver_code 
--					From tbDrivers_Log with (nolock) 
--					Where log_time > '20220625')
Order by driver_code
";
                listLongTimeNoCheckUser = (List<DeliveryBlackUser>)cn.Query<DeliveryBlackUser>(sql);

            }
        }
        catch (Exception e)
        {

        }
    }


    /// <summary>
    /// 同筆貨號在 x 天之內有沒有發過簡訊
    /// </summary>
    /// <param name="check_number"></param>
    /// <param name="calc_day"></param>
    /// <returns></returns>
    private bool CheckIsEverSendSmsByCheckNumber(string check_number, int calc_day = 0)
    {
        bool isEverSendSms = false;
        DateTime NowPre = DateTime.Now;
        DateTime now = new DateTime(NowPre.AddDays(calc_day).Year,
                                    NowPre.AddDays(calc_day).Month,
                                    NowPre.AddDays(calc_day).Day, 0, 0, 0);
        int SendCount = 0;

        List<CheckNumberMobile> listMobile = new List<CheckNumberMobile>();
        try
        {
            using (var cn = new SqlConnection(db_junfu_sql_conn_str))
            {
                string sql = @"
Select Count(*) as counts 
From [JubFuAPPWebServiceLog_SMS] with (nolock)
Where check_number = @check_number
and cdate >= @now
";
                listMobile = (List<CheckNumberMobile>)cn.Query<CheckNumberMobile>(
                                                                sql,
                                                                new
                                                                {
                                                                    check_number = check_number,
                                                                    now = now
                                                                }
                                                                );

                if (listMobile.Count == 1)
                {
                    SendCount = listMobile[0].counts;
                }
            }
        }
        catch (Exception e)
        {

        }

        if (SendCount == 0)
        {
            isEverSendSms = false;
        }
        else
        {
            isEverSendSms = true;
        }

        return isEverSendSms;
    }
    /// <summary>
    /// 取得託運單的收件人手機號碼
    /// </summary>
    /// <param name="check_number"></param>
    /// <returns></returns>
    private string GetCheckNumberMobile(string check_number)
    {
        string ReturnValue = "";

        List<CheckNumberMobile> listMobile = new List<CheckNumberMobile>();
        try
        {
            using (var cn = new SqlConnection(db_junfu_sql_conn_str))
            {
                string sql = @"
Select check_number , replace(receive_tel1,'-','') as receive_tel1 
From tcDeliveryRequests with (nolock)
Where check_number = @check_number
and ( cancel_date is null or cancel_date = '' )
and ( 
	( latest_scan_item <> '3' )
	or 
	( latest_scan_item = '3' and latest_arrive_option <> '3' )
	or 
	( latest_scan_item = '3' and latest_arrive_option is null )
	)
and replace(receive_tel1,'-','') like '09[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
";
                listMobile = (List<CheckNumberMobile>)cn.Query<CheckNumberMobile>(sql, new { check_number = check_number });

                if (listMobile.Count == 1)
                {
                    ReturnValue = listMobile[0].receive_tel1;
                }
            }
        }
        catch (Exception e)
        {

        }


        return ReturnValue;
    }

    /// <summary>
    /// 取得司機+站所的電話
    /// </summary>
    /// <param name="driver_code"></param>
    /// <returns></returns>
    private DriverStationData GetDriverAndStationTel(string driver_code)
    {
        DriverStationData ReturnValue = new DriverStationData();

        List<DriverStationData> listMobile = new List<DriverStationData>();
        try
        {
            using (var cn = new SqlConnection(db_junfu_sql_conn_str))
            {
                string sql = @"
Select	b.station_scode , b.station_name , isnull(b.tel,'') as station_tel , 
		a.driver_code , a.driver_name  , isnull(a.driver_mobile,'') as driver_mobile
From 
(
Select driver_code , driver_name , station , driver_mobile From tbDrivers with (nolock)
Where active_flag = 1 
and driver_code = @driver_code
) a 
inner join 
(
Select station_scode , station_name , tel From tbStation
Where active_flag = 1 
) b on a.station = b.station_scode
";
                listMobile = (List<DriverStationData>)cn.Query<DriverStationData>(sql, new { driver_code = driver_code });

                if (listMobile.Count >= 1)
                {
                    ReturnValue = listMobile[0];
                }
            }
        }
        catch (Exception e)
        {

        }

        return ReturnValue;
    }


    /// <summary>
    /// 配異傳送簡訊
    /// </summary>
    /// <param name="check_number"></param>
    /// <param name="mobile"></param>
    /// <param name="send_type"></param>
    /// <param name="send_sub_type"></param>
    /// <param name="driver_code"></param>
    public string SendSms(string check_number, string mobile, string send_type, string send_sub_type,
                        string driver_code = "")
    {
        string application_id = "";
        string fun_id = "";
        string detail_id = "";
        string sms_title = "";
        string sms_content = "";
        try
        {
            DriverStationData DriverStationTel = GetDriverAndStationTel(driver_code);



            if (send_type.Equals("3"))
            {
                //private String[] spinnerContentA = new String[]{"3 .正常配交", "1 .客戶不在", "2 .約定再配", "4 .拒收", "5 .地址錯誤", "6 .查無此人", "11.公司行號休息","26.聯絡不上","21.到站:自取" };
                /*
                "3 .正常配交", 
                "1 .客戶不在", 
                "2 .約定再配", 
                "4 .拒收", 
                "5 .地址錯誤", 
                "6 .查無此人", 
                "11.公司行號休息",
                "26.聯絡不上",
                "21.到站:自取"    
                */
                application_id = "APP";
                fun_id = "配達2.0";
                detail_id = "配異";

                switch (send_sub_type)
                {
                    case "1":
                        sms_title = "客戶不在";
                        //sms_content = "您好! 您的貨件{貨號}於{MM/DD}配達，但由於您不在府上故配送失敗，若有疑問請洽當區配送員{MOBILE}，謝謝";
                        sms_content = "您的貨件{貨號}於{MM/DD}送至地址，由於您不在故配送失敗，若有任何問題請洽當區配送員{MOBILE}，謝謝";

                        break;
                    case "2":
                        sms_title = "約定再配";
                        //sms_content = "您好！您的貨件{貨號}已於{MM/DD}與您電聯，並另約時間配送，若有疑問請洽當區配送員{MOBILE}，謝謝";
                        sms_content = "您的貨件{貨號}已於與您電聯，另約時間配送，若有任何問題請洽總公司客服0800-306-080，謝謝";
                        break;
                    case "5":
                        sms_title = "地址錯誤";
                        //sms_content = "您好！您的貨件{貨號}因地址錯誤故配送失敗，若有任何問題請洽當區營業所{STATION_TEL}，謝謝";
                        sms_content = "您的貨件{貨號}因地址錯誤故配送失敗，若有任何問題請洽當區營業所{STATION_TEL}，謝謝";
                        break;
                    case "6":
                        sms_title = "查無此人";
                        //sms_content = "您好！您的貨件{貨號}因此地址查無此人故配送失敗，若有任何問題請洽當區營業所{STATION_TEL}，謝謝";
                        sms_content = "您的貨件{貨號}因此地址查無此人故配送失敗，若有任何問題請洽當區營業所{STATION_TEL}，謝謝";
                        break;
                    case "11":
                        sms_title = "公司行號休息";
                        //sms_content = "您好！您的貨件{貨號}因司機抵達時公司行號休息，若有任何問題請洽當區配送員{MOBILE}，謝謝";
                        sms_content = "您的貨件{貨號}因司機抵達時公司行號休息，若有任何問題請洽當區配送員{MOBILE}，謝謝";
                        break;
                    case "26":
                        sms_title = "聯絡不上";
                        //sms_content = "您好！您的貨件{貨號}因電聯不上配送失敗，若有任何問題請洽當區配送員{MOBILE}，謝謝";
                        sms_content = "您的貨件{貨號}因電聯不上配送失敗，若有任何問題請洽當區配送員{MOBILE}，謝謝";
                        break;
                    default:
                        sms_title = "";
                        sms_content = "";
                        break;
                }
            }

            sms_content = sms_content.Replace("{貨號}", check_number);
            sms_content = sms_content.Replace("{MOBILE}", DriverStationTel.driver_mobile);
            sms_content = sms_content.Replace("{STATION_TEL}", DriverStationTel.station_tel);
            sms_content = sms_content.Replace("{MM/DD}", DateTime.Now.ToString("MM/dd"));

            if (sms_title != "" && sms_content != "" && check_number != "" && mobile != "")
            {

            }
            else
            {
                return "";
            }

            //return sms_title + " ; " + sms_content;

            //
            // API還沒好 以下待補完
            //
            var ApiUrl = "http://map.fs-express.com.tw/Notification/SMSSend";

            //打API
            var client = new RestClient(ApiUrl);
            var request = new RestRequest(Method.POST);
            request.Timeout = 5000;
            request.RequestFormat = DataFormat.Json;
            //request.AddJsonBody(new { Address = Address.Trim().Replace(" ", ""), Type, ComeFrom, CustomerCode }); // Anonymous type object is converted to Json body
            //request.AddParameter("ComeFrom", "6", ParameterType.UrlSegment);//處理可能出現的保留字
            //request.AddParameter("Mobile", DriverStationTel.driver_mobile, ParameterType.UrlSegment);//處理可能出現的保留字
            //request.AddParameter("Title", sms_title, ParameterType.UrlSegment);//處理可能出現的保留字
            //request.AddParameter("Content", sms_content, ParameterType.UrlSegment);//處理可能出現的保留字
            //request.AddParameter("SendUser", driver_code, ParameterType.UrlSegment);//處理可能出現的保留字
            //request.AddParameter("CheckNumber", check_number, ParameterType.UrlSegment);//處理可能出現的保留字

            SMSSend_Req obj = new SMSSend_Req();
            obj.ComeFrom = "6";
            obj.Mobile = mobile;
            obj.Title = sms_title;
            obj.Content = sms_content;
            obj.SendUser = driver_code;
            obj.CheckNumber = check_number;

            request.AddBody(obj);



            var response = client.Post<object>(request);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                try
                {
                    //Info = response.Data.Data;
                }
                catch (Exception e)
                {
                    //throw e;
                }

            }
        }
        catch (Exception ex)
        {
            return ex.Message + ex.Source + ex.StackTrace;
        }

        return sms_title + " ; " + sms_content;
    }



    [WebMethod(Description = "更新籠車 RFID 紀錄")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string CageMana_InsertCageRecord(string CageId, string CageType, string station_scode, string driver_code, string wgs_x, string wgs_y)
    {
        bool resultCode = true;
        string errorMessage = string.Format("沒有問題");

        CageMana_InsertCageRecord_DbMain(CageId, CageType, station_scode, driver_code, wgs_x, wgs_y);

        string response = JsonConvert.SerializeObject(new { resultcode = resultCode, resultdesc = errorMessage });
        return response;

    }


    public void CageMana_InsertCageRecord_DbMain(string CageId, string CageType, string station_scode, string driver_code, string wgs_x, string wgs_y)
    {

        using (SqlConnection connection = new SqlConnection(db_junfu_sql_conn_str))
        {
            try
            {
                using (var cn = new SqlConnection(db_junfu_sql_conn_str))
                {
                    string sql = "exec CageMana_InsertCageRecord @CageId , @CageType , @station_scode , @driver_code , @wgs_x , @wgs_y";

                    cn.Query(sql, new
                    {
                        CageId = CageId,
                        CageType = CageType,
                        station_scode = station_scode,
                        driver_code = driver_code,
                        wgs_x = wgs_x,
                        wgs_y = wgs_y
                    });
                }
            }
            catch (Exception e)
            {

            }
        }

    }

    /// <summary>
    /// 資產盤點
    /// </summary>
    /// <param name="CageId"></param>
    /// <param name="CageType"></param>
    /// <param name="station_scode"></param>
    /// <param name="driver_code"></param>
    /// <param name="wgs_x"></param>
    /// <param name="wgs_y"></param>
    public void InsertAsset_Management(string CageId, string CageType, string station_scode, string driver_code, string wgs_x, string wgs_y)
    {

        using (SqlConnection connection = new SqlConnection(db_junfu_sql_conn_str))
        {
            try
            {
                using (var cn = new SqlConnection(db_junfu_sql_conn_str))
                {
                    string sql = @"Insert Into AssetManagement ([CageId],[CageType],[station_scode],[driver_code],
											[cdate],[wgs_x],[wgs_y] )
							values ( @CageId , @CageType , @station_scode , @driver_code , 
											getdate() , @wgs_x , @wgs_y )";


                    cn.Query(sql, new
                    {
                        CageId = CageId,
                        CageType = CageType,
                        station_scode = station_scode,
                        driver_code = driver_code,
                        wgs_x = wgs_x,
                        wgs_y = wgs_y
                    });
                }
            }
            catch (Exception e)
            {

            }
        }

    }

    /// <summary>
    /// 到著時 顯示到著站所司機的SDMD
    /// </summary>
    /// <param name="check_number"></param>
    /// <returns></returns>
    [WebMethod]
    public string getArriveSDMDCode(string check_number)
    {

        string strSQLCMD = "";

        strSQLCMD = @"
--Declare @check_number nvarchar(100)
--Set @check_number = ''

Declare @CBM nvarchar(100)
Set @CBM = ''
Declare @LWH int
Set @LWH = 0
Declare @sdmd nvarchar(100)
Set @sdmd = ''
Declare @sdmd_code nvarchar(10)
Set @sdmd_code = ''
Declare @sdmd_code_SD nvarchar(10)
Set @sdmd_code_SD = ''
Declare @sdmd_code_MD nvarchar(10)
Set @sdmd_code_MD = ''

Select @CBM = upper(max(CBM))
From CBMDetail with (nolock)
Where CBM is not null and CBM <> ''
and CheckNumber = @check_number
Group by CheckNumber 

Select @LWH = max(isnull(Length,0) + isnull(Width,0) + isnull(Height,0))
From CBMDetail with (nolock)
Where CheckNumber = @check_number
Group by CheckNumber 

--有過丈量機
if ( @LWH > 0 )
Begin 
	--Select @LWH
	if ( @LWH >= 1100 )
	Begin 
		Set @sdmd = 'SD'
	End
	else
	Begin 
		Set @sdmd = 'MD'
	End
End

if (@sdmd='')
Begin 
	Set @sdmd = (
				Case 
					When @CBM = 'B01'   then 'MD'
					When @CBM = 'B02'   then 'MD'
					When @CBM = 'B03'   then 'MD'
					When @CBM = 'B04'   then 'MD'
					When @CBM = 'B1'    then 'MD'
					When @CBM = 'B2'    then 'MD'
					When @CBM = 'B3'    then 'MD'
					When @CBM = 'B4'    then 'MD'
					When @CBM = 'B5'    then 'MD'
					When @CBM = 'C001'  then 'MD'
					When @CBM = 'C002'  then 'MD'
					When @CBM = 'C003'  then 'SD'
					When @CBM = 'C004'  then 'SD'
					When @CBM = 'C005'  then 'SD'
					When @CBM = 'O01'   then 'MD'
					When @CBM = 'O02'   then 'MD'
					When @CBM = 'O03'   then 'MD'
					When @CBM = 'O04'   then 'MD'
					When @CBM = 'P01'   then 'MD'
					When @CBM = 'P02'   then 'MD'
					When @CBM = 'P03'   then 'MD'
					When @CBM = 'P04'   then 'MD'
					When @CBM = 'S060'  then 'MD'
					When @CBM = 'S090'  then 'MD'
					When @CBM = 'S110'  then 'MD'
					When @CBM = 'S120'  then 'SD'
					When @CBM = 'S150'  then 'SD'
					else 'MD'
				End
				)

End

--預設值
if (@sdmd='')
Begin 
	Set @sdmd = 'MD'
End


Select @sdmd_code = (Case 
						When @sdmd = 'SD' then ReceiveSD 
						When @sdmd = 'MD' then ReceiveMD
						Else ''
						End ) , 
        @sdmd_code_SD = ReceiveSD , 
        @sdmd_code_MD = ReceiveMD
From tcDeliveryRequests with (nolock)
Where check_number = @check_number

Set @sdmd_code = isnull(@sdmd_code,'')

if ( @sdmd_code = '0' )
Begin 
    Set @sdmd_code = ''
End

Set @sdmd_code_SD = isnull(@sdmd_code_SD,'')

if ( @sdmd_code_SD = '0' )
Begin 
    Set @sdmd_code_SD = ''
End

Set @sdmd_code_MD = isnull(@sdmd_code_MD,'')

if ( @sdmd_code_MD = '0' )
Begin 
    Set @sdmd_code_MD = ''
End

if ( @sdmd_code = '' )
Begin 
    if ( @sdmd = 'SD' )
    Begin 
        Set @sdmd = 'MD' 
        Set @sdmd_code = @sdmd_code_MD
    End
    else if ( @sdmd = 'MD' )
    Begin 
        Set @sdmd = 'SD' 
        Set @sdmd_code = @sdmd_code_SD
    End
End

Select @sdmd as sdmd_type , @sdmd_code as sdmd_code
";

        writeLog("call", "getArriveSDMDCode", check_number, "");
        string serverdate = System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        DataTable dt = new DataTable();
        Boolean resultcode = true;
        string error_msg = string.Empty;

        string sdmd_type = "";
        string sdmd_code = "";
        try
        {
            using (SqlCommand cmd = new SqlCommand())
            {

                cmd.CommandText = strSQLCMD;
                cmd.Parameters.Add("@check_number", SqlDbType.VarChar).Value = check_number;

                try
                {
                    dt = dbAdapter.getDataTable(cmd);
                    if (dt == null && dt.Rows.Count == 0)
                    {
                        resultcode = false;
                        error_msg = "查無資料";
                    }

                    sdmd_type = dt.Rows[0]["sdmd_type"].ToString();
                    sdmd_code = dt.Rows[0]["sdmd_code"].ToString();

                    string parameter = JsonConvert.SerializeObject(new { check_number, sdmd_type, sdmd_code });

                    Guid g = Guid.NewGuid();
                    var sessionid = g.ToString();
                    JubFuAPPWebServiceLog(sessionid, "getArriveSDMDCode", "parameter," + check_number, parameter);


                    writeLog("call", "getArriveSDMDCode", "sdmd_type-" + sdmd_type + " ; sdmd_code-" + sdmd_code, "check_number-" + check_number);
                }
                catch (Exception e)
                {
                    resultcode = false;
                    error_msg = e.Message;
                    writeLog("err", "getArriveSDMDCode", "", e.Message);
                }
            }
            writeLog("ref", "getArriveSDMDCode", "", "");
        }
        catch (Exception e)
        {
            resultcode = false;
            error_msg = e.Message;
            writeLog("err", "getArriveSDMDCode", "", e.Message);
        }


        Newtonsoft.Json.Converters.IsoDateTimeConverter timeConverter = new Newtonsoft.Json.Converters.IsoDateTimeConverter();
        //这里使用自定义日期格式，如果不使用的话，默认是ISO8601格式  
        //timeConverter.DateTimeFormat = "yyyy-'MM'-'dd' 'HH':'mm':'ss";
        timeConverter.DateTimeFormat = "yyyy-MM-dd HH:mm:ss";//為了Android,調整
        string strAppRequest = JsonConvert.SerializeObject(new { resultcode = resultcode, resultdesc = error_msg, sdmd_type = sdmd_type, sdmd_code = sdmd_code }, Formatting.Indented);
        return strAppRequest;
    }

    #region Json Response

    /// <summary>
    /// Response 輸出
    /// </summary>
    /// <param name="Code"></param>
    private void DoContextResponse(string Code)
    {
        Code = Code.Replace("\\", "\\\\");
        Code = Code.Replace("\r\n", "\\r\\n");
        Code = Code.Replace("'", "\'");
        Context.Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
        Context.Response.ContentType = "application/json; charset=utf-8";
        Context.Response.Write(Code);
        Context.Response.End();
    }

    #endregion

    #region "Json"

    /// <summary>
    /// DataTable 轉成 JSON
    /// </summary>
    /// <param name="DT"></param>
    /// <returns></returns>
    public static string DoDataTableToJson(System.Data.DataTable DT)
    {
        string Res = "{";

        try
        {


            if (DT.Rows.Count > 0)
            {
                Res += "\"DATA\":[";
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    for (int j = 0; j < DT.Columns.Count; j++)
                    {
                        if (j == 0) Res += " {";
                        Res += string.Format("\"{0}\":\"{1}\"", DT.Columns[j].ColumnName, DT.Rows[i][j].ToString());
                        if (j != DT.Columns.Count - 1)
                        {
                            Res += ",";
                        }
                        else
                        {
                            Res += "}";
                        }
                    }

                    if (i != DT.Rows.Count - 1) Res += ", ";

                }

                Res += " ]";

            }

            Res += "}";
        }
        catch (Exception ex)
        {
            throw ex;
        }

        return Res;
    }

    /// <summary>
    /// HashTable 轉成 JSON
    /// </summary>
    /// <param name="HT"></param>
    /// <returns></returns>
    public string DoHashTableToJson(System.Collections.Hashtable HT)
    {
        string Res = "{";

        if (HT.Count > 0)
        {
            Res += "\"DATA\":[";

            string strTmp = DoHashTableToString(HT);
            if (strTmp == "")
                return "";
            Res += strTmp;

            Res += "]";
        }

        Res += "}";
        return Res;
    }
    public static string DoHashTableToString(System.Collections.Hashtable HT)
    {
        string Res = "";

        if (HT.Count > 0)
        {
            Res += "{";

            int i = 0;
            foreach (string _key in HT.Keys)
            {
                object item = HT[_key];

                if (item != null)
                {
                    switch (item.GetType().ToString())
                    {
                        case "System.Collections.Hashtable":
                            {
                                string strTmp = DoHashTableToString((System.Collections.Hashtable)item);
                                strTmp = string.Format("\"{0}\":[{1}]", _key, strTmp);
                                Res += strTmp;
                                break;
                            }

                        case "System.Collections.ArrayList":
                            {
                                string strTmp = DoArrayListToString((System.Collections.ArrayList)item);
                                strTmp = string.Format("\"{0}\":[{1}]", _key, strTmp);
                                Res += strTmp;
                                break;
                            }

                        case "System.String":
                        case "System.Boolean":
                            {
                                string strTmp = "";
                                strTmp = string.Format("\"{0}\":\"{1}\"", _key, item.ToString());
                                Res += strTmp;
                                break;
                            }

                        case "System.Data.DataTable":
                            {
                                string strTmp = "";
                                strTmp = string.Format("\"{0}\":[{1}]", _key, DoArrayListToString(DoDataTableToArrayList((System.Data.DataTable)item)));
                                Res += strTmp;
                                break;
                            }

                        default:
                            {
                                try
                                {
                                    string strTmp = "";
                                    strTmp = string.Format("\"{0}\":\"{1}\"", _key, item.ToString());
                                    Res += strTmp;
                                }
                                catch (Exception ex)
                                {
                                    return "";
                                }

                                break;
                            }
                    }
                }
                else
                {
                    string strTmp = "";
                    strTmp = string.Format("\"{0}\":null", _key);
                    Res += strTmp;
                }

                if (i != HT.Count - 1) Res += ",";
                i++;
            }

            Res += "}";
        }

        return Res;
    }
    public static string DoArrayListToString(System.Collections.ArrayList HT)
    {
        string Res = "";

        if (HT.Count > 0)
        {
            // Res &= "{"

            for (int i = 0; i <= HT.Count - 1; i++)
            {
                switch (HT[i].GetType().ToString())
                {
                    case "System.Collections.Hashtable":
                        {
                            string strTmp = DoHashTableToString((System.Collections.Hashtable)HT[i]);
                            Res += strTmp;
                            break;
                        }

                    case "System.Collections.ArrayList":
                        {
                            string strTmp = DoArrayListToString((System.Collections.ArrayList)HT[i]);
                            Res += strTmp;
                            break;
                        }

                    case "System.String":
                    case "System.Boolean":
                        {
                            string strTmp = "";
                            strTmp = string.Format("\"{0}\"", HT[i].ToString());
                            Res += strTmp;
                            break;
                        }

                    default:
                        {
                            try
                            {
                                string strTmp = "";
                                strTmp = string.Format("\"{0}\"", HT[i].ToString());
                                Res += strTmp;
                            }
                            catch (Exception ex)
                            {
                                return "";
                            }

                            break;
                        }
                }

                if (i != HT.Count - 1)
                    Res += ",";
            }
        }

        return Res;
    }
    public static System.Collections.ArrayList DoDataTableToArrayList(System.Data.DataTable DT)
    {
        if (DT == null)
            return null/* TODO Change to default(_) if this is not a reference type */;
        if (DT.Rows.Count == 0)
            return null/* TODO Change to default(_) if this is not a reference type */;

        System.Collections.ArrayList Res = new System.Collections.ArrayList();

        foreach (System.Data.DataRow _row in DT.Rows)
        {
            System.Collections.Hashtable _ht = new System.Collections.Hashtable();
            for (int i = 0; i <= DT.Columns.Count - 1; i++)
                _ht.Add(DT.Columns[i].ColumnName, _row[DT.Columns[i].ColumnName].ToString().Trim());
            Res.Add(_ht);
        }

        return Res;
    }

    #endregion

}





public class DeliveryWhiteUser
{
    public string driver_code { get; set; }
    public string driver_name { get; set; }
}

public class DeliveryBlackUser
{
    public string driver_code { get; set; }
    public string driver_name { get; set; }
}

public class CheckNumberMobile
{
    public int counts { get; set; }
    public string check_number { get; set; }
    public string receive_tel1 { get; set; }
}

//站所/司機的電話
public class DriverStationData
{
    /*
Select	b.station_scode , b.station_name , isnull(b.tel,'') as tel , 
		a.driver_code , a.driver_name , a.station , isnull(a.driver_mobile,'') as driver_mobile
    */
    public string station_scode { get; set; }
    public string station_name { get; set; }
    public string station_tel { get; set; }
    public string driver_code { get; set; }
    public string driver_name { get; set; }
    public string driver_mobile { get; set; }
}

public class SMSSend_Req
{
    /// <summary>
    /// 來源
    /// </summary>
    public string ComeFrom { get; set; }
    /// <summary>
    /// 手機
    /// </summary>
    public string Mobile { get; set; }
    /// <summary>
    /// 標題
    /// </summary>
    public string Title { get; set; }
    /// <summary>
    /// 內容
    /// </summary>
    public string Content { get; set; }
    /// <summary>
    /// 發送人
    /// </summary>
    public string SendUser { get; set; }

    /// <summary>
    /// 託運單號
    /// </summary>
    public string CheckNumber { get; set; }
}

