﻿Imports Microsoft.VisualBasic
Imports SmartGAP.db
Imports System.Data
Imports System.Web.UI.WebControls
Public Class AppUtility
    ''' <summary>資料庫連結</summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared ReadOnly Property ConnectionString() As String
        Get
            Return ConfigurationManager.ConnectionStrings("prmonitorConnectionString").ConnectionString & "Password=" & GetPassword("Default")
        End Get
    End Property

    Private Shared Function GetPassword(ByVal Kind As String) As String
        Dim Pwd As String = ""
        Select Case Kind
            Case "Default" : Pwd = "gis5200"
        End Select
        Return Pwd
    End Function

    ''' <summary>將資料填入ListItemCollection</summary>
    ''' <param name="_dbDataTable"></param>
    ''' <param name="Items"></param>
    ''' <remarks></remarks>
    Public Shared Sub FillListItemColl(ByRef Items As ListItemCollection, ByRef _dbDataTable As dbDataTable)
        Items.Clear()
        For i As Integer = 0 To _dbDataTable.Count() - 1
            _dbDataTable.go(i)
            Items.Add(New ListItem(_dbDataTable.field("ItemText").GetStr, _dbDataTable.field("ItemValue").GetStr))
        Next
    End Sub
    ''' <summary>將資料填入ListItemCollection</summary>
    ''' <param name="_DataTable"></param>
    ''' <param name="Items"></param>
    ''' <remarks></remarks>
    Public Shared Sub FillListItemColl(ByRef Items As ListItemCollection, ByRef _DataTable As DataTable)
        Items.Clear()
        For i As Integer = 0 To _DataTable.Rows.Count - 1
            Items.Add(New ListItem(_DataTable.Rows(i).Item(0), _DataTable.Rows(i).Item(1)))
        Next
    End Sub
    ''' <summary>將資料填入ListItemCollection，並加入首項</summary>
    ''' <param name="_dbDataTable"></param>
    ''' <param name="Items"></param>
    ''' <param name="HeadItemText"></param>
    ''' <param name="HeadItemValue"></param>
    ''' <remarks></remarks>
    Public Shared Sub FillListItemColl(ByRef Items As ListItemCollection, ByVal HeadItemText As String, ByVal HeadItemValue As String, ByRef _dbDataTable As dbDataTable)
        Items.Clear()
        Items.Add(New ListItem(HeadItemText, HeadItemValue))
        For i As Integer = 0 To _dbDataTable.Count() - 1
            _dbDataTable.go(i)
            Items.Add(New ListItem(_dbDataTable.field("ItemText").GetStr, _dbDataTable.field("ItemValue").GetStr))
        Next
    End Sub
    ''' <summary>將資料填入ListItemCollection，並加入首項</summary>
    ''' <param name="_DataTable"></param>
    ''' <param name="Items"></param>
    ''' <remarks></remarks>
    Public Shared Sub FillListItemColl(ByRef Items As ListItemCollection, ByVal HeadItemText As String, ByVal HeadItemValue As String, ByRef _DataTable As DataTable)
        Items.Clear()
        Items.Add(New ListItem(HeadItemText, HeadItemValue))
        For i As Integer = 0 To _DataTable.Rows.Count - 1
            Items.Add(New ListItem(_DataTable.Rows(i).Item(0), _DataTable.Rows(i).Item(1)))
        Next
    End Sub
    ''' <summary>將資料填入ListItemCollection，並加入首項、末項</summary>
    ''' <param name="_dbDataTable"></param>
    ''' <param name="HeadItemText"></param>
    ''' <param name="HeadItemValue"></param>
    ''' <param name="FootItemText"></param>
    ''' <param name="FootItemValue"></param>
    ''' <param name="Items"></param>
    ''' <remarks></remarks>
    Public Shared Sub FillListItemColl(ByRef Items As ListItemCollection, ByVal HeadItemText As String, ByVal HeadItemValue As String, ByVal FootItemText As String, ByVal FootItemValue As String, ByRef _dbDataTable As dbDataTable)
        Items.Clear()
        Items.Add(New ListItem(HeadItemText, HeadItemValue))
        For i As Integer = 0 To _dbDataTable.Count() - 1
            _dbDataTable.go(i)
            Items.Add(New ListItem(_dbDataTable.field("ItemText").GetStr, _dbDataTable.field("ItemValue").GetStr))
        Next
        Items.Add(New ListItem(FootItemText, FootItemValue))
    End Sub
    ''' <summary></summary>
    ''' <param name="Items"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetSelectedValue(ByRef Items As ListItemCollection) As String
        Dim RtValue As String = ""
        For Each Item As ListItem In Items
            If Item.Selected Then
                RtValue = Item.Value
            End If
        Next
        Return RtValue
    End Function


    ''' <summary></summary>
    ''' <param name="Items"></param>
    ''' <param name="Value"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function SetSelectedValue(ByRef Items As ListItemCollection, ByVal Value As String) As ListItemCollection
        For Each Item As ListItem In Items
            If String.Equals(Item.Value, Value) Then
                Item.Selected = True
            Else
                Item.Selected = False
            End If
        Next
        Return Items
    End Function

    ''' <summary></summary>
    ''' <remarks></remarks>
    Public Enum DbAccess
        Insert
        Update
        Delete
    End Enum
End Class
