﻿'********************************************
' DAL, Data Access Layer
'
' Author: Lai,Chien-Hung <ken@gis.tw>
' Verion: 1.0
'********************************************
Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient

Public Class DAL

    Private _ConnStr As String = System.Configuration.ConfigurationManager.ConnectionStrings("dbSkyEyesWeb_app_ConnStr").ConnectionString

    Private Conn As SqlConnection = New SqlConnection()

    Sub New()
        Conn.ConnectionString = _ConnStr
    End Sub

    ''' <summary>
    ''' Open DB Connection
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub OpenConnection()
        If (Conn.State <> Data.ConnectionState.Open) Then
            Conn.Open()
        End If
    End Sub

    ''' <summary>
    ''' Close DB Connection
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub CloseConnection()
        Conn.Close()
    End Sub


    ''' <summary>
    ''' 取得特定Table資料(Select)
    ''' </summary>
    ''' <param name="SqlCmd"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ExecQuery(ByVal SqlCmd As String, ByVal params As SqlParameter(), Optional ByVal isStoredProcedure As Boolean = False) As DataTable

        Dim dt As DataTable = New DataTable()
        Dim da As SqlDataAdapter = New SqlDataAdapter()
        Try

            da.SelectCommand = Me.PrepareCmd(SqlCmd, params, isStoredProcedure)
            da.SelectCommand.CommandTimeout = 600
            da.Fill(dt)
        Catch ex As Exception
            Throw ex
        Finally
            da.Dispose()
            Me.CloseConnection()
        End Try

        Return dt
    End Function

    ''' <summary>
    ''' 取得首欄位
    ''' </summary>
    ''' <param name="SqlCmd"></param>
    ''' <param name="params"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function ExecScalar(ByVal SqlCmd As String, ByVal params As SqlParameter(), Optional ByVal isStoredProcedure As Boolean = False) As Object

        Dim Cmd As SqlCommand = Nothing

        Try
            Cmd = PrepareCmd(SqlCmd, params, isStoredProcedure)
            Me.OpenConnection()
            Return Cmd.ExecuteScalar()
        Catch ex As Exception
            Throw ex
        Finally
            Me.CloseConnection()
            If Not Cmd Is Nothing Then
                Cmd.Parameters.Clear()
                Cmd.Dispose()
            End If
        End Try

    End Function

    ''' <summary>
    ''' 執行特定Sql指令(insert,update,delete)
    ''' </summary>
    ''' <param name="SqlCmd"></param>
    ''' <param name="params"></param>
    ''' <remarks></remarks>
    Public Sub ExecNonQuery(ByVal SqlCmd As String, ByVal params As SqlParameter(), Optional ByVal isStoredProcedure As Boolean = False)
        Dim Cmd As SqlCommand = Nothing

        Try
            Cmd = PrepareCmd(SqlCmd, params, isStoredProcedure)
            Me.OpenConnection()
            Cmd.ExecuteNonQuery()
        Catch ex As Exception
            Throw ex
        Finally
            Me.CloseConnection()

            If Not Cmd Is Nothing Then
                Cmd.Parameters.Clear()
                Cmd.Dispose()
            End If
        End Try
    End Sub


    ''' <summary>
    ''' 建立SqlCommand
    ''' </summary>
    ''' <param name="CmdTxt"></param>
    ''' <param name="params"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function PrepareCmd(ByVal CmdTxt As String, ByVal params As SqlParameter(), Optional ByVal isStoredProcedure As Boolean = False) As SqlCommand
        Dim Cmd As SqlCommand = New SqlCommand()
        If isStoredProcedure Then
            Cmd.CommandType = CommandType.StoredProcedure
        Else
            Cmd.CommandType = CommandType.Text
        End If
        Cmd.Connection = Conn
        Cmd.CommandText = CmdTxt
        'If (trans <> null) Then
        '           cmd.Transaction = trans;
        If (Not params Is Nothing) Then
            For Each para As SqlParameter In params
                Cmd.Parameters.Add(para)
            Next
        End If

        Return Cmd
    End Function

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

   
End Class
