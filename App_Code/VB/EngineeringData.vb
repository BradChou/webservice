﻿Imports Microsoft.VisualBasic
Imports SmartGAP.db
Imports System.Data
Public Class EngineeringData
    ''' <summary>
    ''' 取得辦理年度
    ''' </summary>
    ''' <param name="Items"></param>
    ''' <remarks></remarks>
    Public Shared Sub GetYear(ByRef Items As ListItemCollection)
        Dim WraDB As New ado(AppUtility.ConnectionString)
        Dim SqlCmd As String = ""
        SqlCmd = "select distinct P_YEAR as ItemText,P_YEAR as ItemValue from WRA_93_TB_PROSPECT where  CHILD_P_ID='' and SW_ID='' order by P_YEAR"
        AppUtility.FillListItemColl(Items, WraDb.getDT(SqlCmd))
    End Sub
    ''' <summary>
    ''' 取得水庫別
    ''' </summary>
    ''' <param name="Items"></param>
    ''' <param name="pyear"></param>
    ''' <remarks></remarks>
    Public Shared Sub GetRSV(ByRef Items As ListItemCollection, ByVal pyear As String)
        Dim WraDB As New ado(AppUtility.ConnectionString)
        Dim SqlCmd As String = ""
        Dim SqlParam As New SqlParameterBuilder
        SqlCmd = " select distinct RSV_NAME as ItemText,RSV_NAME as ItemValue from WRA_93_TB_PROSPECT where P_YEAR=@pyear  and  CHILD_P_ID='' and SW_ID='' order by RSV_NAME"
        SqlParam.Add("@pyear", SqlDbType.SmallInt, CSng(pyear))
        AppUtility.FillListItemColl(Items, "請選擇水庫", "請選擇水庫", WraDB.getDT(SqlCmd, SqlParam.getParamList))
    End Sub
    ''' <summary>
    ''' 取得工程勘查登錄資料
    ''' </summary>
    ''' <param name="pyear"></param>
    ''' <param name="rsv"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetWriteList(ByVal pyear As String, ByVal rsv As String) As dbDataTable
        Dim WraDB As New ado(AppUtility.ConnectionString)
        Dim SqlCmd As String = ""
        Dim SqlParam As New SqlParameterBuilder
        SqlCmd = " select * from WRA_93_TB_PROSPECT where P_YEAR=@pyear and RSV_NAME=@rsv and  CHILD_P_ID='' and SW_ID=''"
        SqlParam.Add("@pyear", SqlDbType.SmallInt, CSng(pyear))
        SqlParam.Add("@rsv", SqlDbType.VarChar, rsv)
        Return WraDB.getDT(SqlCmd, SqlParam.getParamList)
    End Function
    ''' <summary>
    ''' 取得單筆工程勘查資料
    ''' </summary>
    ''' <param name="PID">工程ID</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetSingleWriteData(ByVal PID As String) As dbDataTable
        Dim WraDB As New ado(AppUtility.ConnectionString)
        Dim SqlCmd As String = ""
        Dim SqlParam As New SqlParameterBuilder
        SqlCmd = " select * from WRA_93_TB_PROSPECT where P_ID=@PID and CHILD_P_ID='' and SW_ID=''"
        SqlParam.Add("@PID", SqlDbType.VarChar, PID)
        Return WraDB.getDT(SqlCmd, SqlParam.getParamList)
    End Function
    ''' <summary>
    ''' 取得單筆工程勘查照片
    ''' </summary>
    ''' <param name="PID">工程ID</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetSinglePic(ByVal PID As String) As dbDataTable
        Dim WraDB As New ado(AppUtility.ConnectionString)
        Dim SqlCmd As String = ""
        Dim SqlParam As New SqlParameterBuilder
        SqlCmd = " select * from  WRA_93_TB_PROSPECT_PIC where P_ID=@PID "
        SqlParam.Add("@PID", SqlDbType.VarChar, PID)
        Return WraDB.getDT(SqlCmd, SqlParam.getParamList)
    End Function
    ''' <summary>
    ''' 上傳照片
    ''' </summary>
    ''' <param name="PID"></param>
    ''' <param name="code"></param>
    ''' <param name="no"></param>
    ''' <remarks></remarks>
    Public Shared Sub UploadPic(ByVal PID As String, ByVal code As Byte, ByVal no As String)
        Dim WraDB As New ado(AppUtility.ConnectionString)
        Dim SqlCmd As String = ""
        Dim SqlParam As New SqlParameterBuilder
        Select Case no
            Case "1"
                SqlCmd = " update WRA_93_TB_PROSPECT_PIC set PIC1=@PIC where P_ID=@PID "
            Case "2"
                SqlCmd = " update WRA_93_TB_PROSPECT_PIC set PIC2=@PIC where P_ID=@PID "
            Case "3"
                SqlCmd = " update WRA_93_TB_PROSPECT_PIC set PIC3=@PIC where P_ID=@PID "
        End Select
        SqlParam.Add("@PIC", SqlDbType.VarChar, code)
        SqlParam.Add("@PID", SqlDbType.VarChar, PID)
        WraDB.exec(SqlCmd, SqlParam.getParamList)
    End Sub
    Public Shared Sub SavePage(ByVal meta As EngineeringMeta, ByVal PID As String, ByVal Page As PageType)
        Dim WraDB As New ado(AppUtility.ConnectionString)
        Dim SqlCmd As String = ""
        Dim SqlParam As New SqlParameterBuilder
        SqlParam.Add("@PID", SqlDbType.VarChar, PID)
        Select Case Page
            Case PageType.Page1
                SqlCmd = " update WRA_93_TB_PROSPECT set P_Name=@P_Name,COLLABORATE_NEED_BGT=@COLLABORATE_NEED_BGT,COLLABORATE_PRED_BGT=@COLLABORATE_PRED_BGT where P_ID=@PID "
                SqlParam.Add("@P_Name", SqlDbType.VarChar, meta.P_Name)
                SqlParam.Add("@COLLABORATE_NEED_BGT", SqlDbType.Money, CDec(meta.COLLABORATE_NEED_BGT))
                SqlParam.Add("@COLLABORATE_PRED_BGT", SqlDbType.Money, CDec(meta.COLLABORATE_PRED_BGT))
                WraDB.exec(SqlCmd, SqlParam.getParamList)
            Case PageType.Page2
                SqlCmd = " update WRA_93_TB_PROSPECT set Priority=@Priority,Content=@Content,Note=@Note where P_ID=@PID "
                SqlParam.Add("@Priority", SqlDbType.SmallInt, CSng(meta.Priority))
                SqlParam.Add("@Content", SqlDbType.NVarChar, meta.Content)
                SqlParam.Add("@Note", SqlDbType.NVarChar, meta.Note)
                WraDB.exec(SqlCmd, SqlParam.getParamList)
            Case PageType.Page3
                If meta.PIC1 IsNot Nothing OrElse meta.PIC2 IsNot Nothing OrElse meta.PIC3 IsNot Nothing Then
                    SqlCmd = " update WRA_93_TB_PROSPECT_PIC set "
                    Dim tmpSqlCmd As String = ""
                    If meta.PIC1 IsNot Nothing Then
                        tmpSqlCmd &= ",PIC1=@PIC1"
                        SqlParam.Add("@PIC1", SqlDbType.Image, meta.PIC1)
                    End If
                    If meta.PIC2 IsNot Nothing Then
                        tmpSqlCmd &= ",PIC2=@PIC2"
                        SqlParam.Add("@PIC2", SqlDbType.Image, meta.PIC2)
                    End If
                    If meta.PIC3 IsNot Nothing Then
                        tmpSqlCmd &= ",PIC3=@PIC3"
                        SqlParam.Add("@PIC3", SqlDbType.Image, meta.PIC3)
                    End If
                    SqlCmd &= tmpSqlCmd.Substring(1) & " where P_ID=@PID"
                    WraDB.exec(SqlCmd, SqlParam.getParamList)
                End If
        End Select
    End Sub
    Public Enum PageType
        Page1
        Page2
        Page3
    End Enum
    Public Class EngineeringMeta
        ''' <summary>工程名稱</summary>
        Protected Friend _P_Name As String = ""
        ''' <summary>提報經費</summary>
        Protected Friend _COLLABORATE_NEED_BGT As String = ""
        ''' <summary>初估經費</summary>
        Protected Friend _COLLABORATE_PRED_BGT As String = ""
        ''' <summary>優先順序</summary>
        Protected Friend _Priority As String = ""
        ''' <summary>工程內容及工法</summary>
        Protected Friend _Content As String = ""
        ''' <summary>備註</summary>
        Protected Friend _Note As String = ""
        ''' <summary>照片一</summary>
        Protected Friend _PIC1 As Byte()
        ''' <summary>照片二</summary>
        Protected Friend _PIC2 As Byte()
        ''' <summary>照片三</summary>
        Protected Friend _PIC3 As Byte()

        ''' <summary>工程名稱</summary>
        Public Property P_Name() As String
            Get
                Return _P_Name
            End Get
            Set(ByVal value As String)
                _P_Name = value
            End Set
        End Property
        ''' <summary>提報經費</summary>
        Public Property COLLABORATE_NEED_BGT() As String
            Get
                Return _COLLABORATE_NEED_BGT
            End Get
            Set(ByVal value As String)
                _COLLABORATE_NEED_BGT = value
            End Set
        End Property
        ''' <summary>初估經費</summary>
        Public Property COLLABORATE_PRED_BGT() As String
            Get
                Return _COLLABORATE_PRED_BGT
            End Get
            Set(ByVal value As String)
                _COLLABORATE_PRED_BGT = value
            End Set
        End Property
        ''' <summary>優先順序</summary>
        Public Property Priority() As String
            Get
                Return _Priority
            End Get
            Set(ByVal value As String)
                _Priority = value
            End Set
        End Property
        ''' <summary>工程內容及工法</summary>  
        Public Property Content() As String
            Get
                Return _Content
            End Get
            Set(ByVal value As String)
                _Content = value
            End Set
        End Property
        ''' <summary>備註</summary>
        Public Property Note() As String
            Get
                Return _Note
            End Get
            Set(ByVal value As String)
                _Note = value
            End Set
        End Property
        ''' <summary>照片一</summary>
        Public Property PIC1() As Byte()
            Get
                Return _PIC1
            End Get
            Set(ByVal value As Byte())
                _PIC1 = value
            End Set
        End Property
        ''' <summary>照片二</summary> 
        Public Property PIC2() As Byte()
            Get
                Return _PIC2
            End Get
            Set(ByVal value As Byte())
                _PIC2 = value
            End Set
        End Property
        ''' <summary>照片三</summary>
        Public Property PIC3() As Byte()
            Get
                Return _PIC3
            End Get
            Set(ByVal value As Byte())
                _PIC3 = value
            End Set
        End Property
    End Class
End Class

